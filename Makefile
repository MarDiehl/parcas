#############################################################################
#
#  Makefile for PARCAS
#
#  Compilation flags and other variables are set in the make.mk file
#
#############################################################################


#
# Default build instructions. These can be overridden when running make, by
# typing either of the follwing after the normal make command:
#
#     build=release
#     build=debug
#
#     platform=gcc
#     platform=intel
#     platform=cray
#
# They can also be combined for applying both commands
#

platform := gcc
build    := release

ifeq ($(build),release)
else ifeq ($(build),debug)
else
    $(error Use build=release or build=debug)
endif


SRC := src
OBJ := obj-$(build)

# Decide on the executable name based on the build command
COMPTARGET = parcas
ifeq ($(build),debug)
    COMPTARGET = parcas-debug
endif


# Include the correct make-*.mk file for the current platform used
include make/make-$(platform).mk



OBJDEPS := src/object.deps

SRC_EXT = f90 c

SRCFILES := $(foreach EXT,$(SRC_EXT),$(wildcard src/*.$(EXT)))
OBJECTS_SUBS = $(patsubst %.$(EXT),%.o,$(filter %.$(EXT), $(SRCFILES)))
OBJECTS := $(subst src,$(OBJ),$(foreach EXT,$(SRC_EXT),$(OBJECTS_SUBS)))



#
# The default target
#

.PHONY: all
all: $(COMPTARGET)


#
# Implicit build rule
#

$(OBJ)/%.o : src/%.f90 | $(OBJ)/.
	$(F90) $(FFLAGS) -c -o $@ $<


$(OBJ)/%.mod : | $(OBJ)/.
	@test -n $^ || make $^


$(OBJ)/%.o : src/%.c | $(OBJ)/.
	$(CC) $(CFLAGS) -c -o $@ $<


#
# The actual targets
#

$(OBJ)/.:
	mkdir -p $(OBJ)


$(COMPTARGET): $(OBJECTS)
	$(LINK) $(FFLAGS) $^ -o $@ $(LDFLAGS)


.PHONY: clean
clean:
	rm -f $(COMPTARGET) src/object.deps
	rm -f *.mod $(OBJ)/*.o $(OBJ)/*.mod $(OBJ)/*__genmod.f90
	if [ -e $(OBJ) ] ; then rmdir $(OBJ) ; fi


.PHONY: clean-all
clean-all:
	rm -rf obj/ obj-debug/ obj-release/
	rm -f parcas parcas-debug



.PHONY: check
check: all
	(cd test; ./run.sh)


.PHONY: print
print:
	@echo "    Sources:"
	@echo $(SRCFILES)
	@echo ""
	@echo "    Objects subs:"
	@echo $(foreach EXT,$(SRC_EXT),$(OBJECTS_SUBS))
	@echo ""
	@echo "    Objects:"
	@echo $(OBJECTS)


$(OBJDEPS): $(SRCFILES) resolve_dependencies.awk
	@echo "Resolving source files dependencies to '$@'"
# add fortran module dependencies
	@awk -f resolve_dependencies.awk $(SRCFILES) > $@
# add c object dependencies
	@if [ -n "$(filter %.c, $(SRCFILES))" ] ; then \
		$(CC) -MM $(filter %.c, $(SRCFILES)) \
			| sed -e 's|\(.*\.o\)|$$(OBJ)\/\1|g' >> $@ ; \
	fi

#
# Module dependencies
#

-include $(OBJDEPS)
