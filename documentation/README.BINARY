Binary input/output format
==========================


This file contains the specifications for the binary input/output
format (version 4) implemented in binout.f90.

Abbreviations for data types used are:
- i4   4-byte two's complement signed integer
- i8   8-byte two's complement signed integer
- r4   4-byte real (IEEE 754 single precision)
- r8   8-byte real (IEEE 754 double precision)
- r?   Either r4 or r8, determined by realSize (see header)
- c4   4-byte ASCII string, space-padded
The endianness of all data is determined by the first 8 bytes of
the header (see below). No padding is used unless otherwise
indicated below.

The file consists of a header section, an optional human-readable
description, and a block of atom data, each of which immediately
follow the previous.



The file header
---------------

The header begins at the start of the file. It contains the following
fields, in the given order:


 1    r4    protoReal
    The r4 value (721409.0 / 1048576.0).
    protoReal and protoInt can be used to determine the file endianness.

 2    i4    protoInt
    The value 0x11223344.
    protoReal and protoInt can be used to determine the file endianness.

 3    i4    fileVersion
    The version of the PARCAS binary format. For the version described
    here, it has the value 4.

 4    i4    realSize
    The size of real values in the atom data block. Either 4 or 8.
    Determines whether r? is equal to r4 or r8.

 5    i8    descriptionOffset
    An offset in bytes from the start of the file to the start of the
    description. The description is immediately followed by the atom
    data, so having descriptionOffset equal to dataOffset means that
    there is no description.

 6    i8    dataOffset
    An offset in bytes from the start of the file to the start of the
    atom data block.

 7    i4    frameID
    The frame number of this file, if the file is part of a series of
    files. If not, frameID is undefined.

 8    i4    partNumber
 9    i4    partCount
    The file may be one of many parts which together contain all atoms
    of a given system. partNumber is the number of this file in the
    whole set of files, from 0 to partCount-1. partCount is the total
    number of files forming the whole system. It is at least one. For a
    single file containing the whole system, partNumber is zero and
    partCount is one.
    Note: As of 2019-01, no software is known to support systems being
    split into parts.

10    i4    numFields
    The number of additional fields (not counting those always present,
    see below) present in the file.

11    i8    natoms
    The total number of atoms contained in the file.

12    i4    typeLow
13    i4    typeHigh
    The smallest and largest atom type. Atom types are zero or positive.
    Both values are inclusive. The number of atoms of any specific type
    contained in this file may be zero. The actual atom type may be
    negative (see below).

14    i4    numCPUs
    The number of CPUs used to write this file. This determines the size
    of the CPU information block described below. Each CPU's atom data
    is written in one block, in the order used in the CPU header section
    below. Each CPU's atoms are within a cuboid sub-box of the simulation
    box. This can be used to optimize the reading of the file. numCPUs
    must be at least one.

15    r8    time
    The simulation time in seconds for this frame.

16    r8    deltat
    The time step in seconds for this frame.

17    r8    boxX
18    r8    boxY
19    r8    boxZ
    The size of the simulation box in all three dimensions, in  Ångström.
    The box is centered around the origin, i.e. the point (0,0,0) is at
    the center of the box. The values are positive for dimensions that are
    not periodic and negative for periodic dimensions.

The fields 20 and 21 are present once for each extra field, see numFields.

20    c4    fieldName
21    c4    fieldUnit
    The name and unit of the extra field. See below for a list of known
    values for both name and unit.

The field 22 is present once for each atom type, see typeLow and typeHigh.

22    c4    element
    The element name of the atom type. This is can be any 4-byte ASCII
    string, not necessarily a recognized element.

The fields 23 to 29 are present once for each CPU, see numCPUs.

23    i4    cpuAtoms
    The number of atoms present in the CPU's block.

24    r8    cpuXMin
25    r8    cpuXMax
26    r8    cpuYMin
27    r8    cpuYMax
28    r8    cpuZMin
29    r8    cpuZMax
    The edge locations of the CPU's sub-box, in each dimension. The values
    are scaled such that the edges of the simulation box are at +/- 0.5 in
    each dimension. To get Ångström, multiply by the box width in that
    dimension. For non-periodic edges of the simulation cell, the CPU's
    corresponding sub-box edge is set to a far-away value, such as 999e30.



The file description
--------------------

The file description is a human-readable description of the contents of
the file. It is encoded in UTF-8 (no BOM).

The description is located at the offset indicated by descriptionOffset,
and continues until either the start of the atom data block (see
dataOffset), or until a 0-byte (null character) is found, whichever comes
first.

As of 2019-01, no known software writes or reads the description.



The atom data block
-------------------

The atom data block is located at the offset indicated by dataOffset.
It consists of data blocks for each CPU (see numCPUs above), each of
which consists of a data block for each of the CPU's atoms. Each atom's
block consists of the fields described below.

The CPU-level block divisions can be ignored without affecting the reading
of the file. They are present merely as an optimization method.

Each atom's block consists of the following fields, in the given order:


 1    i8    atomIndex
    An index or ID for the atom. This does not have any futher meaning
    beyond being an integer tag for the atom. It does not mean an index
    into any array or such.

 2    i4    atomType
    An atom type between typeLow and typeHigh (both inclusive). May be
    negative to indicate that the atom is "fixed" in place, i.e. does not
    move. Atoms of type 0 cannot be "fixed".

 3    r?    x
 4    r?    y
 5    r?    z
    The atom position in Ångström. The position is not necessarily located in
    the simulation box if there are non-periodic dimensions.

The field 6 is present once for each extra field indicated in the header.
 6    r?    extra
    The meaning of this field is determined by the field names and units
    included in the file header. See below for a list of recognized names
    and units.


The file ends at the end of the atom data block.



List of recognized extra field names
------------------------------------

- "Epot"   potential energy
- "Ekin"   kinetic energy
- "V.x "   x-velocity
- "V.y "   y-velocity
- "V.z "   z-velocity
- "W.xx"   xx-virial
- "W.yy"   yy-virial
- "W.zz"   zz-virial
- "W.xy"   xy-virial
- "W.xz"   xz-virial
- "W.yz"   yz-virial



List of recognized extra field units
------------------------------------

- "eV  "   electron volt (for energies and virials)
- "A/fs"   Ångström per femtosecond (for velocities)
