VIRIAL CALCULATIONS IN PARCAS

1) INTRO
2) NEEDED PARAMETERS
3) EXPLANATION OF THE PARAMETERS
4) STRESS CALCULATIONS
5) FUNCTIONS MISSING
6) POSSIBLE PROBLEMS




1) INTRO
---------------------------------------------------------
The virials are calculated according to the formula:

tau_ji = 1/omega SUM( -m1 (u1_i - u_avg_i) (u1_j - u_avg_j) + (1/2) SUM (x1_i - x2_i) f12_j ) 

The factor (1/2) is not expicitly in the calculations. It wasn't there for the diagonal elements and not added for the non diagonal elements. The first part with the velocities is not used in the 0K calculations.

The output is either in eV or in kbar, depending on the moviemode. w?? is in eV/Å and P?? is in kbar.

---------------------------------------------------------





2) NEEDED PARAMETERS
---------------------------------------------------------

moviemode: 15,16,17,18
avgvir: 0 or something else (integer) 
virsym: 0 or something else (integer)
virkbar: 0 or something else (integer)
outtype: 0 or 1
outzmin/outzmin2: -inf to inf
outzmax/outzmax2: -inf to inf
virboxsiz: 0 - 1


avgvir and virkbar parameters can also be used in the old moviemodes 5 and 6, to either calculate the average 0K-virial for the diagonal elements or to print out them in kbar instead of eV/Å.

---------------------------------------------------------






3) EXPLANATION OF THE PARAMETERS
---------------------------------------------------------

The virials will be calculated with the moviemodes 15, 16, 17 and 18. Moviemodes 15 and 16 prints out the 0K virials comparable with moviemodes 5 and 6. The two latter ones takes into account the virial contribution from the velocities of the atoms. The moviemodes 16 and 18 will generate two new files, block_atoms.dat and block_natoms.dat, the former contains the virials for the specified atoms and the latter one contains the number of atoms per step that is printed to the block_atoms.dat file.   

("0K virials")
15: 	Element x y z atomtype atomindex wxx wyy wzz wxy wyx wyz wzy wxz wzx
	or
 	Element x y z atomtype atomindex wxx wyy wzz wxy wyz wxz (with virsym)

16: 	Element x y z atomtype atomindex (For all atoms)
	Element x y z atomtype atomindex wxx wyy wzz wxy wyx wyz wzy wxz wzx (For some atoms specified with parameters described later)
	or
 	Element x y z atomtype atomindex (For all atoms)  (with virsym)
	Element x y z atomtype atomindex wxx wyy wzz wxy wyz wxz (For some atoms specified with parameters described later) (with virsym)

("Finite temperature virials")
17: 	Element x y z atomtype atomindex Pxx Pyy Pzz Pxy Pyx Pyz Pzy Pxz Pzx
	or
 	Element x y z atomtype atomindex Pxx Pyy Pzz Pxy Pyz Pxz (with virsym)

18: 	Element x y z atomtype atomindex (For all atoms)
	Element x y z atomtype atomindex Pxx Pyy Pzz Pxy Pyx Pyz Pzy Pxz Pzx (For some atoms specified with parameters described later)
	or
 	Element x y z atomtype atomindex (For all atoms) (with virsym)
	Element x y z atomtype atomindex Pxx Pyy Pzz Pxy Pyz Pxz (For some atoms specified with parameters described later) (with virsym)

-----------------

There are two different modes for the printout of the virials. The printout frequency is the same as the normal movie printout. The default mode is that the immediate virial is printed out, eg. avgvir-parameter is zero. If this parameter is something else than zero the average, between printout steps, of the virials are printed out. 

avgvir:

0:	Default, the immediate virials are printed out.
else:	The average between moviesteps of the virials are printed out


-----------------


For all moviemodes only half of the virials can be printed out to save space, the parameter is virsym. e.g. wxy = wyx, wyz = wzy and wxz = wzx

virsym
0:	Default, all virials are printed out. Do not use this, as internally it just prints the same values twice for wxy/wyx, ...
else:	Only wxy, wyz and wxz are printed out to md.movie or block_atoms.dat


-----------------

For moviemodes 15 and 16 the parameter virkbar can be used to print out the virial in kbar instead of eV/Å. This can be useful atleast in dislocation simulation.

virkbar
0: 	Default, prints out the virial in eV/Å
else:	Prints out the virials in kbar


-----------------

For the moviemodes 16 and 18 there are two different printouts. The outtype-parameter, either 0 or 1, says which atoms are included in the virial output. For outtype = 0 all fixed atoms are included in the printout. For outtype = 1, two different parameters describes the z-coordinates between which the virials are printed out. These two parameters are called outzmin and outzmax, both with the default 0. Also outzmin2 and outzmax2 can be used to print out multiple intervals

outtype:

0:	Default, prints out the full virials for all fixed atoms
1:	Full virials are printed out for atoms between outzmin and outzmax.

outzmin/outzmin2:

?:	Default 0, the (second) lower limit for printed out atoms


outzmax/outzmax2:

?:	Default 0, the (second) upper limit for printed out atoms


-----------------

virboxsiz must be used to get the right virials in the case that the box is not filled with atoms, eg. a nanowire or a void simulations. The normal routine calculates the required atomic volume as boxsize/natoms. But this boxsize of the simulation cell will be multiplied by the virboxsiz-factor. So if there is a void with ae size of 10% of the block, this factor should be 0.90, to get the right results. 

virboxsiz:

?:	Default 1, eg the box is completely filled.


---------------------------------------------------------






4) STRESS CALCULATIONS
---------------------------------------------------------

To do stess calculations in PARCAS for instance for dislocation simulations all needed parameters/calculations are implemented in the code. Some of the parameters are explained in the README.DOCUMENTATION in chapter "3.2.7 Applying shear and random forces" and the technique is described for instance in "Schäublin et al, J. Nucl. Mater. 362 (2007) 152". The needed calculation routines are explained in this document.


---------------------------------------------------------






5) FUNCTIONS MISSING
---------------------------------------------------------

If these functions are needed please contact Fredric:

The sum of a certain virial is calculated, but it's is not summed over all processors and not printed out anywhere. Also the average calculation of these are not implemented.
The binary output only outputs the immediate virials, eg. the average calculations are not implemented for binary output.


---------------------------------------------------------





6) POSSIBLE PROBLEMS
---------------------------------------------------------

There can appear a problem with the non-averaged off-diagonal viral printout at the last frame, when the simulation is stopped by endtemp-parameter. This is due to that the off-diagonal virials are only calculated when needed, and to predict the temperature is harder than what the next timestep or nstep is. This problem can be "solved" by calculating the virials at every step when near to the endtemp.




---------------------------------------------------------








