Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 99999999      Max number of time steps
tmax     = 10000.0        Maximum time in fs 
restartt = 0.0
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

Atom type-dependent parameters. 
Time step is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 3             Number of atom types
mass(0)  = 63.546        Atom mass in u
name(0)  = Cu            Name of atom type
mass(1)  = 69.723 28.08         Atom mass in u
name(1)  = Ga Si            Name of atom type
mass(2)  = 74.9216 72.59         Atom mass in u
name(2)  = As Ge            Name of atom type
substrate= SiGe          Substrate name, for use in elstop file name

iac(0,0) = -1            Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = -1            Automatic symmetry used: (1,0) = (0,1)
iac(0,2) = -1
iac(1,1) = 1 
iac(1,2) = 1 
iac(2,2) = 1 
            
potmode  = 11             0 LJ; 1 EAM; 2 EAM, direct Epair; 3 Si SW

reppotcut= 10.0           Reppotcut for SW and Tersoff: use 0 for no reppot

Simulation cell
---------------

latflag  = 5             Lattice: 0 FCC 1 Readin 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.25          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2, GaAs ~ 1.25

natoms   = 512          Number of atoms in simulation
box(1)   = 22.91       Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 22.91      Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 22.91       Box size in the Z-dir (Angstroms, sigma for LJ)
ncell(1) = 4             Number of unit cells along X-direction
ncell(2) = 4             Number of unit cells along Y-direction
ncell(3) = 4             Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 0               Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 3000.0        Initial T for mtemp=6
timeini  = 99999999.0    Time steps at temp0 before quench
ntimeini = 1000          Time at temp0 before quench
initemp  = 1000.0          Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 77.0          Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 50.0          Berendsen temp. control tau (fs), if 0 not used
trate    = 5.0           Quench rate for mtemp=6/8 (K/fs)

amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge

bpcbeta  = 1.1d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 100.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcP0z   = 0.0           Berendsen pres. control. desired P_z (kbar)

tscaleth = 5.43          Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor

ndump    = 1             Print data every ndump steps
nmovie   = 500           Number of steps btwn writing to md.movie (0=no movie)
dtmov(1) = 50.0          Time interval for movie output before tmov(1) (fs)
tmov(1)  = 1000.0        Time at which to change output interval (fs)
dtmov(2) = 50.0         Time interval for movie output for rest of run (fs)
ndefmovie= 200           Number of steps btwn writing to defects.out 

dslice(1)= -1.0          Slice selection: -1 all x, >= 0: ECMx +- dslice(1)
dslice(2)= -1.0          Slice selection: -1 all y, >= 0: ECMy +- dslice(2)
dslice(3)= 5.0           Slice selection: -1 all z, >= 0: ECMz +- dslice(3)
dtsli(1) = 100.0         Time interval for slice output before tmov(1) (fs)
tsli(1)  = 1000.0        Time at which to change output interval (fs)
dtsli(2) = 1000.0        Time interval for slice output for rest of run (fs)

nintanal = 1000          Number of steps between interstitial analysis
nliqanal = 100           Nsteps between liquid analysis; works in parallell !

Ekdef    = 0.22          Ekin threshold for labeling an atom defect/liquid 


Recoil calculation definitions
------------------------------

irec     = 0              Index of recoiling atom (-1 closest, -2 create)
recatype = 0              Recoil atom type
xrec     = -10.0          Desired initial position
yrec     = -10.0    
zrec     = 0.0    

recen    = 100.0         Initial recoil energy in eV
rectheta = 90.0          Initial recoil direction in degrees
recphi   = 24.0          Initial recoil direction in degrees

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek>10



Lattice structure for latflag=5
--------------------------------

# This one here is for 100 Si/GaAs structure

nbasis   = 8

offset(1)= 0.125
offset(2)= 0.125
offset(3)= 0.125

lx(1,1)  = 0.0  
lx(1,2)  = 0.0  
lx(1,3)  = 0.0  
ltype(1) = 1
 pchang(1)= 0.01
changt(1)= 2

lx(2,1)  = 0.25 
lx(2,2)  = 0.25 
lx(2,3)  = 0.25 
ltype(2) = 2
 pchang(2)= 0.01
changt(2)= 1

lx(3,1)  = 0.5 
lx(3,2)  = 0.5
lx(3,3)  = 0.0
ltype(3) = 1
 pchang(3)= 0.01
changt(3)= 2

lx(4,1)  = 0.75 
lx(4,2)  = 0.75
lx(4,3)  = 0.25
ltype(4) = 2
 pchang(4)= 0.01
changt(4)= 1

lx(5,1)  = 0.5 
lx(5,2)  = 0.0
lx(5,3)  = 0.5
ltype(5) = 1
 pchang(5)= 0.01
changt(5)= 2

lx(6,1)  = 0.75 
lx(6,2)  = 0.25
lx(6,3)  = 0.75
ltype(6) = 2
 pchang(6)= 0.01
changt(6)= 1

lx(7,1)  = 0.0 
lx(7,2)  = 0.5
lx(7,3)  = 0.5
ltype(7) = 1
 pchang(7)= 0.01
changt(7)= 2

lx(8,1)  = 0.25 
lx(8,2)  = 0.75
lx(8,3)  = 0.75
ltype(8) = 2
 pchang(8)= 0.01
changt(8)= 1



