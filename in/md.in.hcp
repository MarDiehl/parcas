Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 99999999      Max number of time steps
tmax     = 15000.0        Maximum time in fs 
restartt = 0.0
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 23762        Seed for random number generator

Atom type-dependent parameters. 
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 1             Number of atom types
mass(1)  = 58.93         Atom mass in u
name(1)  = Co            Name of atom type
substrate= Co            Substrate name, for use in elstop file name

iac(0,0) = -1            Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = 2               3 create EAM cross potential
iac(1,1) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
potmode  = 2             0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW

Simulation cell
---------------

latflag  = 5             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.2           Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = 28672         Number of atoms in simulation
box(1)   = 70.196        Box size in the X-dir (Angstroms)
box(2)   = 69.476032     Box size in the Y-dir (Angstroms)
box(3)   = 65.101472     Box size in the Z-dir (Angstroms)
ncell(1) = 28            Number of unit cells along X-direction
ncell(2) = 16            Number of unit cells along Y-direction
ncell(3) = 16            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 7             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 77.0          Initial T for mtemp=6
timeini  = 2000.0        Time at temp0 before quench
ntimeini = 991000        Time steps at temp0 before quench
initemp  = 0.0           Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 0.0           Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 50.0          Berendsen temp. control tau (fs), if 0 not used
trate    = 0.03          Quench rate for mtemp=6/8 (K/fs)

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 385.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge, 5.1d-4 Co

bpcbeta  = 5.1d-4        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 0.0           Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)

tscaleth = 2.51          Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor

ndump    = 5             Print data every ndump steps
nmovie   = 99500         Number of steps btwn writing to md.movie (0=no movie)
dtmov(1) = 250.0         Time interval for movie output before tmov(1) (fs)
tmov(1)  = 1000.0        Time at which to change output interval (fs)
dtmov(2) = 1000.0        Time interval for movie output before tmov(1) (fs)
tmov(2)  = 5000.0        Time at which to change output interval (fs)
dtmov(3) = 5000.0        Time interval for movie output for rest of run (fs)

nliqanal = 100           Nsteps between liquid analysis; works in parallell !
ndefmovie= 200           Number of steps btwn writing defects.out/pressures.out
nrestart = 1000          Number of steps between restart output

Ekdef    = 0.229         Ekin threshold for labeling an atom defect/liquid 

Recoil calculation definitions
------------------------------

irec     = -1              Index of recoiling atom (-1 closest, -2 create)
recatype = 1              Recoil atom type
xrec     = -10.0          Desired initial position
yrec     = -6.0    
zrec     = -15.0    

recen    = 2000.0         Initial recoil energy in eV
rectheta = 72.6716        Initial recoil direction in degrees
recphi   = 18.3209        Initial recoil direction in degrees

melstop  = 3             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek>10



Lattice structure for latflag=5
--------------------------------

# This one here is for HCP:
# Orthogonal unit cell corresponding to 2 HCP unit cells.
# Side lengths: x: a, y: sqrt(3)*a, z: c

nbasis   = 4

offset(1)= 0.125
offset(2)= 0.0833333
offset(3)= 0.25

lx(1,1)  = 0.0  
lx(1,2)  = 0.0  
lx(1,3)  = 0.0  
ltype(1) = 1

lx(2,1)  = 0.5
lx(2,2)  = 0.5
lx(2,3)  = 0.0
ltype(2) = 1

# The next two are the previous two plus 0,1/3,1/2

lx(3,1)  = 0.0  
lx(3,2)  = 0.3333333  
lx(3,3)  = 0.5
ltype(3) = 1

lx(4,1)  = 0.5
lx(4,2)  = 0.8333333
lx(4,3)  = 0.5
ltype(4) = 1





