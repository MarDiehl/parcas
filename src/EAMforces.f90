!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module EAM_params_mod
    use datatypes, only: real64b
    use defs, only: EAMTABLESZ

    implicit none
    save

    public
    private :: real64b, EAMTABLESZ

    real(real64b) :: rcutpot, dr, dri, dP
    integer ::  nr, nP

    ! EAM pair params - pair potential parameters
    real(real64b) :: Vp_r(EAMTABLESZ)
    real(real64b) :: dVpdr_r(EAMTABLESZ)
    real(real64b) :: Vpsc(EAMTABLESZ)

    ! EAM P params - electron density parameters (read P as rho!)
    real(real64b) :: P_r(EAMTABLESZ)
    real(real64b) :: dPdr_r(EAMTABLESZ)
    real(real64b) :: Psc(EAMTABLESZ)

    ! EAM Fp params - embedding energy parameters
    real(real64b) :: Pa(EAMTABLESZ)
    real(real64b) :: F_p(EAMTABLESZ)
    real(real64b) :: dFdp_p(EAMTABLESZ)
    real(real64b) :: Fsc(EAMTABLESZ)

end module EAM_params_mod


module eamforces_mod

    use output_logger, only: &
        log_buf, &
        logger, &
        logger_append_buffer, &
        logger_clear_buffer, &
        logger_write

    use defs
    use typeparam
    use datatypes, only: real64b
    use my_mpi

    use timers, only: &
        tmr, &
        TMR_EAM_CHARGE_COMMS, &
        TMR_EAM_FORCE_COMMS

    use splinereppot_mod, only: reppot_only

    use EAM_params_mod

    use para_common, only: &
        myatoms, np0pairtable, &
        debug, iprint

    use mdparsubs_mod, only: &
        pass_border_atoms, &
        pass_back_border_atoms, &
        potential_pass_back_border_atoms

    use EAM_comms, only: &
        RhoPassBackPacker, &
        dFpdpPassPacker


    implicit none

    private
    public :: Calc_P_eam
    public :: Calc_Fp_eam
    public :: Calc_Force_eam


contains

    !***********************************************************************
    ! EAM density calculation - for SANDIA, NRL, and etp versions
    !***********************************************************************
    !
    ! KN  See file README.forcesubs for some notes on what these do
    !

    subroutine Calc_P_eam(P, x0, atype, box, pbc, nborlist, spline)

        !
        !     From input x0 (from here and other nodes), return density P
        !     for all atom pairs which interact by EAM.
        !

        ! Variables passed in and out
        real(real64b), intent(out), contiguous, target :: P(:,:)
        real(real64b), intent(in), contiguous :: x0(:)
        real(real64b), intent(in) :: box(3), pbc(3)
        integer, intent(in), contiguous :: atype(:)
        integer, intent(in), contiguous :: nborlist(:)
        integer, intent(in) :: spline

        ! Local variables and constants
        integer :: j, mij, nbr, nnbors
        real(real64b) :: rs, xp(3), den, Rcuts, boxs(3)

        integer, parameter:: max_active_ngbrs = 2000
        real(real64b) :: ar_r(max_active_ngbrs)
        integer :: ar_j(max_active_ngbrs)
        integer :: active_ngbrs

        integer :: i3, j3, i
        real(real64b) :: t1

        integer :: klo, khi
        real(real64b) :: a, b, ab, yplo, yphi, ylo, yhi

        real(real64b) :: x
        real(real64b) :: percut2


        if (debug) print *, 'EAM 1'

        Rcuts = rcutpot**2
        boxs(:) = box(:)**2

        P(:np0pairtable, 1) = 0.0


        !
        !  Get P for atom pairs indexed in my node.
        !

        if (debug) print  *, 'EAM 2'

        mij = 0
        percut2 = 0.25d0 * minval(boxs)

        ! Check EAM table size, enough to do it once,
        ! as Rcut is the max value that r can get.
        if (sqrt(Rcuts) * dri + 2 > EAMTABLESZ) then
            call my_mpi_abort('Potential EAM overflow', int(Rcuts*dri)+2)
        end if

        do i = 1, myatoms
            i3 = 3*i - 3
            mij = mij + 1
            nnbors = nborlist(mij)
            active_ngbrs = 0
            ! Check here, as a check inside would cost performance.
            if (nnbors > max_active_ngbrs) then
                call my_mpi_abort('Potentially too many active ngbrs in EAM, &
                    &increase max_active_ngbrs', nnbors)
            end if
            ! For performance reasons we have fissioned the loop here, one for
            ! computing active r,j pairs, and one for density.

            ! First part of fissioned loop.
            do nbr = 1, nnbors
                mij = mij + 1
                j = nborlist(mij)
                ! Handle only EAM here.
                if (iac(abs(atype(i)), abs(atype(j))) /= 1) cycle
                j3 = 3*j - 3

                xp(:) = x0(i3+1:i3+3) - x0(j3+1:j3+3)
                rs = sum(xp(:)**2 * boxs(:))

                if (rs >= percut2) then
                    if (xp(1) >=  0.5d0) xp(1) = xp(1) - pbc(1)
                    if (xp(1) <  -0.5d0) xp(1) = xp(1) + pbc(1)
                    if (xp(2) >=  0.5d0) xp(2) = xp(2) - pbc(2)
                    if (xp(2) <  -0.5d0) xp(2) = xp(2) + pbc(2)
                    if (xp(3) >=  0.5d0) xp(3) = xp(3) - pbc(3)
                    if (xp(3) <  -0.5d0) xp(3) = xp(3) + pbc(3)
                    rs = sum(xp(:)**2 * boxs(:))
                end if

                if (rs < Rcuts) then
                    active_ngbrs = active_ngbrs+1
                    ar_r(active_ngbrs) = sqrt(rs)
                    ar_j(active_ngbrs) = j
                end if
            end do


            ! Second part of fissioned loop
            do nbr = 1, active_ngbrs
                x = ar_r(nbr) * dri
                j = ar_j(nbr)

                klo = int(x) + 1
                khi = klo + 1

                a = real(khi-1, real64b) - x
                b = 1 - a

                ylo = P_r(klo) * a
                yhi = P_r(khi) * b

                ab = a * b
                yplo = dPdr_r(klo) * a
                yphi = dPdr_r(khi) * b
                if (spline == 1) then
                    den = a*ylo + b*yhi + ab*(2d0 * (ylo + yhi) + dr * (yplo - yphi))
                else
                    den=ylo + yhi
                end if

                P(i,1) = P(i,1) + den
                P(j,1) = P(j,1) + den
            end do

        end do

        if (debug) print *, 'EAM 3'

        ! Send back contributions to P of other nodes' atoms.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            block
                type(RhoPassBackPacker) :: rho_packer

                rho_packer%nbands = 1
                rho_packer%rho => P
                rho_packer%rho_sum_passed = 0d0
                rho_packer%rho_sum_received = 0d0

                call pass_back_border_atoms(rho_packer)

                if (debug) then
                    write(log_buf,"(A,I3,A,3(A,G13.6))") &
                        'For proc ', myproc, ' rho sum ', &
                        ' local ', sum(P(:myatoms,1)), &
                        ' passed ', rho_packer%rho_sum_passed, &
                        ' received ', rho_packer%rho_sum_received
                    call logger(log_buf)
                end if
            end block
            tmr(TMR_EAM_CHARGE_COMMS) = tmr(TMR_EAM_CHARGE_COMMS) + (mpi_wtime() - t1)
        end if

        if (debug) print *, 'EAM 4'

    end subroutine Calc_P_eam


    !***********************************************************************
    ! EAM F[p(r)] calculation
    !***********************************************************************

    subroutine Calc_Fp_eam(Fp, dFpdp, P, spline, Fpextrap)

        !
        !     For input rho array P, return F[p] and d(F[p])/dp
        !

        !     ------------------------------------------------------------------
        !          Variables passed in and out
        real(real64b), intent(out), contiguous :: Fp(:,:)
        real(real64b), intent(out), contiguous :: dFpdp(:,:)
        real(real64b), intent(in), contiguous :: P(:,:)
        integer, intent(in) :: spline
        integer, intent(in) :: Fpextrap

        integer :: i
        integer :: khi, klo
        real(real64b) :: a, b, h, ab, ylo, yhi, yplo, yphi, x, xx
        logical :: overflow, underflow


        do i = 1, myatoms

            ! Inlined:
            ! CALL Splt2(nP,Pa,F_p,dFdp_p,Fsc,P(i,1),Fp(i,1),dFpdp(i,1))
            ! call Calc_F(Fp(i),dFpdp(i), P(i,1))

            if (P(i,1) == 0.0) then
                ! Must be impurity or sputtered atom
                Fp(i,1) = 0.0
                dFpdp(i,1) = 0.0
                cycle
            end if

            x = P(i,1)

            xx = P(i,1) / dP
            klo = int(xx) + 1
            khi = klo + 1

            overflow = .false.
            underflow = .false.

            if (x < 0d0) then
                print *, 'EAM negative electron density??', i, x
                if (Fpextrap /= 1) then
                    print *, '... proceeding with rho=zero.'
                    x = 0.0
                    xx= 0.0
                    klo = 1
                    khi = 2
                else
                    print *, '... doing downwards linear extrapolation.'
                    klo = 1
                    khi = 2
                    underflow = .true.
                end if
            end if

            if (khi > nP) then
                print *, 'EAM Fp overflow', i, P(i,1), dP, nP, khi
                if (Fpextrap == 1) then
                    ! This will effect a linear extrapolation
                    khi = nP
                    klo = khi - 1
                    overflow = .true.
                else
                    call my_mpi_abort('EAM Fp overflow', khi)
                end if
            end if

            h = dP
            a = ((khi-1) * dP - x) / dP
            b = 1 - a

            ylo = F_p(klo) * a
            yhi = F_p(khi) * b

            if (spline == 1 .and. .not. overflow .and. .not. underflow) then
                ab = a * b
                yplo = dFdp_p(klo) * a
                yphi = dFdp_p(khi) * b

                Fp(i,1) = a*ylo + b*yhi + ab * (2d0 * (ylo + yhi) + h * (yplo - yphi))

                dFpdp(i,1) = ab * Fsc(klo) + yplo + yphi
            else
                Fp(i,1) = ylo + yhi
                dFpdp(i,1) = (F_p(khi) - F_p(klo)) / dP
            end if
        end do

    end subroutine Calc_Fp_eam


    !***********************************************************************
    ! Calculate the force on each atom
    !***********************************************************************

    subroutine Calc_Force_eam(x0, xnp, Epair, atype, &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
            dFpdp, box, pbc, nborlist, &
            rcutmax, spline, calc_vir)

        !
        ! Force calculation: The force xnp(i) obtained is scaled by 1/box,
        ! i.e. if F is the real force in units of eV/A,
        !
        !  xnp(i) = F(i)/box(idim)
        !
        real(real64b), intent(out), contiguous :: xnp(:)
        real(real64b), intent(out), contiguous :: Epair(:)
        real(real64b), intent(out), contiguous :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)

        real(real64b), intent(in), contiguous :: x0(:)
        real(real64b), intent(inout), contiguous, target :: dFpdp(:,:)
        real(real64b), intent(in) :: rcutmax
        real(real64b), intent(in) :: box(3), pbc(3)
        integer, intent(in), contiguous :: atype(:)

        integer, intent(in) :: spline
        logical, intent(in) :: calc_vir

        integer, intent(in), contiguous :: nborlist(:)

        ! Local variables and constants
        integer :: nbr, nnbors, j, mij
        real(real64b) :: V_ij, Vp_ij, vij2, r, rs, xp(3), xptemp(3), rcutpots, boxs(3)

        integer :: j3, i3, i, itype, jtype
        real(real64b) :: tmp, dend, t1
        real(real64b) :: rcutmaxs

        ! Parameters for inlined calc_dend and calc_pair
        real(real64b) :: x, a
        real(real64b) :: ylo, yhi, yplo, yphi

        integer,parameter :: max_active_ngbrs = 2000
        real(real64b) :: ar_r(max_active_ngbrs)
        real(real64b) :: ar_xp(3, max_active_ngbrs)
        integer :: ar_j(max_active_ngbrs)
        integer :: ar_iac(max_active_ngbrs)
        integer :: active_ngbrs

        integer :: klo, khi
        real(real64b) :: percut2


        rcutpots = rcutpot**2
        rcutmaxs = rcutmax**2
        boxs(:) = box(:)**2

        ! Initialize forces

        xnp(:3*np0pairtable) = 0.0
        Epair(:np0pairtable) = 0.0

        wxxi(1 : np0pairtable) = 0.0
        wyyi(1 : np0pairtable) = 0.0
        wzzi(1 : np0pairtable) = 0.0

        if (calc_vir) then
            wxyi(1: np0pairtable) = 0.0
            wxzi(1: np0pairtable) = 0.0
            wyzi(1: np0pairtable) = 0.0
        end if


        ! Send dFpdp values of border atoms to neighbor nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            block
                type(dFpdpPassPacker) :: dFpdp_packer

                dFpdp_packer%nbands = 1
                dFpdp_packer%dFpdp => dFpdp

                call pass_border_atoms(dFpdp_packer)
            end block
            tmr(TMR_EAM_FORCE_COMMS) = tmr(TMR_EAM_FORCE_COMMS) + (mpi_wtime() - t1)
        end if

        !
        !  Get Epair and xnp for all atom pairs
        !
        percut2 = 0.25d0 * minval(boxs)

        ! Check beforhand worst case EAM overflow case
        if (sqrt(rcutpots) * dri + 2 >EAMTABLESZ) then
            call my_mpi_abort('Potential EAM overflow', int(rcutpots*dri)+2)
        end if

        mij = 0
        do i = 1, myatoms
            i3 = 3*i - 3
            mij = mij + 1
            nnbors = nborlist(mij)
            active_ngbrs = 0
            ! Check here, as a check inside would cost performance.
            if (nnbors > max_active_ngbrs) then
                call my_mpi_abort( &
                    'Potentially too many active ngbrs in EAM, &
                    &increase max_active_ngbrs', nnbors)
            end if


            ! First part of fissioned loop.
            itype = abs(atype(i))
            do nbr = 1, nnbors
                mij = mij+1
                j = nborlist(mij)


                jtype = abs(atype(j))

                if (iac(itype,jtype) /= 1) then
                    ! Handle other interaction types
                    if (iac(itype,jtype) == 0) cycle
                    if (iac(itype,jtype) == -1) then
                        call logger("ERROR: IMPOSSIBLE INTERACTION")
                        write(log_buf,*) myproc,i,j,itype,jtype
                        call logger(log_buf)
                        call my_mpi_abort('INTERACTION -1', myproc)
                    endif
                endif

                j3 = 3*j - 3

                xp(:) = x0(i3+1:i3+3) - x0(j3+1:j3+3)

                rs = sum(xp(:)**2 * boxs(:))

                if (rs >= percut2 ) then
                    if (xp(1) >=  0.5d0) xp(1) = xp(1) - pbc(1)
                    if (xp(1) <  -0.5d0) xp(1) = xp(1) + pbc(1)
                    if (xp(2) >=  0.5d0) xp(2) = xp(2) - pbc(2)
                    if (xp(2) <  -0.5d0) xp(2) = xp(2) + pbc(2)
                    if (xp(3) >=  0.5d0) xp(3) = xp(3) - pbc(3)
                    if (xp(3) <  -0.5d0) xp(3) = xp(3) + pbc(3)

                    rs = sum(xp(:)**2 * boxs(:))
                end if

                if (rs < rcutmaxs) then
                    if (iac(itype,jtype) == 2 .or. rs < rcutpots) then
                        active_ngbrs = active_ngbrs + 1
                        ar_r(active_ngbrs) = sqrt(rs)
                        ar_j(active_ngbrs) = j
                        ar_xp(:,active_ngbrs) = xp(:)
                        ar_iac(active_ngbrs) = iac(itype,jtype)
                    end if
                end if
            end do

            ! Second part of fissioned loop.
            do nbr = 1, active_ngbrs
                r = ar_r(nbr)
                j = ar_j(nbr)

                ! Force calc
                if (ar_iac(nbr) == 2) then
                    V_ij = 0.0
                    tmp = 0.0
                    jtype = abs(atype(j))

                    call reppot_only(r, V_ij, tmp, itype, jtype)
                    tmp = tmp / r
                else

                    x = r * dri
                    klo = int(x) + 1
                    khi = klo + 1


                    a = real(khi-1, real64b) - x
                    ylo = Vp_r(klo) * a
                    yhi = Vp_r(khi) * (1.0d0 - a)

                    if (spline == 1) then

                        dend = a * (1 - a) * Psc(klo) + dPdr_r(klo) * a + &
                            dPdr_r(khi) * (1 - a)

                        yplo = dVpdr_r(klo) * a
                        yphi = dVpdr_r(khi) * (1 - a)
                        V_ij = a * ylo + (1 - a) * yhi  + a * (1 - a) * &
                            (2.0d0 * (ylo + yhi) + dr * (yplo - yphi))
                        Vp_ij = a * (1 - a) * Vpsc(klo) + yplo + yphi

                    else
                        dend = (P_r(khi) - P_r(klo)) / dr
                        V_ij = ylo + yhi
                        Vp_ij = (Vp_r(khi) - Vp_r(klo)) / dr
                    end if

                    tmp = (dend * (dFpdp(i,1) + dFpdp(j,1)) + Vp_ij) / r
                end if

                vij2 = V_ij * 0.5d0
                Epair(i) = Epair(i) + vij2
                Epair(j) = Epair(j) + vij2

                ! This calculates the force in the right direction, and scales with box size.
                xp(:) = ar_xp(:, nbr)
                xptemp(:) = xp(:) * tmp

                xnp(i3+1:i3+3) = xnp(i3+1:i3+3) - xptemp(:)

                j3 = 3*j - 3
                xnp(j3+1:j3+3) = xnp(j3+1:j3+3) + xptemp(:)

                xp(:) = xp(:) * 0.5d0

                wxxi(i) = wxxi(i) - xptemp(1) * xp(1)
                wxxi(j) = wxxi(j) - xptemp(1) * xp(1)
                wyyi(i) = wyyi(i) - xptemp(2) * xp(2)
                wyyi(j) = wyyi(j) - xptemp(2) * xp(2)
                wzzi(i) = wzzi(i) - xptemp(3) * xp(3)
                wzzi(j) = wzzi(j) - xptemp(3) * xp(3)

                if (calc_vir) then
                    wxyi(i) = wxyi(i) - xptemp(1) * xp(2)
                    wxyi(j) = wxyi(j) - xptemp(1) * xp(2)
                    wxzi(i) = wxzi(i) - xptemp(1) * xp(3)
                    wxzi(j) = wxzi(j) - xptemp(1) * xp(3)
                    wyzi(i) = wyzi(i) - xptemp(2) * xp(3)
                    wyzi(j) = wyzi(j) - xptemp(2) * xp(3)
                end if

            end do

        end do

        ! Pass back contributions to forces, energies and virials
        ! to neighbor nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()

            if (calc_vir) then
                call potential_pass_back_border_atoms(&
                    xnp, Epair, wxxi, wyyi, wzzi, wxyi, wxzi, wyzi)
            else
                call potential_pass_back_border_atoms(&
                    xnp, Epair, wxxi, wyyi, wzzi)
            end if

            tmr(TMR_EAM_FORCE_COMMS) = tmr(TMR_EAM_FORCE_COMMS) + (mpi_wtime() - t1)
        end if

    end subroutine Calc_Force_eam

end module eamforces_mod
