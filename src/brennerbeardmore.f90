!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

!
! Parcas version of Keith Beardmore's Brenner potential implementation
!
! Forces: calculating fx,fy,fz, setting into xnp() at the end
!
! Atom types: external: atype 1 C 2 H 3 D 4 T 5 Si 6 Ar 7-  Any
!             internal: KTYP  1 C 2 Si 3 H/D/T 4 Ar 5- Any
!
! Cutoff extension: CONRH2(11) holds cutoffs for any interaction
! where any atom is >4.
!
!  POSSIBLE PROBLEM: parameters are given as real(kind=real32b) (e.g. "0.0" "6.0").
!  But the actual variables are real(kind=real64b). So after the 7th digit
!  the parameters may become garbage. E.g. 4.1305 will be instead
!  4.13049983978271.
!
! ----------------------------------------------------------------------
! Code first makes some tables
! BNDLEN(NNEBMX),CUTFCN(NNEBMX),CUTDRV(NNEBMX)
! BNDXNM(NNEBMX),BNDYNM(NNEBMX),BNDZNM(NNEBMX)
! BNDTYP(NNEBMX),NEB(NNEBMX)
!
!  in 1st loop over atoms, then does potential calc.
!
! Important note for pressure calc. : DKXC is really equivalent
! with normalized r_jk i.e. RXNJK (compare with RXNIK and RXNIJ).
!
!  Beardmore internal atom types :
!  KTYP=1    C
!  KTYP=2    Si
!  KTYP=3    H
!  KTYP=4    INERT, e.g. Ar.
!  KTYP=5-  Any atom interacting with pair potential
!
!
!
!  Added covalent radii for CC, CH and HH for bond order analysis.
!  Using Rasmol distances (see abstree.h):
!  C value: 0.72 Å  H value: 0.32 Å Si 1.2 Å  tolerance: 0.56 Å
!  Ergo: CC 2.0 CH 1.6 HH 1.2 Å
!        CSi 2.48 HSi 2.08 SiSi 2.96

!#######################################################################
!      SUBROUTINE CSIHPT
!#######################################################################
!
!   TERSOFF TYPE C/Si/H POTENTIAL.
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!   A COMBINATION OF:
!   DONALD. BRENNERS'S HYDROCARBON POTENTIAL (1st PARAMETERISATION).
!   PARAMETERS FROM : PHYS. REV. B 42, 9458-9471(1990).
!   PLUS CORRECTIONS : PHYS. REV. B 46, 1948(1990).
!   J. TERSOFF'S MULTICOMPONENT POTENTIAL FOR SI - C SYSTEMS.
!   PARAMETERS FROM : PHYS. REV. B 39, 5566-5568(1989).
!   R. MURTY & H. ATWATER'S Si-H POTENTIAL.
!   PARAMETERS FROM : PAPER PRESENTED AT COSIRES '94.
!
!   USES LINKED LIST AND POINTERS TO REDUCE SIZE OF NEB-LIST.
!   PRE-CALCULATES PAIRWISE TERMS.
!   APPLIES PERIODIC BOUNDARY CONDITIONS IN X, Z & Y WHEN NECESSARY.
!
!   ZEIGLER, BIERSACK & LITMARK'S UNIVERSAL POTENTIAL TO
!   DESCRIBE INTERACTION WITH INERT ELEMENT (e.g. Ar).
!   PARAMETERS FROM : COMPUTER SIMULATION OF ION-SOLID INTERACTIONS,
!   PP 40-42, W. ECKSTEIN, SPRINGER-VERLAG(BERLIN HEIDELBERG), (1991).
!
!   ALGORITHM ASSUMES ATOMS ARE:
!   KTYP=1 (C),KTYP=2 (Si), KTYP=3 (H) OR KTYP=4 (INERT, e.g. Ar).
!
!   FOR C/Si/H POTENTIAL :
!
!       M M M   N N N             The energy of bond I-J is
!        \|/     \|/           dependent on all atoms that are
!     M   K       L   N        first or second neighbours of
!      \   \     /   /         I and J. The resulting forces
!   M---K---I===J---L---N      act upon all these atoms.
!      /   /     \   \
!     M   K       L   N           In the code, the atoms and
!        /|\     /|\           bonds are identified as shown
!       M M M   N N N          on the left.
!
!   ZBL UNIVERSAL IS A PAIR POTENTIAL.
!

module Brenner_vars

    use datatypes, only: real64b

    implicit none
    save

    public
    private :: real64b


    integer, parameter :: BCOMBS = 4
    integer, parameter :: BTYPES = 2

    integer, allocatable :: ktyp(:)
    integer, allocatable :: iactmode(:) !some old shit, should be fixed

    ! variables for Brenner C-type atoms  -- Andrea
    integer, allocatable :: BNDTYPBRE(:)
    integer, allocatable :: BRETYP(:)
    integer :: BRETYPI, BRETYPJ, BRETYPK, BRETYPL, IJBPOT, IKBPOT, JLBPOT

    integer, allocatable :: DATUM(:)
    integer, allocatable :: NEB(:)
    integer, allocatable :: BNDTYP(:)

    real(real64b), allocatable :: BNDLEN(:), CUTFCN(:), CUTDRV(:)
    real(real64b), allocatable :: fermifcn(:), fermidrv(:)
    real(real64b), allocatable :: BNDXNM(:), BNDYNM(:), BNDZNM(:)
    real(real64b), allocatable :: NTSI(:)


    ! CONRH2 is of length 11 to hold cutoff for read-in pair potentials
    real(real64b) :: CONRH2(BCOMBS, 11)
    real(real64b), dimension(BCOMBS, 10) :: CONRL, CONFCA, CONFC, fermib, fermir
    real(real64b), dimension(BCOMBS, 6) :: CONFA, CONEA, CONFR, CONER, CONRE
    real(real64b), dimension(BCOMBS, 6) :: CONCD, CONC2, COND2, CONH
    real(real64b), dimension(6) :: CONALP, CONBET, CONN, CONPE, CONAN, CONNM, CONPF
    real(real64b) :: CONCSI
    real(real64b) :: CONC(4,4), COND(4,4)


    real(real64b) :: COVALRAD(4,4)
    real(real64b) :: HCCCF(16,4,4)
    real(real64b) :: HCHCF(16,4,4)

    real(real64b) :: FCCCF(16,4,4,4)

    real(real64b) :: AH(3,4)
    real(real64b) :: AF1(3,4), AF2(3,4)

end module Brenner_vars


module brenner_beardmore_mod

   use typeparam
   use datatypes, only: real64b
   use my_mpi

   use timers, only: tmr, TMR_SEMICON_COMMS

   use splinereppot_mod, only: reppot_only, reppot_fermi
   use defs
   use PhysConsts, only: pi
   use para_common, only: &
       myatoms, np0pairtable, &
       debug, iprint

   use mdparsubs_mod, only: potential_pass_back_border_atoms

   use Brenner_vars

   use output_logger, only: &
       logger, &
       log_buf, &
       logger_write, &
       logger_append_buffer, &
       logger_clear_buffer


    implicit none


    private
    public :: Beardmore_init
    public :: Beardmore_Force

contains

SUBROUTINE Beardmore_Force(x0,atype,xnp,box,pbc, &
      nborlist,Epair, &
      bondstat,bondstat2,bondstat3,reppotcut)

   ! Parcas calling parameters
   real(real64b), contiguous, intent(in) :: x0(:)
   real(real64b), intent(in) :: box(3), pbc(3)
   integer, contiguous, intent(in) :: atype(:)

   real(real64b), contiguous, intent(out) :: xnp(:)
   real(real64b), contiguous, intent(out) :: Epair(:)

   ! nborlist() holds the neighbor lists.
   ! Special Brenner version: NABORS and inverse list added
   !integer, contiguous, intent(in) :: invnborlist(:)
   integer, contiguous, intent(in) :: nborlist(:)

   integer, dimension(4, 0:NNMAX), intent(out) :: &
       bondstat, bondstat2, bondstat3

   real(real64b), intent(in) :: reppotcut

   ! parcas version help params
   integer :: i3,j3,jngbr


   integer :: typetranslate(0:6),typetranslatebs(0:6)
   ! atype translation:
   ! parcas number     0  1  2  3  4  5  6
   ! parcas type:      H  C  H  D  T  Si Ar
   data typetranslate /3, 1, 3, 3, 3, 2, 4/

   ! atype translation for bondstat:
   ! parcas number       0  1  2  3  4  5  6
   ! parcas type:        H  C  H  D  T  Si Ar
   data typetranslatebs /2, 1, 2, 2, 2, 3, 4/

   integer :: nbondsi



   ! Atom types: external: atype 1 C 2 H 3 D 4 T 5 Si 6 Ar 7- Any
   !             internal: KTYP  1 C 2 Si 3 H/D/T 4 Ar 5- Any


   REAL(kind=real64b) :: RXI,RYI,RZI,RXIJ,RYIJ,RZIJ
   REAL(kind=real64b) :: RLIJ,RLIJR,RLIK,RLJL
   REAL(kind=real64b) :: RXNIJ,RYNIJ,RZNIJ,RXNIK,RYNIK,RZNIK,RXNJL,RYNJL,RZNJL
   REAL(kind=real64b) :: ARG
   REAL(kind=real64b) :: FCIJ,DFCIJR,FCIK,DFCIKR,FCJL,DFCJLR
   REAL(kind=real64b) :: FCKM,DFCKMR,FCLN,DFCLNR
   REAL(kind=real64b) :: FAIJ,DFAIJR,FRIJ,DFRIJR
   REAL(kind=real64b) :: ZIJ,ZJI
   REAL(kind=real64b) :: DBIDXI,DBIDYI,DBIDZI,DBIDXJ,DBIDYJ,DBIDZJ
   REAL(kind=real64b) :: DBIDXK(NNMAX),DBIDYK(NNMAX),DBIDZK(NNMAX)
   REAL(kind=real64b) :: DBJDXI,DBJDYI,DBJDZI,DBJDXJ,DBJDYJ,DBJDZJ
   REAL(kind=real64b) :: DBJDXL(NNMAX),DBJDYL(NNMAX),DBJDZL(NNMAX)
   REAL(kind=real64b) :: DISJK,DISIL,DKXC,DKYC,DKZC,DLXC,DLYC,DLZC
   REAL(kind=real64b) :: COSTH,DIFLEN
   REAL(kind=real64b) :: GFACAN,CARG,GDDAN,QFACAN,QFADAN
   REAL(kind=real64b) :: DZFAC,DZDRIJ,DZDRJI,DZDRIK,DZDRJL
   REAL(kind=real64b) :: DGDXI,DGDYI,DGDZI,DGDXJ,DGDYJ,DGDZJ
   REAL(kind=real64b) :: DGDXK,DGDYK,DGDZK,DGDXL,DGDYL,DGDZL
   REAL(kind=real64b) :: DCSDIJ,DCSDJI,DCSDIK,DCSDJK,DCSDIL,DCSDJL
   REAL(kind=real64b) :: DCSDXI,DCSDYI,DCSDZI
   REAL(kind=real64b) :: DCSDXJ,DCSDYJ,DCSDZJ
   REAL(kind=real64b) :: DCSDXK,DCSDYK,DCSDZK
   REAL(kind=real64b) :: DCSDXL,DCSDYL,DCSDZL
   REAL(kind=real64b) :: BIJ,BJI,BAVEIJ,DFBIJ,DFBJI
   REAL(kind=real64b) :: VFAC,HLFVIJ,DFFAC,VFACDN
   REAL(kind=real64b) :: fermihelp,fermihelp2,fermiVr,fermidVr
   REAL(kind=real64b) :: NHI,NCI,NHJ,NCJ
   REAL(kind=real64b) :: NHIDXI,NHIDYI,NHIDZI
   REAL(kind=real64b) :: NHJDXJ,NHJDYJ,NHJDZJ
   REAL(kind=real64b) :: NCIDXI,NCIDYI,NCIDZI
   REAL(kind=real64b) :: NCJDXJ,NCJDYJ,NCJDZJ
   REAL(kind=real64b) :: NTIDXI,NTIDYI,NTIDZI,NTIDXJ,NTIDYJ,NTIDZJ
   REAL(kind=real64b) :: NTJDXJ,NTJDYJ,NTJDZJ,NTJDXI,NTJDYI,NTJDZI
   REAL(kind=real64b) :: NHIDXK(NNMAX),NHIDYK(NNMAX),NHIDZK(NNMAX)
   REAL(kind=real64b) :: NHJDXL(NNMAX),NHJDYL(NNMAX),NHJDZL(NNMAX)
   REAL(kind=real64b) :: NCIDXK(NNMAX),NCIDYK(NNMAX),NCIDZK(NNMAX)
   REAL(kind=real64b) :: NCJDXL(NNMAX),NCJDYL(NNMAX),NCJDZL(NNMAX)

   REAL(kind=real64b) :: NTIDXK(NNMAX),NTIDYK(NNMAX),NTIDZK(NNMAX)
   REAL(kind=real64b) :: NTJDXL(NNMAX),NTJDYL(NNMAX),NTJDZL(NNMAX)
   REAL(kind=real64b) :: HIJ,HJI,DHDNHI,DHDNHJ,DHDNCI,DHDNCJ
   REAL(kind=real64b) :: NTI,NTJ,NCONJ,NCNJDX,NCNJDR
   REAL(kind=real64b) :: NCNDXI,NCNDYI,NCNDZI,NCNDXJ,NCNDYJ,NCNDZJ
   REAL(kind=real64b) :: NCNDXK(NNMAX),NCNDYK(NNMAX),NCNDZK(NNMAX)
   REAL(kind=real64b) :: NCNDXL(NNMAX),NCNDYL(NNMAX),NCNDZL(NNMAX)
   REAL(kind=real64b) :: NCNDXM(NNMAX,NNMAX),NCNDYM(NNMAX,NNMAX),NCNDZM(NNMAX,NNMAX)
   REAL(kind=real64b) :: NCNDXN(NNMAX,NNMAX),NCNDYN(NNMAX,NNMAX),NCNDZN(NNMAX,NNMAX)
   REAL(kind=real64b) :: XIK,XIKDKX,XIKDKY,XIKDKZ,XJL,XJLDLX,XJLDLY,XJLDLZ
   REAL(kind=real64b) :: XIKDMX(NNMAX),XIKDMY(NNMAX),XIKDMZ(NNMAX)
   REAL(kind=real64b) :: XJLDNX(NNMAX),XJLDNY(NNMAX),XJLDNZ(NNMAX)
   REAL(kind=real64b) :: FXIK,DFXIKX,FXJL,DFXJLX
   REAL(kind=real64b) :: FIJ,DFDNTI,DFDNTJ,DFDCNJ
   REAL(kind=real64b) :: F1,F2,DF1DNT,DF2DNT,FASIH,FRSIH
   REAL(kind=real64b) :: DHDNTI,DHDNTJ
   REAL(kind=real64b) :: DBIDH,DBJDH,DBIDNI,DBJDNJ
   REAL(kind=real64b) :: CUTFNA,CUTDVA,TOTPOT,TOTFCE,POT
   !
   INTEGER :: I,J,K,L,M,N,IJ,IK,JL,KM,LN
   INTEGER :: K3,L3,M3,N3, itype, jtype, in, in0, nnbors
   INTEGER :: IKC,JLC,KMC,LNC
   INTEGER :: KTYPI,KTYPJ,KTYPK,KTYPL
   INTEGER :: IJPOT,IKPOT,JLPOT
   INTEGER :: MBTIJ,MBTJI,MBTIJK,MBTJIL
   INTEGER :: NEBTOT,ISTART,IFINSH
   INTEGER :: NEBOFI(NNMAX),NEBOFJ(NNMAX)
   INTEGER :: NEBOFK(NNMAX,NNMAX),NEBOFL(NNMAX,NNMAX)
   INTEGER :: NUMNBI,NUMNBJ,NUMNBK(NNMAX),NUMNBL(NNMAX)
   real(real64b) :: dx, dy, dz
   real(real64b) :: t1


   ! Parcas interface setup stuff

   iactmode=1


   xnp(:3*np0pairtable) = 0d0
   Epair(:np0pairtable) = 0d0


   do i=1,np0pairtable!myatoms   ! <-------------------- Loop over atoms i
      i3=i*3-3
      itype=abs(atype(i))

      if (itype > 6) then
         ! Adding Brenner C-type atoms (iactmode > 1)     Andrea
         if (iactmode(i) == 1) then
            KTYP(i) = itype-2
            BRETYP(i)=1
         else
            KTYP(i)=1
            BRETYP(i) = itype-5
         endif
      else
         KTYP(i)=typetranslate(itype)
         BRETYP(i)=1
      endif
   enddo

   do j=0,NNMAX
      do i=1,4
         bondstat(i,j)=0
         bondstat2(i,j)=0
         bondstat3(i,j)=0
      enddo
   enddo

   !   CALCULATE THE MAIN PAIRWISE TERMS AND STORE THEM.
   !   CALCULATE ZBL PAIR POTENTIALS HERE.

   NEBTOT = 1
   in = 0
   DO I = 1, np0pairtable !myatoms

      i3=I*3-3
      itype=abs(atype(i))


      in=in+1; in0=in+1
      nnbors = nborlist(in)
      in=in+nnbors

      RXI   = x0(i3+1)!*box(1)
      RYI   = x0(i3+2)!*box(2)
      RZI   = x0(i3+3)!*box(3)
      KTYPI = KTYP(I)
      BRETYPI = BRETYP(I)   ! Andrea

      nbondsi=0

      NTSI(I) = 0.0
      DATUM(I) = NEBTOT
      do jngbr=in0,in
         J     = nborlist(jngbr)
         j3=j*3-3

         jtype=abs(atype(j))
         KTYPJ = KTYP(J)
         BRETYPJ = BRETYP(J)    ! Andrea


         ! Take care on non-interacting hydrogens   ! correcting apparent bug here (second KTYPI==3 -> KTYPJ==3)...  Andrea
         if (KTYPI==3 .and. KTYPJ==3 .and. (iactmode(I)==2 .or. iactmode(J)==2)) cycle

         !   TAKE CARE OF PBCs HERE.
         !   REST OF POTENTIAL USES INTERNUCLEAR VECTORS, NOT POSITIONS.

         RXIJ  = x0(j3+1) - RXI
         if (RXIJ >= 0.5d0) RXIJ=RXIJ-pbc(1)
         if (RXIJ < -0.5d0) RXIJ=RXIJ+pbc(1)
         RXIJ=RXIJ*box(1)

         RYIJ  = x0(j3+2) - RYI
         if (RYIJ >= 0.5d0) RYIJ=RYIJ-pbc(2)
         if (RYIJ < -0.5d0) RYIJ=RYIJ+pbc(2)
         RYIJ=RYIJ*box(2)

         RZIJ  = x0(j3+3) - RZI
         if (RZIJ >= 0.5d0) RZIJ=RZIJ-pbc(3)
         if (RZIJ < -0.5d0) RZIJ=RZIJ+pbc(3)
         RZIJ=RZIJ*box(3)

         RLIJ  = RXIJ*RXIJ + RYIJ*RYIJ + RZIJ*RZIJ

         if (itype>6 .or. jtype>6) then
            ! Do nothing, pair potentials not counted
         else
            if (RLIJ <= COVALRAD(typetranslatebs(itype),typetranslatebs(jtype))) then
               nbondsi=nbondsi+1
            endif
         endif

         IF( KTYPI  >=  4 .OR. KTYPJ  >=  4 ) THEN
            !    CALCULATE pair potential and Ar INTERACTIONS HERE.
            ! "SOME SHIT IS HERE, CHECK FOR PARALLEL"
            IF ( I  >  J ) THEN
                ! ibuf(I3+3) probably meant to mean atomindex (?)
               !IF ( ibuf(I3+3)  > ibuf(J3+3)   ) THEN

               IJPOT = KTYPI + KTYPJ + 2

               if (KTYPI > 4 .or. KTYPJ>4) IJPOT=11
               if (IJPOT>11) IJPOT=11

               IF( CONRH2(1,IJPOT)  >  RLIJ ) THEN



                  !     WITHIN CUTOFF, SO CALCULATE INTERACTION.
                  RLIJ = SQRT( RLIJ )

                  if ( KTYPI  < 5 .and. KTYPJ  <  5) then

                     ! Ar ZBL interactions
                     !     CUTOFF FUNCTION AND DERIVATIVE.
                     IF( RLIJ  <  CONRL(1,IJPOT) ) THEN
                        CUTFNA = 1.0
                        CUTDVA = 0.0
                     ELSE
                        ARG = CONFCA(1,IJPOT)*( RLIJ-CONRL(1,IJPOT) )
                        CUTFNA = 0.5 *  ( 1.0 + COS( ARG ) )
                        CUTDVA = CONFC(1,IJPOT) * SIN( ARG )
                     ENDIF

                     !     ZBL UNIVERSAL POTENTIAL.
                     IJPOT = IJPOT - 6
                     TOTPOT = 0.0
                     TOTFCE = 0.0
                     DO N =1,4
                        POT = CONC(IJPOT,N) * EXP ( COND(IJPOT,N) * RLIJ )
                        TOTPOT = TOTPOT + POT
                        TOTFCE = TOTFCE + COND(IJPOT,N) * POT
                     ENDDO
                     TOTPOT = TOTPOT / RLIJ
                     TOTFCE = -CUTFNA * ( TOTFCE / RLIJ - TOTPOT / RLIJ ) -CUTDVA * TOTPOT

                     TOTPOT = CUTFNA * TOTPOT / 2.0

                  else

                     ! Spline pair potential
                     ! Note that there is a different sign convention
                     ! for the forces !

                     call reppot_only(RLIJ,TOTPOT,TOTFCE,itype,jtype)

                     TOTPOT=TOTPOT/2.0d0
                     TOTFCE= -TOTFCE

                  endif

                  ! dirbuf<5 used to mean half of nodes.
                  ! This was commented even before the parallelization change to 3D.
                  !if ( dirbuf(j) < 5 ) then

                  Epair(i)  = Epair(i) + TOTPOT
                  Epair(j)  = Epair(j) + TOTPOT
                  !endif

                  TOTFCE = TOTFCE / RLIJ
                  xnp(i3+1)=xnp(i3+1) - TOTFCE*RXIJ
                  xnp(i3+2)=xnp(i3+2) - TOTFCE*RYIJ
                  xnp(i3+3)=xnp(i3+3) - TOTFCE*RZIJ

                  xnp(j3+1)=xnp(j3+1) + TOTFCE*RXIJ
                  xnp(j3+2)=xnp(j3+2) + TOTFCE*RYIJ
                  xnp(j3+3)=xnp(j3+3) + TOTFCE*RZIJ


               ENDIF
            ENDIF
         ELSE

            !     STORE PAIR TERMS HERE ( ONLY STORE C, Si & H ATOMS ).


            IF( KTYPI  ==  1 )THEN
               IJPOT = KTYPJ
            ELSE IF( KTYPJ  ==  1 )THEN
               IJPOT = KTYPI
            ELSE
               IJPOT = KTYPI + KTYPJ
            ENDIF

            ! IJBPOT keeps track of parameters for additional "brennertype" atoms /= C
            IF ( BRETYPI == 1 .AND. BRETYPJ == 1 ) THEN
               IJBPOT = 1
               ! C-H
            ELSE IF (KTYPI == 3) THEN
               IJBPOT =  BRETYPJ
            ELSE IF (KTYPJ == 3) THEN
               IJBPOT = BRETYPI
            ELSE
               ! C-C  **CHECK THIS** Andrea
               IF (BRETYPI > BRETYPJ) THEN
                  IJBPOT = int((BRETYPI)*(BRETYPI + 1)*0.5 - BRETYPJ + BTYPES)
               ELSE
                  IJBPOT = int((BRETYPJ)*(BRETYPJ + 1)*0.5 - BRETYPI + BTYPES)
               ENDIF
            ENDIF


            IF( CONRH2(IJBPOT,IJPOT)  >  RLIJ ) THEN

               !     WITHIN CUTOFF, SO ADD J TO NEB-LIST.
               NEB(NEBTOT) = J

               !     BOND-LENGTH AND DIRECTION COSINES.
               RLIJ           = SQRT( RLIJ )
               BNDLEN(NEBTOT) = RLIJ
               BNDXNM(NEBTOT) = RXIJ / RLIJ
               BNDYNM(NEBTOT) = RYIJ / RLIJ
               BNDZNM(NEBTOT) = RZIJ / RLIJ
               BNDTYP(NEBTOT) = IJPOT
               BNDTYPBRE(NEBTOT) = IJBPOT

               !     CUTOFF FUNCTION AND DERIVATIVE.
               IF( RLIJ  <  CONRL(IJBPOT,IJPOT) ) THEN
                  CUTFCN(NEBTOT) = 1.0
                  CUTDRV(NEBTOT) = 0.0
               ELSE
                  ARG = CONFCA(IJBPOT,IJPOT)*( RLIJ-CONRL(IJBPOT,IJPOT) )
                  CUTFCN(NEBTOT) = 0.5 *  ( 1.0 + COS( ARG ) )
                  CUTDRV(NEBTOT) = CONFC(IJBPOT,IJPOT) * SIN( ARG )
               ENDIF
               ! Fermi function if needed
               if (reppotcut > RLIJ) then
                  fermihelp=exp(-fermib(IJBPOT,IJPOT)*(RLIJ-fermir(IJBPOT,IJPOT)))
                  fermifcn(NEBTOT)=1.0d0/(1.0d0+fermihelp)
                  fermidrv(NEBTOT)=fermifcn(NEBTOT)*fermifcn(NEBTOT)*fermihelp*fermib(IJBPOT,IJPOT)

               endif


               !     SUM THE NUMBER OF NEIGHBOURS OF SI ATOMS.
               IF( KTYPI  ==  2 ) NTSI(I) = NTSI(I) + CUTFCN(NEBTOT)

               NEBTOT = NEBTOT + 1
            ENDIF
         ENDIF

      enddo  ! End of loop over neighbours j

      if (itype <=6) then
         ! Do bond statistics
         bondstat(typetranslatebs(itype),nbondsi)=    &
            bondstat(typetranslatebs(itype),nbondsi)+1

         bondstat2(typetranslatebs(itype),NEBTOT-DATUM(I))=    &
            bondstat2(typetranslatebs(itype),NEBTOT-DATUM(I))+1

         if (itype >= 0) then
            ! bondstat3: disregard fixed atom bonds
            bondstat3(typetranslatebs(itype),NEBTOT-DATUM(I))=    &
               bondstat3(typetranslatebs(itype),NEBTOT-DATUM(I))+1
         endif
      endif

   ENDDO ! End of first loop over atoms i
   DATUM(np0pairtable+1) = NEBTOT !DATUM(myatoms+1) = NEBTOT

   !   BEGIN POTENTIAL CALCULATION.
   DO I = 1,myatoms !-1

      i3=i*3-3
      itype=abs(atype(i))
      KTYPI = KTYP(I)
      BRETYPI = BRETYP(I)   ! Andrea
      !   ONLY CONSIDER C, Si AND H INTERACTIONS.

      IF( KTYPI  >  3 ) cycle

      ! Note: because atom types > 4 (Ar or larger) are not taken
      ! into the neighbout list, they also will not be part of any
      ! calculation in the remainder of this subroutine. KN note 8.10 2001.

      !   HAVE A LIST OF ALL NON-NEGLIGIBLE BONDS ON ATOM I.
      !   I==J LOOP. CONSIDER ALL PAIRS OF ATOMS I < J.
      ISTART = DATUM(I)
      IFINSH = DATUM(I+1)-1

      DHDNTI = 0; DHDNTJ = 0
      DF1DNT = 0; DF2DNT = 0

      NTIDXI = 0; NTIDYI = 0; NTIDZI = 0
      NTIDXJ = 0; NTIDYJ = 0; NTIDZJ = 0
      NTJDXI = 0; NTJDYI = 0; NTJDZI = 0
      NTJDXJ = 0; NTJDYJ = 0; NTJDZJ = 0

      NHIDXI = 0; NHIDYI = 0; NHIDZI = 0
      NHJDXJ = 0; NHJDYJ = 0; NHJDZJ = 0

      NCNDXI = 0; NCNDYI = 0; NCNDZI = 0
      NCNDXJ = 0; NCNDYJ = 0; NCNDZJ = 0

      NCIDXI = 0; NCIDYI = 0; NCIDZI = 0
      NCJDXJ = 0; NCJDYJ = 0; NCJDZJ = 0

      FASIH = 0; FRSIH = 0
      DBIDH = 0; DBJDH = 0

      DO IJ = ISTART,IFINSH
         J = NEB(IJ)
         j3=j*3-3
         jtype=abs(atype(j))

         ! This excludes exactly half of neighbors crossing the process border.
         ! Only one of the involved processes should compute each neighbor pair.
         if (j > myatoms) then
             dx = x0(i3+1) - x0(j3+1)
             if (dx > 0d0) then
                 cycle
             else if (dx == 0d0) then
                 dy = x0(i3+2) - x0(j3+2)
                 if (dy > 0d0) then
                     cycle
                 else if (dy == 0d0) then
                     dz = x0(i3+3) - x0(j3+3)
                     if (dz > 0d0) cycle
                 end if
             end if
         end if


         IF( J  >  I .or. j > myatoms ) THEN

            KTYPJ = KTYP(J)
            BRETYPJ = BRETYP(J)      ! Andrea
            IJBPOT = BNDTYPBRE(IJ)  !  Andrea
            IJPOT = BNDTYP(IJ)
            RLIJ  = BNDLEN(IJ)
            RLIJR = 1.0 / RLIJ
            RXNIJ = BNDXNM(IJ)
            RYNIJ = BNDYNM(IJ)
            RZNIJ = BNDZNM(IJ)

            !   CUTOFF FUNCTION AND DERIVATIVE.
            fermihelp=1.0d0
            if (reppotcut>RLIJ) then
               fermihelp=fermifcn(IJ)
            endif
            FCIJ   = CUTFCN(IJ)*fermihelp
            DFCIJR = CUTDRV(IJ)*fermihelp

            !   BEGIN TO SUM THE NUMBER OF ATOMS ON I AND J IF BOND CONTAINS CARBON
            !   (NC IS NO. CARBON + NO. SILICON).
            IF( IJPOT  ==  1 .AND. IJBPOT == 1 )THEN    ! Andrea
               !   CARBON-CARBON
               NHI    = 0.0
               NHJ    = 0.0
               NHIDXI = 0.0
               NHIDYI = 0.0
               NHIDZI = 0.0
               NHJDXJ = 0.0
               NHJDYJ = 0.0
               NHJDZJ = 0.0
               NCI    = 0.0
               NCJ    = 0.0
               NCIDXI = 0.0
               NCIDYI = 0.0
               NCIDZI = 0.0
               NCJDXJ = 0.0
               NCJDYJ = 0.0
               NCJDZJ = 0.0
               !   INITIALISE NijConj TERM AND DERIVATIVES.
               NCONJ  = 1.0
               NCNDXI = 0.0
               NCNDYI = 0.0
               NCNDZI = 0.0
               NCNDXJ = 0.0
               NCNDYJ = 0.0
               NCNDZJ = 0.0
            ELSE IF( KTYPI == 1 .AND. BRETYPI == 1 .AND. KTYPJ == 3 )THEN  !Andrea
               !   CARBON-HYDROGEN
               NHI    = 0.0
               NHIDXI = 0.0
               NHIDYI = 0.0
               NHIDZI = 0.0
               NCI    = 0.0
               NCIDXI = 0.0
               NCIDYI = 0.0
               NCIDZI = 0.0
            ELSE IF( KTYPI == 3 .AND. KTYPJ == 1 .AND. BRETYPJ == 1 )THEN  ! Andrea
               !   HYDROGEN-CARBON
               NHJ    = 0.0
               NHJDXJ = 0.0
               NHJDYJ = 0.0
               NHJDZJ = 0.0
               NCJ    = 0.0
               NCJDXJ = 0.0
               NCJDYJ = 0.0
               NCJDZJ = 0.0
            ENDIF

            !   MORSE TERMS AND DERIVATIVES (INCLUDE SPLINES FOR Si-H BONDS).
            FAIJ   = CONFA(IJBPOT,IJPOT) * EXP( CONEA(IJBPOT,IJPOT) * RLIJ)
            FRIJ   = CONFR(IJBPOT,IJPOT) * EXP( CONER(IJBPOT,IJPOT) * RLIJ)
            DFAIJR = CONEA(IJBPOT,IJPOT) * FAIJ
            DFRIJR = CONER(IJBPOT,IJPOT) * FRIJ
            IF( IJPOT  ==  5 )THEN
               FASIH = FAIJ
               FRSIH = FRIJ
               IF( KTYPI  ==  2 )THEN
                  call interpolate_F1_F2(NTSI(I),F1,F2,DF1DNT,DF2DNT)
               ELSE
                  call interpolate_F1_F2(NTSI(J),F1,F2,DF1DNT,DF2DNT)
               ENDIF
               FAIJ   = F2 * FASIH
               FRIJ   = F1 * FRSIH
               DFAIJR = F2 * DFAIJR
               DFRIJR = F1 * DFRIJR
            ENDIF

            !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.
            IF     (KTYPI == 1 .AND. KTYPJ == 2) THEN
               MBTIJ = 4
            ELSE IF(KTYPI == 2 .AND. KTYPJ == 3) THEN
               MBTIJ = 5
            ELSE
               MBTIJ = KTYPI
            ENDIF

            !   USE SPLINE TO GET VALUE FOR H FOR ANGLE TERMS IF NEEDED.
            IF( KTYPI  ==  2 ) THEN
               call interpolate_HSi(NTSI(I),CONH(1,5),DHDNTI)
               NTIDXI = - RXNIJ * DFCIJR
               NTIDYI = - RYNIJ * DFCIJR
               NTIDZI = - RZNIJ * DFCIJR

               NTIDXJ = - NTIDXI
               NTIDYJ = - NTIDYI
               NTIDZJ = - NTIDZI

               DBIDH  = 0.0
            ENDIF

            !   CALCULATE COMPONENTS OF BOND ORDER TERM AND DERIVATIVES
            !   WITH RESPECT TO BOND I-J FOR ATOM I.
            ZIJ    = 0.0
            DBIDXI = 0.0
            DBIDYI = 0.0
            DBIDZI = 0.0
            DBIDXJ = 0.0
            DBIDYJ = 0.0
            DBIDZJ = 0.0

            !   I--K LOOP.
            IKC = 0
            DO IK =ISTART,IFINSH
               !   CONSIDER ALL ATOMS BOUND TO I, EXCEPT ATOM J.
               IF( IK  /=  IJ )THEN
                  IKC   = IKC + 1
                  K     = NEB(IK)
                  NEBOFI(IKC) = K
                  KTYPK = KTYP(K)
                  BRETYPK = BRETYP(K)       !Andrea
                  IKBPOT = BNDTYPBRE(IK)
                  IKPOT = BNDTYP(IK)
                  RLIK  = BNDLEN(IK)
                  RXNIK = BNDXNM(IK)
                  RYNIK = BNDYNM(IK)
                  RZNIK = BNDZNM(IK)

                  FCIK   = CUTFCN(IK)
                  DFCIKR = CUTDRV(IK)

                  !   SUM THE NUMBER OF ATOMS ON I IF CARBON OR SILICON.
                  IF( KTYPI  ==  1 .AND. BRETYPI == 1 ) THEN   !Andrea
                     IF( KTYPK  <=  2  .AND. BRETYPK == 1 )THEN  !Andrea
                        !   K-CARBON OR SILICON
                        NCI         = NCI    + FCIK
                        NCIDXK(IKC) = RXNIK  * DFCIKR
                        NCIDYK(IKC) = RYNIK  * DFCIKR
                        NCIDZK(IKC) = RZNIK  * DFCIKR

                        NCIDXI      = NCIDXI - NCIDXK(IKC)
                        NCIDYI      = NCIDYI - NCIDYK(IKC)
                        NCIDZI      = NCIDZI - NCIDZK(IKC)

                        NHIDXK(IKC) = 0.0
                        NHIDYK(IKC) = 0.0
                        NHIDZK(IKC) = 0.0
                     ELSE
                        !   K-HYDROGEN
                        NHI         = NHI    + FCIK
                        NHIDXK(IKC) = RXNIK  * DFCIKR
                        NHIDYK(IKC) = RYNIK  * DFCIKR
                        NHIDZK(IKC) = RZNIK  * DFCIKR

                        NHIDXI      = NHIDXI - NHIDXK(IKC)
                        NHIDYI      = NHIDYI - NHIDYK(IKC)
                        NHIDZI      = NHIDZI - NHIDZK(IKC)

                        NCIDXK(IKC) = 0.0
                        NCIDYK(IKC) = 0.0
                        NCIDZK(IKC) = 0.0
                     ENDIF
                  ELSE IF( KTYPI  ==  2 ) THEN
                     !   K-C, SI OR H
                     NTIDXK(IKC) = RXNIK  * DFCIKR
                     NTIDYK(IKC) = RYNIK  * DFCIKR
                     NTIDZK(IKC) = RZNIK  * DFCIKR

                     NTIDXI      = NTIDXI - NTIDXK(IKC)
                     NTIDYI      = NTIDYI - NTIDYK(IKC)
                     NTIDZI      = NTIDZI - NTIDZK(IKC)
                  ENDIF

                  !   SUM NijConj IF C-C BOND AND K  IS CARBON.
                  IF( IJPOT  ==  1 .AND. IJBPOT == 1 ) THEN   ! Andrea
                     IF( KTYPK  ==  1 .AND. BRETYPK == 1 ) THEN  !Andrea
                        XIK    = 0.0

                        XIKDKX = 0.0
                        XIKDKY = 0.0
                        XIKDKZ = 0.0


                        !   CONSIDER ALL NEBS OF K, EXCEPT I.
                        !   K--M LOOP.
                        KMC = 0
                        DO KM =DATUM(K),DATUM(K+1)-1
                           M  = NEB(KM)
                           IF ( M  /=  I ) THEN
                              KMC = KMC + 1
                              NEBOFK(IKC,KMC) = M

                              FCKM   = CUTFCN(KM)
                              DFCKMR = CUTDRV(KM)

                              !   SUM Xik AND DERIVATIVES.
                              XIK         = XIK    + FCKM

                              XIKDMX(KMC) = DFCKMR * BNDXNM(KM)
                              XIKDMY(KMC) = DFCKMR * BNDYNM(KM)
                              XIKDMZ(KMC) = DFCKMR * BNDZNM(KM)

                              XIKDKX      = XIKDKX - XIKDMX(KMC)
                              XIKDKY      = XIKDKY - XIKDMY(KMC)
                              XIKDKZ      = XIKDKZ - XIKDMZ(KMC)

                           ENDIF
                           !   END OF K--M LOOP.
                        ENDDO
                        NUMNBK(IKC) = KMC

                        !   SUM F(Xik) AND DERIVATIVES.
                        IF     ( XIK  <=  2.0 ) THEN
                           FXIK   = 1.0
                           DFXIKX = 0.0
                        ELSE IF( XIK  >=  3.0 ) THEN
                           FXIK   = 0.0
                           DFXIKX = 0.0
                        ELSE
                           ARG    = PI  * ( XIK - 2.0 )
                           FXIK   = 0.5 * ( 1.0 + COS( ARG ) )
                           DFXIKX =-0.5 * PI * SIN( ARG )
                        ENDIF

                        NCNJDR = FXIK * DFCIKR
                        NCNJDX = FCIK * DFXIKX

                        NCONJ       = NCONJ  + FCIK * FXIK
                        NCNDXI      = NCNDXI - NCNJDR * RXNIK
                        NCNDYI      = NCNDYI - NCNJDR * RYNIK
                        NCNDZI      = NCNDZI - NCNJDR * RZNIK

                        NCNDXK(IKC) = NCNJDR * RXNIK + NCNJDX * XIKDKX
                        NCNDYK(IKC) = NCNJDR * RYNIK + NCNJDX * XIKDKY
                        NCNDZK(IKC) = NCNJDR * RZNIK + NCNJDX * XIKDKZ

                        DO KMC=1,NUMNBK(IKC)
                           NCNDXM(IKC,KMC) = NCNJDX *XIKDMX(KMC)
                           NCNDYM(IKC,KMC) = NCNJDX *XIKDMY(KMC)
                           NCNDZM(IKC,KMC) = NCNJDX *XIKDMZ(KMC)
                        ENDDO
                     ELSE
                        NUMNBK(IKC) = 0
                        NCNDXK(IKC) = 0.0
                        NCNDYK(IKC) = 0.0
                        NCNDZK(IKC) = 0.0

                     ENDIF
                  ENDIF


                  !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.
                  IF     (KTYPI == 2 .AND. KTYPK == 3) THEN
                     MBTIJK = 5
                  ELSE IF(KTYPI == 3 .AND. KTYPJ == 2 .AND. KTYPK == 2) THEN
                     MBTIJK = 6
                  ELSE
                     MBTIJK = MBTIJ
                  ENDIF

                  !   CALCULATE LENGTH DEPENDENT TERMS.
                  IF( ( KTYPI == 1 .AND. (KTYPJ /= 3 .OR.  KTYPK /= 3) )     &
                        .OR. ( KTYPI == 2 .AND. ((IJPOT+IKPOT)  <=  6       ) )     &
                        .OR. ( KTYPI == 3 .AND. BRETYPJ /= 1 )   &
                        .OR. ( MBTIJK == 6 ) .OR. ( IJBPOT /= 1 ) ) THEN
                     QFACAN = 1.0
                     QFADAN = 0.0
                  ELSE IF ( MBTIJK  ==  2 .OR. MBTIJK  ==  5 ) THEN
                     !   BETA = 3.0
                     !   EXP( ALPHA * {( Rij - Rik )}^BETA ) & dEXP / d(Rij-Rik).
                     DIFLEN = (RLIJ-CONRE(1,IJPOT)) - (RLIK-CONRE(1,IKPOT))
                     QFACAN = EXP( CONALP(MBTIJK) * DIFLEN * DIFLEN * DIFLEN )
                     QFADAN = CONALP(MBTIJK) * 3.0 * QFACAN * DIFLEN * DIFLEN
                  ELSE
                     !   BETA = 1.0
                     !   EXP( ALPHA * {( Rij - Rik )}^BETA ) & dEXP / d(Rij-Rik).
                     DIFLEN = (RLIJ-CONRE(IJBPOT,IJPOT)) - (RLIK-CONRE(IKBPOT,IKPOT))
                     QFACAN = EXP( CONALP(MBTIJK) * DIFLEN )
                     QFADAN = CONALP(MBTIJK) * QFACAN
                  ENDIF

                  !   CALCULATE ANGLE DEPENDENT TERMS.
                  IF( MBTIJK  /=  3 .OR. IJBPOT /= 1 )THEN    ! Andrea
                     !   COS( THETAijk ), G( THETAijk ) & dG / d{H-COS( THETA )}
                     COSTH  = RXNIK * RXNIJ + RYNIK * RYNIJ + RZNIK * RZNIJ
                     IF( MBTIJK == 1 .OR. MBTIJK == 4 .OR. MBTIJK == 3 ) THEN     !Andrea
                        CARG = COND2(IKBPOT,MBTIJK) + &
                            (CONH(IKBPOT,MBTIJK) - COSTH)* &
                            (CONH(IKBPOT,MBTIJK) - COSTH)
                        GFACAN = CONCD(IKBPOT,MBTIJK) - CONC2(IKBPOT,MBTIJK) / CARG
                        GDDAN  = 2.0 * CONC2(IKBPOT,MBTIJK) * &
                            (CONH(IKBPOT,MBTIJK) - COSTH)/(CARG*CARG)
                     ELSE
                        CARG   = CONH(1,MBTIJK) - COSTH
                        GFACAN = CONC2(1,MBTIJK) + COND2(1,MBTIJK) * CARG*CARG
                        GDDAN  = 2.0 * COND2(1,MBTIJK) * CARG
                     ENDIF


                     !    DIRECTION COSINES OF Rjk = ( Rik - Rij ) / DISjk
                     DKXC  = RXNIK * RLIK - RXNIJ * RLIJ
                     DKYC  = RYNIK * RLIK - RYNIJ * RLIJ
                     DKZC  = RZNIK * RLIK - RZNIJ * RLIJ

                     DISJK = SQRT( DKXC*DKXC+DKYC*DKYC+DKZC*DKZC )
                     DKXC  = DKXC / DISJK
                     DKYC  = DKYC / DISJK
                     DKZC  = DKZC / DISJK

                     !   dCOS( THETAijk ) / dRwh [WHERE w=x,y,z AND h =i,j,k]
                     DCSDIJ =  1.0 / RLIK - COSTH * RLIJR
                     DCSDIK =  RLIJR      - COSTH / RLIK
                     DCSDJK = - DISJK * RLIJR / RLIK

                     DCSDXI = -DCSDIJ*RXNIJ - DCSDIK*RXNIK
                     DCSDYI = -DCSDIJ*RYNIJ - DCSDIK*RYNIK
                     DCSDZI = -DCSDIJ*RZNIJ - DCSDIK*RZNIK

                     DCSDXJ =  DCSDIJ*RXNIJ                - DCSDJK*DKXC
                     DCSDYJ =  DCSDIJ*RYNIJ                - DCSDJK*DKYC
                     DCSDZJ =  DCSDIJ*RZNIJ                - DCSDJK*DKZC

                     DCSDXK =                 DCSDIK*RXNIK + DCSDJK*DKXC
                     DCSDYK =                 DCSDIK*RYNIK + DCSDJK*DKYC
                     DCSDZK =                 DCSDIK*RZNIK + DCSDJK*DKZC


                     !   FCik * EXP * dG( THETAijk ) / dCOS(THETAijk)
                     DZFAC = - FCIK * GDDAN * QFACAN

                     !   ADD DERIVATIVES OF H IF IT IS A SPLINE.
                     !   dETAij/dH = SUM [ FCik * EXP * dG( THETAijk ) / dH ]
                     IF(MBTIJK == 5) DBIDH = DBIDH - DZFAC

                     !   FCik * EXP * dG( THETAijk ) / dRwh [WHERE w=x,y,z AND h =i,j,k]
                     DGDXI = DZFAC * DCSDXI
                     DGDYI = DZFAC * DCSDYI
                     DGDZI = DZFAC * DCSDZI

                     DGDXJ = DZFAC * DCSDXJ
                     DGDYJ = DZFAC * DCSDYJ
                     DGDZJ = DZFAC * DCSDZJ

                     DGDXK = DZFAC * DCSDXK
                     DGDYK = DZFAC * DCSDYK
                     DGDZK = DZFAC * DCSDZK

                  ELSE
                     GFACAN = CONCD(1,3)

                     DGDXI = 0.0
                     DGDYI = 0.0
                     DGDZI = 0.0

                     DGDXJ = 0.0
                     DGDYJ = 0.0
                     DGDZJ = 0.0

                     DGDXK = 0.0
                     DGDYK = 0.0
                     DGDZK = 0.0

                  ENDIF

                  !   SUM ETAij
                  ZIJ    = ZIJ + FCIK * GFACAN * QFACAN

                  !   FCik * G( THETAijk ) * dEXP / dRij
                  DZDRIJ = GFACAN * FCIK * QFADAN

                  !   G( THETAijk ) * ( dFCik / dRik * EXP + FCik * dEXP / dRik )
                  DZDRIK = GFACAN * ( DFCIKR *QFACAN - FCIK *QFADAN )

                  !   SUM dETAij / dRwh [WHERE w=x,y,z AND h =i,j,k]
                  !   G * ( FCik * dEXP/dRwi + dFCik/dRwi * EXP ) + FCik * EXP * dG/dRwi
                  DBIDXI      =DBIDXI -DZDRIJ*RXNIJ -DZDRIK*RXNIK +DGDXI
                  DBIDYI      =DBIDYI -DZDRIJ*RYNIJ -DZDRIK*RYNIK +DGDYI
                  DBIDZI      =DBIDZI -DZDRIJ*RZNIJ -DZDRIK*RZNIK +DGDZI

                  !   G * FCik * dEXP/dRwj + FCik * EXP * dG/dRwj
                  DBIDXJ      =DBIDXJ +DZDRIJ*RXNIJ               +DGDXJ
                  DBIDYJ      =DBIDYJ +DZDRIJ*RYNIJ               +DGDYJ
                  DBIDZJ      =DBIDZJ +DZDRIJ*RZNIJ               +DGDZJ

                  !   G * ( FCik * dEXP/dRwk + dFCik/dRwk * EXP ) + FCik * EXP * dG/dRwk
                  DBIDXK(IKC) =                      DZDRIK*RXNIK +DGDXK
                  DBIDYK(IKC) =                      DZDRIK*RYNIK +DGDYK
                  DBIDZK(IKC) =                      DZDRIK*RZNIK +DGDZK

               ENDIF
               !   END OF I--K LOOP.
            ENDDO
            NUMNBI = IKC

            !   ADD ON DERIVATIVES OF BOND ORDER TERM DUE TO SPLINE FOR H(NtSi).
            IF( KTYPI  ==  2) THEN
               !   dETAij/dNti = dETAij/dH * dH/dNti
               DBIDNI = DBIDH * DHDNTI
               !   dETAij/dRwh = dETAij/dRwh + dETAij/dNti * dNti/dRwh
               DBIDXI = DBIDXI + DBIDNI * NTIDXI
               DBIDYI = DBIDYI + DBIDNI * NTIDYI
               DBIDZI = DBIDZI + DBIDNI * NTIDZI

               DBIDXJ = DBIDXJ + DBIDNI * NTIDXJ
               DBIDYJ = DBIDYJ + DBIDNI * NTIDYJ
               DBIDZJ = DBIDZJ + DBIDNI * NTIDZJ

               DO IKC = 1,NUMNBI
                  DBIDXK(IKC) = DBIDXK(IKC) + DBIDNI * NTIDXK(IKC)
                  DBIDYK(IKC) = DBIDYK(IKC) + DBIDNI * NTIDYK(IKC)
                  DBIDZK(IKC) = DBIDZK(IKC) + DBIDNI * NTIDZK(IKC)
               ENDDO
            ENDIF

            !   ADD ON Hij(Nhi,Nci) IF THIS IS A C-C OR C-H BOND. ! NOT IF BRETYP /= 1      Andrea
            IF( ( KTYPI == 1 .AND. BRETYPI == 1 .AND. BRETYPJ==1 .AND. KTYPJ /= 2 )  &
                  .AND. NHI<4.0 .AND. NCI<4.0 )THEN
               IF( KTYPJ  ==  1 )THEN
                  !   C-C BOND.
                  call interpolate_Hcc(NHI,NCI,HIJ,DHDNHI,DHDNCI)
               ELSE
                  !   C-H BOND.
                  call interpolate_Hch(NHI,NCI,HIJ,DHDNHI,DHDNCI)
               ENDIF

               !   ADD CONTRIBUTIONS TO ETAij AND ITS DERIVATIVES.
               ZIJ    = ZIJ    + HIJ
               DBIDXI = DBIDXI + DHDNHI * NHIDXI + DHDNCI * NCIDXI
               DBIDYI = DBIDYI + DHDNHI * NHIDYI + DHDNCI * NCIDYI
               DBIDZI = DBIDZI + DHDNHI * NHIDZI + DHDNCI * NCIDZI

               !   ADD CONTRIBUTIONS FROM DERIVATIVES TO NEIGHBOURS OF I.
               DO IKC = 1,NUMNBI
                  DBIDXK(IKC) = DBIDXK(IKC) +DHDNHI*NHIDXK(IKC) +DHDNCI*NCIDXK(IKC)
                  DBIDYK(IKC) = DBIDYK(IKC) +DHDNHI*NHIDYK(IKC) +DHDNCI*NCIDYK(IKC)
                  DBIDZK(IKC) = DBIDZK(IKC) +DHDNHI*NHIDZK(IKC) +DHDNCI*NCIDZK(IKC)
               ENDDO

            ENDIF

            !   DO THE SAME FOR TO ATOM J.

            !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.
            IF     (KTYPJ == 1 .AND. KTYPI == 2) THEN
               MBTJI = 4
            ELSE IF(KTYPJ == 2 .AND. KTYPI == 3) THEN
               MBTJI = 5
            ELSE
               MBTJI = KTYPJ
            ENDIF

            !   USE SPLINE TO GET VALUE FOR H FOR ANGLE TERMS IF NEEDED.
            IF( KTYPJ  ==  2 ) THEN
               call interpolate_HSi(NTSI(J),CONH(1,5),DHDNTJ)
               NTJDXJ = RXNIJ * DFCIJR
               NTJDYJ = RYNIJ * DFCIJR
               NTJDZJ = RZNIJ * DFCIJR
               NTJDXI = - NTJDXJ
               NTJDYI = - NTJDYJ
               NTJDZI = - NTJDZJ

               DBJDH  = 0.0
            ENDIF

            !   CALCULATE COMPONENTS OF BOND ORDER TERM AND DERIVATIVES
            !   WITH RESPECT TO BOND I-J FOR ATOM J.
            ZJI    = 0.0
            DBJDXI = 0.0
            DBJDYI = 0.0
            DBJDZI = 0.0
            DBJDXJ = 0.0
            DBJDYJ = 0.0
            DBJDZJ = 0.0


            !   J--L LOOP.
            JLC = 0
            DO JL =DATUM(J),DATUM(J+1)-1
               L = NEB(JL)
               !   CONSIDER ALL NEIGHBOURS OF J, EXCEPT I.
               IF( L  /=  I )THEN
                  JLC = JLC + 1
                  NEBOFJ(JLC) = L
                  KTYPL = KTYP(L)
                  BRETYPL = BRETYP(L)
                  JLBPOT = BNDTYPBRE(JL)
                  JLPOT = BNDTYP(JL)
                  RLJL  = BNDLEN(JL)
                  RXNJL = BNDXNM(JL)
                  RYNJL = BNDYNM(JL)
                  RZNJL = BNDZNM(JL)

                  FCJL   = CUTFCN(JL)
                  DFCJLR = CUTDRV(JL)

                  !   SUM THE NUMBER OF ATOMS ON J IF CARBON OR SILICON.
                  IF( KTYPJ  ==  1 .AND. BRETYPJ == 1 ) THEN  !Andrea

                     IF( KTYPL  <=  2 .AND. BRETYPL == 1 )THEN  !Andrea
                        !   L-CARBON OR SILICON
                        NCJ         = NCJ    + FCJL
                        NCJDXL(JLC) = RXNJL  * DFCJLR
                        NCJDYL(JLC) = RYNJL  * DFCJLR
                        NCJDZL(JLC) = RZNJL  * DFCJLR

                        NCJDXJ      = NCJDXJ - NCJDXL(JLC)
                        NCJDYJ      = NCJDYJ - NCJDYL(JLC)
                        NCJDZJ      = NCJDZJ - NCJDZL(JLC)
                        NHJDXL(JLC) = 0.0
                        NHJDYL(JLC) = 0.0
                        NHJDZL(JLC) = 0.0

                     ELSE
                        !   L-HYDROGEN
                        NHJ         = NHJ    + FCJL

                        NHJDXL(JLC) = RXNJL  * DFCJLR
                        NHJDYL(JLC) = RYNJL  * DFCJLR
                        NHJDZL(JLC) = RZNJL  * DFCJLR

                        NHJDXJ      = NHJDXJ - NHJDXL(JLC)
                        NHJDYJ      = NHJDYJ - NHJDYL(JLC)
                        NHJDZJ      = NHJDZJ - NHJDZL(JLC)

                        NCJDXL(JLC) = 0.0
                        NCJDYL(JLC) = 0.0
                        NCJDZL(JLC) = 0.0

                     ENDIF
                  ELSE IF( KTYPJ  ==  2 ) THEN
                     !   K-C, SI OR H
                     NTJDXL(JLC) = RXNJL  * DFCJLR
                     NTJDYL(JLC) = RYNJL  * DFCJLR
                     NTJDZL(JLC) = RZNJL  * DFCJLR

                     NTJDXJ      = NTJDXJ - NTJDXL(JLC)
                     NTJDYJ      = NTJDYJ - NTJDYL(JLC)
                     NTJDZJ      = NTJDZJ - NTJDZL(JLC)
                  ENDIF

                  !   SUM NijConj IF C-C BOND AND L  IS CARBON.
                  IF( IJPOT  ==  1 .AND. IJBPOT == 1 ) THEN   ! Andrea
                     IF( KTYPL  ==  1 .AND. BRETYPL == 1 ) THEN
                        XJL    = 0.0
                        XJLDLX = 0.0
                        XJLDLY = 0.0
                        XJLDLZ = 0.0

                        !     CONSIDER ALL NEBS OF L, EXCEPT J.

                        !     L--N LOOP.
                        LNC = 0
                        DO LN =DATUM(L),DATUM(L+1)-1
                           N = NEB(LN)
                           IF ( N  /=  J ) THEN
                              LNC = LNC + 1
                              NEBOFL(JLC,LNC) = N

                              FCLN   = CUTFCN(LN)
                              DFCLNR = CUTDRV(LN)

                              !     SUM Xik AND DERIVATIVES.
                              XJL         = XJL    + FCLN
                              XJLDNX(LNC) = DFCLNR * BNDXNM(LN)
                              XJLDNY(LNC) = DFCLNR * BNDYNM(LN)
                              XJLDNZ(LNC) = DFCLNR * BNDZNM(LN)

                              XJLDLX      = XJLDLX - XJLDNX(LNC)
                              XJLDLY      = XJLDLY - XJLDNY(LNC)
                              XJLDLZ      = XJLDLZ - XJLDNZ(LNC)

                           ENDIF
                           !     END OF L--N LOOP.
                        ENDDO
                        NUMNBL(JLC) = LNC

                        !     SUM F(Xik) AND DERIVATIVES.
                        IF     ( XJL  <=  2.0 ) THEN
                           FXJL   = 1.0
                           DFXJLX = 0.0
                        ELSE IF( XJL  >=  3.0 ) THEN
                           FXJL   = 0.0
                           DFXJLX = 0.0
                        ELSE
                           ARG    = PI  * ( XJL - 2.0 )
                           FXJL   = 0.5 * ( 1.0 + COS( ARG ) )
                           DFXJLX =-0.5 * PI * SIN( ARG )
                        ENDIF

                        NCNJDR = FXJL * DFCJLR
                        NCNJDX = FCJL * DFXJLX

                        NCONJ       = NCONJ  + FCJL * FXJL
                        NCNDXJ      = NCNDXJ - NCNJDR * RXNJL
                        NCNDYJ      = NCNDYJ - NCNJDR * RYNJL
                        NCNDZJ      = NCNDZJ - NCNJDR * RZNJL

                        NCNDXL(JLC) = NCNJDR * RXNJL + NCNJDX * XJLDLX
                        NCNDYL(JLC) = NCNJDR * RYNJL + NCNJDX * XJLDLY
                        NCNDZL(JLC) = NCNJDR * RZNJL + NCNJDX * XJLDLZ

                        DO LNC=1,NUMNBL(JLC)
                           NCNDXN(JLC,LNC) = NCNJDX *XJLDNX(LNC)
                           NCNDYN(JLC,LNC) = NCNJDX *XJLDNY(LNC)
                           NCNDZN(JLC,LNC) = NCNJDX *XJLDNZ(LNC)
                        ENDDO
                     ELSE
                        NUMNBL(JLC) = 0
                        NCNDXL(JLC) = 0.0
                        NCNDYL(JLC) = 0.0
                        NCNDZL(JLC) = 0.0
                     ENDIF
                  ENDIF

                  !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.
                  IF     (KTYPJ == 2 .AND. KTYPL == 3) THEN
                     MBTJIL = 5
                  ELSE IF(KTYPJ == 3 .AND. KTYPI == 2 .AND. KTYPL == 2) THEN
                     MBTJIL = 6
                  ELSE
                     MBTJIL = MBTJI
                  ENDIF

                  !   CALCULATE LENGTH DEPENDENT TERMS.
                  IF( ( KTYPJ == 1 .AND. (KTYPI /= 3 .OR.  KTYPL /= 3) )  &
                        .OR. ( KTYPJ == 2 .AND. ((IJPOT+JLPOT)  <=  6       ) )  &
                        .OR. ( KTYPJ == 3 .AND. BRETYPI /= 1 )   &
                        .OR. ( MBTJIL == 6 ) .OR. ( IJBPOT /= 1 ) ) THEN
                     !   CONSTANT  (ALSO FOR ALL BRETYPES  .ANDREA)
                     QFACAN = 1.0
                     QFADAN = 0.0
                  ELSE IF ( MBTJIL  ==  2 .OR. MBTJIL  ==  5 ) THEN
                     !   BETA = 3.0
                     !   EXP( ALPHA * {( Rij - Rjl )}^BETA ) & dEXP / d(Rij-Rjl).
                     DIFLEN = (RLIJ-CONRE(1,IJPOT)) - (RLJL-CONRE(1,JLPOT))
                     QFACAN = EXP( CONALP(MBTJIL) * DIFLEN * DIFLEN * DIFLEN )
                     QFADAN = CONALP(MBTJIL) * 3.0 * QFACAN * DIFLEN * DIFLEN
                  ELSE
                     !   BETA = 1.0
                     !   EXP( ALPHA * {( Rij - Rjl )}^BETA ) & dEXP / d(Rij-Rjl).
                     DIFLEN = (RLIJ-CONRE(IJBPOT,IJPOT)) - (RLJL-CONRE(JLBPOT,JLPOT))
                     QFACAN = EXP( CONALP(MBTJIL) * DIFLEN )
                     QFADAN = CONALP(MBTJIL) *  QFACAN
                  ENDIF

                  !   CALCULATE ANGLE DEPENDENT TERMS.
                  IF( MBTJIL  /=  3 .OR. IJBPOT /= 1)THEN
                     !   COS( THETAjil ), G( THETAjil ) & dG / d{H-COS( THETA )}
                     COSTH  = -(RXNJL  * RXNIJ  + RYNJL * RYNIJ  + RZNJL  * RZNIJ)
                     IF( MBTJIL == 1 .OR. MBTJIL == 4 .OR. MBTJIL == 3 ) THEN
                        CARG = COND2(JLBPOT,MBTJIL) + &
                            (CONH(JLBPOT,MBTJIL) - COSTH) * &
                            (CONH(JLBPOT,MBTJIL) - COSTH)
                        GFACAN = CONCD(JLBPOT,MBTJIL) - CONC2(JLBPOT,MBTJIL) / CARG
                        GDDAN = 2.0 * CONC2(JLBPOT,MBTJIL) * &
                            (CONH(JLBPOT,MBTJIL) - COSTH) / (CARG*CARG)
                     ELSE
                        CARG   = CONH(1,MBTJIL) - COSTH
                        GFACAN = CONC2(1,MBTJIL) + COND2(1,MBTJIL) * CARG*CARG
                        GDDAN  = 2.0 * COND2(1,MBTJIL) * CARG
                     ENDIF

                     !    DIRECTION COSINES OF Ril = ( Rjl - Rji ) / DISjl
                     DLXC  = RXNJL * RLJL + RXNIJ * RLIJ
                     DLYC  = RYNJL * RLJL + RYNIJ * RLIJ
                     DLZC  = RZNJL * RLJL + RZNIJ * RLIJ

                     DISIL = SQRT( DLXC*DLXC+DLYC*DLYC+DLZC*DLZC )
                     DLXC  = DLXC / DISIL
                     DLYC  = DLYC / DISIL
                     DLZC  = DLZC / DISIL


                     !   dCOS( THETAjil ) / dRwh [WHERE w=x,y,z AND h =j,i,l]
                     DCSDJI =  1.0 / RLJL - COSTH * RLIJR
                     DCSDJL =  RLIJR - COSTH / RLJL
                     DCSDIL = -DISIL * RLIJR / RLJL

                     DCSDXJ =  DCSDJI*RXNIJ - DCSDJL*RXNJL
                     DCSDYJ =  DCSDJI*RYNIJ - DCSDJL*RYNJL
                     DCSDZJ =  DCSDJI*RZNIJ - DCSDJL*RZNJL

                     DCSDXI = -DCSDJI*RXNIJ                - DCSDIL*DLXC
                     DCSDYI = -DCSDJI*RYNIJ                - DCSDIL*DLYC
                     DCSDZI = -DCSDJI*RZNIJ                - DCSDIL*DLZC

                     DCSDXL =                 DCSDJL*RXNJL + DCSDIL*DLXC
                     DCSDYL =                 DCSDJL*RYNJL + DCSDIL*DLYC
                     DCSDZL =                 DCSDJL*RZNJL + DCSDIL*DLZC


                     !   FCjl * EXP * dG( THETAjil ) / dCOS(THETAjil)
                     DZFAC = - FCJL * GDDAN * QFACAN

                     !   ADD DERIVATIVES OF H IF IT IS A SPLINE.
                     !   dETAji/dH = SUM [ FCjl * EXP * dG( THETAjil ) / dH ]
                     IF(MBTJIL == 5) DBJDH = DBJDH - DZFAC

                     !   FCjl * EXP * dG( THETAjil ) / dRwh [WHERE w=x,y,z AND h =j,i,l]

                     DGDXJ = DZFAC * DCSDXJ
                     DGDYJ = DZFAC * DCSDYJ
                     DGDZJ = DZFAC * DCSDZJ

                     DGDXI = DZFAC * DCSDXI
                     DGDYI = DZFAC * DCSDYI
                     DGDZI = DZFAC * DCSDZI

                     DGDXL = DZFAC * DCSDXL
                     DGDYL = DZFAC * DCSDYL
                     DGDZL = DZFAC * DCSDZL

                  ELSE
                     GFACAN = CONCD(1,3)

                     DGDXJ = 0.0
                     DGDYJ = 0.0
                     DGDZJ = 0.0

                     DGDXI = 0.0
                     DGDYI = 0.0
                     DGDZI = 0.0

                     DGDXL = 0.0
                     DGDYL = 0.0
                     DGDZL = 0.0

                  ENDIF

                  !   SUM ETAji
                  ZJI    = ZJI + FCJL * GFACAN * QFACAN

                  !   FCjl * G( THETAjil ) * dEXP / dRji
                  DZDRJI = GFACAN * FCJL * QFADAN

                  !   G( THETAjil ) * ( dFCjl / dRjl * EXP + FCjl * dEXP / dRjl )
                  DZDRJL = GFACAN * ( DFCJLR *QFACAN - FCJL *QFADAN )

                  !   SUM dETAji / dRwh [WHERE w=x,y,z AND h =j,i,l]
                  !   G * ( FCjl * dEXP/dRwj + dFCjl/dRwj * EXP ) + FCjl * EXP * dG/dRwj
                  DBJDXJ      =DBJDXJ +DZDRJI*RXNIJ -DZDRJL*RXNJL +DGDXJ
                  DBJDYJ      =DBJDYJ +DZDRJI*RYNIJ -DZDRJL*RYNJL +DGDYJ
                  DBJDZJ      =DBJDZJ +DZDRJI*RZNIJ -DZDRJL*RZNJL +DGDZJ

                  !   G * FCjl * dEXP/dRwi * FCjl * EXP * dG/dRwi
                  DBJDXI      =DBJDXI -DZDRJI*RXNIJ               +DGDXI
                  DBJDYI      =DBJDYI -DZDRJI*RYNIJ               +DGDYI
                  DBJDZI      =DBJDZI -DZDRJI*RZNIJ               +DGDZI

                  !   G * ( FCjl * dEXP/dRwl + dFCjl/dRwl * EXP ) + FCjl * EXP * dG/dRwl
                  DBJDXL(JLC) =                      DZDRJL*RXNJL +DGDXL
                  DBJDYL(JLC) =                      DZDRJL*RYNJL +DGDYL
                  DBJDZL(JLC) =                      DZDRJL*RZNJL +DGDZL

               ENDIF

               !   END OF J--L LOOP.
            ENDDO
            NUMNBJ = JLC

            !   ADD ON DERIVATIVES OF BOND ORDER TERM DUE TO SPLINE FOR H(NtSi).
            IF( KTYPJ  ==  2) THEN
               !   dETAji/dNtj = dETAji/dH * dH/dNtj
               DBJDNJ = DBJDH * DHDNTJ
               !   dETAji/dRwh = dETAji/dRwh + dETAji/dNtj * dNtj/dRwh
               DBJDXJ = DBJDXJ + DBJDNJ * NTJDXJ
               DBJDYJ = DBJDYJ + DBJDNJ * NTJDYJ
               DBJDZJ = DBJDZJ + DBJDNJ * NTJDZJ

               DBJDXI = DBJDXI + DBJDNJ * NTJDXI
               DBJDYI = DBJDYI + DBJDNJ * NTJDYI
               DBJDZI = DBJDZI + DBJDNJ * NTJDZI

               DO JLC = 1,NUMNBJ
                  DBJDXL(JLC) = DBJDXL(JLC) + DBJDNJ * NTJDXL(JLC)
                  DBJDYL(JLC) = DBJDYL(JLC) + DBJDNJ * NTJDYL(JLC)
                  DBJDZL(JLC) = DBJDZL(JLC) + DBJDNJ * NTJDZL(JLC)
               ENDDO
            ENDIF

            !   ADD ON Hji(Nhj,Ncj) IF THIS IS A C-C OR H-C BOND.
            IF( ( KTYPJ == 1 .AND. KTYPI /= 2 .AND. BRETYPI == 1 .AND. BRETYPJ == 1 ) &
                  .AND. NHJ < 4.0 .AND. NCJ < 4.0 )THEN
               IF( KTYPI  ==  1 )THEN
                  !   C-C BOND.
                  call interpolate_Hcc(NHJ,NCJ,HJI,DHDNHJ,DHDNCJ)

               ELSE
                  !   C-H BOND.
                  call interpolate_Hch(NHJ,NCJ,HJI,DHDNHJ,DHDNCJ)
               ENDIF

               !   ADD CONTRIBUTIONS TO ETAji AND ITS DERIVATIVES.
               ZJI    = ZJI    + HJI
               DBJDXJ = DBJDXJ + DHDNHJ * NHJDXJ + DHDNCJ * NCJDXJ
               DBJDYJ = DBJDYJ + DHDNHJ * NHJDYJ + DHDNCJ * NCJDYJ
               DBJDZJ = DBJDZJ + DHDNHJ * NHJDZJ + DHDNCJ * NCJDZJ


               !   ADD CONTRIBUTIONS FROM DERIVATIVES TO NEIGHBOURS OF J.
               DO JLC = 1,NUMNBJ
                  DBJDXL(JLC) = DBJDXL(JLC) +DHDNHJ*NHJDXL(JLC) +DHDNCJ*NCJDXL(JLC)
                  DBJDYL(JLC) = DBJDYL(JLC) +DHDNHJ*NHJDYL(JLC) +DHDNCJ*NCJDYL(JLC)
                  DBJDZL(JLC) = DBJDZL(JLC) +DHDNHJ*NHJDZL(JLC) +DHDNCJ*NCJDZL(JLC)
               ENDDO
            ENDIF

            !   Bij & 0.5 * FCij * FAij * dBij / dETAij
            IF( MBTIJ  /=  2 .AND. MBTIJ  /=  4 )  THEN
               !   ZIJ CAN BE NEGATIVE IN BRENNER, PLUS DON'T NEED Ni & Ni-1 (Ni=1).
               ARG = 1.0 + ZIJ
               BIJ = ARG ** CONPE(MBTIJ)
               DFBIJ = CONAN(MBTIJ) * FCIJ * FAIJ * ARG ** CONPF(MBTIJ)
            ELSE IF( ZIJ  <=  0.0 )  THEN
               BIJ = 1.0
               DFBIJ = 0.0
            ELSE
               ARG = 1.0 + ZIJ ** CONN(MBTIJ)
               BIJ = ARG ** CONPE(MBTIJ)
               DFBIJ = CONAN(MBTIJ) * FCIJ * FAIJ * ( ZIJ ** CONNM(MBTIJ) ) * ( ARG ** CONPF(MBTIJ) )
            ENDIF

            !   Bji & 0.5 * FCij * FAij * dBji / dETAji
            IF( MBTJI  /=  2 .AND. MBTJI  /=  4 )  THEN
               !   ZJI CAN BE NEGATIVE IN BRENNER, PLUS DON'T NEED Nj & Nj-1 (Nj=1).
               ARG = 1.0 + ZJI
               BJI = ARG ** CONPE(MBTJI)
               DFBJI = CONAN(MBTJI) * FCIJ * FAIJ * ARG ** CONPF(MBTJI)
            ELSE IF( ZJI  <=  0.0 )  THEN
               BJI = 1.0
               DFBJI = 0.0
            ELSE
               ARG = 1.0 + ZJI ** CONN(MBTJI)
               BJI = ARG ** CONPE(MBTJI)
               DFBJI = CONAN(MBTJI) * FCIJ * FAIJ * ( ZJI ** CONNM(MBTJI) ) * ( ARG ** CONPF(MBTJI) )
            ENDIF


            !   SCALE IF THIS IS A C-SI MIXED BOND.
            IF( IJPOT  ==  2 ) THEN
               BIJ    = BIJ   * CONCSI
               DFBIJ  = DFBIJ * CONCSI
               BJI    = BJI   * CONCSI
               DFBJI  = DFBJI * CONCSI
            ENDIF

            !   ADD ON Fij(Nti,Ntj,Nconj)/2 IF THIS IS A C-C BOND.
            NTI = NHI + NCI
            NTJ = NHJ + NCJ


            IF( IJPOT == 1 .AND. IJBPOT == 1 .AND. NTI < 4.0 .AND. NTJ < 4.0 )THEN !Andrea
               IF(NCONJ > 2.0)NCONJ = 2.0
               !print *,'ADDING CONJUGATION TERMS!!!'
               call interpolate_FCC(NTI,NTJ,NCONJ,FIJ,DFDNTI,DFDNTJ,DFDCNJ)

               DFDNTI = 0.5 * FCIJ * FAIJ * DFDNTI
               DFDNTJ = 0.5 * FCIJ * FAIJ * DFDNTJ
               DFDCNJ = 0.5 * FCIJ * FAIJ * DFDCNJ

               !   ADD CONTRIBUTION TO BOND ORDER TERM.
               BIJ = BIJ + FIJ

               !   ADD FORCES DUE TO Fcc.
               !   - ( 0.5 * FCij * FAij * dFcc/dRwi ).


               xnp(i3+1)=xnp(i3+1)-( DFDNTI *(NHIDXI+NCIDXI) +DFDCNJ*NCNDXI )
               xnp(i3+2)=xnp(i3+2)-( DFDNTI *(NHIDYI+NCIDYI) +DFDCNJ*NCNDYI )
               xnp(i3+3)=xnp(i3+3)-( DFDNTI *(NHIDZI+NCIDZI) +DFDCNJ*NCNDZI )

               !   - ( 0.5 * FCij * FAij * dFcc/dRwj ).
               xnp(j3+1)=xnp(j3+1)-( DFDNTJ *(NHJDXJ+NCJDXJ) +DFDCNJ*NCNDXJ )
               xnp(j3+2)=xnp(j3+2)-( DFDNTJ *(NHJDYJ+NCJDYJ) +DFDCNJ*NCNDYJ )
               xnp(j3+3)=xnp(j3+3)-( DFDNTJ *(NHJDZJ+NCJDZJ) +DFDCNJ*NCNDZJ )

               !   CALCULATE FORCES ON NEIGHBOURS OF I.
               DO IKC = 1,NUMNBI
                  K     = NEBOFI(IKC)
                  k3=k*3-3
                  !     - ( 0.5 * FCij * FAij * dFcc/dRwk ).
                  xnp(k3+1)= xnp(k3+1) -( DFDNTI *(NHIDXK(IKC)+NCIDXK(IKC)) + DFDCNJ * NCNDXK(IKC))
                  xnp(k3+2)= xnp(k3+2) -( DFDNTI *(NHIDYK(IKC)+NCIDYK(IKC)) + DFDCNJ * NCNDYK(IKC))
                  xnp(k3+3)= xnp(k3+3) -( DFDNTI *(NHIDZK(IKC)+NCIDZK(IKC)) + DFDCNJ * NCNDZK(IKC))

                  !   FORCES ON SECOND NEIGHBOURS DUE TO CONJUGATION.
                  DO KMC=1,NUMNBK(IKC)
                     M = NEBOFK(IKC,KMC)
                     m3=m*3-3
                     xnp(m3+1) = xnp(m3+1) - DFDCNJ*NCNDXM(IKC,KMC)
                     xnp(m3+2) = xnp(m3+2) - DFDCNJ*NCNDYM(IKC,KMC)
                     xnp(m3+3) = xnp(m3+3) - DFDCNJ*NCNDZM(IKC,KMC)
                  ENDDO
               ENDDO

               !   CALCULATE FORCES ON NEIGHBOURS OF J.
               DO JLC = 1,NUMNBJ
                  L     = NEBOFJ(JLC)
                  l3=l*3-3
                  !   - ( 0.5 * FCij * FAij * dFcc/dRwl  ).
                  xnp(l3+1)= xnp(l3+1) -( DFDNTJ *(NHJDXL(JLC)+NCJDXL(JLC)) + DFDCNJ * NCNDXL(JLC) )
                  xnp(l3+2)= xnp(l3+2) -( DFDNTJ *(NHJDYL(JLC)+NCJDYL(JLC)) + DFDCNJ * NCNDYL(JLC) )
                  xnp(l3+3)= xnp(l3+3) -( DFDNTJ *(NHJDZL(JLC)+NCJDZL(JLC)) + DFDCNJ * NCNDZL(JLC) )

                  !   FORCES ON SECOND NEIGHBOURS DUE TO CONJUGATION.
                  DO LNC=1,NUMNBL(JLC)
                     N = NEBOFL(JLC,LNC)
                     n3=n*3-3
                     xnp(n3+1) = xnp(n3+1) - DFDCNJ*NCNDXN(JLC,LNC)
                     xnp(n3+2) = xnp(n3+2) - DFDCNJ*NCNDYN(JLC,LNC)
                     xnp(n3+3) = xnp(n3+3) - DFDCNJ*NCNDZN(JLC,LNC)

                  ENDDO
               ENDDO
            ENDIF ! End of C-C bond if

            !   AVERAGE THE BOND ORDER TERMS FOR ATOMS I AND J.
            BAVEIJ = 0.5 * ( BIJ + BJI )

            !   NOW CALCULATE THE POTENTIAL ENERGIES AND FORCES FOR I AND J.
            VFAC   = FRIJ + BAVEIJ * FAIJ
            HLFVIJ = FCIJ * VFAC / 2.0
            Epair(I)  = Epair(I) + HLFVIJ
            Epair(J)  = Epair(J) + HLFVIJ

            !   dVij / dRij
            DFFAC=( DFRIJR + BAVEIJ * DFAIJR ) * FCIJ + DFCIJR * VFAC

            ! reppot_fermi here??. Yes, add term dF fc Vb and reppot terms!
            if (reppotcut > RLIJ) then
               ! Spline pair potential
               ! Note that there is a different sign convention
               ! for the forces !
               fermiVr=0.0d0
               fermidVr=0.0d0
               call reppot_fermi(RLIJ,fermiVr,fermidVr,fermib(IJBPOT,IJPOT), &
                  fermir(IJBPOT,IJPOT),itype,jtype,fermihelp,fermihelp2)
               if (abs(fermihelp-fermifcn(IJ)) > 1d-10 ) then
                  print *,'HORROR Fermi ERROR',fermihelp,fermifcn(IJ)
                  STOP 'Fermi'
               endif
               if (abs(fermihelp2-fermidrv(IJ)) > 1d-10 ) then
                  print *,'HORROR Fermi drv ERROR',fermihelp2,fermidrv(IJ)
                  STOP 'Fermi drv'
               endif

               ! Add reppot and reppot deriv to ordinary forces

               Epair(I)  = Epair(I) + fermiVr/2.0d0
               Epair(J)  = Epair(J) + fermiVr/2.0d0

               xnp(i3+1)=xnp(i3+1) + fermidVr*RXNIJ
               xnp(i3+2)=xnp(i3+2) + fermidVr*RYNIJ
               xnp(i3+3)=xnp(i3+3) + fermidVr*RZNIJ

               xnp(j3+1)=xnp(j3+1) - fermidVr*RXNIJ
               xnp(j3+2)=xnp(j3+2) - fermidVr*RYNIJ
               xnp(j3+3)=xnp(j3+3) - fermidVr*RZNIJ

               ! Add dFermi force term

               fermihelp=fermidrv(IJ)*VFAC*FCIJ

               xnp(i3+1)=xnp(i3+1) + fermihelp*RXNIJ
               xnp(i3+2)=xnp(i3+2) + fermihelp*RYNIJ
               xnp(i3+3)=xnp(i3+3) + fermihelp*RZNIJ

               xnp(j3+1)=xnp(j3+1) - fermihelp*RXNIJ
               xnp(j3+2)=xnp(j3+2) - fermihelp*RYNIJ
               xnp(j3+3)=xnp(j3+3) - fermihelp*RZNIJ

            endif

            !   - ( dVij / dRwi + 0.5 * FCij * FAij * ( dBij / dRwi + dBji / dRwi )
            xnp(i3+1)=xnp(i3+1) -( -DFFAC*RXNIJ  + DFBIJ*DBIDXI + DFBJI*DBJDXI )
            xnp(i3+2)=xnp(i3+2) -( -DFFAC*RYNIJ  + DFBIJ*DBIDYI + DFBJI*DBJDYI )
            xnp(i3+3)=xnp(i3+3) -( -DFFAC*RZNIJ  + DFBIJ*DBIDZI + DFBJI*DBJDZI )

            !   - ( dVij / dRwj + 0.5 * FCij * FAij * ( dBij / dRwj + dBji / dRwj )
            xnp(j3+1)=xnp(j3+1) -( DFFAC*RXNIJ + DFBIJ*DBIDXJ + DFBJI*DBJDXJ )
            xnp(j3+2)=xnp(j3+2) -( DFFAC*RYNIJ + DFBIJ*DBIDYJ + DFBJI*DBJDYJ )
            xnp(j3+3)=xnp(j3+3) -( DFFAC*RZNIJ + DFBIJ*DBIDZJ + DFBJI*DBJDZJ )

            !   CALCULATE FORCES ON NEIGHBOURS OF I.
            DO IKC = 1,NUMNBI
               K = NEBOFI(IKC)
               k3=k*3-3
               !   - ( 0.5 * FCij * FAij * dBij / dRwk ).
               xnp(k3+1)= xnp(k3+1) - DFBIJ * DBIDXK(IKC)
               xnp(k3+2)= xnp(k3+2) - DFBIJ * DBIDYK(IKC)
               xnp(k3+3)= xnp(k3+3) - DFBIJ * DBIDZK(IKC)
            ENDDO


            !   CALCULATE FORCES ON NEIGHBOURS OF J.
            DO JLC = 1,NUMNBJ
               L = NEBOFJ(JLC)
               l3=l*3-3
               !   - ( 0.5 * FCij * FAij * dBji / dRwl ).
               xnp(l3+1)= xnp(l3+1) - DFBJI * DBJDXL(JLC)
               xnp(l3+2)= xnp(l3+2) - DFBJI * DBJDYL(JLC)
               xnp(l3+3)= xnp(l3+3) - DFBJI * DBJDZL(JLC)
            ENDDO

            !   ADD ON FORCES DUE TO SPLINES IN SI-H MORSE TERMS.
            IF( IJPOT  ==  5 ) THEN
               !   FCij * ( dFRij/dNtSi + Bij*dFAij/dNtSi ).
               ! This was a bug!! ------------->|---->|------->|----------->|
               !              VFACDN = FCIJ*( DF2DNT*FASIH + DF1DNT*BAVEIJ*FRSIH )
               VFACDN = FCIJ*( DF1DNT*FRSIH + DF2DNT*BAVEIJ*FASIH )
               IF( KTYPI  ==  2 ) THEN
                  !     CALCULATE FORCES ON I AND NEIGHBOURS.
                  !     - FCij * ( dFRij/dRwi + Bij*dFAij/dRwi ).
                  xnp(i3+1)= xnp(i3+1) - VFACDN * NTIDXI
                  xnp(i3+2)= xnp(i3+2) - VFACDN * NTIDYI
                  xnp(i3+3)= xnp(i3+3) - VFACDN * NTIDZI

                  xnp(j3+1)= xnp(j3+1) - VFACDN * NTIDXJ
                  xnp(j3+2)= xnp(j3+2) - VFACDN * NTIDYJ
                  xnp(j3+3)= xnp(j3+3) - VFACDN * NTIDZJ

                  DO IKC = 1,NUMNBI
                     K = NEBOFI(IKC)
                     k3=k*3-3
                     !     - FCij * ( dFRij/dRwk + Bij*dFAij/dRwk ).
                     xnp(k3+1)= xnp(k3+1) - VFACDN * NTIDXK(IKC)
                     xnp(k3+2)= xnp(k3+2) - VFACDN * NTIDYK(IKC)
                     xnp(k3+3)= xnp(k3+3) - VFACDN * NTIDZK(IKC)
                  ENDDO
               ELSE
                  !     CALCULATE FORCES ON J AND NEIGHBOURS.
                  !     - FCij * ( dFRij/dRwj + Bij*dFAij/dRwj ).
                  xnp(j3+1)= xnp(j3+1) - VFACDN * NTJDXJ
                  xnp(j3+2)= xnp(j3+2) - VFACDN * NTJDYJ
                  xnp(j3+3)= xnp(j3+3) - VFACDN * NTJDZJ

                  xnp(i3+1)= xnp(i3+1) - VFACDN * NTJDXI
                  xnp(i3+2)= xnp(i3+2) - VFACDN * NTJDYI
                  xnp(i3+3)= xnp(i3+3) - VFACDN * NTJDZI
                  DO JLC = 1,NUMNBJ
                     L = NEBOFJ(JLC)
                     l3=l*3-3
                     !     - FCij * ( dFRij/dRwl + Bij*dFAij/dRwl ).
                     xnp(l3+1)= xnp(l3+1) - VFACDN * NTJDXL(JLC)
                     xnp(l3+2)= xnp(l3+2) - VFACDN * NTJDYL(JLC)
                     xnp(l3+3)= xnp(l3+3) - VFACDN * NTJDZL(JLC)
                  ENDDO
               ENDIF
            ENDIF

         ENDIF
         !   END OF I==J LOOP.
      ENDDO ! End of loop over J

   ENDDO ! End of loop over I

   ! Convert from eV/A to internal units.
   do i = 1, 3*np0pairtable, 3
       xnp(i+0) = xnp(i+0) / box(1)
       xnp(i+1) = xnp(i+1) / box(2)
       xnp(i+2) = xnp(i+2) / box(3)
   end do

   ! Pass back contributions to forces and energies of other
   ! nodes' atoms.
   if (nprocs > 1) then
       t1 = mpi_wtime()
       call potential_pass_back_border_atoms(xnp, Epair)
       tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
   end if


   RETURN
END


    !#######################################################################
    !
    !   Cicubic interpolation of Hch.
    !   Assumes 0.0 <= Nhi,Nci < 4.0
    !   Copyright: Keith Beardmore 02/12/93.
    !
    subroutine interpolate_Hch(Nhi, Nci, Hch, dHchdH, dHchdC)

        real(real64b), intent(in) :: Nhi, Nci
        real(real64b), intent(out) :: Hch, dHchdH, dHchdC

        real(real64b) :: x1, x2, sHch, sHchdC, coefij
        integer :: nHbox, nCbox, ibox, i, j

        nHbox = int(Nhi)
        nCbox = int(Nci)

        ! Find which box we're in and convert to normalised coordinates.
        ibox = 1 + 4*nHbox + nCbox
        x1 = Nhi - nHbox
        x2 = Nci - nCbox

        Hch = 0.0
        dHchdH = 0.0
        dHchdC = 0.0
        do i = 4, 1, -1
            sHch = 0.0
            sHchDC = 0.0
            do j = 4, 1, -1
                coefij = HCHCF(ibox, I, J)
                sHch = sHch * x2 + coefij
                if (j > 1) sHchdC = sHchdC * x2 + (j-1) * coefij
            end do
            Hch = Hch * x1 + sHch
            if (i > 1) dHchdH = dHchdH * x1 + (i-1) * sHch
            dHchdC = dHchdC * x1 + sHchDC
        end do

    end subroutine interpolate_Hch


    !#######################################################################
    !
    !   Cicubic interpolation of Hcc.
    !   Assumes 0.0 <= Nhi,Nci < 4.0
    !   Copyright: Keith Beardmore 02/12/93.
    !
    subroutine interpolate_Hcc(Nhi, Nci, Hcc, dHccdH, dHccdC)

        real(real64b), intent(in) :: Nhi, Nci
        real(real64b), intent(out) :: Hcc, dHccdH, dHccdC

        real(real64b) :: x1, x2, sHcc, sHccdC, coefij
        integer :: nHbox, nCbox, ibox, i, j

        nHbox = int(Nhi)
        nCbox = int(Nci)

        ! Find which box we're in and convert to normalised coordinates.
        ibox = 1 + 4*nHbox + nCbox
        x1 = Nhi - nHbox
        x2 = Nci - nCbox

        Hcc = 0.0
        dHccdH = 0.0
        dHccdC = 0.0
        do i = 4, 1, -1
            sHcc = 0.0
            sHccDC = 0.0
            do j = 4, 1, -1
                coefij = HCCCF(ibox, I, J)
                sHcc = sHcc * x2 + coefij
                if (j > 1) sHccdC = sHccdC * x2 + (j-1) * coefij
            end do
            Hcc = Hcc * x1 + sHcc
            if (i > 1) dHccdH = dHccdH * x1 + (i-1) * sHcc
            dHccdC = dHccdC * x1 + sHccDC
        end do

    end subroutine interpolate_Hcc


    !#######################################################################
    !
    !   Tricubic interpolation of Fcc.
    !   Assumes 0.0 <= Nti,Ntj < 4.0 and 1 <= Nconj <=2
    !   Copyright: Keith Beardmore 02/12/93.
    !
    subroutine interpolate_Fcc(Nti,Ntj,Nconj,Fcc,dFccdi,dFccdj,dFccdC)

        real(real64b), intent(in) :: Nti, Ntj, Nconj
        real(real64b), intent(out) :: Fcc, dFccdi, dFccdj, dFccdC

        real(real64b) :: x1, x2, x3
        real(real64b) :: sFcc,sFccdj,sFccdC,tFcc,tFccdC,coefij
        integer :: nibox, njbox, ibox, i, j, k

        nibox = int(nti)
        njbox = int(ntj)

        ! Find which box we're in and convert to normalised coordinates.
        ibox = 1 + 4*nibox + njbox
        x1 = Nti   - nibox
        x2 = Ntj   - njbox
        x3 = Nconj - 1

        Fcc = 0.0
        dFccdi = 0.0
        dFccdj = 0.0
        dFccdC = 0.0
        do i = 4, 1, -1
            sFcc   = 0.0
            sFccdj = 0.0
            sFccdC = 0.0
            do j = 4, 1, -1
                tFcc   = 0.0
                tFccdC = 0.0
                do k = 4, 1, -1
                    coefij = FCCCF(ibox,i,j,k)
                    tFcc   =   tFcc * x3 + coefij
                    if (K > 1) tFccdC = tFccdC * x3 + (k-1) * coefij
                end do
                sFcc = sFcc *x2 + tFcc
                if(j > 1) sFccdj = sFccdj * x2+ (j-1) * tFcc
                sFccdC = sFccdC * x2 + tFccdC
            end do
            Fcc    = Fcc * x1 + sFcc
            if (i > 1) dFccdi = dFccdi * x1 + (i-1) * sFcc
            dFccdj = dFccdj * x1 + sFccdj
            dFccdc = dFccdc * x1 + sFccdc
        end do

    end subroutine interpolate_Fcc


    !#######################################################################
    !
    !   Cubic spline interpolation of F1(NtSi) & F2(NtSi).
    !   Copyright: Keith Beardmore 31/08/94.
    !
    subroutine interpolate_F1_F2(NtSi_spl, F1, F2, dF1dNT, dF2dNt)

        real(real64b), intent(in) :: NtSi_spl
        real(real64b), intent(out) :: F1, F2, dF1dNt, dF2dNt

        real(real64b) :: xi
        integer :: interv, i

        ! Find interval and convert to normalised coordinates.
        if (NtSi_spl >= 4.0) then
            interv = 3
            xi = 1.0
        else if (NtSi_spl > 1.0) then
            interv = int(NtSi_spl)
            xi = NtSi_spl - interv
        else
            interv = 1
            xi = 0.0
        end if

        F1 = AF1(interv, 4)
        F2 = AF2(interv, 4)
        dF1dNt = 0.0
        dF2dNt = 0.0
        do i = 3, 1, -1
            dF1dNt = dF1dNt * xi + F1
            dF2dNt = dF2dNt * xi + F2
            F1     = F1     * xi + AF1(interv,i)
            F2     = F2     * xi + AF2(interv,i)
        end do

    end subroutine interpolate_F1_F2


    !#######################################################################
    !
    !   Cubic spline interpolation of H(NtSi).
    !   Copyright: Keith Beardmore 31/08/94.
    !
    subroutine interpolate_HSi(NtSi_spl, H, dHdNt)

        real(real64b), intent(in) :: NtSi_spl
        real(real64b), intent(out) :: H, dHdNt

        real(real64b) :: xi
        integer :: interv, i

        ! Find interval and convert to normalised coordinates.
        if (NtSi_spl >= 4.0) then
            interv = 3
            xi = 1.0
        else if (NtSi_spl > 1.0) then
            interv = int(NtSi_spl)
            xi = NtSi_spl - interv
        else
            interv = 1
            xi = 0.0
        end if

        H = AH(interv, 4)
        dHdNt = 0.0
        do i = 3, 1, -1
            dHdNt = dHdNt * xi + H
            H     = H     * xi + AH(interv, I)
        end do

    end subroutine interpolate_HSi


    !#######################################################################
    !
    !   Calculates the values of the constants used in C/Si/H potential.
    !   Copyright: Keith Beardmore 31/08/94.
    !   A combination of:
    !   Donald. Brenners's hydrocarbon potential (1st parameterisation).
    !   Parameters from: Phys. Rev. B 42, 9458-9471(1990).
    !   Plus corrections: Phys. Rev. B 46, 1948(1990).
    !   J. Tersoff's multicomponent potential for Si - C systems.
    !   Parameters from: Phys. Rev. B 39, 5566-5568(1989).
    !        and errata: Phys. Rev. B 41, 3248     (1990).
    !   R. Murty & H. Atwater's Si-H potential.
    !   Parameters from: paper presented at COSIRES 1994.
    !
    !
    subroutine gen_brenner_beardmore_params(R1CCin, R2CCin)

        real(real64b), intent(in) :: R1CCin, R2CCin

        integer :: i, j


        ! Additions by Kai N: Rasmol covalent radii. Same as in brenner.h.
        ! Covalent radii atom types: 1 C 2 H 3 Si
        COVALRAD(:,:) = 0.0d0

        COVALRAD(1,1) = 2.0d0
        COVALRAD(2,2) = 1.2d0
        COVALRAD(1,2) = 1.6d0
        COVALRAD(1,3) = 2.48d0
        COVALRAD(2,3) = 2.08d0
        COVALRAD(3,3) = 2.96d0

        do i = 1, 4
            do j = i, 4
                if (iprint) then
                    call logger_clear_buffer()
                    call logger_append_buffer("Using covalent radii:")
                    call logger_append_buffer("typei", i, 4)
                    call logger_append_buffer("typej", j, 4)
                    call logger_append_buffer("r    ", COVALRAD(i, j), 4)
                    call logger_write(trim(log_buf))
                end if
                COVALRAD(i,j) = COVALRAD(i,j)**2
                COVALRAD(j,i) = COVALRAD(i,j)
            end do
        end do


        ! Generate Brenner parameters.
        call gen_CH_params(R1CCin, R2CCin)

        ! Generate Tersoff parameters.
        call gen_CSi_params()

        ! Generate Murty/Atwater parameters.
        call gen_SiH_params()

        ! Set up ZBL-universal for noble (e.g. Ar) interactions.
        call ZBL_params()

    end subroutine gen_brenner_beardmore_params


    !#######################################################################
    !
    !   Calculates the values of constants used in C/Si/H potential.
    !   Copyright: Keith Beardmore 31/08/94.
    !   Brenner #1 for C-C, C-H and H-H bonds.
    !   Donald Brenners's hydrocarbon potential (1st parameterisation) *** see below ***.
    !   Parameters from: Phys. Rev. B 42, 9458-9471(1990).
    !   Plus corrections: Phys. Rev. B 46, 1948(1990).
    !
    subroutine gen_CH_params(R1CCin, R2CCin)

        real(real64b), intent(in) :: R1CCin, R2CCin

        !
        !    **** 20.4.99 Modified to Brenner II ****
        !

        REAL(kind=real64b) :: RECC(BCOMBS),REHH,RECH(BTYPES),DECC(BCOMBS),DEHH,DECH(BTYPES)
        REAL(kind=real64b) :: betaCC(BCOMBS),betaHH,betaCH(BTYPES),SCC(BCOMBS),SHH,SCH(BTYPES)
        REAL(kind=real64b) :: deltaC,deltaH,alphaCC(BCOMBS),alphaHH,alphaCH(BTYPES)
        REAL(kind=real64b) :: R1CC(BCOMBS),R1CH(BTYPES),R1HH,R2CC(BCOMBS),R2CH(BTYPES),R2HH
        REAL(kind=real64b) :: A0(BCOMBS),C02(BCOMBS),D02(BCOMBS),GHH

        ! Values for the parameters from table II.
        DATA ReCC(1)   , ReHH   , ReCH(1)   / 1.39       , 0.74144 , 1.1199  /
        DATA DeCC(1)   , DeHH   , DeCH(1)   / 6.0        , 4.7509  , 3.6422  /
        DATA betaCC(1) , betaHH , betaCH(1) / 2.1        , 1.9436  , 1.9583  /
        DATA SCC(1)    , SHH    , SCH(1)    / 1.22       , 2.3432  , 1.69077 /
        DATA deltaC    , deltaH             / 0.5        , 0.5               /
        DATA alphaCC(1), alphaHH, alphaCH(1)/ 0.0        , 4.0     , 4.0     /
        DATA             GHH                /              12.33             /
        DATA R1CC(1)   , R1HH   , R1CH(1)   / 1.70       , 1.1     , 1.3     /
        DATA R2CC(1)   , R2HH   , R2CH(1)   / 2.00       , 1.7     , 1.8     /
        DATA A0(1)                          / 0.00020813                     /
        DATA C02(1)                         / 108900.0                       /
        DATA D02(1)                         / 12.25                          /

        !*************************************************************************************
        !if (potmode > 19) then

        ! Values for Caros Be-C-H system (potmode 83 in parcas)   Andrea

        ! IJBPOT = 3: Be-Be II
        ! IJBPOT = 4: Be-C

        RECC(3) = 2.078799227759839d0
        DECC(3) = 1.035711767714829d0
        BETACC(3) = 1.300000000000000d0
        SCC(3) = 1.889782892624960d0
        alphaCC(3) = 0.0d0
        ! trcut(i,j)         = 2.535000000000000d0
        ! dcut(i,j)          = 0.150000000000000d0
        R1CC(3) = 2.385000000000000d0  ! = TRCUT - DCUT in Tersoff_compound
        R2CC(3) = 2.685000000000000d0  ! = TRCUT + DCUT in Tersoff_compound
        A0(3) = 0.000000819587260d0
        C02(3) = 7990.4620160892
        D02(3) = 0.0753112522519187
        CONH(3,1) = -0.760693434073536d0   ! N.B. Opposite sign from Tersoff_compound

        RECC(4) = 1.724298966530114d0
        DECC(4) = 3.909329821145575d0
        BETACC(4) = 1.586760903647416d0
        SCC(4) = 2.766724347332735d0
        alphaCC(4) = 0.0d0
        R1CC(4) = 2.400000000000000d0  ! = TRCUT - DCUT in Tersoff_compound
        R2CC(4) = 2.800000000000000d0  ! = TRCUT + DCUT in Tersoff_compound
        A0(4) = 0.000030018449777d0
        C02(4) = 3249.466702990
        D02(4) = 0.128381621074000d0
        CONH(4,1) = -0.559996014890212d0     ! N.B. Opposite sign from Tersoff_compound

        ! IJBPOT = 2: Be-H
        RECH(2) = 1.338000000000000d0
        DECH(2) = 2.60000000000000d0
        BETACH(2) = 2.20000000000000d0
        SCH(2) = 2.50000000d0
        alphaCH(2) = 0.0d0
        ! trcut(i,j) = 1.80000000000000d0
        ! dcut(i,j) =  0.15000000000000d0
        R1CH(2) = 1.65000000000000d0   ! = TRCUT - DCUT in Tersoff_compound
        R2CH(2) = 1.95000000000000d0   ! = TRCUT + DCUT in Tersoff_compound
        !   A0(-) = 0.19000000000d0
        !   C02(-) = 0.00003249
        !   D02(-) = 0.000016
        CONCD(2,3) = 0.575818750000000d0 ! A0(:) * ( 1.0 + C02(:) / D02(:) )
        CONC2(2,3) = 0.000006173100000d0 ! A0(:) * C02(:)
        COND2(2,3) = 0.000016000000000d0 ! D02(:)
        CONH(2,3) = -1

        !endif
        !**************************************************************************

        ! Overwrite with read-in parameters
        R1CC(1) = R1CCin
        R2CC(1) = R2CCin

        ! Report possibly modified parameters
        print *, 'BrennerBeardmore using R1CC', R1CC
        print *, 'BrennerBeardmore using R2CC', R2CC

        ! Morse term constants (for C-C, C-H and H-H bonds).
        CONER(:,1) = -SQRT(2.0*SCC(:)) * betaCC(:)
        CONER(1:BTYPES,3) = -SQRT(2.0*SCH(:)) * betaCH(:)
        CONER(1,6) = -SQRT(2.0*SHH) * betaHH
        where (SCC(:) /= 0d0)
            CONEA(:,1) = -SQRT(2.0/SCC(:)) * betaCC(:)
        end where
        CONEA(1:BTYPES,3) = -SQRT(2.0/SCH(:)) * betaCH(:)
        CONEA(1,6) = -SQRT(2.0/SHH) * betaHH
        CONFR(:,1) =  ( DECC(:)     / (SCC(:)-1.0) ) * EXP( -CONER(:,1) * RECC(:) )
        CONFR(1:BTYPES,3) = (DECH(:) / (SCH(:)-1.0) ) * EXP( -CONER(1:BTYPES,3) * RECH(:) )
        CONFR(1,6) =  ( DEHH     / (SHH-1.0) ) * EXP( -CONER(1,6) * REHH )
        CONFA(:,1) = -( DECC(:)*SCC(:) / (SCC(:)-1.0) ) * EXP( -CONEA(:,1) * RECC(:) )
        CONFA(1:BTYPES,3) = -(DECH(:)*SCH(:) / (SCH(:)-1.0))*EXP(-CONEA(1:BTYPES,3)*RECH(:) )
        CONFA(1,6) = -( DEHH*SHH / (SHH-1.0) ) * EXP( -CONEA(1,6) * REHH )
        CONRE(:,1) = RECC(:)
        CONRE(1:BTYPES,3) = RECH(:)
        CONRE(1,6) = REHH

        ! Cutoff constants (for C-C, C-H and H-H bonds).
        CONRL(:,1)  = R1CC(:)
        CONRL(1:BTYPES,3)  = R1CH(:)
        CONRL(1,6)  = R1HH
        CONRH2(:,1) = R2CC(:)** 2
        CONRH2(1:BTYPES,3) = R2CH(:) ** 2
        CONRH2(1,6) = R2HH ** 2
        CONFCA(:,1) = PI / ( R2CC(:) - R1CC(:) )
        CONFCA(1:BTYPES,3) = PI / ( R2CH(:) - R1CH(:) )
        CONFCA(1,6) = PI / ( R2HH - R1HH )
        CONFC(:,1)  = - 0.5 * CONFCA(:,1)
        CONFC(1:BTYPES,3)  = - 0.5 * CONFCA(1:BTYPES,3)
        CONFC(1,6)  = - 0.5 * CONFCA(1,6)

        ! Fermi join constants (taken from parcas WCH potential). Used if reppotcut>0
        !C-C
        fermib(1,1) = 8.0d0
        fermir(1,1) = 0.6d0
        !Be-Be
        fermib(3,1) = 15.0d0
        fermir(3,1) = 0.8d0
        !Be-C
        fermib(4,1) = 16.0d0
        fermir(4,1) = 0.7d0
        !C-H
        fermib(1,3) = 10.0d0
        !fermir(3,3) = 0.5d0 what is this??  Andrea
        fermir(1,3) = 0.5d0
        !H-Be
        fermib(2,3) = 15.0d0
        !fermir(3,1) = 0.8d0  what is this??  Andrea
        fermir(2,3) = 0.8d0
        !H-H
        fermib(1,6) = 15.0d0
        !fermir(3,6) = 0.35d0  what is this??  Andrea
        fermir(1,6) = 0.35d0

        ! Bond order constants (for C centres on C-[C or H] bonds
        !                   and for H centres on H-[C or H] bonds).
        CONN(1)  = 1.0
        CONN(3)  = 1.0
        CONPE(1) = - deltaC
        CONPE(3) = - deltaH
        CONAN(1) = 0.5 * CONPE(1)
        CONAN(3) = 0.5 * CONPE(3)
        CONNM(1) = 0.0
        CONNM(3) = 0.0
        CONPF(1) = CONPE(1) - 1.0
        CONPF(3) = CONPE(3) - 1.0

        ! Bond angle constants (for C centres on C-[C or H] bonds
        !                   and for H centres on H-[C or H] bonds).
        where (D02(:) /= 0d0)
            CONCD(:,1) = A0(:) * ( 1.0 + C02(:) / D02(:) )
        end where
        CONCD(2,1) = CONCD(2,3) ! for Be-H
        CONCD(1,3) = GHH
        COND2(:,1)  = D02(:)
        COND2(2,1) = COND2(2,3)! for Be-H
        COND2(1,3)  = 1.0
        CONC2(:,1)  = A0(:) * C02(:)
        CONC2(2,1) = CONC2(2,3) ! for Be-H
        CONC2(1,3)  = 0.0d0
        CONH(1,1)  = -1.0
        CONH(2,1)  = -1.0
        CONH(1,3)  = -1.0

        ! Bond length constants (for C centres on C-[C or H] bonds
        !                    and for H centres on H-[C or H] bonds).
        CONALP(1)   = alphaCH(1)
        CONALP(3)   = alphaHH
        CONBET(1)   = 1.0
        CONBET(3)   = 1.0

        ! Generate the coefficients for the bi- and tri-
        ! cubic interpolation functions.
        call gen_hch_interpolation()
        call gen_hcc_interpolation()
        call gen_fcc_interpolation()

    end subroutine gen_CH_params


    !#######################################################################
    !
    !   Calculates the values of constants used in C/Si/H potential.
    !   Copyright: Keith Beardmore 31/08/94.
    !   Tersoff Si/C for C-Si bonds.
    !   J. Tersoff's multicomponent potential for Si - C systems.
    !   Parameters from: Phys. Rev. B 39, 5566-5568(1989).
    !
    subroutine gen_CSi_params()

        real(real64b) :: acc,acsi,asisi,bcc,bcsi,bsisi
        real(real64b) :: lmcc,lmcsi,lmsisi,mucc,mucsi,musisi
        real(real64b) :: betac,betasi,nc,nsi,cc,csi,dc,dsi,hc,hsi
        real(real64b) :: rcc,rcsi,rsisi,scc,scsi,ssisi
        real(real64b) :: xcsi,recsi

        ! Values for the parameters from table 1.
        DATA aCC   , aSiSi  /  1.3936E+3 ,  1.8308E+3 /
        DATA bCC   , bSiSi  /  3.4674E+2 ,  4.7118E+2 /
        DATA lmCC  , lmSiSi /  3.4879    ,  2.4799    /
        DATA muCC  , muSiSi /  2.2119    ,  1.7322    /
        DATA betaC , betaSi /  1.5724E-7 ,  1.1000E-6 /
        DATA nC    , nSi    /  7.2751E-1 ,  7.8734E-1 /
        DATA cC    , cSi    /  3.8049E+4 ,  1.0039E+5 /
        DATA dC    , dSi    /  4.3484E+0 ,  1.6217E+1 /
        DATA hC    , hSi    / -5.7058E-1 , -5.9825E-1 /
        DATA rCC   , rSiSi  /  1.8       ,  2.7       /
        DATA sCC   , sSiSi  /  2.1       ,  3.0       /
        DATA xCSi           /  0.9776                 /
        DATA reCSi          /  1.85                   /

        ! Interpolation for mixed bond constants.
        aCSi  = ( aCC *  aSiSi)**0.5
        bCSi  = ( bCC *  bSiSi)**0.5
        lmCSi = (lmCC + lmSiSi)*0.5
        muCSi = (muCC + muSiSi)*0.5
        rCSi  = ( rCC *  rSiSi)**0.5
        sCSi  = ( sCC *  sSiSi)**0.5

        ! Morse term constants (for C-Si bonds).
        CONFR(1,2) =   aCSi
        CONFA(1,2) =  -bCSi
        CONER(1,2) = -lmCSi
        CONEA(1,2) = -muCSi
        CONRE(1,2) =  reCSi

        ! Cutoff constants (for C-Si bonds).
        CONRL(1,2)  = rCSi
        CONRH2(1,2) = sCSi**2
        CONFCA(1,2) = PI / (sCSi - rCSi)
        CONFC(1,2)  = - 0.5 * CONFCA(1,2)

        ! Fermi join constants (taken from parcas Si and C potentials).
        ! Used if reppotcut > 0.
        fermib(1,2) = sqrt(12.0+8.0)
        fermir(1,2) = (1.6d0 + 0.6d0) / 2.0d0


        ! Bond order constants (for C centres on C-Si bonds).
        CONN(4)  = nC
        CONPE(4) = -1.0 / (2.0 * CONN(4))
        CONAN(4) = 0.5 * CONN(4) * CONPE(4)
        CONNM(4) = CONN(4)  - 1.0
        CONPF(4) = CONPE(4) - 1.0

        ! Bond angle constants (for C centres on C-Si bonds).
        CONCD(1,4) = betaC * (1.0 + ((cC  ** 2) / (dC  ** 2)))
        COND2(1,4) = dC**2
        CONC2(1,4) = betaC * (cC**2)
        CONH(1,4)  = hC

        ! Bond length constants (for C centres on C-Si bonds).
        CONALP(4) = 0.0
        CONBET(4) = 1.0

        ! Mixed bond constant.
        CONCSI    = xCSI

    end subroutine gen_CSi_params


    !#######################################################################
    !
    !   Calculates the values of constants used in C/Si/H potential.
    !   Copyright: Keith Beardmore 31/08/94.
    !   Murty & Atwater Si/H for Si-Si and Si-H bonds.
    !   R. Murty & H. Atwater's Si-H potential.
    !   Parameters from: paper presented at COSIRES 1994.
    !
    subroutine gen_SiH_params()

        real(real64b) :: aSiSi,aSiH,bSiSi,bSiH
        real(real64b) :: lmSiSi,lmSiH,muSiSi,muSiH
        real(real64b) :: alphaSi,alphaH,alphaHb,betaSi,betaH,betaHb
        real(real64b) :: reSiSi,reSiH,cSi,cH,cHb,dSi,dH,dHb,HSi,HH,HHb
        real(real64b) :: rSiSi,rSiH,dSiSi,dSiH
        real(real64b) :: nSi,nH,deltSi,deltH

        ! Values for the parameters from table 1.
        data aSiSi  , aSiH           /  1830.8  ,  323.54         /
        data bSiSi  , bSiH           /  471.18  ,  84.18          /
        data lmSiSi , lmSiH          /  2.4799  ,  2.9595         /
        data muSiSi , muSiH          /  1.7322  ,  1.6158         /
        data alphaSi, alphaH, alphaHb/  5.1975  ,  4.0000 ,  0.00 /
        data betaSi , betaH , betaHb /  3       ,  3      ,  1    /
        data reSiSi , reSiH          /  2.35    ,  1.475          /
        data cSi    , cH    , cHb    /  0.0     ,  0.0216 ,  0.70 /
        data dSi    , dH    , dHb    /  0.160   ,  0.27   ,  1.00 /
        data HSi    , HH    , HHb    / -0.59826 ,  0.0    , -1.00 /
        data rSiSi  , rSiH           /  2.85    ,  1.85           /
        data dSiSi  , dSiH           /  0.15    ,  0.15           /
        data nSi    , nH             /  0.78734 ,  1.00           /
        data deltSi , deltH          /  0.635   ,  0.80469        /

        ! Morse term constants (for Si-Si and Si-H bonds).
        CONFR(1,4) =  aSiSi
        CONFR(1,5) =  aSiH
        CONFA(1,4) = -bSiSi
        CONFA(1,5) = -bSiH
        CONER(1,4) = -lmSiSi
        CONER(1,5) = -lmSiH
        CONEA(1,4) = -muSiSi
        CONEA(1,5) = -muSiH
        CONRE(1,4) =  reSiSi
        CONRE(1,5) =  reSiH

        ! Cutoff constants (for Si-Si and Si-H bonds).
        CONRL(1,4)  = rSiSi - dSiSi
        CONRL(1,5)  = rSiH  - dSiH
        CONRH2(1,4) = ( rSiSi + dSiSi )  ** 2
        CONRH2(1,5) = ( rSiH  + dSiH  )  ** 2
        CONFCA(1,4) = pi / ( 2.0 * dSiSi )
        CONFCA(1,5) = pi / ( 2.0 * dSiH  )
        CONFC(1,4)  = - 0.5 * CONFCA(1,4)
        CONFC(1,5)  = - 0.5 * CONFCA(1,5)

        ! Fermi join constants (Si taken from parcas,
        ! SiH determined for this). Used if reppotcut > 0.
        fermib(1,4) = 12.0d0
        fermir(1,4) = 1.6d0
        fermib(1,5) = 10.0d0
        fermir(1,5) = 0.6d0


        ! Bond order constants (for Si centres on Si-C bonds
        !                       for Si centres on Si-H bonds
        !                   and for bond centred H).
        CONN(2)  = nSi
        CONN(5)  = nH
        CONN(6)  = nH
        CONPE(2) = -deltSi
        CONPE(5) = -deltH
        CONPE(6) = -deltH
        CONAN(2) = 0.5 * CONN(2) * CONPE(2)
        CONAN(5) = 0.5 * CONN(5) * CONPE(5)
        CONAN(6) = 0.5 * CONN(6) * CONPE(6)
        CONNM(2) = CONN(2)  - 1.0
        CONNM(5) = CONN(5)  - 1.0
        CONNM(6) = CONN(6)  - 1.0
        CONPF(2) = CONPE(2) - 1.0
        CONPF(5) = CONPE(5) - 1.0
        CONPF(6) = CONPE(6) - 1.0

        ! Bond angle constants (for Si centres on Si-C bonds
        !                       for Si centres on Si-H bonds
        !                   and for bond centred H).
        CONCD(1,2) = 0.0
        CONCD(1,5) = 0.0
        CONCD(1,6) = 0.0
        CONC2(1,2) = cSi
        CONC2(1,5) = cH
        CONC2(1,6) = cHb
        COND2(1,2) = dSi
        COND2(1,5) = dH
        COND2(1,6) = dHb
        CONH(1,2)  = HSi
        CONH(1,5)  = HH
        CONH(1,6)  = HHb

        ! Bond length constants (for Si centres on Si-C bonds
        !                        for Si centres on Si-H bonds
        !                    and for bond centred H).
        CONALP(2)   = alphaSi
        CONALP(5)   = alphaH
        CONALP(6)   = alphaHb
        CONBET(2)   = betaSi
        CONBET(5)   = betaH
        CONBET(6)   = betaHb

        ! Generate the coefficients for
        ! the cubic spline interpolation functions.
        call gen_f1_spline()
        call gen_f2_spline()
        call gen_h_spline()

    end subroutine gen_SiH_params


    !#######################################################################
    !
    !   Generates the coefficients for bicubic interpolation of Hch(Nih,Nic)
    !   Copyright: Keith Beardmore 30/11/93.
    !
    subroutine gen_hch_interpolation()

        real(real64b), dimension(0:4, 0:4) :: hch, dhchdh, dhchdc, d2hchd

        ! Values given in table III.

        ! Changed (0,1), was -0.17920.
        ! Changed (1,1), was -0.24770.
        data hch / &
              0.00000 , -0.09840 , -0.28780 , -0.45070 ,  0.00000 ,  &
             -0.24790 , -0.33440 , -0.44380 ,  0.00000 ,  0.00000 ,  &
             -0.32210 , -0.44490 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
             -0.44600 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 /

        ! Changed (1,1), was -0.07640.
        data dhchdh / &
              0.00000 ,  0.00000 , -0.17615 ,  0.00000 ,  0.00000 ,  &
              0.00000 , -0.09795 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 /

        ! Changed (0,2), was -0.07655.
        data dhchdc / &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 , -0.17325 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
             -0.09905 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 /

        d2hchd(:,:) = 0.0


        call gen_2d_bicubic_interpolation(hch, dhchdh, dhchdc, d2hchd, hchcf)

    end subroutine gen_hch_interpolation


    !#######################################################################
    !
    !   Generates the coefficients for bicubic interpolation of Hcc(Nih,Nic)
    !   Copyright: Keith Beardmore 02/12/93.
    !
    subroutine gen_hcc_interpolation()

        real(real64b), dimension(0:4, 0:4) :: hcc, dhccdh, dhccdc, d2hccd

        ! Values given in table III.

        ! Changed (2,0), was -0.00700.
        data hcc / &
              0.00000 ,  0.00000 , -0.00610 ,  0.01730 ,  0.00000 ,  &
              0.00000 , -0.02260 ,  0.01600 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.01490 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 /

        dhccdh(:,:) = 0.0
        dhccdc(:,:) = 0.0
        d2hccd(:,:) = 0.0


        call gen_2d_bicubic_interpolation(hcc, dhccdh, dhccdc, d2hccd, hcccf)

    end subroutine gen_hcc_interpolation


    !#######################################################################
    !
    !   Generates the coefficients for 2-D bicubic interpolation.
    !   Helper used by gen_hcc_interpolation and gen_hcp_interpolation.
    !
    subroutine gen_2d_bicubic_interpolation(val, dvaldh, dvaldc, d2vald, outarr)

        real(real64b), dimension(0:4, 0:4), intent(in) :: val, dvaldh, dvaldc, d2vald
        real(real64b), intent(out) :: outarr(16, 4, 4)

        integer :: i, j
        integer :: icorn, irow, icol
        integer :: nx1, nx2
        integer :: npow1, npow2
        integer :: npow1m, npow2m
        integer :: nhbox, ncbox, ibox

        real(real64b) :: a(16,16), b(16,16)

        ! Normalised coordinates.
        !     4--<--3
        !     |     ^
        !     v     |
        !     1-->--2
        integer, parameter :: ix1(4) = [0,1,1,0]
        integer, parameter :: ix2(4) = [0,0,1,1]

        ! Calculate 2-D cubic parameters within each box.
        !
        ! For each box, create and solve the matrix equation.
        !    / values of  \     /              \     / function and \
        !  A |  products  | * X | coefficients | = B |  derivative  |
        !    \within cubic/     \ of 2D cubic  /     \    values    /
        !
        ! Construct the matrix.
        ! This is the same for all boxes as coordinates are normalised.
        ! Loop through corners.
        do icorn = 1, 4
            irow = icorn
            nx1 = ix1(icorn)
            nx2 = ix2(icorn)

            ! Loop through powers of variables.
            do npow1 = 0, 3
                do npow2 = 0, 3
                    npow1m = npow1 - 1
                    if (npow1m < 0) npow1m = 0
                    npow2m = npow2 - 1
                    if (npow2m < 0) npow2m = 0
                    icol = 1 + 4*npow1 + npow2

                    ! Values of products within cubic and derivatives.
                    a(irow   ,icol) = powerprod2(1    ,nx1,npow1 ,1    ,nx2,npow2)
                    a(irow+4 ,icol) = powerprod2(npow1,nx1,npow1m,1    ,nx2,npow2)
                    a(irow+8 ,icol) = powerprod2(1    ,nx1,npow1 ,npow2,nx2,npow2m)
                    a(irow+12,icol) = powerprod2(npow1,nx1,npow1m,npow2,nx2,npow2m)
                end do
            end do
        end do

        ! Construct the 16 r.h.s. vectors (1 for each box).
        ! Loop through boxes.
        do nhbox = 0, 3
            do ncbox = 0, 3
                icol = 1 + 4*nhbox + ncbox
                do icorn = 1, 4
                    irow = icorn
                    nx1 = ix1(icorn) + nhbox
                    nx2 = ix2(icorn) + ncbox

                    ! Values of function and derivatives at corner.
                    b(irow   ,icol) =   val  ( nx1 , nx2 )
                    b(irow+4 ,icol) =  dvaldh( nx1 , nx2 )
                    b(irow+8 ,icol) =  dvaldc( nx1 , nx2 )
                    b(irow+12,icol) = d2vald ( nx1 , nx2 )
                end do
            end do
        end do

        ! Solve by Gauss-Jordan elimination with full pivoting.
        call gaussj(a, 16, 16, b, 16, 16)

        ! Get the coefficient values.
        do ibox = 1, 16
            icol = ibox
            do i = 1, 4
                do j = 1, 4
                    irow = 4*(i-1) + j
                    outarr(ibox,i,j) = b(irow,icol)
                end do
            end do
        end do

    end subroutine gen_2d_bicubic_interpolation



    !#######################################################################
    !
    !   Generates the coefficients
    !   for bicubic interpolation of Fcc(Nit,Njt,Nconj)
    !   Copyright: Keith Beardmore 02/12/93.
    !
    subroutine gen_fcc_interpolation()

        integer, parameter :: np = 64
        integer, parameter :: mp = 16
        integer :: i, j, k
        integer :: icorn, irow, icol
        integer :: nx1, nx2, nx3
        integer :: npow1, npow2, npow3
        integer :: npow1m, npow2m, npow3m
        integer :: nibox, njbox
        integer :: ibox

        real(real64b) :: a(np,np), b(np,mp)

        real(real64b), dimension(0:4, 0:4, 1:2) :: &
            fcc, dfccdi, dfccdj, dfccdc, d2fdij, d2fdic, d2fdjc, d3fccd

        !   Normalised coordinates.
        !
        !       8--<--7
        !      /|    /|
        !     5-->--6 |
        !     | 4--<|-3
        !     |/    |/
        !     1-->--2
        integer, parameter ::  ix1(8) = [0,1,1,0,0,1,1,0]
        integer, parameter ::  ix2(8) = [0,0,1,1,0,0,1,1]
        integer, parameter ::  ix3(8) = [0,0,0,0,1,1,1,1]

        ! Values given in table III.

        ! Changed (0,2,1) and (2,0,1), were  0.03200.
        ! Changed (0,2,2) and (2,0,2), were -0.04450.
        ! Changed (1,3,2) and (3,1,2), were -0.11300.
        ! Changed (2,3,2) and (3,2,2), were -0.04650.
        ! Changed (1,1,1), was 0.15110.
        ! Changed (2,2,1), was 0.07500.
        data fcc / &
              0.00000 ,  0.09660 ,  0.04270 , -0.09040 ,  0.00000 ,  &
              0.09960 ,  0.12640 ,  0.01200 , -0.09030 ,  0.00000 ,  &
              0.04270 ,  0.01200 ,  0.06050 , -0.04650 ,  0.00000 ,  &
             -0.09040 , -0.09030 , -0.04650 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 , -0.02690 , -0.09040 ,  0.00000 ,  &
              0.00000 ,  0.01080 , -0.03550 , -0.09030 ,  0.00000 ,  &
             -0.02690 , -0.03550 ,  0.00000 , -0.04650 ,  0.00000 ,  &
             -0.09040 , -0.09030 , -0.04650 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 /

        ! Changed (1,2,1) and (2,1,1), were  0.00000.
        ! Changed (1,2,2) and (2,1,2), were  0.02225.
        ! Changed (2,1,1) and (1,2,1), were -0.13205.
        ! Changed (2,1,2) and (1,2,2), were -0.06020.
        ! Changed (2,3,2) and (3,2,2), were  0.05650.
        ! Changed (1,3,2) and (3,1,2), were  0.03775.
        data dfccdi / &
              0.00000 ,  0.00000 , -0.09500 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 , -0.10835 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.04515 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 , -0.04520 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 , -0.05055 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.01345 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.02705 ,  0.04515 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 /

        data dfccdj / &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
             -0.09500 , -0.10835 ,  0.00000 ,  0.04515 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.01345 ,  0.02705 ,  0.00000 ,  &
             -0.04520 , -0.05055 ,  0.00000 ,  0.04515 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  &
              0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 /

        dfccdc(:,:,:) = 0.0
        d2fdij(:,:,:) = 0.0
        d2fdic(:,:,:) = 0.0
        d2fdjc(:,:,:) = 0.0
        d3fccd(:,:,:) = 0.0

        !   Calculate 3-d cubic parameters within each box.
        !
        !   For each box, create and solve the matrix equation.
        !      / values of  \     /              \     / function and \
        !    A |  products  | * X | coefficients | = B |  derivative  |
        !      \within cubic/     \ of 3D cubic  /     \    values    /
        !
        !   Construct the matrix.
        !   This is the same for all boxes as coordinates are normalised.
        !   Loop through corners.
        do icorn = 1, 8
            irow = icorn
            nx1 = ix1(icorn)
            nx2 = ix2(icorn)
            nx3 = ix3(icorn)

            ! Loop through powers of variables.
            do npow1 = 0, 3
                do npow2 = 0, 3
                    do npow3 = 0, 3
                        npow1m = npow1 - 1
                        if(npow1m < 0) npow1m = 0
                        npow2m = npow2 - 1
                        if(npow2m < 0) npow2m = 0
                        npow3m = npow3 - 1
                        if(npow3m < 0) npow3m = 0

                        icol = 1 + 16*npow1 + 4*npow2 + npow3

                        ! Values of products within cubic and derivatives.
                        a(irow   ,icol) = powerprod3(1    ,nx1,npow1 ,1    ,nx2,npow2 ,1    ,nx3,npow3)
                        a(irow+8 ,icol) = powerprod3(npow1,nx1,npow1m,1    ,nx2,npow2 ,1    ,nx3,npow3)
                        a(irow+16,icol) = powerprod3(1    ,nx1,npow1 ,npow2,nx2,npow2m,1    ,nx3,npow3)
                        a(irow+24,icol) = powerprod3(1    ,nx1,npow1 ,1    ,nx2,npow2 ,npow3,nx3,npow3m)
                        a(irow+32,icol) = powerprod3(npow1,nx1,npow1m,npow2,nx2,npow2m,1    ,nx3,npow3)
                        a(irow+40,icol) = powerprod3(npow1,nx1,npow1m,1    ,nx2,npow2 ,npow3,nx3,npow3m)
                        a(irow+48,icol) = powerprod3(1    ,nx1,npow1 ,npow2,nx2,npow2m,npow3,nx3,npow3m)
                        a(irow+56,icol) = powerprod3(npow1,nx1,npow1m,npow2,nx2,npow2m,npow3,nx3,npow3m)
                    end do
                end do
            end do
        end do

        ! Construct the 16 r.h.s. vectors (1 for each box).
        ! Loop through boxes.
        do nibox = 0, 3
            do njbox = 0, 3
                icol = 1 + 4*nibox + njbox
                do icorn = 1, 8
                    irow = icorn
                    nx1 = ix1(icorn) + nibox
                    nx2 = ix2(icorn) + njbox
                    nx3 = ix3(icorn) + 1

                    ! Values of function and derivatives at corner.
                    b(irow   ,icol) =   fcc  ( nx1 , nx2 , nx3 )
                    b(irow+8 ,icol) =  dfccdi( nx1 , nx2 , nx3 )
                    b(irow+16,icol) =  dfccdj( nx1 , nx2 , nx3 )
                    b(irow+24,icol) =  dfccdc( nx1 , nx2 , nx3 )
                    b(irow+32,icol) = d2fdij ( nx1 , nx2 , nx3 )
                    b(irow+40,icol) = d2fdic ( nx1 , nx2 , nx3 )
                    b(irow+48,icol) = d2fdjc ( nx1 , nx2 , nx3 )
                    b(irow+56,icol) = d3fccd ( nx1 , nx2 , nx3 )
                end do
            end do
        end do

        ! Solve by Gauss-Jordan elimination with full pivoting.
        call gaussj(a, 64, np, b, 16, mp)

        ! Get the coefficient values.
        do ibox = 1, 16
            icol = ibox
            do i = 1, 4
                do j = 1, 4
                    do k = 1, 4
                        irow = 16 * (i-1) + 4 * (j-1) + k
                        fcccf(ibox,i,j,k) = b(irow,icol)
                    end do
                end do
            end do
        end do

    end subroutine gen_fcc_interpolation


    !----------------------------------------------------------------------------
    pure real(real64b) function powerprod2(f1, i1, p1, f2, i2, p2)

        integer, intent(in) :: f1, i1, p1, f2, i2, p2
        integer :: t1, t2

        if (.not. (i1 == 0 .and. p1 == 0)) then
            t1 = i1**p1
        else
            t1 = 1
        end if
        if (.not. (i2 == 0 .and. p2 == 0)) then
            t2 = i2**p2
        else
            t2 = 1
        end if

        powerprod2 = 1.0 * f1 * t1 * f2 * t2

    end function powerprod2


    !----------------------------------------------------------------------------
    pure real(real64b) function powerprod3(f1, i1, p1, f2, i2, p2, f3, i3, p3)

        integer, intent(in) :: f1, i1, p1, f2, i2, p2, f3, i3, p3
        integer :: t1, t2, t3

        if (.not. (i1 == 0 .and. p1 == 0)) then
            t1 = i1**p1
        else
            t1 = 1
        end if
        if (.not. (i2 == 0 .and. p2 == 0)) then
            t2 = i2**p2
        else
            t2 = 1
        end if
        if (.not. (i3 == 0 .and. p3 == 0)) then
            t3 = i3**p3
        else
            t3 = 1
        endif

        powerprod3 = 1.0 * f1 * t1 * f2 * t2 * f3 * t3

    end function powerprod3


    !#######################################################################
    !
    !   Solves an nxn matrix equation with m right hand side vectors
    !   using Gauss-Jordan elimination with full pivoting.
    !   Inverse matrix and solution vectors are returned in place of inputs.
    !   Actual arrays for a and b are np x np and np x mp.
    !   nmax must be larger than n.
    !   Copyright: Keith Beardmore 30/11/93.
    !   Algorithm from Numerical Recipes p28.
    !
    !     a(i,j) is aij row i column j.
    !
    subroutine gaussj(a, n, np, b, m, mp)

        integer, intent(in) :: n, np, m, mp
        real(real64b), intent(inout) :: a(np,np), b(np,mp)

        integer, parameter :: nmax = 64
        integer :: ipiv(nmax)
        integer :: indxr(nmax)
        integer :: indxc(nmax)

        integer :: i, j, k
        integer :: l, ll
        integer :: irow, icol
        real(real64b) :: big, dum, pivinv

        ipiv(1:n) = 0
        irow = 0
        icol = 0

        do i = 1, n
            big = 0.0
            do j = 1, n
                if (ipiv(j) /= 1) then
                    do k = 1, n
                        if (ipiv(k) == 0) then
                            if (abs(a(j,k)) >= big) then
                                big = abs(a(j,k))
                                irow = j
                                icol = k
                            end if
                        else if(ipiv(k) > 1) then
                            call my_mpi_abort("Brenner-Beardmore singular matrix", k)
                        end if
                    end do
                end if
            end do
            ipiv(icol) = ipiv(icol) + 1
            if (irow /= icol) then
                do l = 1, n
                    dum = a(irow,l)
                    a(irow,l) = a(icol,l)
                    a(icol,l) = dum
                end do
                do l = 1, m
                    dum = b(irow,l)
                    b(irow,l) = b(icol,l)
                    b(icol,l) = dum
                end do
            end if
            indxr(i) = irow
            indxc(i) = icol
            if (a(icol,icol) == 0.0) then
                call my_mpi_abort("Brenner-Beardmore singular matrix", icol)
            end if
            pivinv = 1.0 / a(icol,icol)
            a(icol,icol) = 1.0
            do l = 1, n
                a(icol,l) = a(icol,l) * pivinv
            end do
            do l = 1, m
                b(icol,l) = b(icol,l) * pivinv
            end do
            do ll = 1, n
                if (ll /= icol) then
                    dum = a(ll,icol)
                    a(ll,icol) = 0.0
                    do l = 1, n
                        a(ll,l) = a(ll,l) - a(icol,l) * dum
                    end do
                    do l = 1, m
                        b(ll,l) = b(ll,l) - b(icol,l) * dum
                    end do
                end if
            end do
        end do
        do l = n, 1, -1
            if (indxr(l) /= indxc(l)) then
                do k = 1, n
                    dum = a(k,indxr(l))
                    a(k,indxr(l)) = a(k,indxc(l))
                    a(k,indxc(l)) = dum
                end do
            end if
        end do

    end subroutine gaussj


    !#######################################################################
    !
    !   Generates the coefficients for cubic spline interpolation of f1(nt)
    !   Copyright: Keith Beardmore 31/08/94.
    !
    subroutine gen_f1_spline

        integer, parameter :: n = 4
        real(real64b) :: y2(n)
        integer :: interv

        ! Values given in table I.
        real(real64b), parameter :: x(n) = [1.000 , 2.000 , 3.000 , 4.000]
        real(real64b), parameter :: y(n) = [1.005 , 1.109 , 0.953 , 1.000]
        real(real64b), parameter :: yp1 = 0.000
        real(real64b), parameter :: ypn = 0.000

        ! Calculate second derivative values at interpolation points.
        call spline(x, y, n, yp1, ypn, y2)

        ! Get the coefficient values.
        do interv = 1, n-1
            af1(interv, 1) =  y(interv)
            af1(interv, 2) =  y(interv+1)     -  y(interv)      - y2(interv+1)/6.0 - y2(interv)/3.0
            af1(interv, 3) =                    y2(interv)/2.0
            af1(interv, 4) = y2(interv+1)/6.0 - y2(interv)/6.0
        end do

    end subroutine gen_f1_spline


    !#######################################################################
    !
    !   Generates the coefficients for cubic spline interpolation of f2(nt)
    !   Copyright: Keith Beardmore 31/08/94.
    !
    subroutine gen_f2_spline

        integer, parameter :: n = 4
        real(real64b) :: y2(n)
        integer :: interv

        ! Values given in table I.
        real(real64b), parameter :: x(n) = [1.000 , 2.000 , 3.000 , 4.000]
        real(real64b), parameter :: y(n) = [0.930 , 1.035 , 0.934 , 1.000]
        real(real64b), parameter :: yp1 = 0.000
        real(real64b), parameter :: ypn = 0.000

        ! Calculate second derivative values at interpolation points.
        call spline(x, y, n, yp1, ypn, y2)

        ! Get the coefficient values.
        do interv = 1, n-1
            af2(interv, 1) =  y(interv)
            af2(interv, 2) =  y(interv+1)     -  y(interv)      - y2(interv+1)/6.0 - y2(interv)/3.0
            af2(interv, 3) =                    y2(interv)/2.0
            af2(interv, 4) = y2(interv+1)/6.0 - y2(interv)/6.0
        end do

    end subroutine gen_f2_spline


    !#######################################################################
    !
    !   Generates the coefficients for cubic spline interpolation of h(nt).
    !   Copyright: Keith Beardmore 31/08/94.
    !
    subroutine gen_h_spline()

        integer, parameter :: n = 4
        real(real64b) :: y2(n)
        integer :: interv

        ! Values given in table I.
        real(real64b), parameter :: x(n) = [ 1.000 ,  2.000 ,  3.000 ,  4.000]
        real(real64b), parameter :: y(n) = [-0.040 , -0.040 , -0.276 , -0.470]
        real(real64b), parameter :: yp1 = 0.000
        real(real64b), parameter :: ypn = 0.000

        ! Calculate second derivative values at interpolation points.
        call spline(x, y, n, yp1, ypn, y2)

        ! Get the coefficient values.
        do interv = 1, n-1
            ah(interv, 1) =  y(interv)
            ah(interv, 2) =  y(interv+1)     -  y(interv)     - y2(interv+1)/6.0 - y2(interv)/3.0
            ah(interv, 3) =                    y2(interv)/2.0
            ah(interv, 4) = y2(interv+1)/6.0 - y2(interv)/6.0
        end do

    end subroutine gen_h_spline


    !#######################################################################
    !
    !   Given arrays x and y of length n containing a tabulated function,
    !   i.e. yi = f(xi), with x1 < x2 < ... < xn, and given values yp1 and
    !   ypn for the first derivative of the interpolating function at points
    !   1 and n, respectively, this routine returns an array y2 of length n
    !   which contains the second derivatives of the interpolating function
    !   at the tabulated points xi. If yp1 and/or ypn are equal to 1e30 or
    !   larger, the routine is signalled to set the corresponding boundary
    !   condition for a natural spline, with zero second derivative on that
    !   boundary.
    !   Copyright: Keith Beardmore 31/08/94.
    !   Algorithm from Numerical recipes p 88.
    !
    subroutine spline(x, y, n, yp1, ypn, y2)

        integer, intent(in) :: n
        real(real64b), intent(in) :: x(n), y(n)
        real(real64b), intent(in) :: yp1, ypn
        real(real64b), intent(out) :: y2(n)

        real(real64b) :: u(4), qn, un, sig, p
        integer :: i, k

        if (yp1 > 0.99e30) then
            y2(1) = 0.0
            u(1)  = 0.0
        else
            y2(1) = -0.5
            u(1)  = (3.0 / (x(2)-x(1))) * ((y(2)-y(1)) / (x(2)-x(1)) - yp1)
        end if

        do i = 2,n - 1
            sig   = (x(i)-x(i-1)) / (x(i+1)-x(i-1))
            p     = sig * y2(i-1) + 2.0
            y2(i) = (sig - 1.0) / p
            u(i)  = (6.0 * ((y(i+1)-y(i)) / (x(i+1)-x(i)) - (y(i)-y(i-1)) / (x(i)-x(i-1))) / &
                (x(i+1)-x(i-1)) - sig*u(i-1)) / p
        end do

        if (ypn > 0.99e30) then
            qn = 0.0
            un = 0.0
        else
            qn = 0.5
            un = (3.0 / (x(n)-x(n-1))) * (ypn - (y(n)-y(n-1)) / (x(n)-x(n-1)))
        end if

        y2(n) = (un - qn*u(n-1)) / (qn * y2(n-1) + 1.0)
        do k = n-1, 1, -1
            y2(k) = y2(k) * y2(k+1) + u(k)
        enddo

    end subroutine spline


    !#######################################################################
    !
    !   Calculates the values of the constants used in
    !   Zeigler, Biersack & Litmark's universal potential.
    !   Copyright: Keith Beardmore 12/5/94.
    !   Parameters from : Computer Simulation of Ion-Solid Interactions,
    !   pp 40-42, W. Eckstein, Springer-Verlag (Berlin Heidelberg), (1991).
    !
    subroutine ZBL_params()

        real(real64b) :: a(4), cconst(4)
        integer :: itype

        ! TODO use values from Phys_Consts, and check for double precision
        ! constants, i.e. 1d0 instead of 1e0.
        ! Values for the parameters.
        real(real64b), parameter :: E2B4PE = 2.3070796e-28
        real(real64b), parameter :: aconst = 0.88534
        real(real64b), parameter :: echarge = 1.60217733E-19 ! electron charge
        real(real64b), parameter :: zc = 6.0                 ! atomic number C
        real(real64b), parameter :: zsi = 14.0               ! atomic number Si
        real(real64b), parameter :: zh = 1.0                 ! atomic number H
        real(real64b), parameter :: zar = 18.0               ! atomic number Ar
        real(real64b), parameter :: abohr = 0.530            ! Bohr radius (Angstrom)
        real(real64b), parameter :: pow = 0.23

        real(real64b), parameter :: C(4) = [0.028171 , 0.28022 , 0.50986 , 0.18175]
        real(real64b), parameter :: D(4) = [0.20162  , 0.40290 , 0.94229 , 3.1998 ]

        ! E2 = e^2 / 4 Pi Epsilon0 ( in Angstroms and eV ).
        real(real64b), parameter :: E2 = E2B4PE / (1e-10 * echarge)

        ! Cutoff from 90% to 110% of estimated bond length.
        real(real64b), parameter :: r1CAr  = 2.3850
        real(real64b), parameter :: r1SiAr = 2.7481
        real(real64b), parameter :: r1HAr  = 1.9597
        real(real64b), parameter :: r1ArAr = 3.3795
        real(real64b), parameter :: r2CAr  = 2.9150
        real(real64b), parameter :: r2SiAr = 3.3589
        real(real64b), parameter :: r2HAr  = 2.3953
        real(real64b), parameter :: r2ArAr = 4.1305

        ! Calculate bondlength constants.
        a(1) = aconst * abohr / (zar**pow + zc **pow)
        a(2) = aconst * abohr / (zar**pow + zsi**pow)
        a(3) = aconst * abohr / (zar**pow + zh **pow)
        a(4) = aconst * abohr / (zar**pow + zar**pow)
        cconst(1) = zar * zc  * E2
        cconst(2) = zar * zsi * E2
        cconst(3) = zar * zh  * E2
        cconst(4) = zar * zar * E2

        ! Construct C-Ar, Si-Ar, H-Ar and Ar-Ar parameters.
        do itype = 1,4
            CONC(itype,:) =  C(:) * cconst(itype)
            COND(itype,:) = -D(:) / a(itype)
        end do

        ! Cutoff constants.
        CONRL(1,7)   = r1CAr
        CONRL(1,8)   = r1SiAr
        CONRL(1,9)   = r1HAr
        CONRL(1,10)  = r1ArAr
        CONRH2(1,7)  = r2CAr  ** 2
        CONRH2(1,8)  = r2SiAr ** 2
        CONRH2(1,9)  = r2HAr  ** 2
        CONRH2(1,10) = r2ArAr ** 2
        CONFCA(1,7)  = PI / ( r2CAr  - r1CAr  )
        CONFCA(1,8)  = PI / ( r2SiAr - r1SiAr )
        CONFCA(1,9)  = PI / ( r2HAr  - r1HAr  )
        CONFCA(1,10) = PI / ( r2ArAr - r1ArAr )
        CONFC(1,7)   = - 0.5 * CONFCA(1,7)
        CONFC(1,8)   = - 0.5 * CONFCA(1,8)
        CONFC(1,9)   = - 0.5 * CONFCA(1,9)
        CONFC(1,10)  = - 0.5 * CONFCA(1,10)

    end subroutine ZBL_params



!#######################################################################
!#######################################################################

    !
    ! Parcas interface SetCut routine
    ! This is bloody ugly because of hardcoded atom types...
    !
    subroutine Beardmore_SetCut(reppotcutin)

        real(real64b), intent(in) :: reppotcutin

        integer :: i, j

        ! Cutoff for possible spline pair potentials
        CONRH2(1,11) = reppotcutin**2

        ! First do i > j cutoffs, then symmetrisize

        rcut(:,:) = 0.0

        ! TODO why is this commented?
        ! H-H cutoffs
        !rcut(0,0) = sqrt(CONRH2(1,6))
        !rcut(2,0) = sqrt(CONRH2(1,6))
        !rcut(3,0) = sqrt(CONRH2(1,6))
        !rcut(4,0) = sqrt(CONRH2(1,6))
        !do i = 2, 4
        !   do j = 2, 4
        !      rcut(i,j) = sqrt(CONRH2(1,6))
        !   end do
        !end do
        ! H-C
        !rcut(1,0) = sqrt(CONRH2(1,3))
        !rcut(2,1) = sqrt(CONRH2(1,3))
        !rcut(3,1) = sqrt(CONRH2(1,3))
        !rcut(4,1) = sqrt(CONRH2(1,3))
        ! C-C
        rcut(1,1) = sqrt(CONRH2(1,1))
        if (itypehigh > 4) then
            ! C-Si
            rcut(5,1) = sqrt(CONRH2(1,2))
            ! H-Si
            rcut(5,0) = sqrt(CONRH2(1,5))
            rcut(5,2) = sqrt(CONRH2(1,5))
            rcut(5,3) = sqrt(CONRH2(1,5))
            rcut(5,4) = sqrt(CONRH2(1,5))
            ! Si-Si
            rcut(5,5) = sqrt(CONRH2(1,4))
        end if
        if (itypehigh > 5) then
            ! H-Ar
            rcut(6,0) = sqrt(CONRH2(1,9))
            rcut(6,2) = sqrt(CONRH2(1,9))
            rcut(6,3) = sqrt(CONRH2(1,9))
            rcut(6,4) = sqrt(CONRH2(1,9))
            ! C-Ar
            rcut(6,1) = sqrt(CONRH2(1,7))
            ! Si-Ar
            rcut(6,5) = sqrt(CONRH2(1,8))
            ! Ar-Ar
            rcut(6,6) = sqrt(CONRH2(1,10))
        end if

        if (itypehigh > 6) then
            do i = 7, itypehigh
                rcut(i, 0:i) = reppotcutin
            end do
            ! Be-Be
            rcut(7,7) = 2.685
            ! Be-C
            rcut(7,1) = 2.8
            ! Be-H
            rcut(7,0) = 1.95
            rcut(7,2) = 1.95
            rcut(7,3) = 1.95
            rcut(7,4) = 1.95
        end if

        do i = itypelow, itypehigh
            do j = i+1, itypehigh
                rcut(i,j) = rcut(j,i)
            end do
        end do

    end subroutine Beardmore_SetCut


    subroutine Beardmore_init(r1cc, r2cc, reppotcutin)

        real(real64b), intent(in) :: r1cc, r2cc
        real(real64b), intent(in) :: reppotcutin

        integer, parameter :: NNEBMX = NPMAX * NNMAX

        if (iprint) then
            call logger('Initializing BrennerBeardmore potential')
            call logger('PRESSURELESS VERSION !!!')
        end if

        allocate(NTSI(NPMAX))
        allocate(DATUM(NPMAX+1))
        allocate(BRETYP(NPMAX))
        allocate(iactmode(NPMAX)) !some old shit, should be fixed
        allocate(KTYP(NPMAX))
        allocate(BNDLEN(NNEBMX))
        allocate(CUTFCN(NNEBMX))
        allocate(CUTDRV(NNEBMX))
        allocate(fermifcn(NNEBMX))
        allocate(fermidrv(NNEBMX))
        allocate(BNDXNM(NNEBMX))
        allocate(BNDYNM(NNEBMX))
        allocate(BNDZNM(NNEBMX))
        allocate(BNDTYP(NNEBMX))
        allocate(NEB(NNEBMX))
        allocate(BNDTYPBRE(NNEBMX))

        call gen_brenner_beardmore_params(r1cc, r2cc)

        call Beardmore_setcut(reppotcutin)

    end subroutine Beardmore_init

end module brenner_beardmore_mod
