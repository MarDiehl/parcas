!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

!
! The code in this module is based on the code available at
! http://web.mit.edu/bazant/www/EDIP/index.html
!
! Despite the license text shown there and in the downloadable
! source files, Prof. Bazant has in private communication allowed
! the use and distribution of the code under the GPL3 license.
!

module edip_mod

    use output_logger, only: log_buf, logger

    use typeparam
    use datatypes, only: real64b
    use my_mpi

    use timers, only: tmr, TMR_SEMICON_COMMS

    use typeparam
    use defs
    use splinereppot_mod, only: reppot_only, reppot_fermi

    use mdparsubs_mod, only: potential_pass_back_border_atoms

    use para_common, only: myatoms, np0pairtable, buf


    implicit none

    private
    public :: Init_Edip
    public :: EDIP_forces


    real(real64b) :: par_cap_A, par_cap_B, par_rh, par_a, par_sig
    real(real64b) :: par_lam, par_gam, par_b, par_c, par_delta
    real(real64b) :: par_mu, par_Qo, par_palp, par_bet, par_alp
    real(real64b) :: delta_safe, pot_cutoff, par_eta, par_bg
    real(real64b) :: u1, u2, u3, u4
    real(real64b) :: reppotcut


  contains

    subroutine EDIP_forces(x0,atype,xnp,box,pbc,            &
            nborlist,Epair,Ethree, &
            wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_vir)

        !
        !     In the PARCAS interface:
        !     Input:      x0      contains positions in A scaled by 1/box,
        !                 natoms  Total number of atoms over all nodes
        !                 myatoms Number of atoms in my node, in para_common.f90
        !                 box(3)  Box size (box centered on 0)
        !                 pbc(3)  Periodics: if = 1.0d0 periodic
        !
        !     Output:     xnp     contains forces in eV/A scaled by 1/box
        !                 Epair   V_2 per atom
        !                 Ethree  V_3 per atom
        !
        !

        ! nborlist(i) has following format (not same as original code !):
        !
        !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
        !  nborlist(2)    Index of first neighbour of atom 1
        !  nborlist(3)    Index of second neighbour of atom 1
        !   ...
        !  nborlist(N1+1)  Index of last neighbour of atom 1
        !  nborlist(N1+2)  Number of neighbours for atom 2
        !  nborlist(N1+3)  Index of first neighbour of atom 2
        !   ...
        !   ...           And so on for all N atoms
        !


        !
        ! ------------------------------------------------------------------------
        !     Variables passed in and out

        real(real64b), contiguous, intent(in) :: x0(:)
        real(real64b), intent(in) :: box(3),pbc(3)
        integer, contiguous, intent(in) :: nborlist(:)
        integer, contiguous, intent(in) :: atype(:)

        real(real64b), contiguous, intent(out) :: xnp(:)
        real(real64b), contiguous, intent(out) :: Epair(:), Ethree(:)
        real(real64b), contiguous, intent(out) :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)

        logical, intent(in) :: calc_vir

        !
        !     PARAMETERS FOR edip
        !
        !

        integer :: l,n
        real(kind=real64b) :: dx,dy,dz,rsqr,asqr
        real(kind=real64b) :: rinv,rmainv,xinv,xinv3,den,Z,fZ
        real(kind=real64b) :: dV2j,dV2ijx,dV2ijy,dV2ijz,pZ,dp
        real(kind=real64b) :: temp0,temp1
        real(kind=real64b) :: Qort,muhalf,u5
        real(kind=real64b) :: rmbinv,winv,dwinv,tau,dtau,lcos,H,dHdx,dhdl,x_
        real(kind=real64b) :: dV3rij,dV3rijx,dV3rijy,dV3rijz
        real(kind=real64b) :: dV3rik,dV3rikx,dV3riky,dV3rikz
        real(kind=real64b) :: dV3l,dV3ljx,dV3ljy,dV3ljz,dV3lkx,dV3lky,dV3lkz
        real(kind=real64b) :: dV2dZ,dxdZ,dV3dZ
        real(kind=real64b) :: dEdrl,dEdrlx,dEdrly,dEdrlz
        real(kind=real64b) :: bmc,cmbinv
        real(kind=real64b) :: fjx,fjy,fjz,fkx,fky,fkz

        real(kind=real64b) :: s2_t0(NNMAX)
        real(kind=real64b) :: s2_t1(NNMAX)
        real(kind=real64b) :: s2_t2(NNMAX)
        real(kind=real64b) :: s2_t3(NNMAX)
        real(kind=real64b) :: s2_dx(NNMAX)
        real(kind=real64b) :: s2_dy(NNMAX)
        real(kind=real64b) :: s2_dz(NNMAX)
        real(kind=real64b) :: s2_r(NNMAX)
        integer :: n2
        !   size of s2[]
        integer :: num2(NNMAX)
        !   atom ID numbers for s2[]

        real(kind=real64b) :: s3_g(NNMAX)
        real(kind=real64b) :: s3_dg(NNMAX)
        real(kind=real64b) :: s3_rinv(NNMAX)
        real(kind=real64b) :: s3_dx(NNMAX)
        real(kind=real64b) :: s3_dy(NNMAX)
        real(kind=real64b) :: s3_dz(NNMAX)
        real(kind=real64b) :: s3_r(NNMAX)

        integer :: n3
        !   size of s3[]
        integer :: num3(NNMAX)
        !   atom ID numbers for s3[]
        ! Janne Nord: simplified usage of sz_sum: every array element
        ! always got the same value, so there is no need to
        ! have it as an array.


        real(kind=real64b) :: sz_df(NNMAX)
        real(kind=real64b) :: sz_sum
        real(kind=real64b) :: sz_dx(NNMAX)
        real(kind=real64b) :: sz_dy(NNMAX)
        real(kind=real64b) :: sz_dz(NNMAX)
        real(kind=real64b) :: sz_r(NNMAX)
        integer :: nz
        !   size of sz[]
        integer :: numz(NNMAX)
        !   atom ID numbers for sz[]

        integer :: nj,nk,nl
        !   indices for the store arrays

        real(kind=real64b) :: L_x_div_2, L_y_div_2, L_z_div_2
        real(kind=real64b) :: L_x,L_y,L_z

        integer, parameter :: fixZ = 0
        integer, parameter :: tricks_Zfix = 5


        !     lparams
        integer :: k,neip,indi,indj,indk,indl,typei,nneip,latn,typej
        real(kind=real64b) :: tran,trans
        real(kind=real64b) :: fermi,dfermi

        !
        !     PARAMETERS FOR INTERFACE AND PARALLELLIZATION
        !
        integer :: i,j
        real(kind=real64b) :: r,dummy1,dummy2,trans2,trans3
        real(real64b) :: t1

        !-----------------------------------------------------------------------


        ! Convert from internal units to Angstrom.
        ! Use buf(i) instead of x0(i) in the computations.
        do i = 1, 3*np0pairtable, 3
            buf(i+0) = x0(i+0) * box(1)
            buf(i+1) = x0(i+1) * box(2)
            buf(i+2) = x0(i+2) * box(3)
        end do


        !-----------------------------------------------------------------------------
        ! Compute the accelerations into xnp in actual units, then convert
        ! to internal units at the end. Compute energies into Epair and Ethree.
        !
        ! Take care to calculate pressure, total pot. only for atoms in my node!
        !
        ! Boundary conditions:
        ! If pcb == 1.0d0, periodic
        ! Note that different conditions may be specified in x, y and z direction.
        !
        ! rnmef and rnlef are the differences between distance and the potential cutoff.
        !
        ! N.B. No particles are moved at this stage.
        !

        ! Initialize force calculation stuff

        L_x       = box(1)
        L_y       = box(2)
        L_z       = box(3)

        L_x_div_2 = L_x*0.5D0
        L_y_div_2 = L_y*0.5D0
        L_z_div_2 = L_z*0.5D0

        Epair(:myatoms) = 0.0
        xnp(:3*np0pairtable) = 0.0

        do N = 1,myatoms
            Ethree(N) = 0d0
            wxxi(N) = 0d0
            wyyi(N) = 0d0
            wzzi(N) = 0d0
        enddo

        if (calc_vir) then
            do N = 1,myatoms
                wxyi(N) = 0d0
                wxzi(N) = 0d0
                wyzi(N) = 0d0
            enddo
        endif

        !     CALCULATE ACCELERATIONS USING NEIGHBOUR LIST
        !     --------------------------------------------
        !

        !   COMBINE COEFFICIENTS

        asqr = par_a*par_a
        Qort = sqrt(par_Qo)
        muhalf = par_mu*0.5D0
        u5 = u2*u4
        bmc = par_b-par_c
        cmbinv = 1.0D0/(par_c-par_b)

        !  --- LEVEL 1: OUTER LOOP OVER ATOMS ---

        neip=0

        do i=1, myatoms

            !   RESET COORDINATION AND NEIGHBOR NUMBERS

            Z = 0.0
            n2 = 1
            n3 = 1
            nz = 1


            !  --- LEVEL 2: LOOP PREPASS OVER PAIRS ---

            neip  = neip+1
            indi  = i*3-2
            typei = abs(atype(i))
            nneip = nborlist(neip)

            do latn=1, nneip
                neip=neip+1
                j=nborlist(neip)
                typej = abs(atype(j))
                if(iac(typei,typej)==0) cycle
                if(iac(typei,typej)==-1) then
                    call logger("ERROR: IMPOSSIBLE INTERACTION")
                    call my_mpi_abort('INTERACTION -1', int(myproc))
                endif

                indj=j*3-2
                if (indi==indj) cycle

                dx=buf(indj)-buf(indi)
                if(pbc(1)==1.0d0) then
                    if (dx .gt. L_x_div_2) then
                        dx = dx - L_x
                    else if (dx .lt. -L_x_div_2) then
                        dx = dx + L_x
                    end if
                end if
                if (dabs(dx) >= par_a) cycle

                dy = buf(indj+1)-buf(indi+1)
                if(pbc(2)==1.0d0) then
                    if (dy .gt. L_y_div_2) then
                        dy = dy - L_y
                    else if (dy .lt. -L_y_div_2) then
                        dy = dy + L_y
                    end if
                end if
                if (dabs(dy) >= par_a) cycle

                dz = buf(indj+2)-buf(indi+2)
                if(pbc(3)==1.0d0) then
                    if (dz .gt. L_z_div_2) then
                        dz = dz - L_z
                    else if (dz .lt. -L_z_div_2) then
                        dz = dz + L_z
                    end if
                end if
                if (dabs(dz) >= par_a) cycle

                rsqr = dx*dx + dy*dy + dz*dz
                if (rsqr >= asqr) cycle

                r = sqrt(rsqr)


                if (iac(typei,typej) == 2) then
                    call reppot_only(r,tran,trans,typei,typej)
                    tran=tran*0.5
                    trans=trans*0.5
                    Epair(i)=Epair(i)+tran
                    dummy1=trans/r
                    dummy2=dx*dummy1
                    xnp(indi)=xnp(indi)+dummy2
                    xnp(indj)=xnp(indj)-dummy2
                    wxxi(i)=wxxi(i)+dummy2*dx

                    if (calc_vir) then
                        wxyi(i)=wxyi(i)+dummy2*dy
                        wxzi(i)=wxzi(i)+dummy2*dz
                    endif

                    dummy2=dy*dummy1
                    xnp(indi+1)=xnp(indi+1)+dummy2
                    xnp(indj+1)=xnp(indj+1)-dummy2
                    wyyi(i)=wyyi(i)+dummy2*dy

                    if (calc_vir) then
                        wyzi(i)=wyzi(i)+dummy2*dz
                    endif

                    dummy2=dz*dummy1
                    xnp(indi+2)=xnp(indi+2)+dummy2
                    xnp(indj+2)=xnp(indj+2)-dummy2
                    wzzi(i)=wzzi(i)+dummy2*dz

                    cycle
                endif

                !   PARTS OF TWO-BODY INTERACTION r<par_a

                num2(n2) = j
                rinv = 1.0d0/r
                dx = dx * rinv
                dy = dy * rinv
                dz = dz * rinv
                rmainv = 1.0d0/(r-par_a)
                s2_t0(n2) = par_cap_A*dexp(par_sig*rmainv)
                s2_t1(n2) = (par_cap_B*rinv)**par_rh
                s2_t2(n2) = par_rh*rinv
                s2_t3(n2) = par_sig*rmainv*rmainv
                s2_dx(n2) = dx
                s2_dy(n2) = dy
                s2_dz(n2) = dz
                s2_r(n2) = r
                n2 = n2 + 1

                !   RADIAL PARTS OF THREE-BODY INTERACTION r<par_b

                if (r >= par_bg) cycle

                num3(n3) = j
                rmbinv = 1.0d0/(r-par_bg)
                temp1 = par_gam*rmbinv
                temp0 = dexp(temp1)
                s3_g(n3) = temp0
                s3_dg(n3) = -rmbinv*temp1*temp0
                s3_dx(n3) = dx
                s3_dy(n3) = dy
                s3_dz(n3) = dz
                s3_rinv(n3) = rinv
                s3_r(n3) = r
                n3 = n3 + 1

                if (fixZ /= 0) cycle

                !   COORDINATION AND NEIGHBOR FUNCTION par_c<r<par_b

                if (r < par_b) then
                    if (r < par_c) then
                        Z = Z + 1.0d0
                    else
                        xinv = bmc/(r-par_c)
                        xinv3 = xinv*xinv*xinv
                        den = 1.0d0/(1 - xinv3)
                        temp1 = par_alp*den
                        fZ = dexp(temp1)
                        Z = Z + fZ
                        numz(nz) = j
                        sz_df(nz) = fZ*temp1*den*3.0*xinv3*xinv*cmbinv
                        !   df/dr
                        sz_dx(nz) = dx
                        sz_dy(nz) = dy
                        sz_dz(nz) = dz
                        sz_r(nz) = r
                        nz = nz + 1
                    end if
                    !  r < par_C
                end if
                !  r < par_b
            end do

            if (fixZ /= 0) then

                Z = tricks_Zfix
                pZ = par_palp*dexp(-par_bet*Z*Z)
                dp = 0.0

            else
                !   ZERO ACCUMULATION ARRAY FOR ENVIRONMENT FORCES

                sz_sum=0.0

                !   ENVIRONMENT-DEPENDENCE OF PAIR INTERACTION

                temp0 = par_bet*Z
                pZ = par_palp*dexp(-temp0*Z)
                !   bond order
                dp = -2.0*temp0*pZ
                !   derivative of bond order

            end if


            !  --- LEVEL 2: LOOP FOR PAIR INTERACTIONS ---

            do nj=1, n2-1

                temp0 = s2_t1(nj) - pZ
                j = num2(nj)
                indj = j*3-2
                typej = abs(atype(j))

                !   two-body energy V2(rij,Z)
                trans = temp0*s2_t0(nj)
                dV2j = - (s2_t0(nj)) * ((s2_t1(nj))*(s2_t2(nj)) + temp0 * (s2_t3(nj)))

                fermi=1d0
                dfermi=1d0

                if (s2_r(nj) < reppotcut) then
                    trans2=0
                    trans3=0
                    call reppot_fermi(s2_r(nj),trans2,trans3,14.0d0,1.50d0,typei,typej,fermi,dfermi)
                    dV2j =dV2j *fermi+0.5*trans3+dfermi*trans
                    trans=trans*fermi+0.5*trans2
                endif

                Epair(i) = Epair(i) + trans


                !   two-body forces

                !   dV2/dr
                dV2ijx = dV2j * s2_dx(nj)
                dV2ijy = dV2j * s2_dy(nj)
                dV2ijz = dV2j * s2_dz(nj)
                xnp(indi)   = xnp(indi) + dV2ijx
                xnp(indi+1) = xnp(indi+1) + dV2ijy
                xnp(indi+2) = xnp(indi+2) + dV2ijz

                xnp(indj)   = xnp(indj)   - dV2ijx
                xnp(indj+1) = xnp(indj+1) - dV2ijy
                xnp(indj+2) = xnp(indj+2) - dV2ijz


                !   dV2/dr contribution to virial

                wxxi(i) = wxxi(i) - s2_r(nj)*(dV2ijx*s2_dx(nj))
                wyyi(i) = wyyi(i) - s2_r(nj)*(dV2ijy*s2_dy(nj))
                wzzi(i) = wzzi(i) - s2_r(nj)*(dV2ijz*s2_dz(nj))

                if (calc_vir) then
                    wxyi(i)=wxyi(i) - s2_r(nj)*(dV2ijx*s2_dy(nj))
                    wxzi(i)=wxzi(i) - s2_r(nj)*(dV2ijx*s2_dz(nj))
                    wyzi(i)=wyzi(i) - s2_r(nj)*(dV2ijy*s2_dz(nj))
                endif

                if(fixZ .eq. 0) then

                    !  --- LEVEL 3: LOOP FOR PAIR COORDINATION FORCES ---

                    dV2dZ = - dp * s2_t0(nj)
                    sz_sum =  sz_sum + dV2dZ

                end if
                !  fixZ
            end do

            if(fixZ .ne. 0) then
                winv = Qort*dexp(-muhalf*Z)
                dwinv = 0.0
                temp0 = dexp(-u4*Z)
                tau = u1+u2*temp0*(u3-temp0)
                dtau = 0.0
            else

                !   COORDINATION-DEPENDENCE OF THREE-BODY INTERACTION

                winv = Qort*exp(-muhalf*Z)
                !   inverse width of angular function
                dwinv = -muhalf*winv
                !   its derivative
                temp0 = exp(-u4*Z)
                tau = u1+u2*temp0*(u3-temp0)
                !   -cosine of angular minimum
                dtau = u5*temp0*(2*temp0-u3)
                !   its derivative
            end if


            !  --- LEVEL 2: FIRST LOOP FOR THREE-BODY INTERACTIONS ---

            do nj=1, n3-2
                j = num3(nj)
                indj=j*3-2

                !  --- LEVEL 3: SECOND LOOP FOR THREE-BODY INTERACTIONS ---

                do nk=nj+1, n3-1
                    k = num3(nk)
                    indk=k*3-2
                    !   angular function h(l,Z)

                    lcos = s3_dx(nj) * s3_dx(nk) + s3_dy(nj) * s3_dy(nk)+ s3_dz(nj) * s3_dz(nk)
                    x_ = (lcos + tau)*winv
                    temp0 = exp(-x_*x_)

                    H = par_lam*(1 - temp0 + par_eta*x_*x_)
                    dHdx = 2*par_lam*x_*(temp0 + par_eta)

                    dhdl = dHdx*winv


                    !   three-body energy

                    temp1 = s3_g(nj) * s3_g(nk)
                    trans = temp1*H
                    Ethree(i)=Ethree(i)+trans


                    !   (-) radial force on atom j

                    dV3rij = s3_dg(nj) * s3_g(nk) * H
                    dV3rijx = dV3rij * s3_dx(nj)
                    dV3rijy = dV3rij * s3_dy(nj)
                    dV3rijz = dV3rij * s3_dz(nj)
                    fjx = dV3rijx
                    fjy = dV3rijy
                    fjz = dV3rijz


                    !   (-) radial force on atom k

                    dV3rik = s3_g(nj) * s3_dg(nk) * H
                    dV3rikx = dV3rik * s3_dx(nk)
                    dV3riky = dV3rik * s3_dy(nk)
                    dV3rikz = dV3rik * s3_dz(nk)
                    fkx = dV3rikx
                    fky = dV3riky
                    fkz = dV3rikz


                    !   (-) angular force on j

                    dV3l = temp1*dhdl
                    dV3ljx = dV3l * (s3_dx(nk) - lcos * s3_dx(nj)) * s3_rinv(nj)
                    dV3ljy = dV3l * (s3_dy(nk) - lcos * s3_dy(nj)) * s3_rinv(nj)
                    dV3ljz = dV3l * (s3_dz(nk) - lcos * s3_dz(nj)) * s3_rinv(nj)
                    fjx = fjx + dV3ljx
                    fjy = fjy + dV3ljy
                    fjz = fjz + dV3ljz


                    !   (-) angular force on k

                    dV3lkx = dV3l * (s3_dx(nj) - lcos * s3_dx(nk)) * s3_rinv(nk)
                    dV3lky = dV3l * (s3_dy(nj) - lcos * s3_dy(nk)) * s3_rinv(nk)
                    dV3lkz = dV3l * (s3_dz(nj) - lcos * s3_dz(nk)) * s3_rinv(nk)
                    fkx = fkx + dV3lkx
                    fky = fky + dV3lky
                    fkz = fkz + dV3lkz



                    !   apply radial + angular forces to i, j, k

                    xnp(indj)   = xnp(indj) - fjx
                    xnp(indj+1) = xnp(indj+1) - fjy
                    xnp(indj+2) = xnp(indj+2) - fjz
                    xnp(indk)   = xnp(indk) - fkx
                    xnp(indk+1) = xnp(indk+1) - fky
                    xnp(indk+2) = xnp(indk+2) - fkz
                    xnp(indi)   = xnp(indi) + fjx + fkx
                    xnp(indi+1) = xnp(indi+1) + fjy + fky
                    xnp(indi+2) = xnp(indi+2) + fjz + fkz

                    !   dV3/dR contributions to virial

                    wxxi(i) = wxxi(i) - s3_r(nj)*(fjx*s3_dx(nj))
                    wyyi(i) = wyyi(i) - s3_r(nj)*(fjy*s3_dy(nj))
                    wzzi(i) = wzzi(i) - s3_r(nj)*(fjz*s3_dz(nj))
                    wxxi(i) = wxxi(i) - s3_r(nk)*(fkx*s3_dx(nk))
                    wyyi(i) = wyyi(i) - s3_r(nk)*(fky*s3_dy(nk))
                    wzzi(i) = wzzi(i) - s3_r(nk)*(fkz*s3_dz(nk))

                    if (calc_vir) then
                        wxyi(i)=wxyi(i) - s3_r(nj)*(fjx*s3_dy(nj))
                        wxzi(i)=wxzi(i) - s3_r(nj)*(fjx*s3_dz(nj))
                        wyzi(i)=wyzi(i) - s3_r(nj)*(fjy*s3_dz(nj))

                        wxyi(i)=wxyi(i) - s3_r(nk)*(fkx*s3_dy(nk))
                        wxzi(i)=wxzi(i) - s3_r(nk)*(fkx*s3_dz(nk))
                        wyzi(i)=wyzi(i) - s3_r(nk)*(fky*s3_dz(nk))
                    endif

                    if(fixZ .eq. 0) then


                        !   prefactor for 4-body forces from coordination
                        dxdZ = dwinv*(lcos + tau) + winv*dtau
                        dV3dZ = temp1*dHdx*dxdZ


                        !  --- LEVEL 4: LOOP FOR THREE-BODY COORDINATION FORCES ---

                        sz_sum = sz_sum + dV3dZ

                    end if
                end do
            end do

            if(fixZ .eq. 0) then

                !  --- LEVEL 2: LOOP TO APPLY COORDINATION FORCES ---
                do nl=1, nz-1

                    dEdrl = sz_sum * sz_df(nl)
                    dEdrlx = dEdrl * sz_dx(nl)
                    dEdrly = dEdrl * sz_dy(nl)
                    dEdrlz = dEdrl * sz_dz(nl)
                    xnp(indi)   = xnp(indi) + dEdrlx
                    xnp(indi+1) = xnp(indi+1) + dEdrly
                    xnp(indi+2) = xnp(indi+2) + dEdrlz
                    l = numz(nl)
                    indl = l*3-2
                    xnp(indl)   = xnp(indl) - dEdrlx
                    xnp(indl+1) = xnp(indl+1) - dEdrly
                    xnp(indl+2) = xnp(indl+2) - dEdrlz


                    ! dE/dZ*dZ/dr contribution to virial

                    wxxi(i) = wxxi(i) - sz_r(nl)*(dEdrlx*sz_dx(nl))
                    wyyi(i) = wyyi(i) - sz_r(nl)*(dEdrly*sz_dy(nl))
                    wzzi(i) = wzzi(i) - sz_r(nl)*(dEdrlz*sz_dz(nl))

                    if (calc_vir) then
                        wxyi(i)=wxyi(i) - sz_r(nl)*(dEdrlx*sz_dy(nl))
                        wxzi(i)=wxzi(i) - sz_r(nl)*(dEdrlx*sz_dz(nl))
                        wyzi(i)=wyzi(i) - sz_r(nl)*(dEdrly*sz_dz(nl))
                    endif
                end do
            end if

        end do  ! End of loop over atoms i

        !**********************************************************************


        ! Send back forces to the neighboring nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            ! No need to send Epair, since it is calculated twice for
            ! each pair (once for each atom).
            call potential_pass_back_border_atoms(xnp)
            tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
        end if


        ! Convert from eV/A to internal units.
        do i = 1, 3*myatoms, 3
            xnp(i+0) = xnp(i+0) / box(1)
            xnp(i+1) = xnp(i+1) / box(2)
            xnp(i+2) = xnp(i+2) / box(3)
        end do

    end subroutine EDIP_forces

    !-----------------------------------------------------------------------------


    subroutine Init_Edip(reppotcutin)

        integer :: i,j
        real(kind=real64b) :: reppotcutin

        par_cap_A = 5.6714030

        call logger("EDIP initialization")

        par_cap_B = 2.0002804
        par_rh    = 1.2085196
        par_a     = 3.1213820
        par_sig   = 0.5774108
        par_lam   = 1.4533109
        par_gam   = 1.1247945
        par_b     = 3.1213820
        par_c     = 2.5609104
        par_mu    = 0.6966326
        par_Qo    = 312.1341346
        par_palp  = 1.4074424
        par_bet   = 0.0070975
        par_alp   = 3.1083847



        u1 = -0.165799
        u2 = 32.557
        u3 = 0.286198
        u4 = 0.66

        par_delta =78.7590539
        par_eta = par_delta/par_Qo

        delta_safe = 0.2
        pot_cutoff = par_a
        par_bg=par_a

        do i=itypelow,itypehigh
            do j=itypelow,itypehigh
                if (iac(i,j) == 1) then
                    if (element(i).ne.'Si') then
                        call my_mpi_abort('Invalid atom type for EDIP', int(i))
                    end if
                    reppotcut=pot_cutoff
                    if(reppotcutin<pot_cutoff) reppotcut=reppotcutin
                end if
                rcut(i,j)=pot_cutoff
            enddo
        enddo

    end subroutine Init_Edip


end module edip_mod
