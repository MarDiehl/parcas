!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module mdcasc_mod

    use iso_fortran_env, only: IOSTAT_END, IOSTAT_EOR
    use datatypes, only: real64b
    use PhysConsts, only: pi, e, kB
    use file_units, only: &
        TEMP_FILE, &
        TRACK_ARRAY_FILE, &
        OUT_RECOIL_FILE, &
        OUT_PRESSURES_FILE, &
        OUT_LIQUIDAT_FILE

    use typeparam

    use para_common, only: &
        myatoms, np0pairtable, &
        iprint, debug, &
        dbuf, pbuf, ibuf, buf

    use mdparsubs_mod, only: &
        pass_border_atoms, &
        pass_back_border_atoms, &
        PassPacker, PassBackPacker

    use random, only: MyRanf
    use output_logger, only: log_buf, logger
    use my_mpi

    implicit none
    save

    private
    public :: Init_Units
    public :: Get_Tstep
    public :: get_maximum_force
    public :: print_recoil_position
    public :: print_pressures
    public :: get_ecm
    public :: liquid_analysis
    public :: read_recoil_sequence_data
    public :: getnextrec
    public :: addvel
    public :: addtrack

    type, extends(PassPacker) :: LiqanalPassPacker
        real(real64b), pointer, contiguous :: x0(:)
        real(real64b), pointer, contiguous :: Ekin(:)
    contains
        procedure :: do_pack => liqanal_pass_pack
        procedure :: do_unpack => liqanal_pass_unpack
    end type

    type, extends(PassBackPacker) :: LiqanalPassBackPacker
    contains
        procedure :: do_pack => liqanal_pass_back_pack
        procedure :: do_unpack => liqanal_pass_back_unpack
    end type


    ! Arrays of recoil data.
    integer, allocatable, dimension(:) :: rsirec, rstype
    real(real64b), allocatable, dimension(:) :: &
        rsxrec, rsyrec, rszrec, rstheta, rsphi, rsE, rst


  contains

    !
    !  Subroutines related to cascade calculations for PARCAS
    !
    !  Added to code beginning 05/07/96
    !

    !
    ! Table of internal units/conversions.
    ! Internal units all have mass m == 1, so the change to real
    ! units involves factors of m.
    !
    ! Internal quantity   To get units...  Multiply with
    ! energy E            eV               1
    ! mass m              u                1
    ! distance r          A                1
    ! velocity v          A/fs             0.0982266/sqrt(m)
    ! acceleration a      A/fs^2           0.0096485/m
    ! time t              fs               10.1805*sqrt(m)
    ! force F             eV/A             1
    !
    ! These do not yet contain scaling by box size or time step.
    ! The arrays x0, x1, ..., and xnp are scaled by the box size.
    ! x0, ..., x5 also contain factors of (dt^i)/(i!) (like the
    ! Taylor series).
    !
    ! In MD units E = 0.5*v^2, F = a, and p = v.
    ! The velocity unit for SI is then
    ! vunit = sqrt(eV/m_i) m/s
    !       = sqrt((eV/u)/m_i[u]) m/s
    !       = 9822.66/sqrt(m_i[u]) m/s
    ! timeunit follows from t = r / v
    ! aunit follows from a = v / t
    !
    subroutine Init_Units()

        integer :: i

        if (iprint) then
            call logger("Units:", 2)
            call logger("------", 2)
        end if

        do i=itypelow,itypehigh
            if (mass(i) <= 0d0) then
                call logger("ERROR Invalid mass found for type:", i, 0)
                call my_mpi_abort("Zero mass", INT(i))
            endif

            timeunit(i)=10.1805*SQRT(mass(i))
            vunit(i)=0.0982266/SQRT(mass(i))
            aunit(i)=0.0096485/mass(i)

            ! TODO: add physical units to the output
            if (iprint) then
                call logger("Type:", i, 6)
                call logger("mass: ", mass(i), 10)
                call logger("tunit:", timeunit(i), 10)
                call logger("vunit:", vunit(i), 10)
                call logger("aunit:", aunit(i), 10)
            end if

        enddo

        if (iprint) then
            call logger()
            call logger()
        end if

    end subroutine Init_Units


    !*********************************************************************
    !
    ! Calculate time step, following kt and Et criteria in
    ! Nordlund, Comp. Mat. Sci. 3 (1995) 448
    !
    ! Time step units are sqrt(atom_mass*u*Angstrom**2/e) where u and e
    ! are the constant values.
    !
    ! Returns the internal time step delta(ntype) and deltas(ntype),
    ! the real time step deltat_fs and the time step ratio.
    !
    !
    subroutine Get_Tstep(adaptive_dt, vmax, Fmax, timekt, timeEt, timeCh, &
            deltat_fs, deltamax_fs, delta, deltas, deltaratio, rstmax)

        real(kind=real64b), intent(out) :: delta(itypelow:itypehigh), deltas(itypelow:itypehigh)
        real(kind=real64b), intent(out) :: deltaratio
        real(kind=real64b), intent(inout) :: deltat_fs

        logical, intent(in) :: adaptive_dt
        real(kind=real64b), intent(in) :: vmax, Fmax, timekt, timeEt, timeCh
        real(kind=real64b), intent(in) :: rstmax
        real(kind=real64b), intent(in) :: deltamax_fs

        real(kind=real64b), save :: prevdeltat_fs
        logical, save :: firsttime = .true.
        integer :: i

        !
        ! First get time step in units of fs. vmax is max velocity in A/fs.
        !
        deltat_fs = deltamax_fs

        if (adaptive_dt) then
            ! Do not allow large displacements in one step.
            if (vmax > 0) then
                deltat_fs = min(deltat_fs, timekt / vmax)
            end if

            ! Do not allow large changes in atom energies in one step.
            if (vmax > 0 .and. Fmax > 0) then
                deltat_fs = min(deltat_fs, timeEt / (vmax * Fmax))
            end if

            ! Do not allow large change from previous step.
            if (.not. firsttime) then
                deltat_fs = min(deltat_fs, timeCh * prevdeltat_fs)
            end if

            ! Necessary for recoil sequences, otherwise not needed
            ! Constant 0.01 comes from the fact that it should be larger
            ! than the ratio timeCh, which is 1.1 per default
            deltat_fs = min(deltat_fs, rstmax / (timeCh + 0.01))
        end if

        if (firsttime) then
            deltaratio = 1d0
        else
            deltaratio = deltat_fs / prevdeltat_fs
        end if
        prevdeltat_fs = deltat_fs

        !
        ! Then transform into internal units.
        !
        do i = itypelow, itypehigh
            delta(i) = deltat_fs / timeunit(i)
            deltas(i) = 0.5d0 * delta(i)**2

            if (firsttime .and. iprint) then
                write(log_buf, "(A,I3,A,2G13.6)") &
                    "Atom type", i, &
                    " Initial time step (fs, internal un.)", &
                    deltat_fs, delta(i)
                call logger(log_buf)
            endif
        enddo

        firsttime = .false.
    end subroutine Get_Tstep


    !***********************************************************************
    !***********************************************************************
    !***********************************************************************

    !
    !     Get maximum absolute force, in units of eV/A
    !
    subroutine get_maximum_force(Fmax, Fmaxnosput, myatoms, timesputlim, nisputlim, &
            box, xnp, x0, Epair, Ethree)

        use mdsubs_mod, only: is_sputtered_atom

        real(real64b), intent(out) :: Fmax, Fmaxnosput

        integer, intent(in) :: myatoms
        real(real64b), intent(in) :: timesputlim, nisputlim
        real(real64b), intent(in), contiguous :: xnp(:)
        real(real64b), intent(in), contiguous :: x0(:)
        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: Epair(:), Ethree(:)

        integer :: i, i3
        real(real64b) :: F

        Fmax = -1.0d0
        Fmaxnosput = -1.0d0

        do i = 1, myatoms
            i3 = i*3 - 3

            F = sqrt(sum((xnp(i3+1:i3+3) * box)**2))
            Fmax = max(Fmax, F)

            if (.not. is_sputtered_atom(x0(i3+1:i3+3), timesputlim, nisputlim, &
                            Epair(i), Ethree(i))) then
                Fmaxnosput = max(Fmaxnosput, F)
            end if
        enddo

        call my_mpi_max(Fmax, 1)
        call my_mpi_max(Fmaxnosput, 1)
    end subroutine get_maximum_force


    !***********************************************************************
    !***********************************************************************
    !***********************************************************************

    subroutine print_recoil_position(x0, Ekin, box, time_fs, Ekrec, &
            istep, nliqanal, irec, irecproc)

        integer, intent(in) :: istep, nliqanal, irec, irecproc
        real(real64b), intent(in) :: x0(:), Ekin(:), box(3)
        real(real64b), intent(in) :: time_fs, Ekrec

        integer :: i, i3, ierror
        real(real64b) :: tmpbuf(4)

        if (debug) print *, 'recoilat output', myproc, irecproc

        if (irecproc == myproc) then
            ! Pack data of recoil for sending to printing process
            i3 = irec*3 - 3
            tmpbuf(1:3) = x0(i3+1:i3+3)
            tmpbuf(4) = Ekin(irec)

            if (myproc /= 0) then
                call MPI_Send(tmpbuf, 4, mpi_double_precision, 0, &
                    12345, MPI_COMM_WORLD, ierror)
            end if
        end if

        if (iprint) then
            if (myproc /= irecproc) then
                call MPI_Recv(tmpbuf, 4, mpi_double_precision, irecproc, &
                    12345, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
            end if

            if (tmpbuf(4) > Ekrec .or. MOD(istep, nliqanal) == 0) then
                ! Write to recoilat.out
                write (OUT_RECOIL_FILE, "(3F10.4,F10.1,I8,F15.2,I3)") &
                    tmpbuf(1:3)*box, &
                    time_fs, irec, tmpbuf(4), &
                    irecproc
            end if

            i = nliqanal
            if (i > 10) i = i / 5

            if (MOD(istep, i) == 0) then
                ! Write to md.out
                write(log_buf,"(A,3F10.4,F10.1,I8,F15.2,I3)") 'rec ', &
                    tmpbuf(1:3)*box, &
                    time_fs, irec, tmpbuf(4), &
                    irecproc
                call logger(log_buf)
            end if
        end if
    end subroutine print_recoil_position


    !***********************************************************************
    !***********************************************************************
    !***********************************************************************


    !
    ! Calculate temperature and pressure in concentric spherical
    ! shells around the energy center ECM. The largest shell
    ! contains all atoms.
    !
    subroutine print_pressures(natoms, x0, box, Ekin, wxxi, wyyi, wzzi, time_fs, ECM)

        integer, intent(in) :: natoms
        real(real64b), intent(in) :: x0(:)
        real(real64b), intent(in) :: box(3), ECM(3)
        real(real64b), intent(in) :: Ekin(:)
        real(real64b), intent(in) :: wxxi(:), wyyi(:), wzzi(:)
        real(real64b), intent(in) :: time_fs

        integer :: nnsh, i, i3, j
        real(real64b) :: maxr, r
        real(real64b) :: Temp, Pxx, Pyy, Pzz, Vol

        integer, parameter :: nsh = 18 ! Number of concentric shells

        integer :: natomsh(nsh)      ! Number of atoms in the shell
        real(real64b) :: Ekinsh(nsh) ! Kinetic energy in the shell
        real(real64b) :: wxxsh(nsh)  ! Virials in the shell
        real(real64b) :: wyysh(nsh)
        real(real64b) :: wzzsh(nsh)

        if (debug) print *, 'pressure output', myproc

        natomsh(:) = 0
        Ekinsh(:) = 0.0
        wxxsh(:) = 0.0
        wyysh(:) = 0.0
        wzzsh(:) = 0.0

        maxr = sqrt(sum(box**2)) / 2.0d0

        do i = 1, myatoms
            i3 = 3*i - 3

            ! Distance from the energy center ECM.
            r = sqrt(sum((x0(i3+1:i3+3)*box(:) - ECM(:))**2))

            ! Number of the spherical shell around the ECM that the atom is in.
            nnsh = int(1.0d0 + r/maxr * (nsh - 1))
            nnsh = min(nnsh, nsh)

            natomsh(nnsh:nsh) = natomsh(nnsh:nsh) + 1
            Ekinsh(nnsh:nsh) = Ekinsh(nnsh:nsh) + Ekin(i)
            wxxsh(nnsh:nsh) = wxxsh(nnsh:nsh) + wxxi(i)
            wyysh(nnsh:nsh) = wyysh(nnsh:nsh) + wyyi(i)
            wzzsh(nnsh:nsh) = wzzsh(nnsh:nsh) + wzzi(i)
        end do

        call my_mpi_sum(natomsh, nsh)
        call my_mpi_sum(Ekinsh, nsh)
        call my_mpi_sum(wxxsh, nsh)
        call my_mpi_sum(wyysh, nsh)
        call my_mpi_sum(wzzsh, nsh)

        do j = 1, nsh
            Vol = natomsh(j) * product(box) / natoms * 1.0d-30

            if (natomsh(j) > 0) then
                Temp = Ekinsh(j) * 2d0/3d0 / natomsh(j) * e/kB
            else
                Temp = 0.0
            end if

            ! Pressure terms. The factor in front is (Pascal -> kBar)
            ! This calculation assumes E_k is isotropic in x, y and z.
            Pxx = 0.0
            Pyy = 0.0
            Pzz = 0.0
            if (Vol > 0.0) then
                Pxx = (1.0/1.0d8) * (natomsh(j)*kB*Temp + wxxsh(j)*e) / Vol
                Pyy = (1.0/1.0d8) * (natomsh(j)*kB*Temp + wyysh(j)*e) / Vol
                Pzz = (1.0/1.0d8) * (natomsh(j)*kB*Temp + wzzsh(j)*e) / Vol
            end if

            if (iprint) then
                write (OUT_PRESSURES_FILE, "(A,F10.1,I3,I8,1X,F9.2,3F8.2)") &
                    "t shell N T Pxx Pyy Pzz", &
                    time_fs, j, natomsh(j), Temp, Pxx, Pyy, Pzz
            end if
        end do
    end subroutine print_pressures

    !***********************************************************************
    !***********************************************************************
    !***********************************************************************

    !
    !     Get energy center of mass (ECM) position of cascade
    !
    subroutine get_ecm(ECM, Ekinsum, x0, box, Ekin)

        real(kind=real64b), intent(inout) :: ECM(3), Ekinsum
        real(kind=real64b), intent(in) :: x0(:), box(3), Ekin(:)

        integer :: i, i3
        real(real64b) :: tempbuf(4)

        if (debug) print *, 'ECM calc', myproc

        ECM = 0.0
        Ekinsum = 0.0

        do i = 1, myatoms
            i3 = 3*i - 3
            ECM(1) = ECM(1) + x0(i3+1) * Ekin(i)
            ECM(2) = ECM(2) + x0(i3+2) * Ekin(i)
            ECM(3) = ECM(3) + x0(i3+3) * Ekin(i)
            Ekinsum = Ekinsum + Ekin(i)
        end do

        tempbuf(1:3) = ECM(1:3)
        tempbuf(4) = Ekinsum
        call my_mpi_sum(tempbuf, 4)
        ECM(1:3) = tempbuf(1:3)
        Ekinsum = tempbuf(4)

        if (Ekinsum > 0.0) then
            ECM = ECM / Ekinsum * box
        end if

    end subroutine get_ecm


    !***********************************************************************
    !***********************************************************************
    !***********************************************************************

    !
    !  Do liquid analysis.
    !
    subroutine liquid_analysis(time_fs, Ekdef, nabors, nborlist, &
            x0, atomindex, Ekin, box, pbc, unitcell, &
            latflag, half_neighbor_list)

        integer, intent(in) :: latflag
        integer, intent(in), contiguous :: nabors(:), nborlist(:)
        integer, intent(in), contiguous :: atomindex(:)
        real(real64b), intent(in) :: time_fs
        real(real64b), intent(inout), contiguous, target :: x0(:)
        real(real64b), intent(inout), contiguous, target :: Ekin(:)
        real(real64b), intent(in) :: box(3), pbc(3), unitcell(:)
        real(real64b), intent(in) :: Ekdef
        logical, intent(in) :: half_neighbor_list

        integer :: nliqat
        integer :: ierror ! MPI error code

        !  Note that since the neighbour list calc. with half_neighbor_list==.true.
        !  includes only half the neighbours, we have to get Ekin for all _pairs_
        !  into Ekin first, then decide which atoms are liquid.

        if (debug) print *, 'liquidat output', myproc

        ! Collect position and Ekin data about atoms at the process
        ! boundaries from neighboring processes. The data can then be
        ! found normally in x0 and Ekin.
        if (nprocs > 1) then
            block
                type(LiqanalPassPacker) :: pass_packer

                pass_packer%x0 => x0
                pass_packer%Ekin => Ekin

                call pass_border_atoms(pass_packer)
            end block
        end if

        ! Buffers for pairwise logic:
        ! dbuf contains sum of Ekin over neighbours
        ! pbuf contains max of Ekin over neighbours
        ! ibuf contains number of neighbours

        ! Count each Ekin only once, so own atoms start with dbuf,...
        ! filled. Other atoms start with zero.
        dbuf(:myatoms) = Ekin(:myatoms)
        pbuf(:myatoms) = Ekin(:myatoms)
        ibuf(:myatoms) = 1
        dbuf(myatoms+1 : np0pairtable) = 0.0
        pbuf(myatoms+1 : np0pairtable) = 0.0
        ibuf(myatoms+1 : np0pairtable) = 0


        !  Now we have all x0 and Ekin information we need, let's get Eksum
        !  for all atoms and their nearest neighbours within rsumcrit.
        !  Store the Ekin sums for each pair into dbuf, remembering that
        !  the neighbor list might contain each pair only once.
        block
            integer :: i, i3, j, j3
            integer :: nnbors, nbr, nlistpos
            real(real64b) :: rs, xp(3)
            real(real64b) :: rsumcrit, rsumcritsq

            if (latflag == 0) then ! FCC
                rsumcrit = unitcell(1) * (1.0d0 / (2.0d0*sqrt(2.0d0)) + 0.5d0)
            else if (latflag == 2) then ! DIA
                rsumcrit = unitcell(1) * (sqrt(3.0d0)/4.0d0 + 1.0d0/sqrt(2.0d0)) / 2.0d0
            else
                rsumcrit = sum(unitcell(1:3)) / 3.0d0 - 0.1d0
            endif
            rsumcritsq = rsumcrit**2

            ! Get Ekinsum of all pairs into dbuf, Ekinmax into pbuf
            do i = 1, myatoms
                i3 = 3*i - 3
                nlistpos = nabors(i) - 1
                nnbors = nborlist(nlistpos)

                do nbr = 1, nnbors
                    nlistpos = nlistpos + 1
                    j = nborlist(nlistpos)
                    j3 = 3*j - 3

                    xp(1:3) = x0(i3+1:i3+3) - x0(j3+1:j3+3)

                    if (xp(1) >=  0.5d0) xp(1) = xp(1) - pbc(1)
                    if (xp(1) <  -0.5d0) xp(1) = xp(1) + pbc(1)
                    if (xp(2) >=  0.5d0) xp(2) = xp(2) - pbc(2)
                    if (xp(2) <  -0.5d0) xp(2) = xp(2) + pbc(2)
                    if (xp(3) >=  0.5d0) xp(3) = xp(3) - pbc(3)
                    if (xp(3) <  -0.5d0) xp(3) = xp(3) + pbc(3)

                    rs = sum((xp * box)**2)
                    if (rs < rsumcritsq) then
                        dbuf(i) = dbuf(i) + Ekin(j)
                        pbuf(i) = max(pbuf(i), Ekin(j))
                        ibuf(i) = ibuf(i) + 1

                        if (half_neighbor_list) then
                            dbuf(j) = dbuf(j) + Ekin(i)
                            pbuf(j) = max(pbuf(j), Ekin(i))
                            ibuf(j) = ibuf(j) + 1
                        end if
                    end if
                end do
            end do
        end block


        ! Pass back dbuf, pbuf and ibuf of atoms belonging to other processes
        ! to get total values. Only necessary for half_neighbor_list.
        if (nprocs > 1 .and. half_neighbor_list) then
            block
                type(LiqanalPassBackPacker) :: pass_back_packer
                call pass_back_border_atoms(pass_back_packer)
            end block
        end if


        call mpi_barrier(mpi_comm_world, ierror)   ! Don't remove this sync!

        ! Now, finally, find actual liquid atoms and pack them in buffers
        ! for sending to processor 0. Note: overwrites ibuf.
        nliqat = 0
        block
            integer :: nsumnbrs, i, i3, j4
            real(real64b) :: Eksum, Ekmax

            do i = 1, myatoms
                Eksum = dbuf(i)
                Ekmax = pbuf(i)
                nsumnbrs = ibuf(i)

                if (nsumnbrs == 0) cycle
                if (Eksum / nsumnbrs <= Ekdef) cycle
                if ((Eksum - Ekmax) / nsumnbrs <= Ekdef / 2.0) cycle

                nliqat = nliqat + 1

                ! Store liquid atom info in sending buffers
                i3 = 3*i - 3
                j4 = 4*nliqat - 4

                buf(j4+1) = x0(i3+1)
                buf(j4+2) = x0(i3+2)
                buf(j4+3) = x0(i3+3)
                buf(j4+4) = Ekin(i)
                ! This is safe since nliqat <= i.
                ibuf(nliqat) = atomindex(i)
            end do
        end block


        ! Send back liquid atoms to processor 0 for printing.
        block
            integer :: i, i4, node, nsend, nrecv

            nsend = nliqat
            node_loop: do node = 0, nprocs - 1

                if (node > 0.and. node == myproc) then
                    call mpi_send(nsend, 1, my_mpi_integer, 0, myproc, &
                        mpi_comm_world, ierror)
                    if (nsend > 0) then
                        call mpi_send(buf, nsend*4, mpi_double_precision, &
                            0, nprocs+myproc, mpi_comm_world, ierror)
                        call mpi_send(ibuf, nsend, my_mpi_integer, &
                            0, nprocs*2+myproc, mpi_comm_world, ierror)
                    end if
                end if

                if (myproc == 0) then
                    if (node > 0) then
                        call mpi_recv(nrecv, 1, my_mpi_integer, mpi_any_source, &
                            node, mpi_comm_world, mpi_status_ignore, ierror)
                        if (nrecv > 0) then
                            call mpi_recv(buf, nrecv*4, mpi_double_precision, &
                                mpi_any_source, nprocs+node, &
                                mpi_comm_world, mpi_status_ignore, ierror)
                            call mpi_recv(ibuf, nrecv, my_mpi_integer, &
                                mpi_any_source, nprocs*2+node, &
                                mpi_comm_world, mpi_status_ignore, ierror)
                        end if
                    else
                        nrecv = nsend  ! No send from proc 0 to proc 0.
                    end if

                    ! Print liquid atoms.
                    do i = 1, nrecv
                        i4 = 4*i - 4
                        write(OUT_LIQUIDAT_FILE, &
                            "(3(F10.4,1X),F10.1,1X,I7,1X,F10.4,1X,I4,1X,I8)") &
                            buf(i4+1:i4+3)*box(:), &
                            time_fs, i, buf(i4+4), node, ibuf(i)
                    end do
                end if
            end do node_loop
        end block


        ! Liquid analysis done.
        call my_mpi_sum(nliqat, 1)
        if (iprint) then
            write (log_buf, "(A,I12,A,F10.1)") "Nliq", nliqat, &
                " at time", time_fs
            call logger(log_buf)
        end if

    end subroutine liquid_analysis


    subroutine liqanal_pass_pack(this, sendlist, buffer, filled_size)
        class(LiqanalPassPacker), intent(inout) :: this
        integer, intent(in) :: sendlist(:)
        real(real64b), contiguous, intent(out) :: buffer(:)
        integer, intent(out) :: filled_size

        integer :: isend, i, i3

        filled_size = 0

        do isend = 1, size(sendlist)
            i = sendlist(isend)
            i3 = 3*i - 3

            buffer(filled_size + 1) = this%x0(i3 + 1)
            buffer(filled_size + 2) = this%x0(i3 + 2)
            buffer(filled_size + 3) = this%x0(i3 + 3)
            buffer(filled_size + 4) = this%Ekin(i)
            filled_size = filled_size + 4
        end do
    end subroutine liqanal_pass_pack


    subroutine liqanal_pass_unpack(this, first_atom, num_atoms, buffer)
        class(LiqanalPassPacker), intent(inout) :: this
        integer, intent(in) :: first_atom
        integer, intent(in) :: num_atoms
        real(real64b), contiguous, intent(in) :: buffer(:)

        integer :: i, i3, bufind

        bufind = 0
        i3 = 3*first_atom - 3 - 3

        do i = first_atom, first_atom + num_atoms - 1
            i3 = i3 + 3

            this%x0(i3 + 1) = buffer(bufind + 1)
            this%x0(i3 + 2) = buffer(bufind + 2)
            this%x0(i3 + 3) = buffer(bufind + 3)
            this%Ekin(i) = buffer(bufind + 4)
            bufind = bufind + 4
        end do
    end subroutine liqanal_pass_unpack


    subroutine liqanal_pass_back_pack(this, first_atom, num_atoms, buffer, filled_size)
        class (LiqanalPassBackPacker), intent(inout) :: this
        integer, intent(in) :: first_atom
        integer, intent(in) :: num_atoms
        real(real64b), contiguous, intent(out) :: buffer(:)
        integer, intent(out) :: filled_size

        integer :: i
        integer :: iarr(2)
        real(real64b) :: r64

        filled_size = 0
        iarr(2) = 0

        do i = first_atom, first_atom + num_atoms - 1
            buffer(filled_size + 1) = dbuf(i)
            buffer(filled_size + 2) = pbuf(i)
            iarr(1) = ibuf(i)
            buffer(filled_size + 3) = transfer(iarr, r64)
            filled_size = filled_size + 3
        end do
    end subroutine liqanal_pass_back_pack


    subroutine liqanal_pass_back_unpack(this, recvlist, buffer)
        class(LiqanalPassBackPacker), intent(inout) :: this
        integer, intent(in) :: recvlist(:)
        real(real64b), contiguous, intent(in) :: buffer(:)

        integer :: irecv, i, bufind
        integer :: iarr(2)

        bufind = 0

        do irecv = 1, size(recvlist)
            i = recvlist(irecv)

            dbuf(i) = dbuf(i) + buffer(bufind + 1)
            pbuf(i) = max(pbuf(i), buffer(bufind + 2))
            iarr(:) = transfer(buffer(bufind + 3), iarr)
            ibuf(i) = ibuf(i) + iarr(1)
            bufind = bufind + 3
        end do
    end subroutine liqanal_pass_back_unpack


    !**********************************************************************
    !     Recoil sequence subroutines
    !**********************************************************************

    !
    ! Read in the recoil sequence data from recoildata.in.
    ! recoildata.in must contain rsnum rows of the format:
    !
    ! irec1 xrec1 yrec1 zrec1 theta1 phi1 E1 tmax1 [recatype1]
    ! irec2 xrec2 yrec2 zrec2 theta2 phi2 E2 tmax2 [recatype2]
    ! ...
    !
    ! If tmax<0 it loses its meaning, and endtemp is used instead.
    ! If tmax is used, recoil ends whenever tmax is reached.
    ! Otherwise it ends when T<endtemp.
    !
    ! The recatype is an optional parameter for setting the atom
    ! type of the added recoil. This is useful if one wants to
    ! add recoils of any of the atom types included in the simulation.
    !
    subroutine read_recoil_sequence_data(rsnum)

        integer, intent(in) :: rsnum
        character(len=160) ::  str

        integer :: ierr      ! File reading error code
        integer :: i, nline

        allocate(rsirec(rsnum), rsxrec(rsnum), rsyrec(rsnum), rszrec(rsnum))
        allocate(rstheta(rsnum), rsphi(rsnum), rsE(rsnum), rst(rsnum), rstype(rsnum))

        if (iprint) then
            open(TEMP_FILE, file='in/recoildata.in', status='old', iostat=ierr)
            if (ierr /= 0) then
                call logger("Error while opening in/recoildata.in:", ierr, 0)
                call my_mpi_abort("error opening in/recoildata.in", ierr)
            end if

            nline = 0
            i = 0
            do while (i < rsnum)
                nline = nline + 1

                read(TEMP_FILE, '(A)', iostat=ierr) str(:len(str)-3)
                if (ierr > 0) then
                    call logger("recoildata read error")
                    call logger("line number: ", nline, 4)
                    call my_mpi_abort("recoildata.in read error", ierr)
                else if (ierr < 0) then
                    call logger("Too few recoils in recoildata.in", i, 0)
                    call my_mpi_abort("Too few recoils in recoildata.in", i)
                end if

                ! Skip the current iteration if a comment is found.
                if (str(1:1) == '#') cycle
                i = i + 1

                ! Since the last parameter rstype is optional with default -1, add
                ! a -1 to the end of the line. If rstype is present, the -1 is
                ! ignored. Else it is read into the array.
                str(len(str)-2:) = " -1"
                read(unit=str, fmt=*, iostat=ierr) &
                    rsirec(i), rsxrec(i), rsyrec(i), rszrec(i), &
                    rstheta(i), rsphi(i), rsE(i), rst(i), rstype(i)

                if (ierr /= 0) then
                    call logger("recoildata parse error")
                    call logger("line number: ", nline, 4)
                    call logger("line content:", str, 4)
                    call my_mpi_abort("recoildata.in read error", ierr)
                end if

                rstheta(i) = rstheta(i) * pi / 180.0d0
                rsphi(i) = rsphi(i) * pi / 180.0d0
            end do

            close(TEMP_FILE)

            ! Inform the user of how many recoils have been read from the sequence file.
            write (log_buf, "(A,I5,A)") "Read in recoil information for ", &
                rsnum, " recoils"
            call logger(log_buf)
        end if

        ! Send the data over to all the different processes for read access
        call my_mpi_bcast(rsirec,  rsnum, 0)
        call my_mpi_bcast(rsxrec,  rsnum, 0)
        call my_mpi_bcast(rsyrec,  rsnum, 0)
        call my_mpi_bcast(rszrec,  rsnum, 0)
        call my_mpi_bcast(rstheta, rsnum, 0)
        call my_mpi_bcast(rsphi,   rsnum, 0)
        call my_mpi_bcast(rsE,     rsnum, 0)
        call my_mpi_bcast(rst,     rsnum, 0)
        call my_mpi_bcast(rstype,  rsnum, 0)

    end subroutine read_recoil_sequence_data


    !
    ! Return the information for recoild # rsn in the variables
    ! irec, xrec, ...
    !
    subroutine getnextrec(rsn, irec, xrec, yrec, zrec, &
            rectheta, recphi, recen, rstmax, recatypemod)

        integer, intent(out) :: irec, recatypemod
        real(real64b), intent(out) :: &
            xrec, yrec, zrec, rectheta, recphi, recen, rstmax

        integer, intent(in) :: rsn

        if (.not. allocated(rsirec)) then
            call my_mpi_abort("BUG: getnextrec: arrays not allocated", rsn)
        end if
        if (rsn > size(rsirec)) then
            call my_mpi_abort("BUG: getnextrec: rsn > size(rsirec)", rsn)
        end if

        ! Select the desired recoil from the sequence.
        irec = rsirec(rsn)
        xrec = rsxrec(rsn)
        yrec = rsyrec(rsn)
        zrec = rszrec(rsn)
        rectheta = rstheta(rsn)
        recphi = rsphi(rsn)
        recen = rsE(rsn)
        rstmax = rst(rsn)
        recatypemod = rstype(rsn)
    end subroutine getnextrec


    !**********************************************************************
    !     Cluster subroutines
    !**********************************************************************

    subroutine addvel(x0, x1, x2, x3, x4, x5, atype, box, &
            eaddvel, vaddvel, zaddvel, addvelt, addvelp, &
            adaptive_dt, vmaxnosput, timekt, timeEt, timeCh, &
            deltat_fs, deltamax_fs, delta, deltas, deltaratio)

        real(real64b), intent(inout) :: x0(:), x1(:), x2(:), x3(:), x4(:), x5(:)
        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: eaddvel, vaddvel
        real(real64b), intent(in) :: zaddvel
        real(real64b), intent(in) :: addvelt, addvelp
        integer, intent(in) :: atype(:)
        logical, intent(in) :: adaptive_dt
        real(real64b), intent(out) :: vmaxnosput
        real(real64b), intent(in) :: timekt, timeEt, timeCh
        real(real64b), intent(inout) :: deltat_fs, deltamax_fs, deltaratio
        real(real64b), intent(inout) :: delta(itypelow:itypehigh), deltas(itypelow:itypehigh)

        integer :: i, itype
        integer :: num_added
        real(real64b) :: factor(3), emhelp
        real(real64b) :: vsq, vsqmax, vmax

        if (eaddvel > 0d0 .and. vaddvel > 0d0) then
            if (iprint) then
                call logger("ERROR: both eaddvel and vaddvel > 0. This does not make physical sense")
                call logger("eaddvel:", eaddvel, 4)
                call logger("vaddvel:", vaddvel, 4)
            end if
            call my_mpi_abort('double addvel', 0)
        end if

        if (eaddvel <= 0d0 .and. vaddvel <= 0d0) return

        if (iprint) then
            if (eaddvel > 0d0) then
                write (log_buf, "(2(A,F16.6))") &
                    "Adding kinetic energy ", eaddvel," to atoms above", zaddvel
                call logger(log_buf)

                do i = itypelow, itypehigh
                    write (log_buf, "(A,1X,F13.6,1X,A,1X,I3)") &
                        "Energy corresponds to", sqrt(2d0*eaddvel)*vunit(i), &
                        "A/fs for atype", i
                    call logger(log_buf)
                end do
            else
                write (log_buf, "(2(A,F16.6))") "Adding velocity ", &
                    vaddvel, " to atoms above", zaddvel
                call logger(log_buf)

                do i = itypelow, itypehigh
                    write (log_buf, "(A,F16.6,A,I3)") &
                        "Velocity corresponds to", &
                        0.5d0 * (vaddvel/vunit(i))**2, " eV for atype", i
                    call logger(log_buf)
                end do
            end if
        end if

        if (iprint) then
            call logger("Using angles:")
            call logger("theta:", addvelt*180.0/pi, 4)
            call logger("phi:  ", addvelp*180.0/pi, 4)
        end if

        num_added = 0
        vsqmax = 0d0
        vmax = 0d0

        factor(1) = sin(addvelt) * cos(addvelp) / box(1)
        factor(2) = sin(addvelt) * sin(addvelp) / box(2)
        factor(3) = cos(addvelt) / box(3)

        do i = 1, myatoms
            if (x0(i*3)*box(3) <= zaddvel) cycle

            itype = abs(atype(i))

            if (eaddvel > 0d0) then
                emhelp = delta(itype) * sqrt(2.0 * eaddvel);
            else
                emhelp = delta(itype) * vaddvel / vunit(itype)
            end if

            associate (v => x1(3*i-2:3*i))
                v(:) = v(:) + emhelp * factor(:)

                vsq = sum((v(:) * box(:))**2) / delta(itype)**2
            end associate

            ! Calculate maximum velocity
            if (vsq > vsqmax) then
                vsqmax = vsq
                vmax = sqrt(vsqmax) * vunit(itype)
            endif
            num_added = num_added + 1
        end do
        call my_mpi_sum(num_added, 1)

        if (iprint) then
            write (log_buf, "(A,I7,A)") "Added energy or velocity to ",num_added," atoms"
            call logger(log_buf)
        end if

        ! Also change maximum velocity, otherwise Tstep will not
        ! be handled sensibly. TODO: what if some other atom is faster?
        vmaxnosput = vmax
        call my_mpi_max(vmaxnosput, 1)

        if (iprint) then
            write (log_buf, "(A,G13.6,A)") "Maximum velocity afterwards", &
                vmax, " A/fs"
            call logger(log_buf)
        end if

        ! Then immediately get new time step
        call Get_Tstep(adaptive_dt, vmaxnosput, 0d0, timekt, timeEt, timeCh, &
            deltat_fs, deltamax_fs, delta, deltas, deltaratio, 1d30)

        ! Correct x1 and x2 units for the new time step
        x1(:3*myatoms) = x1(:3*myatoms) * deltaratio
        x2(:3*myatoms) = x2(:3*myatoms) * deltaratio**2
        x3(:3*myatoms) = x3(:3*myatoms) * deltaratio**3
        x4(:3*myatoms) = x4(:3*myatoms) * deltaratio**4
        x5(:3*myatoms) = x5(:3*myatoms) * deltaratio**5

    end subroutine addvel




    !**********************************************************************
    !     track subroutines
    !**********************************************************************

    subroutine addtrack(x0, x1, x2, x3, x4, x5, atype, box, &
            trackstarted, trackstopped, trackx, tracky, trackt, trackmode, &
            adaptive_dt, vmaxnosput, timekt, timeEt, timeCh, &
            deltat_fs, deltamax_fs, delta, deltas, deltaratio, time_fs)

        real(real64b), contiguous, intent(inout) :: x0(:), x1(:), x2(:), x3(:), x4(:), x5(:)
        integer, contiguous, intent(in) :: atype(:)
        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: trackx, tracky
        real(real64b), intent(inout) :: trackt
        logical, intent(inout) :: trackstarted, trackstopped
        integer, intent(in) :: trackmode

        real(real64b), intent(in) :: time_fs
        logical, intent(in) :: adaptive_dt
        real(real64b), intent(in) :: timekt, timeEt, timeCh
        real(real64b), intent(inout) :: vmaxnosput
        real(real64b), intent(inout) :: deltat_fs, deltamax_fs, deltaratio
        real(real64b), intent(inout) :: delta(itypelow:itypehigh)
        real(real64b), intent(inout) :: deltas(itypelow:itypehigh)

        ! local variables
        integer :: i, itype, ii, ll, ul, ntrack, j
        real(real64b) :: ri
        real(real64b) :: eadd, emhelp, theta, phi
        real(real64b) :: trackEtot, dEkin, Ekinsum
        real(real64b) :: rlow, rhigh, Elow, Ehigh
        real(real64b) :: Ekold, vsq, vsqmax, vmax

        integer, parameter :: tracksize = 100
        real(real64b), save :: trackr(tracksize)
        real(real64b), save :: trackE1(tracksize)
        !real(real64b), save :: trackE2(tracksize)
        !real(real64b), save :: trackE3(tracksize)

        real(real64b), save :: trackEtotsum = 0d0
        real(real64b), save :: trackdEkinsum = 0d0

        integer, save :: imax = -1
        real(real64b), save :: rmax = -1d0


        if (trackstopped) return

        if (iprint) call logger("Adding kinetic energy to track atoms")


        if (trackmode == 1) then
            trackstopped = .true. ! Only add energy once
            call read_trackmode_1(trackr, trackE1, imax)

            rmax = trackr(imax)

            if (iprint) then
                call logger("Read in track:")
                call logger("imax:  ", imax, 4)
                call logger("rmax:  ", rmax, 4)
                call logger("trackt:", trackt, 4)
                call logger("time:  ", time_fs, 4)
            end if

        else if (trackmode==2) then
            ! If current read track is still valid then skip reading
            if (.not. ((trackt > time_fs) .and. trackstarted)) then
                call read_trackmode_2(trackr, trackE1, imax, trackt, time_fs, trackstopped)
                if (trackstopped) return

                rmax = trackr(imax)

                if (iprint) then
                    call logger("Read in track:")
                    call logger("imax:  ", imax, 4)
                    call logger("rmax:  ", rmax, 4)
                    call logger("trackt:", trackt, 4)
                    call logger("time:  ", time_fs, 4)
                end if
            end if

        else
            if (iprint) call logger("Unknown trackmode in addtrack", trackmode, 0)
            call my_mpi_abort('Unknown trackmode', trackmode)
        end if


        if (trackr(1)**2 > 0.1d-12) then   ! > 0
            if (iprint) then
                call logger("track ERROR: first r must be 0", trackr(1), 0)
            end if
            call my_mpi_abort('Track error', 0)
        end if
        if (trackE1(imax)**2 > 0.1d-12) then  ! > 0
            if (iprint) then
                call logger("track ERROR: last E1 must be 0")
                call logger("E1:      ", trackE1(imax), 4)
                call logger("imax:    ", imax, 4)
                call logger("trackr:  ", trackr(imax), 4)
            end if
            call my_mpi_abort('Track error', 0)
        end if

        ntrack = 0
        trackEtot = 0
        dEkin = 0
        Ekinsum = 0
        vmax = 0
        vsqmax = 0

        do i = 1, myatoms
            itype = abs(atype(i))

            ! Calculate and sum up old Ekin
            Ekold = 0.5d0 * sum((x1(3*i-2:3*i) * box)**2) / delta(itype)**2
            Ekinsum = Ekinsum + Ekold

            ! Distance to track center
            ! Note that periodics ARE NOT taken into account
            ri = sqrt((x0(i*3-2)*box(1) - trackx)**2 + (x0(i*3-1)*box(2) - tracky)**2)
            if (ri >= rmax) cycle

            ntrack = ntrack + 1

            ! Inside track, add E
            if (atype(i) < 0) then
                if (iprint) then
                    call logger("You try to add track E to a fixed atom?")
                    call logger("index:", i, 4)
                    call logger("atype:", atype(i), 4)
                end if
            end if

            ! Binary search of track file data
            ul = imax
            ll = 1
            do
                ii = (ul - ll) / 2
                if (ri < trackr(ll + ii)) then
                    ul = ll + ii
                else
                    ll = ll + ii
                end if
                if (ul - ll <= 1) exit
            end do

            if (ul <= ll .or. ul > imax) then
                if (iprint) then
                    call logger("track ERROR:")
                    call logger("ll:    ", ll, 4)
                    call logger("ul:    ", ul, 4)
                    call logger("imax:  ", imax, 4)
                    call logger("trackr:", 4)
                    do j = 1, size(trackr)
                        call logger("", trackr(j), 8)
                    end do
                end if
                call my_mpi_abort('addtrack binary search ERROR', ul)
            end if

            rlow = trackr(ll)
            rhigh = trackr(ul)
            !if (itype == 1) then
                Elow = trackE1(ll)
                Ehigh = trackE1(ul)
            !else if (itype == 2) then
            !    Elow = trackE2(ll)
            !    Ehigh = trackE2(ul)
            !else if (itype == 3) then
            !    Elow = trackE3(ll)
            !    Ehigh = trackE3(ul)
            !else
            !    call my_mpi_abort("Unexpected atom type for track", itype)
            !end if

            ! Linear extrapolation between points
            eadd = (Ehigh - Elow) / (rhigh - rlow) * (ri - rlow) + Elow

            if (trackmode == 2) then
                eadd = eadd * deltat_fs     ! eadd = eadd/fs * time interval
            end if

            trackEtot = trackEtot + eadd

            emhelp = delta(itype) * sqrt(2 * eadd)

            ! Get random direction in 3D
            phi = MyRanf(0) * 2* pi
            theta = acos(cos(pi) + MyRanf(0) * (1 - cos(pi)))

            ! Add velocity
            x1(i*3-2) = x1(i*3-2) + emhelp * sin(theta) * cos(phi) / box(1)
            x1(i*3-1) = x1(i*3-1) + emhelp * sin(theta) * sin(phi) / box(2)
            x1(i*3-0) = x1(i*3-0) + emhelp * cos(theta) / box(3)

            ! Calculate maximum velocity
            vsq = sum((x1(3*i-2:3*i) * box)**2) / delta(itype)**2
            if (vsq > vsqmax) then
                vsqmax = vsq
                vmax = sqrt(vsq) * vunit(itype)
            end if

            ! Calculate change in total Ekin
            dEkin = dEkin + (0.5d0 * vsq - Ekold)
        end do


        call my_mpi_sum(ntrack, 1)
        call my_mpi_sum(trackEtot, 1)
        call my_mpi_sum(dEkin, 1)
        call my_mpi_sum(Ekinsum, 1)

        trackEtotsum = trackEtotsum + trackEtot
        trackdEkinsum = trackdEkinsum + dEkin

        if (trackEtot > 0) then
            if (iprint) then
                write (log_buf,'(A,F13.3,A,I7,A)') 'Now added E ', &
                    trackEtot,' to ',ntrack,' atoms'
                call logger(log_buf)
                write (log_buf,'(A,F13.3,A,F13.3,A)') 'Ekin ', Ekinsum, &
                    ' increased at this step by ',dEkin ! For testing only
                call logger(log_buf)
                write (log_buf,'(A,F13.3,A,I7,A)') &
                    'Total E deposited to track ',trackEtotsum
                call logger(log_buf)
                write (log_buf,'(A,F13.3,A,I7,A)') 'Total Ekin increases ', &
                    trackdEkinsum
                call logger(log_buf)
            end if
        end if

        ! Also change maximum velocity, otherwise Tstep will not
        ! be handled sensibly
        vmaxnosput = max(vmaxnosput, vmax)
        call my_mpi_max(vmaxnosput, 1)

        ! Then immediately get new time step
        call Get_Tstep(adaptive_dt,vmaxnosput,0.0d0,timekt,timeEt,timeCh, &
            deltat_fs,deltamax_fs,delta,deltas,deltaratio,1d30)

        ! Correct x1, x2, ... units for the new time step
        x1(:3*myatoms) = x1(:3*myatoms) * deltaratio
        x2(:3*myatoms) = x2(:3*myatoms) * deltaratio**2
        x3(:3*myatoms) = x3(:3*myatoms) * deltaratio**3
        x4(:3*myatoms) = x4(:3*myatoms) * deltaratio**4
        x5(:3*myatoms) = x5(:3*myatoms) * deltaratio**5

    end subroutine addtrack


    subroutine read_trackmode_1(trackr, trackE1, num)

        real(real64b), intent(out) :: trackr(:)
        real(real64b), intent(out) :: trackE1(:)
        integer, intent(out) :: num

        integer :: i, ios

        open(TEMP_FILE, file="in/track.in", status='old', iostat=ios)
        if (ios /= 0) then
            if (iprint) call logger("ERROR: track.in open error")
            call my_mpi_abort('track.in', ios)
        end if

        do i = 1, size(trackr)
            read(TEMP_FILE, *, iostat=ios) trackr(i), trackE1(i) !,trackE2(i),trackE3(i)

            if (ios == IOSTAT_END) exit ! end of file
            if (ios > 0 .or. ios == IOSTAT_EOR) then
                if (iprint) call logger("ERROR: track.in read error")
                call my_mpi_abort('track.in', ios)
            end if
        end do

        if (i > size(trackr)) then
            if (iprint) call logger("Track array too big", size(trackr), 0)
            call my_mpi_abort('Track array size', myproc)
        end if

        close(TEMP_FILE)

        num = i - 1

    end subroutine read_trackmode_1


    subroutine read_trackmode_2(trackr, trackE1, num, trackt, time_fs, trackstopped)

        real(real64b), intent(out) :: trackr(:)
        real(real64b), intent(out) :: trackE1(:)
        integer, intent(out) :: num
        real(real64b), intent(inout) :: trackt
        real(real64b), intent(in) :: time_fs
        logical, intent(inout) :: trackstopped

        integer :: i, ios
        logical, save :: fileopen = .false.

        ! Open the file when here for the first time.
        ! Then keep the file open until it is no longer needed.

        ! Header lines have start time in first column and negative number
        ! in second column. Always read up to (and including) the first
        ! header which has a time in the future. Then the previously read
        ! radius/energy values are the ones to use.

        if (.not. fileopen) then
            open(TRACK_ARRAY_FILE, file="in/track.array", status='old', iostat=ios)
            if (ios /= 0) then
                if (iprint) call logger("ERROR: track.array open error")
                call my_mpi_abort('track.array', ios)
            end if
            fileopen = .true.

            ! Read in the first time
            read(TRACK_ARRAY_FILE,*, iostat=ios) trackt
            if (ios /= 0) then
                if (iprint) call logger("ERROR: track.array first read error")
                call my_mpi_abort('track.array', ios)
            end if

            trackt = 1d15 * trackt  ! Convert from s to fs
        end if

        outer: do while (trackt <= time_fs)
            do i = 1, size(trackr)
                num = i - 1
                read(TRACK_ARRAY_FILE, *, iostat=ios) trackr(i), trackE1(i) !,trackE2(i),trackE3(i)

                if (ios == IOSTAT_END) then ! end of file
                    trackstopped = .true.
                    close(TRACK_ARRAY_FILE)
                    fileopen = .false.
                    if (iprint) call logger("Track stopped at time", time_fs, 0)
                    return
                end if

                if (ios > 0 .or. ios == IOSTAT_EOR) then
                    if (iprint) call logger("ERROR: track.array read error")
                    call my_mpi_abort('track.array', ios)
                end if

                if (trackE1(i) < -1d-3) then ! trackE<0 means a line for time
                    trackt = 1d15 * trackr(i) ! Actually read the time
                    trackr(i)  = 0 ! Clean the wrong input
                    trackE1(i) = 0
                    !trackE2(i) = 0
                    !trackE3(i) = 0
                    cycle outer
                end if
            end do
            if (i > size(trackr)) then
                if (iprint) then
                    call logger("Track array too big")
                    call logger("max size:", size(trackr), 4)
                    call logger("time:    ", time_fs, 4)
                end if
                call my_mpi_abort('Track array size', size(trackr))
            end if
        end do outer

    end subroutine read_trackmode_2

end module mdcasc_mod
