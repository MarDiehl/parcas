!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module mdinit_types_mod

    use datatypes, only: real64b
    use output_logger, only: log_buf, logger
    use my_mpi, only: my_mpi_abort

    use para_common, only: iprint

    implicit none

    integer, parameter :: STATUS_NONE = -1
    integer, parameter :: STATUS_FOUND = 0
    integer, parameter :: STATUS_DEFAULT = 1
    integer, parameter :: STATUS_MISSING = 2


    integer :: INDENT_PARAMETER = 18
    integer :: INDENT_PARAMETER_DEFAULT = 8
    integer :: INDENT_PARAMETER_MISSING = 6


    type :: param
        integer :: read_status = STATUS_NONE
        character(10) :: name = ""
      contains
        procedure :: assign_type_value
        generic :: assignment(=) => assign_type_value
        procedure :: read_val
        procedure :: print_status
    end type

    type, extends(param) :: int_param
        integer :: v
    end type

    type, extends(param) :: real_param
        real(real64b) :: v
    end type

    type, extends(param) :: character_param
!        character(:), allocatable :: v
        character(300) :: v
    end type


  contains


    subroutine read_val(this, line)
        class(param), intent(inout) :: this
        character(*), intent(in) :: line

        integer :: ios

        select type(this)
            type is (int_param)
                read(line, *, iostat=ios) this%v
            type is (real_param)
                read(line, *, iostat=ios) this%v
            type is (character_param)
                read(line, *, iostat=ios) this%v
        end select

        if (ios /= 0) then
            call logger("Error parsing value for parameter " // trim(this%name))
            call my_mpi_abort("Bad md.in", ios)
        end if
    end subroutine


    subroutine assign_type_value(this, other)
        class(param), intent(inout) :: this
        class(param), intent(in) :: other

        select type(this)
            class is (int_param)
                select type(other)
                    class is(int_param)
                        this%v = other%v
                end select

            class is (real_param)
                select type(other)
                    class is(real_param)
                        this%v = other%v
                end select

            class is (character_param)
                select type(other)
                    class is(character_param)
                        this%v = other%v
                end select
        end select
    end subroutine


    subroutine print_status(this)
        class(param), intent(in) :: this

        integer :: indent

        if (.not. iprint) return

        select case (this%read_status)
            case (STATUS_FOUND)
                indent = INDENT_PARAMETER
                select type (this)
                    type is (int_param)
                        call logger(this%name, this%v, indent)
                    type is (real_param)
                        call logger(this%name, this%v, indent)
                    type is (character_param)
                        call logger(this%name, this%v, indent)
                    class default
                        call logger(this%name, "[NOT YET IMPLEMENTED]", indent)
                end select
            case (STATUS_DEFAULT)
                indent = INDENT_PARAMETER_DEFAULT
                select type (this)
                    type is (int_param)
                        call logger("[default]", this%name, this%v, indent)
                    type is (real_param)
                        call logger("[default]", this%name, this%v, indent)
                    type is (character_param)
                        call logger("[default]", this%name, this%v, indent)
                    class default
                        call logger("[default]", this%name, "[NOT YET IMPLEMENTED]", indent)
                end select
            case (STATUS_MISSING)
                indent = INDENT_PARAMETER_MISSING
                call logger("> [missing]", this%name, indent)
        end select
    end subroutine

end module


module mdinit_mod

    use datatypes, only: int32b, real64b
    use PhysConsts, only: pi, invkBeV
    use file_units, only: TEMP_FILE
    use lat_flags, only: LATFLAG_CREATE_LATTICE
    use border_params_mod, only: BorderParams
    use my_mpi

    use typeparam
    use basis
    use casc_common
    use Temp_Time_Prog

    use mdlattice_mod, only: Cryst_Gen, Get_Atoms
    use random, only: MyRanf, gasdev

    use output_logger, only: &
        log_buf, &
        logger, &
        logger_append_buffer, &
        logger_clear_buffer, &
        logger_write

    use mdparsubs_mod, only: &
        init_node_grid, &
        print_node_configuration, &
        rank_from_dimrank, &
        Rectangle

    use para_common, only: &
        myproc, nprocs, &
        debug, iprint, &
        myatoms, dimrank, &
        nnodes, &
        rmn, rmx

    use mdinit_types_mod, only: &
        param, &
        int_param, &
        character_param, &
        real_param, &
        STATUS_FOUND, STATUS_DEFAULT, STATUS_MISSING

    use utils, only: to_str


    implicit none


    private
    public :: Md_Init
    public :: read_all_parameters
    public :: set_initial_velocities


    interface md_in
        module procedure :: read_int32b
        module procedure :: read_real64b
        module procedure :: read_character
        module procedure :: read_logical

        module procedure :: read_int32b_1d_array
        module procedure :: read_real64b_1d_array
        module procedure :: read_character_1d_array

        module procedure :: read_int32b_2d_array
        module procedure :: read_real64b_2d_array
    end interface md_in


    abstract interface
        logical function check_key_interface(p, pName, pKey, pVal) result(ret)
            import param
            class(param), intent(inout) :: p
            character(*), intent(in) :: pName
            character(*), intent(in) :: pKey
            character(*), intent(in) :: pVal
        end function
    end interface


  contains



    !***********************************************************************
    ! Initialize the simulation.
    !***********************************************************************

    subroutine Md_Init(x0,x1,atomindex,                                   &
        atype,natoms,nfixed,amp,initemp,tdebye,box,pbc,ncells,           &
        latflag,restartmode,mdlatxyz,fixzmin,fixzmax,fixperbrdr,fixxybrdr, &
        dsliceIn,ECMIn, Fzmaxz,dydtmaxz, seed)

        !------------------------------------------------------------------
        ! Variables passed in and out
        integer, intent(inout) :: natoms
        integer, intent(out) :: nfixed
        integer(int32b), contiguous, intent(out) :: atype(:), atomindex(:)
        real(real64b), contiguous, intent(out) :: x0(:), x1(:)
        real(real64b), intent(in) :: fixzmin,fixzmax,fixperbrdr,fixxybrdr
        real(real64b), intent(in) :: box(3), pbc(3)
        integer, intent(in) :: ncells(3)
        real(real64b), intent(in) :: Fzmaxz, dydtmaxz
        real(real64b), intent(in) :: initemp, tdebye, amp
        integer, intent(in) :: latflag, mdlatxyz, restartmode
        ! slicing on restart reading
        real(real64b), intent(in) :: ECMIn(3), dsliceIn(3)
        integer, intent(in) :: seed

        !          Variables for parallel operation
        !     myatoms = number of atoms that my node is responsible for
        !     myproc = my relative processor number (0..nprocs-1)
        !     rmn(1..2),rmx(1..2) = x & y min & max boundaries for my nodes region
        !     nnodes(1) = number of nodes in x direction (columns)
        !     nnodes(2) = number of nodes in y direction (rows)
        !     ------------------------------------------------------------------
        !          Local variables
        real(kind=real64b)   :: x,y,z
        integer(kind=int32b) :: i,n,i3,ii
        integer :: tmpdimrank(3)

        !     ------------------------------------------------------------------


        ! Choose the node arrangement to match the simulation box ratios
        ! as closely as possible subject to the maximums from the input file.

        if (nprocs > 1) then
            if (any(nnodes <= 0)) then
                call Rectangle(box)
            end if

            if (mod(nprocs,2) == 1 .and. iprint) then
                call logger("USE AN EVEN NUMBER OF PROCESSORS")
                call logger("I'll try an odd number, but beware")
            end if
        else
            nnodes(:) = 1
        end if

        ! Create the node grid and everything for passing stuff
        ! between neighbor nodes.
        call init_node_grid(nnodes, pbc)

        ! Initialize the random number generator.
        ! Try to get the same random numbers as before the change to the new
        ! process grid division.
        tmpdimrank(:) = dimrank(:)
        tmpdimrank(2) = nnodes(2) - dimrank(2) - 1
        x = MyRanf(seed + 37 * rank_from_dimrank(tmpdimrank, nnodes))

        ! Create the atoms, either from a file or a generated lattice.
        nfixed=0
        if (latflag == 1 .or. latflag == 3 .or. latflag == 4) then
            call Get_Atoms(x0,x1,atomindex,atype,                              &
                natoms,nfixed,amp,initemp,tdebye,box,pbc,latflag,restartmode, &
                mdlatxyz,dsliceIn,ECMIn)
        else
            call Cryst_Gen(x0,atomindex,atype,natoms,amp,initemp,tdebye,       &
                box,pbc,ncells,latflag)
        endif

        ! Count atom types
        noftype(:) = 0
        do i = 1, myatoms
            ii = abs(atype(i))
            noftype(ii) = noftype(ii) + 1
        enddo

        call my_mpi_sum(noftype, size(noftype))

        ! TODO: Continue with the formatting from here...
        if (iprint) then
            call logger("Number of atoms of different types:")
            do i=itypelow,itypehigh
                write(log_buf,"(I3,1X,A,1X,I7)") i,":",noftype(i)
                call logger(log_buf)
            enddo
            call logger()
        endif

        if (fixzmax>-1.0d30) then
            n=0
            do i=1,myatoms
                z=x0(i*3)
                if (z*box(3)>=fixzmin.and.z*box(3)<=fixzmax) then
                    if (atype(i) > 0) then
                        ! Only fix if not fixed already
                        atype(i)=-ABS(atype(i))
                        n=n+1
                    endif
                endif
            enddo
            call my_mpi_sum(n, 1)
            nfixed=nfixed+n

            if (iprint) then
                write (log_buf, "(A,2G13.5,A,I7)") "Number of atoms fixed between", &
                    fixzmin, fixzmax, " is ", n
                call logger(log_buf)
            end if
        endif

        if (fixperbrdr > 0d0) then
            n=0
            do i=1,myatoms
                i3=i*3-3
                x=x0(i3+1)*box(1)
                y=x0(i3+2)*box(2)
                z=x0(i3+3)*box(3)
                if ((pbc(1)==1d0.and.(x<-box(1)/2+fixperbrdr .or. x>box(1)/2-fixperbrdr)) .or. &
                    (pbc(2)==1d0.and.(y<-box(2)/2+fixperbrdr .or. y>box(2)/2-fixperbrdr)) .or. &
                    (pbc(3)==1d0.and.(z<-box(3)/2+fixperbrdr .or. z>box(3)/2-fixperbrdr)) ) then
                    if (atype(i) > 0) then
                        ! Only fix if not fixed already
                        atype(i)=-ABS(atype(i))
                        n=n+1
                    endif
                endif
            enddo
            call my_mpi_sum(n, 1)
            nfixed=nfixed+n

            if (iprint) then
                write (log_buf, "(A,G13.5,A,I7)") "Number of atoms fixed within ", &
                    fixperbrdr, " of periodic borders is", n
                call logger(log_buf)
            end if
        endif

        if (fixxybrdr > 0d0) then
            n=0
            do i=1,myatoms
                i3=i*3-3
                x=x0(i3+1)*box(1)
                y=x0(i3+2)*box(2)
                z=x0(i3+3)*box(3)
                if ((x<-box(1)/2+fixxybrdr .or. x>box(1)/2-fixxybrdr) .or. &
                    (y<-box(2)/2+fixxybrdr .or. y>box(2)/2-fixxybrdr) ) then
                    if (atype(i) > 0) then
                        ! Only fix if not fixed already
                        atype(i)=-ABS(atype(i))
                        n=n+1
                    endif
                endif
            enddo
            call my_mpi_sum(n, 1)
            nfixed=nfixed+n

            if (iprint) then
                write (log_buf, "(A,G13.5,A,I7)") "Number of atoms fixed within ", &
                    fixxybrdr, " of xy borders is", n
                call logger(log_buf)
            end if
        endif

        if (dydtmaxz /= 0d0) then
            n=0
            do i=1,myatoms
                z=x0(i*3)
                if (z*box(3)>=Fzmaxz) then
                    if (atype(i) > 0) then
                        ! Only fix if not fixed already
                        atype(i)=-abs(atype(i))
                        n=n+1
                    endif
                endif
            enddo
            call my_mpi_sum(n,1)
            nfixed=nfixed+n

            if (iprint) then
                write (log_buf, "(A,G13.5,A,I7)") &
                    "Number of atoms fixed for dydtzmax above", Fzmaxz, " is ", n
                call logger(log_buf)
            end if
        endif

        if (iprint) then
            call logger("Total number of fixed atoms:", nfixed, 0)
            call logger()
        end if

        call print_node_configuration()

    end subroutine Md_Init


    !***********************************************************************
    ! Generate initial velocities for atoms.
    !
    ! Input: initemp = T to set in units of eV (kB*T/e)
    !        myatoms = number of atoms for this processor
    !        natoms = total number of atoms for all processors
    !        x1(:) = velocity array, units of v/box
    !        box(3) = box size in A
    !
    ! If initemp is negative, all atoms are given same velocity.
    ! Otherwise a Gaussian distribution is used.
    !***********************************************************************
    subroutine set_initial_velocities(initemp, myatoms, natoms, x1, box)

        real(real64b), intent(in) :: initemp
        integer, intent(in) :: myatoms, natoms
        real(real64b), intent(out) :: x1(:)
        real(real64b), intent(in) :: box(3)

        real(real64b) :: theta, phi
        real(real64b) :: vtot(3), std
        integer :: i

        ! Factor 2 due to equipartition theorem
        std = sqrt(2d0 * abs(initemp))

        vtot(:) = 0d0
        do i = 1, 3*myatoms, 3
            if (initemp > 0d0) then
                ! Gaussian distributed velocities
                x1(i)   = std * gasdev() / box(1)
                x1(i+1) = std * gasdev() / box(2)
                x1(i+2) = std * gasdev() / box(3)
            else
                ! Same velocity for every atom.
                ! Factor sqrt(1.5) is ad hoc.
                phi = MyRanf(0) * 2d0 * pi
                theta = acos(cos(pi) + MyRanf(0) * (1d0 - cos(pi)))
                x1(i)   = sqrt(1.5d0) * std * sin(theta) * cos(phi) / box(1)
                x1(i+1) = sqrt(1.5d0) * std * sin(theta) * sin(phi) / box(2)
                x1(i+2) = sqrt(1.5d0) * std * cos(theta) / box(3)
            end if
            vtot(:) = vtot(:) + x1(i : i+2)
        end do
        call my_mpi_sum(vtot, 3)
        vtot(:) = vtot(:) / natoms

        do i = 1, 3*myatoms, 3
            x1(i : i+2) = x1(i : i+2) - vtot(:)
        end do

    end subroutine set_initial_velocities


    !***********************************************************************
    ! Open and read in the parameter file for the md run
    !***********************************************************************

    subroutine read_all_parameters(nsteps,tmax,deltaini,natoms,box,ncells,                          &
        pbc,mtemp,temp,toll,damp,pscale,remrot,amp,tdebye,latflag,mdlatxyz,nprtbl, &
        ndump,nmovie,timekt,timeEt,timeCh,adaptivet, &
        neiskinr,Ekdef,Ekrec,ndefmovie,nliqanal,                         &
        temp0,trate,ntimeini,timeini,nrestart,bpcbeta,               &
        bpctau,bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,bpcmode,                       &
        btctau,restartmode,restartt,potmode,spline,eamnbands,Fpextrap,initemp,    &
        melstop,melstopst,elstopmin,tmodtau,rsnum,rsmode,endtemp,dtmovie,tmovie,dtslice,  &
        tslice,dslice,ECM,ECMfix,dsliceIn,ECMIn,sw2mod,sw3mod,prandom,mrandom,          &
        timerandom,Fzmaxz,FzmaxYz,FzmaxZz,Fzmaxzr,Fzmaxtyp,dydtmaxz,     &
        fixzmin,fixzmax,fixperbrdr,fixxybrdr,                            &
        reppotcutin,elstopsputlim,timesputlim,nisputlim, &
        taddvel,zaddvel,eaddvel,vaddvel,Emaxbrdr,moviemode,binmode,slicemode,fdamp,border_params, &
        addvelt,addvelp,trackx,tracky,trackt,trackmode,forcesum,sumatype,zipout,avgvir,dydttime, &
        outtype,outzmin,outzmax,outzmin2,outzmax2,virsym,virkbar,virboxsiz,R1CC,R2CC,zmin, &
        recstopz, recstoptype, recindex, final_xyz, final_xyz_path, nnodes, seed, nbalance)

    ! maximum number of time steps [out]
        integer :: nsteps
        ! number of atoms (default -1 = auto) [out]
        integer :: natoms
        ! number of cells [out]
        integer ::ncells(3)
        ! temperature control mode, see README.DOCUMENTATION [out]
        integer :: mtemp
        ! used to determine how the initial atom coordinates are obtained [out]
        integer :: latflag
        ! Controls whether mdlat.in or mdlat.in.xyz is read, value 1 means .xyz [out]
        integer :: mdlatxyz
        ! number of steps between pair table calculations [out]
        integer :: nprtbl
        ! number of steps between output to stdout [out]
        integer :: ndump
        ! number of steps between movie output, values <= 0 for no movie output.
        ! The exact positive value is not used when dtmov(1) > 0. [out]
        integer :: nmovie
        ! Number of steps between writing pressures.out [out]
        integer :: ndefmovie

        integer, intent(out) :: nbalance ! How often to rebalance nodes
        integer, intent(out) :: nnodes(3)
        integer :: seed

        ! whether to output summed forces, value 1 means yes. Only sum atoms of type sumatype [out]
        integer :: forcesum
        ! atom type to use for force summing [out]
        integer :: sumatype
        ! number of steps between liquid analysis output [out]
        integer :: nliqanal
        ! number of steps between restart output [out]
        integer :: nrestart
        ! maximum timestep [out]
        real(kind=real64b) :: deltaini
        ! size of simulation box in 3d [out]
        real(kind=real64b) :: box(3)
        ! Periodic Boundary Conditions, value 1 means periodic and 0 means open [out]
        real(kind=real64b) :: pbc(3)
        ! Desired temperature [out]
        real(kind=real64b) :: temp
        ! tolerance for temperature, don't scale if temperature is within toll of temp [out]
        real(kind=real64b) :: toll
        ! damping factor, TODO: what is it? [out]
        real(kind=real64b) :: damp
        ! Amplitude of initial displacement (Angstroms), TODO: what is it? [out]
        real(kind=real64b) :: amp
        integer(kind=int32b) :: pscale,remrot

        ! Is adaptive time step enabled? 1 = yes, 0 = no, -1 = auto (for recoils)
        integer, intent(out) :: adaptivet
        real(kind=real64b) :: timekt,timeEt,timeCh,fixzmin,fixzmax,fixperbrdr,fixxybrdr,reppotcutin
        real(kind=real64b) :: tmax
        real(kind=real64b) :: neiskinr,Ekdef,Ekrec
        real(kind=real64b) :: taddvel,zaddvel,eaddvel,vaddvel,Emaxbrdr,addvelt,addvelp
        ! Border region definition for temperature control
        type(BorderParams), intent(out) :: border_params

        real(kind=real64b) :: temp0,trate,restartt,initemp,endtemp,timeini,tdebye
        real(kind=real64b) :: elstopmin,elstopsputlim,timesputlim,nisputlim

        integer :: ntimeini,potmode,spline,eamnbands,Fpextrap,melstop,melstopst,rsnum,rsmode

        integer :: avgvir
        integer :: dydttime
        integer :: outtype
        real(kind=real64b) :: outzmin,outzmax
        real(kind=real64b) :: outzmin2,outzmax2
        integer :: virsym
        integer :: virkbar
        real(kind=real64b) :: virboxsiz

        real(kind=real64b) :: bpcbeta,bpctau,bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,tmodtau
        integer :: bpcmode
        real(kind=real64b) :: btctau
        real(kind=real64b) :: dtmovie(:),tmovie(:),dtslice(:),tslice(:)
        real(kind=real64b) :: dslice(:),ECM(:),sw2mod(3),sw3mod(3)
        real(kind=real64b) :: dsliceIn(:),ECMIn(:)
        integer :: ECMfix
        real(kind=real64b) :: prandom,mrandom,timerandom
        real(kind=real64b) :: Fzmaxz,FzmaxYz,FzmaxZz,Fzmaxzr,fdamp,dydtmaxz
        integer :: Fzmaxtyp
        integer :: moviemode,slicemode,restartmode
        character(*), intent(out) :: binmode

        ! Write final simulation frame to given path if enabled.
        character(len=*), intent(out) :: final_xyz_path
        logical, intent(out) :: final_xyz

        real(kind=real64b) :: trackx,tracky,trackt
        integer :: trackmode
        ! Write output to compressed files
        integer :: zipout

        !      BrennerBeardmore params
        real(kind=real64b) :: R1CC,R2CC

        ! Min height (z)
        real(kind=real64b) :: zmin

        ! Recoil stopping height and type
        real(real64b) :: recstopz
        integer :: recstoptype

        ! Atomindex for the new recoil
        integer :: recindex


        integer :: nl, pnl, io
        integer :: i, j

        character(len=80) :: line
        character(len=80), allocatable :: pLines(:)

        integer :: ierror


        !--------------------------------------------------------------
        ! READING THE FILE INTO AN ARRAY CONTAINING ALL PARAMETERS
        !--------------------------------------------------------------

        ! Count the lines of the input file
        if (iprint) then
            call logger("Input file for PARCAS:", 2)
            open(TEMP_FILE, file="in/md.in", status="old", iostat=io)

            if (io /= 0) then
                call logger("Could not open file in/md.in")
                call my_mpi_abort("md.in open", 0)
            end if

            nl = 0      ! Number of lines
            pnl = 0     ! Number of parameter lines
            do
                read(TEMP_FILE, "(A80)", iostat=io) line

                if (io < 0) exit    ! EOF
                if (io > 0) then    ! Error
                    call logger("Something went wrong while reading md.in")
                    call my_mpi_abort('md.in read error', myproc)
                end if

                nl = nl + 1

                ! Count all lines with the equal sign on the 10th character as
                ! a parameter line.
                if (index(line, "=") == 10) pnl = pnl + 1
            end do

            rewind(TEMP_FILE)
        end if

        call mpi_bcast(pnl, 1, mpi_integer, 0, mpi_comm_world, ierror)

        allocate(pLines(pnl))

        ! Populate the arrays
        if (iprint) then
            j = 1
            do i = 1, nl
                read(TEMP_FILE, "(A80)", iostat=io) line

                if (io /= 0) then
                    call logger("Something went wrong while reading md.in")
                    call my_mpi_abort('md.in read error', myproc)
                end if

                if (index(line, "=") == 10) then
                    pLines(j) = line
                    j = j + 1
                end if
            end do

            close(TEMP_FILE)
        end if

        do i = 1, size(pLines)
            call mpi_bcast(pLines(i), len(pLines(i)), mpi_character, 0, &
                mpi_comm_world, ierror)
        end do

        !----------------------------------------------------------------

        if (iprint) then
            call logger()
            call logger("md.in file:", 6)
            call logger("Number of lines:     ", nl, 10)
            call logger("Number of parameters:", pnl, 10)
            call logger()
            call logger("List of parameters:", 6)
            call logger("-------------------", 6)
            call logger()
        end if

        call md_in(pLines, "debug", debug, .false.)

        call md_in(pLines, "nsteps", nsteps)
        call md_in(pLines, "tmax", tmax)
        call md_in(pLines, "endtemp", endtemp, 0.0d0)
        endtemp = endtemp/invkBeV

        call md_in(pLines, "nnodes", nnodes, .false., 0)

        !=========================================================
        ! GENERAL TYPE PARAMETERS
        !=========================================================

        call md_in(pLines, "ntype", ntype, 1)
        itypelow = 0; itypehigh = ntype-1
        if (ntype == 1) then
            itypelow = 1; itypehigh = 1
        end if

!        if (iprint) then
!            call logger("Allocating type arrays:")
!            call logger("itypelow: ", itypelow, 4)
!            call logger("itypehigh:", itypehigh, 4)
!        end if

        ! 1D arrays
        allocate(mass(itypelow:itypehigh))
        allocate(timeunit(itypelow:itypehigh))
        allocate(vunit(itypelow:itypehigh),aunit(itypelow:itypehigh))
        allocate(element(itypelow:itypehigh))
        allocate(noftype(itypelow:itypehigh))
        allocate(atomZ(itypelow:itypehigh))

        ! 2D arrays
        allocate(rcut(itypelow:itypehigh,itypelow:itypehigh))
        allocate(rcutin(itypelow:itypehigh,itypelow:itypehigh))
        allocate(iac(itypelow:itypehigh,itypelow:itypehigh))


        call md_in(pLines, "mass", mass, .true.)
        call md_in(pLines, "name", element, .true.)
        call md_in(pLines, "substrate", substrate, "NONE")
        call md_in(pLines, "atomZ", atomZ, .true., -1.0d0)


        call md_in(pLines, "iac", iac, (ntype > 1), .true., 1)
        call md_in(pLines, "rcin", rcutin, .true., .true., -1.0d0)


        !=========================================================
        ! GENERAL TIME PARAMETERS
        !=========================================================
        call md_in(pLines, "adaptivet", adaptivet, -1)
        call md_in(pLines, "delta", deltaini)
        call md_in(pLines, "timekt", timekt, 0.1d0)
        call md_in(pLines, "timeEt", timeEt, 30.0d0)
        call md_in(pLines, "timeCh", timeCh, 1.1d0)


        !=========================================================
        ! GENERAL TEMPERATURE PARAMETERS
        !=========================================================

        call md_in(pLines, "tscaleth",  border_params%thickness, 0.0d0)
        call md_in(pLines, "tscalzmin", border_params%zmin, 1.0d30)
        call md_in(pLines, "tscalzmax", border_params%zmax, 1.0d30)
        call md_in(pLines, "tscalxout", border_params%xout, 1.0d30)
        call md_in(pLines, "tscalyout", border_params%yout, 1.0d30)


        call md_in(pLines, "Emaxbrdr", Emaxbrdr, 1.0d30)
        call md_in(pLines, "neiskinr", neiskinr, 1.3d0)


        call md_in(pLines, "forcesum", forcesum, 0)
        call md_in(pLines, "sumatype", sumatype, 0)


        call md_in(pLines, "natoms", natoms, -1)
        call md_in(pLines, "box", box, .false.)
        call md_in(pLines, "ncell", ncells, .false., -1)
        call md_in(pLines, "pb", pbc, .false.)


        call md_in(pLines, "fixzmin", fixzmin, -1.0d30)
        call md_in(pLines, "fixzmax", fixzmax, -1.0d30)
        call md_in(pLines, "fixper", fixperbrdr, 0.0d0)
        call md_in(pLines, "fixxy", fixxybrdr, 0.0d0)


        ! Minimum height coordinate (in angstrom) allowed
        call md_in(pLines, "zmin", zmin, -1.0d30)


        call md_in(pLines, "taddvel", taddvel, 0.0d0)
        call md_in(pLines, "zaddvel", zaddvel, 0.0d0)
        call md_in(pLines, "eaddvel", eaddvel, 0.0d0)
        call md_in(pLines, "vaddvel", vaddvel, 0.0d0)


        call md_in(pLines, "addvelt", addvelt, 180.0d0)
        call md_in(pLines, "addvelp", addvelp, 0.0d0)
        addvelt = addvelt*pi/180.0
        addvelp = addvelp*pi/180.0


        call md_in(pLines, "mtemp", mtemp)
        call md_in(pLines, "Tcontrolt", border_params%Tcontroltype, -1)
        call md_in(pLines, "temp0", temp0, 0.0d0)
        call md_in(pLines, "ntimeini", ntimeini, 99999999)
        call md_in(pLines, "timeini", timeini, 1.0d30)
        call md_in(pLines, "initemp", initemp, 0.0d0)
        call md_in(pLines, "tdebye", tdebye, 0.0d0)
        call md_in(pLines, "temp", temp)
        call md_in(pLines, "trate", trate, 1.0d0)
        call md_in(pLines, "fdamp", fdamp, 0.9d0)
        if (mtemp /= 3) then
            temp0 = temp0/invkBeV
            initemp = initemp/invkBeV
            tdebye = tdebye/invkBeV
            temp = temp/invkBeV
            trate = trate/invkBeV
        end if

        call md_in(pLines, "btctau", btctau, 0.0d0)

        call md_in(pLines, "toll", toll, 0d0)
        if (mtemp /= 3) then
            toll = toll/invkBeV
        end if

        call md_in(pLines, "TTact", TT_activated, .false.)
        call md_in(pLines, "TTtim", TT_time, .false., 1.0d30)
        call md_in(pLines, "TTmt", TT_mtemp, .false., 0)
        call md_in(pLines, "TTtem", TT_temp, .false., 0.0d0)
        call md_in(pLines, "TTtr", TT_trate, .false., 0.0d0)
        call md_in(pLines, "TTtau", TT_btctau, .false., btctau)


        call md_in(pLines, "bpcmode", bpcmode, 1)

        call md_in(pLines, "bpcbeta", bpcbeta, 0.0d0)
        call md_in(pLines, "bpctau", bpctau, 100.0d0)
        call md_in(pLines, "bpcP0", bpcP0, 0.0d0)
        call md_in(pLines, "bpcP0x", bpcP0x, bpcP0)
        call md_in(pLines, "bpcP0y", bpcP0y, bpcP0)
        call md_in(pLines, "bpcP0z", bpcP0z, bpcP0)

        call md_in(pLines, "bpcextz", bpcextz, 0.0d0)

        call md_in(pLines, "tmodtau", tmodtau, 0.0d0)


        call md_in(pLines, "amp", amp, 0d0)

        call md_in(pLines, "pscale", pscale, 0)
        call md_in(pLines, "remrot", remrot, 0)


        call md_in(pLines, "damp", damp, 0d0)
        call md_in(pLines, "prandom", prandom, 0.0d0)
        call md_in(pLines, "mrandom", mrandom, 10.0d0)
        call md_in(pLines, "timerand", timerandom, 1.0d20)


        call md_in(pLines, "Fzmaxz", Fzmaxz, 0.0d0)
        call md_in(pLines, "Fzmaxzr", Fzmaxzr, 0.0d0)
        call md_in(pLines, "FzmaxYz", FzmaxYz, 0.0d0)
        call md_in(pLines, "FzmaxZz", FzmaxZz, 0.0d0)
        call md_in(pLines, "dydtmaxz", dydtmaxz, 0.0d0)
        call md_in(pLines, "Fzmaxtyp", Fzmaxtyp, -1)


        call md_in(pLines, "avgvir", avgvir, 0)
        call md_in(pLines, "dydttime", dydttime, 0)
        call md_in(pLines, "outtype", outtype, 0)
        call md_in(pLines, "outzmin", outzmin, 0.0d0)
        call md_in(pLines, "outzmax", outzmax, 0.0d0)
        call md_in(pLines, "outzmin2", outzmin2, 0.0d0)
        call md_in(pLines, "outzmax2", outzmax2, 0.0d0)
        call md_in(pLines, "virsym", virsym, 0)
        call md_in(pLines, "virkbar", virkbar, 0)
        call md_in(pLines, "virboxsiz", virboxsiz, 1.0d0)


        call md_in(pLines, "melstop", melstop, 0)
        call md_in(pLines, "melstopst", melstopst, 0)   ! Elstop straggling?
        call md_in(pLines, "elstopmin", elstopmin, 1.0d0)


        call md_in(pLines, "sputlim", timesputlim, 1.0d0)
        call md_in(pLines, "nisputlim", nisputlim, 10.d30)
        call md_in(pLines, "essputlim", elstopsputlim, timesputlim)
        call md_in(pLines, "tssputlim", border_params%sputlim, 0.5d0)


        call md_in(pLines, "latflag", latflag)
        call md_in(pLines, "mdlatxyz", mdlatxyz, 0)


        if (latflag == LATFLAG_CREATE_LATTICE) then
            if (iprint) then
                call logger()
                call logger("Reading in atom basis:", 6)
                call logger("----------------------", 6)
                call logger()
            end if

            call md_in(pLines, "nbasis", nreadbasis)
            call md_in(pLines, "offset", readoffset, .false.)

            allocate(readbasis(nreadbasis, 3), readtype(nreadbasis))
            allocate(changeprob(nreadbasis), changeto(nreadbasis))
            call md_in(pLines, "lx", readbasis, .false., .false.)
            call md_in(pLines, "ltype", readtype, .false., 1)
            call md_in(pLines, "pchang", changeprob, .false., 0.0d0)
            call md_in(pLines, "changt", changeto, .false., 1)
        else
            allocate(readbasis(LARGEST_SUPPORTED_LATTICE, 3))
            allocate(readtype(LARGEST_SUPPORTED_LATTICE))
            allocate(changeprob(LARGEST_SUPPORTED_LATTICE))
            allocate(changeto(LARGEST_SUPPORTED_LATTICE))
        end if


        call md_in(pLines, "restartm", restartmode, 0)
        call md_in(pLines, "restartt", restartt, 0.0d0)

        call md_in(pLines, "nprtbl", nprtbl)

        call md_in(pLines, "ndump", ndump)
        call md_in(pLines, "nmovie", nmovie)
        call md_in(pLines, "moviemode", moviemode, 1)
        call md_in(pLines, "binmode", binmode, "Epot;Ekin;V.x;V.y;V.z")
        call md_in(pLines, "slicemode", slicemode, 1)
        call md_in(pLines, "zipout", zipout, 0)

        call md_in(pLines, "finalxyz", final_xyz_path, ":NO:FINAL:XYZ:")
        final_xyz = (final_xyz_path /= ":NO:FINAL:XYZ:")

        call md_in(pLines, "dtmov", dtmovie, .false., 0.0d0)
        call md_in(pLines, "tmov", tmovie, .false., 1.0d30)

        call md_in(pLines, "dslice", dslice, .false., -1.0d0)
        call md_in(pLines, "ECM", ECM, .false., 0.0d0)
        call md_in(pLines, "ECMfix", ECMfix, 0)
        call md_in(pLines, "dtsli", dtslice, .false., 1000.0d0)
        call md_in(pLines, "tsli", tslice, .false., 1.0d30)

        call md_in(pLines, "dCutIn", dsliceIn, .false., -1.0d0)
        call md_in(pLines, "ECMIn", ECMIn, .false., 0.0d0)


        call md_in(pLines, "ndefmovie", ndefmovie, 0)
        call md_in(pLines, "nliqanal", nliqanal, 0)
        call md_in(pLines, "nrestart", nrestart, 10000)

        call md_in(pLines, "nbalance", nbalance, -1)


        !===============
        ! RECOIL RELATED PARAMETERS
        ! Introduced by KN
        !==============

        call md_in(pLines, "irec", irec, 0)
        call md_in(pLines, "recatype", recatype, 1)
        call md_in(pLines, "recatypem", recatypem, 0)

        xrec = 1e30
        yrec = 1e30
        zrec = 1e30

        if (irec < 0) then
            call md_in(pLines, "xrec", xrec)
            call md_in(pLines, "yrec", yrec)
            call md_in(pLines, "zrec", zrec)
        end if

        call md_in(pLines, "recen", recen, 0.0d0)
        call md_in(pLines, "rectheta", rectheta, 0.0d0)
        call md_in(pLines, "recphi", recphi, 0.0d0)
        rectheta = rectheta*pi/180.0
        recphi = recphi*pi/180.0


        !     Parameter to determine whether recoil sequences should be used
        !     If they are, recoil data read in from recoildata.in

        call md_in(pLines, "rsnum", rsnum, 0)
        call md_in(pLines, "rsmode", rsmode, 1)

        call md_in(pLines, "recstopz", recstopz, -huge(1.0d0))
        call md_in(pLines, "recstopty", recstoptype, -999)

        call md_in(pLines, "recindex", recindex, -1)


        !
        !     Initialize random number generator
        !     To get a different series for each processor, add myproc !
        !
        call md_in(pLines, "seed", seed, 2347834)

        call md_in(pLines, "Ekdef", Ekdef, 0.0d0)
        call md_in(pLines, "Ekrec", Ekrec, 0.1d0)


        call md_in(pLines, "potmode", potmode, 1)
        call md_in(pLines, "spline", spline, 1)
        call md_in(pLines, "eamnbands", eamnbands, 1)
        call md_in(pLines, "Fpextrap", Fpextrap, 0)
        call md_in(pLines, "reppotcut", reppotcutin, 10.0d0)
        call md_in(pLines, "sw2mod", sw2mod, .false., 1.0d0)
        call md_in(pLines, "sw3mod", sw3mod, .false., 1.0d0)

        ! track params

        call md_in(pLines, "trackx", trackx, 0.0d0)
        call md_in(pLines, "tracky", tracky, 0.0d0)
        call md_in(pLines, "trackt", trackt, -1.0d0)
        call md_in(pLines, "trackmode", trackmode, 0)

        !
        ! BrennerBeardmore potential modifications
        !
        call md_in(pLines, "R1CC", R1CC, 1.70d0)
        call md_in(pLines, "R2CC", R2CC, 2.00d0)


        ! Parameters that have been removed from PARCAS.
        call removed_parameter(pLines, "mx_Xnodes")
        call removed_parameter(pLines, "mx_Ynodes")
        call removed_parameter(pLines, "nvac")
        call removed_parameter(pLines, "vaczmin")
        call removed_parameter(pLines, "Epdef")
        call removed_parameter(pLines, "Ebscale")
        call removed_parameter(pLines, "rcrit")
        call removed_parameter(pLines, "nisolmov")
        call removed_parameter(pLines, "nprocsreq")
        call removed_parameter(pLines, "reppotspl", "Old relic from hcparcas: Use reppotcut instead")

    end subroutine read_all_parameters


    !===========================================================================
    ! GENERIC FUNCTIONS FOR READING PARAMETERS
    !===========================================================================

    function get_parameter_name(pName, ind1, ind2) result(ret)
        character(*), intent(in) :: pName
        integer, intent(in), optional :: ind1, ind2

        character(:), allocatable :: ret

        if (present(ind1) .and. present(ind2)) then
            ret = pName // "(" // to_str(ind1) // "," // to_str(ind2) // ")"
        else if (present(ind1)) then
            ret = pName // "(" // to_str(ind1) // ")"
        else
            ret = pName
        end if
    end function

    ! Read a scalar value from the parameter list
    subroutine read_loop(pLines, pName, p, defaultPresent, arri, arrj)
        character(*), intent(in) :: pLines(:)
        character(*), intent(in) :: pName
        class(param), intent(inout) :: p
        logical, intent(in) :: defaultPresent
        ! Array parameters
        integer, intent(in), optional :: arri, arrj

        ! Local helpers
        character(len=:), allocatable :: pKey
        character(len=:), allocatable :: pVal

        integer :: i
        logical :: found

        found = .false.

        !--------------------------------------------------------------
        ! parameter information
        !--------------------------------------------------------------
        p%name = get_parameter_name(pName, arri, arrj)
        !--------------------------------------------------------------

        ! Loop over all parameter lines in the md.in file, looking for the
        ! desired parameter, pName
        do i = 1, size(pLines)
            ! Separate the left hand side from the right hand side of the
            ! current parameter line
            call extract_key_value(pLines(i), pKey, pVal)

            ! Check if either of the array indices are present, then we are
            ! looking for an array. By separating the parenthesis containing
            ! the array indices, it is possible to check that the names are
            ! matching as well.
            if ((present(arri) .or. present(arrj)) .and. &
                    check_array_name(pKey, pName)) then

                ! If both indices are present, we are looking for a 2D array
                if (present(arri) .and. present(arrj)) then
                    found = check_array_index(pKey, arri, arrj)

                ! If only one index is present, we are looking for a 1D array
                else if (present(arri)) then
                    found = check_array_index(pKey, arri)
                end if

            ! If looking for a scalar value, and the names are matching, we
            ! found what we were looking for
            else
                found = (pKey == pName)
            end if

            ! If we found the parameter, we can stop the search
            if (found) then
                exit
            end if
        end do

        if (found) then
            call p%read_val(pVal)
            p%read_status = STATUS_FOUND
            p%name = pKey
        else
            p%read_status = merge(STATUS_DEFAULT, STATUS_MISSING, defaultPresent)
        end if

    end subroutine


    ! Warn the user if the given parameter is present.
    subroutine removed_parameter(pLines, pName, message)
        character(len=*), intent(in) :: pLines(:), pName
        character(len=*), intent(in), optional :: message

        integer :: i, last

        do i = 1, size(pLines)
            last = scan(pLines(i), "=(")
            if (pLines(i)(:last-1) == pName) then
                if (iprint) then
                    call logger("WARNING: The parameter " // trim(pName) &
                        // " has been removed!")
                    if (present(message)) call logger(message, 4)
                end if
                return
            end if
        end do

    end subroutine removed_parameter


    subroutine read_int32b(pLines, pName, v, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        integer(int32b), intent(out) :: v
        integer(int32b), intent(in), optional :: vDefault

        type(int_param) :: p

        if (present(vDefault)) p%v = vDefault
        call read_loop(pLines, pName, p, present(vDefault))
        call p%print_status()
        v = p%v
    end subroutine


    subroutine read_real64b(pLines, pName, v, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        real(real64b), intent(out) :: v
        real(real64b), intent(in), optional :: vDefault

        type(real_param) :: p

        if (present(vDefault)) p%v = vDefault
        call read_loop(pLines, pName, p, present(vDefault))
        call p%print_status()
        v = p%v
    end subroutine


    subroutine read_character(pLines, pName, v, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        character(len=*), intent(out) :: v
        character(len=*), intent(in), optional :: vDefault

        type(character_param) :: p

        if (present(vDefault)) p%v = vDefault
        call read_loop(pLines, pName, p, present(vDefault))
        call p%print_status()
        v = p%v
    end subroutine


    subroutine read_logical(pLines, pName, v, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        logical, intent(out) :: v
        logical, intent(in), optional :: vDefault

        ! Hack for reading logicals as integers in the input file
        type(int_param) :: p

        if (present(vDefault)) p%v = merge(1, 0, vDefault)
        call read_loop(pLines, pName, p, present(vDefault))
        call p%print_status()
        v = (p%v /= 0)
    end subroutine


    !===========================================================================
    ! HELPER FUNCTIONS FOR READING 1D ARRAYS
    !===========================================================================

    subroutine read_int32b_1d_array(pLines, pName, v, zeroBased, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        integer(int32b), intent(out) :: v(:)
        logical, intent(in) :: zeroBased
        integer(int32b), intent(in), optional :: vDefault

        type(int_param) :: tmp
        integer :: i

        do i = 1, size(v)
            if (present(vDefault)) tmp%v = vDefault
            call read_loop(pLines, pName, tmp, present(vDefault), &
                merge(i-1, i, zeroBased))
            call tmp%print_status()
            v(i) = tmp%v
        end do
    end subroutine


    subroutine read_real64b_1d_array(pLines, pName, v, zeroBased, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        real(real64b), intent(out) :: v(:)
        logical, intent(in) :: zeroBased
        real(real64b), intent(in), optional :: vDefault

        type(real_param) :: tmp
        integer :: i

        do i = 1, size(v)
            if (present(vDefault)) tmp%v = vDefault
            call read_loop(pLines, pName, tmp, present(vDefault), &
                merge(i-1, i, zeroBased))
            call tmp%print_status()
            v(i) = tmp%v
        end do
    end subroutine


    subroutine read_character_1d_array(pLines, pName, v, zeroBased, vDefault)
        character(len=*), intent(in)  :: pLines(:)
        character(len=*), intent(in)  :: pName
        character(len=*), target, intent(out) :: v(:)
        logical, intent(in)           :: zeroBased
        character(len=*), intent(in), optional :: vDefault

        type(character_param) :: tmp
        integer :: i

        do i = 1, size(v)
            if (present(vDefault)) tmp%v = vDefault
            call read_loop(pLines, pName, tmp, present(vDefault), &
                merge(i-1, i, zeroBased))
            call tmp%print_status()
            v(i) = tmp%v
        end do
    end subroutine



    !===========================================================================
    ! HELPER FUNCTIONS FOR READING 2D ARRAYS
    !===========================================================================

    subroutine read_int32b_2d_array(pLines, pName, v, zeroBased, symmetric, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        integer(kind=int32b), intent(out) :: v(:,:)
        logical, intent(in) :: zeroBased, symmetric
        integer(kind=int32b), intent(in), optional :: vDefault

        integer :: i, j
        integer :: ind1, ind2

        type(int_param) :: tmp(size(v,1), size(v,2))

        ! Copy the default value to all parameters
        if (present(vDefault)) tmp(:,:)%v = vDefault

        ! Read the values
        do i = 1, size(v, 1)
            ind1 = merge(i-1, i, zeroBased)
            do j = 1, size(v, 2)
                ind2 = merge(j-1, j, zeroBased)
                call read_loop(pLines, pName, tmp(i,j), present(vDefault), &
                    ind1, ind2)
            end do
        end do

        ! Copy the symmetric values
        if (symmetric) then
            do i = 1, size(v, 1)
                ind1 = merge(i-1, i, zeroBased)
                do j = 1, size(v, 2)
                    ind2 = merge(j-1, j, zeroBased)
                    ! Copy the value symmetrically if it hasn't yet been found.
                    ! This demands that the symmetrical value has been found.
                    if (tmp(i,j)%read_status /= STATUS_FOUND .and. &
                            tmp(j,i)%read_status == STATUS_FOUND) then
                        tmp(i,j)%v = tmp(j,i)%v
                        tmp(i,j)%read_status = STATUS_FOUND
                        tmp(i,j)%name = get_parameter_name(pName, ind1, ind2)
                    end if
                end do
            end do
        end if

        do i = 1, size(v, 1)
            do j = 1, size(v, 2)
                call tmp(i,j)%print_status()
            end do
        end do

        v(:,:) = tmp(:,:)%v
    end subroutine


    subroutine read_real64b_2d_array(pLines, pName, v, zeroBased, symmetric, vDefault)
        character(len=*), intent(in) :: pLines(:), pName
        real(kind=real64b), target, intent(out) :: v(:,:)
        logical, intent(in) :: zeroBased, symmetric
        real(kind=real64b), intent(in), optional :: vDefault

        integer :: i, j
        integer :: ind1, ind2

        type(real_param) :: tmp(size(v,1), size(v,2))

        ! Copy the default value to all parameters
        if (present(vDefault)) tmp(:,:)%v = vDefault

        ! Read the values
        do i = 1, size(v, 1)
            ind1 = merge(i-1, i, zeroBased)
            do j = 1, size(v, 2)
                ind2 = merge(j-1, j, zeroBased)
                call read_loop(pLines, pName, tmp(i,j), present(vDefault), &
                    ind1, ind2)
            end do
        end do

        ! Copy the symmetric values
        if (symmetric) then
            do i = 1, size(v, 1)
                ind1 = merge(i-1, i, zeroBased)
                do j = 1, size(v, 2)
                    ind2 = merge(j-1, j, zeroBased)
                    ! Copy the value symmetrically if it hasn't yet been found.
                    ! This demands that the symmetrical value has been found.
                    if (tmp(i,j)%read_status /= STATUS_FOUND .and. &
                            tmp(j,i)%read_status == STATUS_FOUND) then
                        tmp(i,j)%v = tmp(j,i)%v
                        tmp(i,j)%read_status = STATUS_FOUND
                        tmp(i,j)%name = get_parameter_name(pName, ind1, ind2)
                    end if
                end do
            end do
        end if

        do i = 1, size(v, 1)
            do j = 1, size(v, 2)
                call tmp(i,j)%print_status()
            end do
        end do

        v(:,:) = tmp(:,:)%v
    end subroutine


    subroutine extract_key_value(line, pKey, pVal)
        character(*), intent(in) :: line
        character(:), allocatable, intent(out) :: pKey
        character(:), allocatable, intent(out) :: pVal

        integer :: pSep

        pSep = scan(line, "=")

        pKey = line(1 : pSep-1)     ! Parameter name
        pVal = line(pSep+1 :)       ! Parameter value + comment
    end subroutine


    logical function check_array_name(pKey, pName) result(ret)
        character(:), allocatable, intent(in) :: pKey
        character(*), intent(in) :: pName

        character(len(pKey)) :: pKeyOnlyName
        integer :: split

        ret = .false.

        ! Find the position of the index parenthesis
        split = scan(pKey, "(")

        ! If there is no parentheses in the variable name, that variable
        ! can't be part of an array
        if (split == 0) return

        ! Use the position of the index parenthesis to find the real
        ! name of the variable
        pKeyOnlyName = pKey(1 : split-1)

        ret = (pKeyOnlyName == pName)
    end function


    logical function check_array_index(pKey, arri, arrj) result(ret)
        character(:), allocatable, intent(in) :: pKey
        integer, intent(in) :: arri
        integer, intent(in), optional :: arrj

        integer :: ai, aj

        integer :: ind1, ind2, sep
        integer :: ios

        ret = .false.

        ! Extract the sub-string containing the array indices
        ind1 = index(pKey, "(")
        ind2 = index(pKey, ")")

        if (ind1 == 0 .or. ind2 == 0) then
            call logger("Missing array parentheses for parameter " // pKey)
            call my_mpi_abort("Bad md.in", myproc)
        end if

        ! Read the current indices from the parameter name in the input file
        if (present(arrj)) then
            sep = index(pKey, ",")
            read(pKey(ind1+1 : sep-1), *, iostat=ios) ai
            read(pKey(sep+1 : ind2-1), *, iostat=ios) aj
            ret = (arri == ai .and. arrj == aj)
        else
            read(pKey(ind1+1 : ind2-1), *, iostat=ios) ai
            ret = (arri == ai)
        end if

    end function

end module mdinit_mod
