!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************


module mdoutput_mod

    use datatypes, only: real64b
    use defs, only: MOVIE_CHUNK_SIZE
    use typeparam, only: itypelow, itypehigh, element, vunit, aunit
    use my_mpi

    use file_units, only: BLOCK_NATOMS_FILE
    use timers, only: tmr, TMR_MOVIE
    use para_common, only: myatoms, iprint, ibuf
    use output_logger, only: log_buf, logger

    implicit none
    save

    private
    public :: mdoutput_allocate
    public :: is_valid_moviemode
    public :: movietime
    public :: slicetime
    public :: write_mdlat_out
    public :: write_atoms_out
    public :: Slice_Movie
    public :: slice_init
    public :: slice_finalize
    public :: Movie
    public :: movie_init
    public :: movie_finalize
    public :: write_xyz_frame


    ! The maximum number of characters needed per atom for the formatted
    ! movie output in moviestrbuf. Remember to increase this if a new
    ! movie mode needs more space.
    integer, parameter :: CHARS_PER_ATOM = 200

    ! The movie buffer into which the formatted string is created.
    character(len=MOVIE_CHUNK_SIZE * CHARS_PER_ATOM), allocatable :: moviestrbuf

    integer :: MOVIE_FILE_HANDLE       = MPI_FILE_NULL ! Handle to the MPI md.movie file.
    integer :: BLOCK_ATOMS_FILE_HANDLE = MPI_FILE_NULL ! Handle to the MPI block_atoms.dat file.
    integer :: SLICE_FILE_HANDLE       = MPI_FILE_NULL ! Handle to the MPI slice.movie file.

  contains


    subroutine mdoutput_allocate()
        allocate(moviestrbuf)
    end subroutine mdoutput_allocate


    logical pure function is_valid_moviemode(n) result(valid)
        integer, intent(in) :: n

        valid = .false.
        if (0 <= n .and. n <= 9) valid = .true.
        if (15 <= n .and. n <= 18) valid = .true.
    end function


    !***************************************************************************

    logical function movietime(istep, nmovie, time_fs, tmovie, &
            dtmovie, tmax, restartt)

        ! Determine whether it is time to output the movie:
        ! if nmovie<0 no movie
        ! otherwise:
        !      - First time always (we wanna see the premiere after all !)
        !      - if dtmovie(1) <= 0 use nmovie values
        !      - if dtmovie(1) > 0 use tmovie, dtmovie logic
        !
        ! Modifications by Tommi Jarvi, 28.10.2008:
        !  -On rare occasions, it happened that prevtime got a value larger
        !   than nextmovietime so that movie output stopped at a random time.
        !  -Corrected the problem by changing the logic.
        !
        ! Modifications by Tommi Jarvi, 5.1.2009:
        !  -Changed so that whenever dtmovie is changed, the movie is output.
        !  -Note that the maximum index for tmovie(i) is 9.
        !  -Note also that tmovie(1:9) by default has to be set very large.
        !   Otherwise the output would not be predictable.


        ! --- variables

        ! input variables
        integer, intent(in) :: istep, nmovie
        real(kind=real64b), intent(in) :: time_fs, tmovie(:), dtmovie(:), tmax, restartt

        ! internal variables
        integer :: i  ! loops
        ! current i in dtmovie(i), start from the first one obviously
        integer, save :: currenti = 1
        ! the next time at which the movie should be output
        real(kind=real64b), save :: nexttime = 0.0


        ! --- begin

        ! on first step, step nexttime to time when simulation begins
        if(istep == 0 .and. restartt > 0.0) then
            nexttime = restartt
        endif

        ! default: no movie
        movietime = .false.


        ! no output for nmovie < 0 or after the simulation should have ended
        ! (last frame output separately)
        if (nmovie <= 0) return
        if (time_fs >= tmax) return

        ! --- BEGIN: check whether movie is output at this timestep

        ! - always take movie at first time step
        if (istep == 0) then
            movietime = .true.

        ! - take movies according to nmovie
        else if (dtmovie(1) <= 0.0) then
            if (MOD(istep,nmovie) == 0) then
                movietime = .true.
            end if

        ! - take movies according to dtmovie scheme
        else
            ! Check if we should take the movie
            !  (and if so, calculate the time for the
            !   next movie frame)
            if (time_fs > nexttime) then
                movietime = .true.
            end if
        end if

        ! --- Calculate new nexttime for dtmovie scheme
        !     -default to previous + current dtmovie
        !     -always take movie when changing dtmovie
        !     -only move to higher dtmovie index
        !     -a dtmovie index can be skipped if the interval
        !      between the tmovies is small
        if (dtmovie(1) > 0.0 .and. movietime) then
            ! default
            nexttime = nexttime + dtmovie(currenti)

            ! check whether a tmovie is reached before nexttime
            do i = currenti, 9
                if (nexttime >= tmovie(i)) then
                    currenti = i + 1
                    nexttime = tmovie(i)
                end if
            end do
        end if

        ! --- END: check whether movie is output at this timestep
    end function movietime


    !***************************************************************************

    logical function slicetime(istep, time_fs, tslice, dtslice)

        !
        ! Determine whether it is time to output the slice movie:
        !      - if dtslice(1)<0 no slice movie
        !      - First time always (we wanna see the premiere after all !)
        !      - if dtslice(1) >= 0 use tslice, dtslice logic
        !
        ! For cascades, center ECM is obtained automatically as energy
        ! center of mass. For non-cascades, it can be read in; thus
        ! ECM, dslice can be used to get any fixed cell region in a
        ! a non-cascade case.
        !
        ! Modifications by Tommi Jarvi, 5.1.2009:
        !  -See movietime() above.
        !  -The only difference is that there is no
        !   check for time_fs >= tmax as there was none in the original version.

        ! --- variables

        ! input variables
        integer, intent(in) :: istep
        real(kind=real64b), intent(in) :: time_fs, tslice(:), dtslice(:)

        ! internal variables
        integer :: i  ! loops
        ! current i in dtmovie(i), start from the first one obviously
        integer, save :: currenti = 1
        ! the next time at which the movie should be output
        real(kind=real64b),save :: nexttime = 0.0


        ! --- begin

        ! default: no slice movie
        slicetime = .false.

        ! no movie for dtslice(1) <= 0
        if (dtslice(1) <= 0.0) return

        ! always take movie at first time step
        if (istep == 0) then
            slicetime = .true.
        end if

        ! check if we want the movie
        if (time_fs > nexttime) then
            slicetime = .true.
        end if

        ! --- Calculate new nexttime for dtslice scheme
        !     -See movietime() above, the logic here is the same
        if (dtslice(1) > 0.0 .and. slicetime) then
            ! default
            nexttime = nexttime + dtslice(currenti)

            ! check whether a tslice is reached before nexttime
            do i = currenti, 9
                if (nexttime >= tslice(i)) then
                    currenti = i + 1
                    nexttime = tslice(i)
                end if
            end do
        end if

    end function slicetime


    !***************************************************************************

    !
    ! Write the atoms out to the mdlat.out file, overwriting
    ! any pre-existing file.
    !
    ! To restart from this., simply move mout/mdlat.out to
    ! in/mdlat.in and change the latflag to 3 in in/md.in.
    !
    ! Due to historical reasons (and for possible future extension),
    ! two "1":s are output after the atom type.
    !
    subroutine write_mdlat_out(x0, x1, box, delta, atype, atomindex)

        real(real64b), intent(in) :: &
            x0(:), x1(:), &
            box(3), &
            delta(itypelow:itypehigh)

        integer, intent(in) :: &
            atype(:), &
            atomindex(:)

        integer :: mdlat_file_handle
        integer :: i
        integer :: nchars
        integer :: ichunk, nchunks, mynchunks
        logical :: file_exists
        integer :: ierror


        if (iprint) then
            call logger("mdlat output for boxsize", box(:), 0)

            do i = itypelow, itypehigh
                write(log_buf,'(A,I3,A,3F13.8)') 'For atype', i,&
                    ' to get v [A/fs] from mdlat, multiply by', &
                    vunit(i)*box(:)
                call logger(log_buf)
            end do
        end if


        ! If the mdlat.out file already exists, save it to mdlat_prev.out.
        if (iprint) then
            inquire(file = "out/mdlat.out", exist=file_exists)
            if (file_exists) then
                call system('mv out/mdlat.out out/mdlat_prev.out')
            end if
        end if


        ! Open the file in all processes.
        call my_mpi_open_output_file("out/mdlat.out", mdlat_file_handle)


        ! Compute how many chunks the atoms have to be split into to make sure
        ! their formatted string fits inside moviestrbuf.
        mynchunks = ceiling(myatoms / real(MOVIE_CHUNK_SIZE, kind=real64b))
        nchunks = mynchunks
        call my_mpi_max(nchunks, 1)


        ! Write the atom data.
        chunk_loop: do ichunk = 1, nchunks
            call format_mdlat_out_chunk(nchars, ichunk, x0, x1, delta, atype, atomindex)

            call mpi_file_write_ordered(mdlat_file_handle, &
                moviestrbuf, nchars, &
                mpi_character, MPI_STATUS_IGNORE, ierror)
        end do chunk_loop


        ! Close the file, writing it to disk.
        call mpi_file_sync(mdlat_file_handle, ierror)
        call mpi_file_close(mdlat_file_handle, ierror)

    end subroutine write_mdlat_out


    !
    ! Format a chunk of MOVIE_CHUNK_ATOMS atoms into a string in the
    ! buffer moviestrbuf, for writing to mdlat.out, and return the
    ! number of written characters in nchars.
    !
    ! If this processor has no atoms in this chunk, just set nchars to zero.
    !
    subroutine format_mdlat_out_chunk(nchars, ichunk, x0, x1, delta, atype, atomindex)

        integer, intent(out) :: nchars

        real(real64b), intent(in) :: &
            x0(:), x1(:), &
            delta(itypelow:itypehigh)

        integer, intent(in) :: &
            ichunk, &
            atype(:), &
            atomindex(:)

        integer :: miniatom, maxiatom
        integer :: iatom, i3
        integer :: thislen

        nchars = 0
        miniatom = chunk_miniatom(ichunk)
        maxiatom = chunk_maxiatom(ichunk, myatoms)

        format_loop: do iatom = miniatom, maxiatom
            i3 = 3 * iatom - 3

            ! Use larger output field if atom is very far.
            ! Note: this is in internal units, i.e. the cell boundary is at 0.5.
            ! TODO: is the 100.0d0 wrong, when all others are 10000.0d0?
            if (any(abs(x0(13+1:13+3)) >= 100.0d0)) then
                thislen = 3 * (1 + 20)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(3(1x,f20.8))') &
                    x0(i3+1:i3+3)
            else
                thislen = 3 * (1 + 12)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(3(1x,f12.8))') &
                    x0(i3+1:i3+3)
            end if
            nchars = nchars + thislen

            thislen = 3 + (2 * 2) + (3 * 13) + 10
            write(moviestrbuf(nchars+1 : nchars+thislen), &
                '(i3,2i2,3g13.5,i10)') &
                atype(iatom), 1, 1, &
                x1(i3+1:i3+3) / delta(abs(atype(iatom))), &
                atomindex(iatom)
            nchars = nchars + thislen


            ! Write end of line
            write(moviestrbuf(nchars+1:nchars+1), '(A1)') new_line('x')
            nchars = nchars + 1

            if (nchars > len(moviestrbuf)) then
                call my_mpi_abort("moviestrbuf overflow", myproc)
            end if

        end do format_loop

    end subroutine format_mdlat_out_chunk


    !***************************************************************************

    !
    ! Write the atoms out to the atoms.out file, overwriting
    ! any pre-existing file.
    !
    ! atoms.out contains the real coordinates along with the final energies.
    !
    ! Output is:
    ! EAM: x y z [A] rho F(rho)[eV] Epair[eV] index Wxx Wyy Wzz [eV] atype
    ! Semic: x y z [A] Ethree[eV] 0 Epair[eV] index Wxx Wyy Wzz [eV] atype
    !
    ! Because V is not always well defined, Virial W is output in eV.
    !
    ! To get kbar from virial, multiply by ev_to_kbar = e/1d-30/1d8
    ! and then divide by the atomic volume Omega.
    !
    subroutine write_atoms_out(x0, box, atype, atomindex, &
                Ethree, P, Epair, EAM, wxxi, wyyi, wzzi)

        real(real64b), intent(in) :: &
            x0(:), &
            box(3), &
            Ethree(:), Epair(:), &
            P(:,:), &
            wxxi(:), wyyi(:), wzzi(:)

        integer, intent(in) :: &
            atype(:), &
            atomindex(:)

        logical, intent(in) :: EAM

        integer :: atoms_file_handle
        integer :: nchars
        integer :: ichunk, nchunks, mynchunks
        integer :: ierror

        ! Open the file in all processes.
        call my_mpi_open_output_file("out/atoms.out", atoms_file_handle)


        ! Compute how many chunks the atoms have to be split into to make sure
        ! their formatted string fits inside moviestrbuf.
        mynchunks = ceiling(myatoms / real(MOVIE_CHUNK_SIZE, kind=real64b))
        nchunks = mynchunks
        call my_mpi_max(nchunks, 1)


        ! Write the atom data.
        chunk_loop: do ichunk = 1, nchunks
            call format_atoms_out_chunk(nchars, ichunk, x0, box, atype, atomindex, &
                Ethree, P, Epair, EAM, wxxi, wyyi, wzzi)

            call mpi_file_write_ordered(atoms_file_handle, &
                moviestrbuf, nchars, &
                mpi_character, MPI_STATUS_IGNORE, ierror)
        end do chunk_loop


        ! Close the file, writing it to disk.
        call mpi_file_sync(atoms_file_handle, ierror)
        call mpi_file_close(atoms_file_handle, ierror)

    end subroutine write_atoms_out


    !
    ! Format a chunk of MOVIE_CHUNK_ATOMS atoms into a string in the
    ! buffer moviestrbuf, for writing to atoms.out, and return the
    ! number of written characters in nchars.
    !
    ! If this processor has no atoms in this chunk, just set nchars to zero.
    !
    subroutine format_atoms_out_chunk(nchars, ichunk, x0, box, atype, atomindex, &
            Ethree, P, Epair, EAM, wxxi, wyyi, wzzi)

        integer, intent(out) :: nchars

        real(real64b), intent(in) :: &
            x0(:), &
            box(3), &
            Ethree(:), Epair(:), &
            P(:,:), &
            wxxi(:), wyyi(:), wzzi(:)

        integer, intent(in) :: &
            ichunk, &
            atype(:), &
            atomindex(:)

        logical, intent(in) :: EAM

        integer :: iatom, i3
        integer :: miniatom, maxiatom
        real(real64b) :: Emany1, Emany2
        integer :: thislen
        character(len=20) :: thisformat
        logical :: huge_dists

        nchars = 0
        miniatom = chunk_miniatom(ichunk)
        maxiatom = chunk_maxiatom(ichunk, myatoms)

        format_loop: do iatom = miniatom, maxiatom
            i3 = 3 * iatom - 3

            ! Use larger output field if atom is very far.
            huge_dists = any(abs(x0(i3+1:i3+3) * box(1:3)) >= 10000.0d0)

            if (huge_dists) then
                thislen = 5 * (1 + 20)
                thisformat = '(5(1x,f20.6))'
            else
                thislen = 5 * (1 + 12)
                thisformat = '(5(1x,f12.6))'
            end if

            if (EAM) then
                Emany1 = P(iatom, 1)
                Emany2 = Ethree(iatom)
            else
                Emany1 = Ethree(iatom)
                Emany2 = 0.0d0
            end if

            write(moviestrbuf(nchars+1 : nchars+thislen), &
                thisformat) &
                x0(i3+1:i3+3) * box(1:3), &
                Emany1, Emany2
            nchars = nchars + thislen

            thislen = 15 + 10
            write(moviestrbuf(nchars+1 : nchars+thislen), &
                '(f15.6,i10)') &
                Epair(iatom), atomindex(iatom)
            nchars = nchars + thislen

            if (huge_dists) then
                thislen = 3 * (1 + 12)
                thisformat = '(3(1x,g12.4))'
            else
                thislen = 3 * (1 + 10)
                thisformat = '(3(1x,g10.4))'
            end if

            write(moviestrbuf(nchars+1 : nchars+thislen), &
                thisformat) &
                wxxi(iatom), &
                wyyi(iatom), &
                wzzi(iatom)
            nchars = nchars + thislen

            thislen = 1 + 2
            write(moviestrbuf(nchars+1 : nchars+thislen), &
                '(1x,i2)') &
                atype(iatom)
            nchars = nchars + thislen


            ! Write end of line
            write(moviestrbuf(nchars+1:nchars+1), '(A1)') new_line('x')
            nchars = nchars + 1

            if (nchars > len(moviestrbuf)) then
                call my_mpi_abort("moviestrbuf overflow", myproc)
            end if

        end do format_loop

    end subroutine format_atoms_out_chunk


    !***************************************************************************

    !
    ! Open file for slice writing.
    !
    subroutine slice_init(dslice, slicemode)
        real(real64b), intent(in) :: dslice(3)
        integer, intent(in) :: slicemode

        if (any(dslice >= 0.0) .and. slicemode /= 9) then
            call my_mpi_open_output_file("out/slice.movie", SLICE_FILE_HANDLE)
        end if
    end subroutine slice_init


    !
    ! Close file for slice writing.
    ! This must be called before calling MPI_Finalize().
    !
    subroutine slice_finalize()
        integer :: ierror

        if (SLICE_FILE_HANDLE /= MPI_FILE_NULL) then
            call mpi_file_sync(SLICE_FILE_HANDLE, ierror)
            call mpi_file_close(SLICE_FILE_HANDLE, ierror)
        end if
    end subroutine slice_finalize


    !
    ! Write a frame to the slice.movie file.
    !
    subroutine Slice_Movie(x0, x1, x2, delta, atype, Ekin, box, atomindex, &
            dslice, ECM, istep, time_fs, slicemode)

        real(kind=real64b), intent(in) :: &
            x0(:), x1(:), x2(:), &
            Ekin(:), &
            box(3), dslice(3), ECM(3), &
            delta(itypelow:itypehigh), &
            time_fs

        integer, intent(in) :: &
            istep, &
            atype(:), atomindex(:), &
            slicemode

        integer :: iatom, i3
        integer :: nchars
        integer :: included_atoms, tot_included_atoms
        integer :: ichunk, nchunks, mynchunks
        real(real64b) :: dr(3)
        character(300) :: tmpstr
        integer :: ierror


        ! Find the indexes of the atoms which are included in the slice
        ! and write them to ibuf.
        included_atoms = 0

        find_loop: do iatom = 1, myatoms
            i3 = 3 * iatom - 3

            ! Check whether the atom is near enough to the energy center of mass.
            dr(:) = abs(x0(i3+1:i3+3) * box(:) - ECM(:))
            !where (dr(:) > box(:) / 2.0d0) dr(:) = box(:) - dr(:)

            if (all(dslice(:) < 0.0 .or. dr(:) < dslice(:))) then
                included_atoms = included_atoms + 1
                ibuf(included_atoms) = iatom
            end if
        end do find_loop


        ! Write the frame header.
        tot_included_atoms = included_atoms
        call my_mpi_sum(tot_included_atoms, 1)

        if (iprint) then
            call logger("nprinted", tot_included_atoms, 0)

            ! Make sure you update the length of tmpstr if changing this format.
            write(tmpstr, '(i10,A1,A,i7,g11.4,A,3F9.3,A,3F9.3,A1)') &
                tot_included_atoms, &
                new_line('x'), &
                " Sliceframe number ", istep, time_fs, &
                " fs center", ECM(:), &
                " size", merge(2 * dslice, -1d0, dslice > 0), &
                new_line('')

            call mpi_file_write_shared(SLICE_FILE_HANDLE, &
                tmpstr, len(trim(tmpstr)), &
                mpi_character, MPI_STATUS_IGNORE, ierror)
        end if


        ! Compute how many chunks the atoms have to be split into to make sure
        ! their formatted string fits inside moviestrbuf.
        mynchunks = ceiling(myatoms / real(MOVIE_CHUNK_SIZE, kind=real64b))
        nchunks = mynchunks
        call my_mpi_max(nchunks, 1)


        ! Write the atom data.
        chunk_loop: do ichunk = 1, nchunks
            call format_slice_chunk(nchars, ichunk, included_atoms, x0, x1, x2, &
                delta, atype, Ekin, box, atomindex, slicemode)

            call mpi_file_write_ordered(SLICE_FILE_HANDLE, &
                moviestrbuf, nchars, &
                mpi_character, MPI_STATUS_IGNORE, ierror)
        end do chunk_loop

    end subroutine Slice_Movie


    !
    ! Format a chunk of MOVIE_CHUNK_ATOMS atoms into a string in the
    ! buffer moviestrbuf, for writing to slice.movie, and return the
    ! number of written characters in nchars.
    !
    ! If this processor has no atoms in this chunk, just set nchars to zero.
    !
    ! The indexes of the included atoms must be in ibuf(1 : included_atoms).
    !
    subroutine format_slice_chunk(nchars, ichunk, included_atoms, x0, x1, x2, delta, atype, &
            Ekin, box, atomindex, slicemode)

        integer, intent(out) :: nchars

        real(kind=real64b), intent(in) :: &
            x0(:), x1(:), x2(:), &
            Ekin(:), &
            box(3), &
            delta(itypelow:itypehigh)

        integer, intent(in) :: &
            ichunk, &
            included_atoms, &
            atype(:), atomindex(:), &
            slicemode

        integer :: mini, maxi
        integer :: i, iatom, i3, itype
        integer :: thislen

        nchars = 0
        mini = chunk_miniatom(ichunk)
        maxi = chunk_maxiatom(ichunk, included_atoms)

        format_loop: do i = mini, maxi
            iatom = ibuf(i)
            i3 = 3 * iatom - 3
            itype = abs(atype(iatom))


            ! Write common values.
            thislen = 2 + 3 * (11 + 1) + 2 + 13 + 1 + 9
            write(moviestrbuf(nchars+1 : nchars+thislen), &
                '(a2,3(f11.5,1x),i2,g13.5,1x,i9)') &
                element(itype), &
                x0(i3+1:i3+3) * box(:), &
                atype(iatom), &
                Ekin(iatom), &
                atomindex(iatom)
            nchars = nchars + thislen


            ! Write more values depending on slicemode.
            if (slicemode == 2) then
                thislen = 6 * (1 + 11)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(6(1x,f11.5))') &
                    x1(i3+1:i3+3) * box(:) / delta(itype) * vunit(itype), &
                    x2(i3+1:i3+3) * box(:) / delta(itype)**2 / 2.0d0 * aunit(itype)
                nchars = nchars + thislen
            end if


            ! Write end of line
            write(moviestrbuf(nchars+1:nchars+1), '(A1)') new_line('x')
            nchars = nchars + 1

            if (nchars > len(moviestrbuf)) then
                call my_mpi_abort("moviestrbuf overflow", myproc)
            end if

        end do format_loop

    end subroutine format_slice_chunk


    !***************************************************************************

    !
    ! Open files for movie writing.
    !
    subroutine movie_init(moviemode)
        integer, intent(in) :: moviemode

        real(real64b) :: time_start

        time_start = mpi_wtime()

        if (moviemode /= 9) then
            call my_mpi_open_output_file("out/md.movie", MOVIE_FILE_HANDLE)
        end if

        if (moviemode == 16 .or. moviemode == 18) then
            call my_mpi_open_output_file("out/block_atoms.dat", BLOCK_ATOMS_FILE_HANDLE)

            if (myproc == 0) then
                open(BLOCK_NATOMS_FILE, file="out/block_natoms.dat", status="replace")
            end if
        end if

        tmr(TMR_MOVIE) = tmr(TMR_MOVIE) + (mpi_wtime() - time_start)
    end subroutine movie_init


    !
    ! Close files used for movies.
    ! This must be called before calling MPI_Finalize().
    !
    subroutine movie_finalize(moviemode)
        integer, intent(in) :: moviemode

        integer :: ierror
        real(real64b) :: time_start

        time_start = mpi_wtime()

        if (MOVIE_FILE_HANDLE /= MPI_FILE_NULL) then
            call mpi_file_sync(MOVIE_FILE_HANDLE, ierror)
            call mpi_file_close(MOVIE_FILE_HANDLE, ierror)
        end if

        if (BLOCK_ATOMS_FILE_HANDLE /= MPI_FILE_NULL) then
            call mpi_file_sync(BLOCK_ATOMS_FILE_HANDLE, ierror)
            call mpi_file_close(BLOCK_ATOMS_FILE_HANDLE, ierror)
        end if

        if (moviemode == 16 .or. moviemode == 18) then
            if (myproc == 0) then
                close(BLOCK_NATOMS_FILE)
            end if
        end if

        tmr(TMR_MOVIE) = tmr(TMR_MOVIE) + (mpi_wtime() - time_start)
    end subroutine movie_finalize


    !
    ! Write a movie frame to md.movie.
    ! For moviemodes 16 and 18, also write block_atoms.dat and block_natoms.dat.
    !
    subroutine Movie(x0, x1, xnp, delta, atype, atomindex, natoms, box, pbc, &
            istep, time, Epair, Ethree, Ekin, wxxi, wyyi, wzzi, &
            wxyi, wxzi, wyzi, moviemode, &
            outtype, outzmin, outzmax, outzmin2, outzmax2, virsym, virkbar, virboxsiz)

        real(kind=real64b), intent(in) :: &
            x0(:), x1(:), &
            xnp(:), &
            box(3), pbc(3), &
            delta(itypelow:itypehigh), &
            time, &
            Epair(:), Ethree(:), Ekin(:), &
            wxxi(:), wyyi(:), wzzi(:), &
            wxyi(:), wxzi(:), wyzi(:), &
            virboxsiz, &
            outzmin, outzmax, outzmin2, outzmax2

        integer, intent(in) :: &
            virsym, virkbar, &
            outtype, &
            istep, &
            natoms, &
            atype(:), atomindex(:), &
            moviemode

        call write_movie_frame(MOVIE_FILE_HANDLE, .true., &
            x0, x1, xnp, delta, atype, atomindex, natoms, box, pbc, &
            istep, time, Epair, Ethree, Ekin, wxxi, wyyi, wzzi, &
            wxyi, wxzi, wyzi, moviemode, &
            outtype, outzmin, outzmax, outzmin2, outzmax2, virsym, virkbar, virboxsiz)

    end subroutine Movie


    !
    ! Write a movie frame as XYZ to the given file path.
    !
    subroutine write_xyz_frame(file_path, &
            x0, x1, xnp, delta, atype, atomindex, natoms, box, pbc, &
            istep, time, Epair, Ethree, Ekin, wxxi, wyyi, wzzi, &
            wxyi, wxzi, wyzi, moviemode, &
            outtype, outzmin, outzmax, outzmin2, outzmax2, virsym, virkbar, virboxsiz)

        real(kind=real64b), intent(in) :: &
            x0(:), x1(:), &
            xnp(:), &
            box(3), pbc(3), &
            delta(itypelow:itypehigh), &
            time, &
            Epair(:), Ethree(:), Ekin(:), &
            wxxi(:), wyyi(:), wzzi(:), &
            wxyi(:), wxzi(:), wyzi(:), &
            virboxsiz, &
            outzmin, outzmax, outzmin2, outzmax2

        integer, intent(in) :: &
            virsym, virkbar, &
            outtype, &
            istep, &
            natoms, &
            atype(:), atomindex(:), &
            moviemode

        character(len=*), intent(in) :: file_path

        integer :: file_handle
        integer :: ierror

        if (iprint) call logger("Writing XYZ frame to file " // trim(file_path))

        ! Open the file in all processes.
        call my_mpi_open_output_file(file_path, file_handle)


        call write_movie_frame(file_handle, .false., &
            x0, x1, xnp, delta, atype, atomindex, natoms, box, pbc, &
            istep, time, Epair, Ethree, Ekin, wxxi, wyyi, wzzi, &
            wxyi, wxzi, wyzi, moviemode, &
            outtype, outzmin, outzmax, outzmin2, outzmax2, virsym, virkbar, virboxsiz)


        ! Close the file, writing it to disk.
        call mpi_file_sync(file_handle, ierror)
        call mpi_file_close(file_handle, ierror)

    end subroutine write_xyz_frame


    !
    ! Write a movie frame to the given MPI file handle.
    ! For moviemodes 16 and 18, also write block_atoms.dat and block_natoms.dat,
    ! but only if allow_write_block_atoms is true.
    !
    subroutine write_movie_frame(file_handle, allow_write_block_atoms, &
            x0, x1, xnp, delta, atype, atomindex, natoms, box, pbc, &
            istep, time, Epair, Ethree, Ekin, wxxi, wyyi, wzzi, &
            wxyi, wxzi, wyzi, moviemode, &
            outtype, outzmin, outzmax, outzmin2, outzmax2, virsym, virkbar, virboxsiz)

        real(kind=real64b), intent(in) :: &
            x0(:), x1(:), &
            xnp(:), &
            box(3), pbc(3), &
            delta(itypelow:itypehigh), &
            time, &
            Epair(:), Ethree(:), Ekin(:), &
            wxxi(:), wyyi(:), wzzi(:), &
            wxyi(:), wxzi(:), wyzi(:), &
            virboxsiz, &
            outzmin, outzmax, outzmin2, outzmax2

        integer, intent(in) :: &
            file_handle, &
            virsym, virkbar, &
            outtype, &
            istep, &
            natoms, &
            atype(:), atomindex(:), &
            moviemode

        logical, intent(in) :: allow_write_block_atoms

        integer :: ichunk, nchunks, mynchunks
        integer :: nchars
        integer :: included_atoms, tot_included_atoms
        integer :: ierror
        character(len=200) :: tmpstr
        real(real64b) :: time_start
        logical :: write_block_atoms

        time_start = mpi_wtime()

        select case (moviemode)
        case (16, 18)
            write_block_atoms = allow_write_block_atoms
        case default
            write_block_atoms = .false.
        end select


        ! Write the frame header.
        if (iprint) then
            ! Make sure you update the length of tmpstr if changing this format.
            write(tmpstr, '(i12,A1,A,i7,g14.5,A,3(f11.6,1x),A,3i2,A1)') &
                natoms, new_line('x'), &
                ' Frame number ', istep, time, &
                ' fs boxsize', box(1:3), &
                'pbc', int(pbc(1:3)), new_line('x')

            call mpi_file_write_shared(file_handle, &
                tmpstr, len(trim(tmpstr)), &
                mpi_character, MPI_STATUS_IGNORE, ierror)
        end if


        ! Compute how many chunks the atoms have to be split into to make sure
        ! their formatted string fits inside moviestrbuf.
        mynchunks = ceiling(myatoms / real(MOVIE_CHUNK_SIZE, kind=real64b))
        nchunks = mynchunks
        call my_mpi_max(nchunks, 1)


        ! Total number of atoms written to block_atoms.dat in this frame
        ! (for moviemodes 16 and 18).
        tot_included_atoms = 0


        chunk_loop: do ichunk = 1, nchunks

            ! The mpi_file_write_ordered call below is collective, so all processes
            ! must call it. If this process has no more atoms, write nothing, i.e.
            ! a zero-length string.
            ! mpi_file_write_ordered is faster than the non-collective function
            ! mpi_file_write_shared, since it allows the MPI implementation to optimize
            ! the file access pattern, parallelizing writes better. It also guarantees
            ! the order of the output, in MPI rank order.

            call format_movie_chunk(nchars, moviemode, ichunk, natoms, &
                virkbar, virsym, virboxsiz, box, x0, x1, xnp, &
                Epair, Ethree, Ekin, &
                wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
                atype, atomindex, delta)

            call mpi_file_write_ordered(file_handle, &
                moviestrbuf, nchars, &
                mpi_character, MPI_STATUS_IGNORE, ierror)


            ! moviemodes 16 and 18 are special in that they also print to the
            ! files block_atoms.dat and block_natoms.dat. Therefore they must
            ! be handled separately.

            if (write_block_atoms) then
                call format_block_atoms_chunk( &
                    nchars, included_atoms, moviemode, ichunk, natoms, &
                    virkbar, virsym, virboxsiz, box, x0, &
                    wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
                    atype, atomindex, &
                    outtype, outzmin, outzmax, outzmin2, outzmax2)

                tot_included_atoms = tot_included_atoms + included_atoms

                call mpi_file_write_ordered(BLOCK_ATOMS_FILE_HANDLE, &
                    moviestrbuf, nchars, &
                    mpi_character, MPI_STATUS_IGNORE, ierror)
            end if

        end do chunk_loop


        if (write_block_atoms) then
            ! Write the number of atoms included to block_natoms.dat.
            call my_mpi_sum(tot_included_atoms, 1)

            if (myproc == 0) then
                write(BLOCK_NATOMS_FILE, "(i9)") tot_included_atoms
            end if
        end if

        tmr(TMR_MOVIE) = tmr(TMR_MOVIE) + (mpi_wtime() - time_start)
    end subroutine write_movie_frame




    !
    ! Return the unit factor that virials are multiplied with for
    ! movie output.
    !
    pure real(real64b) function virial_unit_factor(&
            moviemode, virkbar, virboxsiz, box, natoms)

        integer, intent(in) :: moviemode, virkbar, natoms
        real(real64b), intent(in) :: virboxsiz, box(:)

        real(real64b), parameter :: &
            e = 1.602176462d-19, &
            eV_to_kbar = e / 1d-30 / 1d8

        real(real64b) :: kbarfactor

        kbarfactor = eV_to_kbar / (product(box(1:3)) * virboxsiz / natoms)

        select case (moviemode)
        case (5, 6, 15, 16)
            virial_unit_factor = merge(1.0d0, kbarfactor, virkbar == 0)
        case default
            virial_unit_factor = kbarfactor
        end select
    end function virial_unit_factor


    ! Return the lowest atom index in the movie frame chunk number ichunk.
    pure integer function chunk_miniatom(ichunk)
        integer, intent(in) :: ichunk

        chunk_miniatom = (ichunk - 1) * MOVIE_CHUNK_SIZE + 1
    end function chunk_miniatom


    ! Return the highest atom index in the movie frame chunk number ichunk.
    pure integer function chunk_maxiatom(ichunk, myatoms)
        integer, intent(in) :: ichunk, myatoms

        chunk_maxiatom = min(myatoms, ichunk * MOVIE_CHUNK_SIZE)
    end function chunk_maxiatom


    !
    ! Format a chunk of MOVIE_CHUNK_ATOMS atoms into a string in the
    ! buffer moviestrbuf, for writing to md.movie, and return the
    ! number of written characters in nchars.
    !
    ! If this processor has no atoms in this chunk, just set nchars to zero.
    !
    subroutine format_movie_chunk(nchars, moviemode, ichunk, natoms, &
            virkbar, virsym, virboxsiz, box, x0, x1, xnp, &
            Epair, Ethree, Ekin, &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
            atype, atomindex, delta)

        integer, intent(out) :: nchars

        real(real64b), intent(in) :: &
            virboxsiz, &
            delta(itypelow:itypehigh), &
            x0(:), x1(:), &
            xnp(:), box(3), &
            Epair(:), Ethree(:), Ekin(:)

        real(real64b), intent(in), dimension(:) :: &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi

        integer, intent(in) :: &
            ichunk, &
            virkbar, virsym, &
            moviemode, &
            natoms, &
            atype(:), atomindex(:)

        integer :: iatom, i3, itype, k
        integer :: miniatom, maxiatom
        integer :: thislen
        real(real64b) :: vir_unitfactor
        real(real64b) :: epot


        vir_unitfactor = virial_unit_factor(moviemode, virkbar, virboxsiz, box, natoms)

        nchars = 0
        miniatom = chunk_miniatom(ichunk)
        maxiatom = chunk_maxiatom(ichunk, myatoms)

        atom_loop: do iatom = miniatom, maxiatom
            i3 = 3*iatom - 3
            itype = abs(atype(iatom))


            call movie_common_format(nchars, iatom, moviemode, &
                atype, atomindex, x0, box)


            ! Write the rest of the output depending on moviemode and other parameters.
            select case (moviemode)
            case (2)
                if (delta(itype) == 0.0) then
                    write(log_buf, *) 'delta = 0! Problem!', iatom, atype(iatom), delta(itype)
                    call logger(log_buf)
                end if

                thislen = 3 * (1 + 11)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(3(1x,f11.7))') &
                    (x1(i3+k) * box(k) / delta(itype) * vunit(itype), k = 1, 3)
                nchars = nchars + thislen

            case (3, 4)
                epot = Ethree(iatom) + Epair(iatom)

                thislen = 1 + 12
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(1x,f12.6)') &
                    epot
                nchars = nchars + thislen

            case (5)
                thislen = 3 * (1 + 11)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(3(1x,f11.7))') &
                    wxxi(iatom) * vir_unitfactor, &
                    wyyi(iatom) * vir_unitfactor, &
                    wzzi(iatom) * vir_unitfactor
                nchars = nchars + thislen

            case (6)
                if (delta(itype) == 0.0) then
                    write(log_buf, *) 'delta = 0! Problem!', iatom, atype(iatom), delta(itype)
                    call logger(log_buf)
                end if

                thislen = 4 * (1 + 11)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(4(1x,f11.7))') &
                    (x1(i3+k) * box(k) / delta(itype) * vunit(itype), k = 1, 3), &
                    (wxxi(iatom) + wyyi(iatom) + wzzi(iatom)) * vir_unitfactor
                nchars = nchars + thislen

            case (7)
                epot = Ethree(iatom) + Epair(iatom)

                thislen = 2 * (1 + 12)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(2(1x,f12.6))') &
                    epot, Ekin(iatom)
                nchars = nchars + thislen

            case (8)
                thislen = 3 * (1 + 18)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(3(1x,g18.10))') &
                    (xnp(i3+k) * box(k), k = 1, 3)
                nchars = nchars + thislen

            case (15, 17)
                if (virsym == 0) then
                    thislen = 9 * (1 + 14)
                    write(moviestrbuf(nchars+1 : nchars+thislen), &
                        '(9(1x,f14.7))') &
                        wxxi(iatom) * vir_unitfactor, &
                        wyyi(iatom) * vir_unitfactor, &
                        wzzi(iatom) * vir_unitfactor, &
                        wxyi(iatom) * vir_unitfactor, &
                        wxyi(iatom) * vir_unitfactor, &
                        wyzi(iatom) * vir_unitfactor, &
                        wyzi(iatom) * vir_unitfactor, &
                        wxzi(iatom) * vir_unitfactor, &
                        wxzi(iatom) * vir_unitfactor
                else
                    thislen = 6 * (1 + 14)
                    write(moviestrbuf(nchars+1 : nchars+thislen), &
                        '(6(1x,f14.7))') &
                        wxxi(iatom) * vir_unitfactor, &
                        wyyi(iatom) * vir_unitfactor, &
                        wzzi(iatom) * vir_unitfactor, &
                        wxyi(iatom) * vir_unitfactor, &
                        wyzi(iatom) * vir_unitfactor, &
                        wxzi(iatom) * vir_unitfactor
                end if
                nchars = nchars + thislen

            case (16, 18)
                ! These moviemodes have a trailing space after the last number.
                thislen = 1
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(1x)')
                nchars = nchars + thislen
            end select


            ! Write end of line
            write(moviestrbuf(nchars+1:nchars+1), '(A1)') new_line('x')
            nchars = nchars + 1

            if (nchars > len(moviestrbuf)) then
                call my_mpi_abort("moviestrbuf overflow", myproc)
            end if
        end do atom_loop

    end subroutine format_movie_chunk


    !
    ! Format a chunk of MOVIE_CHUNK_ATOMS atoms into a string in the
    ! buffer moviestrbuf, for writing to block_atoms.dat, and return
    ! the number of written characters in nchars.
    !
    ! If this processor has no atoms in this chunk, just set nchars
    ! and included_atoms to zero.
    !
    subroutine format_block_atoms_chunk(nchars, included_atoms, moviemode, ichunk, natoms, &
            virkbar, virsym, virboxsiz, box, x0, &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
            atype, atomindex, &
            outtype, outzmin, outzmax, outzmin2, outzmax2)

        integer, intent(out) :: nchars, included_atoms

        real(real64b), intent(in) :: &
            virboxsiz, &
            x0(:), box(3), &
            outzmin, outzmax, outzmin2, outzmax2

        real(real64b), intent(in), dimension(:) :: &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi

        integer, intent(in) :: &
            ichunk, &
            virkbar, virsym, outtype, &
            moviemode, &
            natoms, &
            atype(:), atomindex(:)

        integer :: iatom
        integer :: miniatom, maxiatom
        integer :: thislen
        real(real64b) :: vir_unitfactor
        real(real64b) :: ztemp

        vir_unitfactor = virial_unit_factor(moviemode, virkbar, virboxsiz, box, natoms)

        nchars = 0
        included_atoms = 0
        miniatom = chunk_miniatom(ichunk)
        maxiatom = chunk_maxiatom(ichunk, myatoms)

        atom_loop: do iatom = miniatom, maxiatom

            if (outtype == 0 .and. atype(iatom) >= 0) cycle

            if (outtype == 1) then
                ztemp = x0(3*iatom) * box(3)

                if (.not. ( &
                    (ztemp >= outzmin  .and. ztemp <= outzmax) .or. &
                    (ztemp >= outzmin2 .and. ztemp <= outzmax2))) then

                    cycle
                end if
            end if

            included_atoms = included_atoms + 1


            call movie_common_format(nchars, iatom, moviemode, &
                atype, atomindex, x0, box)


            ! Write virials.
            if (virsym == 0) then
                thislen = 9 * (1 + 14)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(9(1x,f14.7))') &
                    wxxi(iatom) * vir_unitfactor, &
                    wyyi(iatom) * vir_unitfactor, &
                    wzzi(iatom) * vir_unitfactor, &
                    wxyi(iatom) * vir_unitfactor, &
                    wxyi(iatom) * vir_unitfactor, &
                    wyzi(iatom) * vir_unitfactor, &
                    wyzi(iatom) * vir_unitfactor, &
                    wxzi(iatom) * vir_unitfactor, &
                    wxzi(iatom) * vir_unitfactor
            else
                thislen = 6 * (1 + 14)
                write(moviestrbuf(nchars+1 : nchars+thislen), &
                    '(6(1x,f14.7))') &
                    wxxi(iatom) * vir_unitfactor, &
                    wyyi(iatom) * vir_unitfactor, &
                    wzzi(iatom) * vir_unitfactor, &
                    wxyi(iatom) * vir_unitfactor, &
                    wyzi(iatom) * vir_unitfactor, &
                    wxzi(iatom) * vir_unitfactor
            end if
            nchars = nchars + thislen


            ! Write end of line
            write(moviestrbuf(nchars+1:nchars+1), '(A)') new_line('x')
            nchars = nchars + 1

            if (nchars > len(moviestrbuf)) then
                call my_mpi_abort("moviestrbuf overflow", myproc)
            end if
        end do atom_loop
    end subroutine format_block_atoms_chunk


    !
    ! Format the common data of an atom into moviestrbuf.
    !
    subroutine movie_common_format(nchars, iatom, moviemode, &
            atype, atomindex, x0, box)

        integer, intent(inout) :: nchars
        integer, intent(in) :: &
            iatom, &
            moviemode, &
            atype(:), atomindex(:)
        real(real64b), intent(in) :: x0(:), box(3)

        integer :: i3
        integer :: thislen
        character(20) :: thisformat
        logical :: huge_dists


        ! Write the element name.
        thislen = 2
        write(moviestrbuf(nchars+1 : nchars+thislen), &
            '(A2)') &
            element(abs(atype(iatom)))
        nchars = nchars + thislen


        ! Write the atom position. Format depends on moviemode and huge_dists.
        i3 = 3 * iatom - 3
        huge_dists = any(abs(x0(i3+1:i3+3) * box(1:3)) >= 10000.0d0)

        if (moviemode == 0) then
            if (huge_dists) then
                thislen = 3 * (1 + 18)
                thisformat = '(3(1x,f18.2))'
            else
                thislen = 3 * (1 + 8)
                thisformat = '(3(1x,f8.2))'
            end if
        else
            if (huge_dists) then
                thislen = 3 * (1 + 22)
                thisformat = '(3(1x,f22.6))'
            else
                thislen = 3 * (1 + 12)
                thisformat = '(3(1x,f12.6))'
            end if
        end if

        write(moviestrbuf(nchars+1 : nchars+thislen), &
            thisformat) &
            x0(i3+1:i3+3) * box(1:3)
        nchars = nchars + thislen


        ! Write the atom type.
        if (moviemode /= 3) then
            thislen = 1 + 2
            write(moviestrbuf(nchars+1 : nchars+thislen), &
                '(1x,i2)') &
                atype(iatom)
            nchars = nchars + thislen
        end if


        ! Write the atom index.
        if (moviemode /= 3 .and. moviemode /= 4) then
            thislen = 1 + 9
            write(moviestrbuf(nchars+1 : nchars+thislen), &
                '(1x,i9)') &
                atomindex(iatom)
            nchars = nchars + thislen
        end if
    end subroutine movie_common_format

end module mdoutput_mod
