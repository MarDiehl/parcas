!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

!/***********************************************************************
!  These are the MPI communication subroutines for
!  the Classical Molecular Dynamics program
!
!  The calling subroutines are defined in defs.h
!
!  Note the various possible SEND routines. Atleast in SGI systems
!  the basic Send is very problematic. Hence use non-buffering
!  send (AL_NBSEND, MPI_Ssend) as much as possible.
!
!  On the other hand, in Cray systems the basic Send appears to be
!  faster.
!
!
!  Notes on Fortran version:
!    Vesa Kolhinen 2008-11-24
!
!  The original C file is translated in Fortran.
!
!  Void pointers cannot be used here. Therefore, separate versions
!  of send/receive routines for real and integer arrays are included.
!  These are named as MY_MPI_...R and MY_MPI_...I, respectively.
!  All the source were referring to these must be modified accordingly.
!  A script create_mpi-fortran.run (and create_mpi-fortran.awk) is
!  included for this; see comments .f90erein.
!
!
!
!***********************************************************************/

!
! wrapper module for mpi library
!
module my_mpi
    ! It is better to use "use mpi", but if unavailable then use mpif.h.
    use mpi

    use datatypes, only: int32b, real64b

    use para_common, only: &
        iprint, &
        myproc, nprocs


    implicit none


    public


    integer, parameter :: mpi_parameters = int32b
    integer(mpi_parameters) :: my_mpi_integer


    interface my_mpi_min
        module procedure :: my_mpi_imin1, my_mpi_imin2
        module procedure :: my_mpi_dmin1, my_mpi_dmin2
    end interface

    interface my_mpi_max
        module procedure :: my_mpi_imax1, my_mpi_imax2
        module procedure :: my_mpi_dmax1, my_mpi_dmax2
    end interface

    interface my_mpi_sum
        module procedure :: my_mpi_isum1, my_mpi_isum2
        module procedure :: my_mpi_dsum1, my_mpi_dsum2
    end interface

    interface my_mpi_bcast
        module procedure :: my_mpi_ibcast1, my_mpi_ibcast2
        module procedure :: my_mpi_dbcast1, my_mpi_dbcast2
    end interface


contains


    subroutine my_mpi_barrier()
        integer(mpi_parameters) :: ierror
        call mpi_barrier(mpi_comm_world, ierror)
    end subroutine


    subroutine my_mpi_init()
        integer :: test=123

        integer(mpi_parameters) :: ierror

        if (BIT_SIZE(test) == 32) then
            my_mpi_integer=MPI_INTEGER
        else if (BIT_SIZE(test) == 64) then
            my_mpi_integer=MPI_INTEGER8
        else
            call my_mpi_abort("Unknown integer size",BIT_SIZE(test))
        end if


        call mpi_init(ierror)
        call mpi_comm_rank(mpi_comm_world, myproc, ierror)
        call mpi_comm_size(mpi_comm_world, nprocs, ierror)

        ! Only process 0 handles I/O activities
        iprint = (nprocs == 1 .or. myproc == 0)

    end subroutine my_mpi_init


    subroutine my_mpi_abort(str, tag)
        character(*), intent(in) :: str
        integer, intent(in) :: tag

        integer(mpi_parameters) :: ierror

        ! TODO: move this filehandle somewhere else
        integer, parameter :: error_file=20

        write(*,*) "MY_MPI_STOP EXECUTED with tag ", tag
        write(*,*) "Message: ", str

        open(unit=error_file, file="errors.out")
        write(error_file,*) "Message: ", str
        close(error_file)

        call MPI_ABORT(MPI_COMM_WORLD, INT(tag, mpi_parameters), ierror)
    end subroutine my_mpi_abort


    subroutine my_mpi_open_output_file(path, handle)
        character(*), intent(in) :: path
        integer, intent(out) :: handle

        integer :: ierror

        ! Delete the file in case it exists, to reset metadata, and because
        ! opening it does not truncate it.
        if (iprint) then
            call MPI_File_delete(path, MPI_INFO_NULL, ierror)
        end if
        call my_mpi_barrier() ! Wait for the file to be deleted.

        ! Due to a bug in OpenMPI 2.1.x, 3.0.x, do not pass MPI_MODE_SEQUENTIAL.
        ! https://github.com/open-mpi/ompi/issues/4991
        ! https://github.com/open-mpi/ompi/pull/4997

        call MPI_File_open(MPI_COMM_WORLD, path, &
                MPI_MODE_CREATE + MPI_MODE_WRONLY, &
                MPI_INFO_NULL, handle, ierror)

        if (ierror /= MPI_SUCCESS) then
            call my_mpi_abort("Failed to open file '" // path // "'", ierror)
        end if

        ! Make all future calls to MPI_File_* on this file treat errors as fatal.
        call MPI_File_set_errhandler(handle, MPI_ERRORS_ARE_FATAL, ierror)

    end subroutine my_mpi_open_output_file


    !
    ! Returns the global minimum value of procmin over all processors,
    ! and the rank of the processor which had that minimum value.
    !
    subroutine getminofallprocs(procmin, iproc)
        real(real64b), intent(inout) :: procmin
        integer, intent(out) :: iproc

        real(real64b) :: buf(2)
        integer :: ierror

        buf(1) = procmin
        buf(2) = myproc

        call mpi_allreduce(MPI_IN_PLACE, buf, 1, MPI_2DOUBLE_PRECISION, &
            MPI_MINLOC, MPI_COMM_WORLD, ierror)

        procmin = buf(1)
        iproc = int(buf(2))
    end subroutine getminofallprocs


    !
    ! Returns the global maximum value of procmax over all processors,
    ! and the rank of the processor which had that maximum value.
    !
    subroutine getmaxofallprocs(procmax, iproc)
        real(real64b), intent(inout) :: procmax
        integer, intent(out) :: iproc

        real(real64b) :: buf(2)
        integer :: ierror

        buf(1) = procmax
        buf(2) = myproc

        call mpi_allreduce(MPI_IN_PLACE, buf, 1, MPI_2DOUBLE_PRECISION, &
            MPI_MAXLOC, MPI_COMM_WORLD, ierror)

        procmax = buf(1)
        iproc = int(buf(2))
    end subroutine getmaxofallprocs


    subroutine my_mpi_dsum2(buf, n)
        integer, intent(in) :: n
        real(real64b), intent(inout) :: buf(n)

        integer(mpi_parameters) :: ierror

        call mpi_allreduce(MPI_IN_PLACE, buf, INT(n,mpi_parameters), &
            MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_dsum2


    subroutine my_mpi_dmax2(buf, n)
        integer, intent(in) :: n
        real(real64b), intent(inout) :: buf(n)

        integer(mpi_parameters) :: ierror

        call mpi_allreduce(MPI_IN_PLACE, buf, INT(n,mpi_parameters), &
            MPI_DOUBLE_PRECISION, MPI_MAX, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_dmax2


    subroutine my_mpi_dmin2(buf, n)
        integer, intent(in) :: n
        real(real64b), intent(inout) :: buf(n)

        integer(mpi_parameters) :: ierror

        call mpi_allreduce(MPI_IN_PLACE, buf, INT(n,mpi_parameters), &
            MPI_DOUBLE_PRECISION, MPI_MIN, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_dmin2


    subroutine my_mpi_dbcast2(buf, n, root)
        integer, intent(in) :: n
        real(real64b), intent(inout) :: buf(n)
        integer, intent(in) :: root

        integer(mpi_parameters) :: ierror

        call mpi_bcast(buf, n, MPI_DOUBLE_PRECISION, root, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_dbcast2


    subroutine my_mpi_isum2(buf, n)
        integer, intent(in) :: n
        integer, intent(inout) :: buf(n)

        integer(mpi_parameters) :: ierror

        call mpi_allreduce(MPI_IN_PLACE, buf, INT(n,mpi_parameters), &
            MY_MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_isum2


    subroutine my_mpi_imax2(buf, n)
        integer, intent(in) :: n
        integer, intent(inout) :: buf(n)

        integer(mpi_parameters) :: ierror

        call mpi_allreduce(MPI_IN_PLACE, buf, INT(n,mpi_parameters), &
            MY_MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_imax2


    subroutine my_mpi_imin2(buf, n)
        integer, intent(in) :: n
        integer, intent(inout) :: buf(n)

        integer(mpi_parameters) :: ierror

        call mpi_allreduce(MPI_IN_PLACE, buf, INT(n,mpi_parameters), &
            MY_MPI_INTEGER, MPI_MIN, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_imin2


    subroutine my_mpi_ibcast2(buf, n, root)
        integer, intent(in) :: n
        integer, intent(inout) :: buf(n)
        integer, intent(in) :: root

        integer(mpi_parameters) :: ierror

        call mpi_bcast(buf, n, MY_MPI_INTEGER, root, MPI_COMM_WORLD, ierror)
    end subroutine my_mpi_ibcast2


    !
    ! Scalar wrappers
    !


    subroutine my_mpi_dsum1(s, n)
        real(real64b), intent(inout) :: s
        integer, intent(in) :: n
        real(real64b) :: buf(1)

        call check_scalar(n, "my_mpi_dsum")

        buf(1) = s
        call my_mpi_dsum2(buf, n)
        s = buf(1)
    end subroutine my_mpi_dsum1


    subroutine my_mpi_dmax1(s, n)
        real(real64b), intent(inout) :: s
        integer, intent(in) :: n
        real(real64b) :: buf(1)

        call check_scalar(n, "my_mpi_dmax")

        buf(1) = s
        call my_mpi_dmax2(buf, n)
        s = buf(1)
    end subroutine my_mpi_dmax1


    subroutine my_mpi_dmin1(s, n)
        real(real64b), intent(inout) :: s
        integer, intent(in) :: n
        real(real64b) :: buf(1)

        call check_scalar(n, "my_mpi_dmin")

        buf(1) = s
        call my_mpi_dmin2(buf, n)
        s = buf(1)
    end subroutine my_mpi_dmin1


    subroutine my_mpi_dbcast1(s, n, root)
        real(real64b), intent(inout) :: s
        integer, intent(in) :: n
        integer, intent(in) :: root
        real(real64b) :: buf(1)

        call check_scalar(n, "my_mpi_dbcast")

        buf(1) = s
        call my_mpi_dbcast2(buf, n, root)
        s = buf(1)
    end subroutine my_mpi_dbcast1


    subroutine my_mpi_isum1(s, n)
        integer, intent(inout) :: s
        integer, intent(in) :: n
        integer :: buf(1)

        call check_scalar(n, "my_mpi_isum")

        buf(1) = s
        call my_mpi_isum2(buf, n)
        s = buf(1)
    end subroutine my_mpi_isum1


    subroutine my_mpi_imax1(s, n)
        integer, intent(inout) :: s
        integer, intent(in) :: n
        integer :: buf(1)

        call check_scalar(n, "my_mpi_imax")

        buf(1) = s
        call my_mpi_imax2(buf, n)
        s = buf(1)
    end subroutine my_mpi_imax1


    subroutine my_mpi_imin1(s, n)
        integer, intent(inout) :: s
        integer, intent(in) :: n
        integer :: buf(1)

        call check_scalar(n, "my_mpi_imin")

        buf(1) = s
        call my_mpi_imin2(buf, n)
        s = buf(1)
    end subroutine my_mpi_imin1


    subroutine my_mpi_ibcast1(s, n, root)
        integer, intent(inout) :: s
        integer, intent(in) :: n
        integer, intent(in) :: root
        integer :: buf(1)

        call check_scalar(n, "my_mpi_ibcast")

        buf(1) = s
        call my_mpi_ibcast2(buf, n, root)
        s = buf(1)
    end subroutine my_mpi_ibcast1


    !
    ! Abort the application if the scalar function is called with a larger
    ! array count than 1
    !
    subroutine check_scalar(n, func_name)
        integer, intent(in) :: n
        character(*), intent(in) :: func_name

        if (n /= 1) then
            call my_mpi_abort(func_name // " n /= 1 with a scalar", 0)
        end if
    end subroutine


end module my_mpi
