!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module readsubs_mod
    use datatypes, only: real64b

    implicit none

  contains


  !
  ! Subroutines for reading the PARCAS file. Data format is:
  !
  ! param    = value     Comment
  !
  ! i.e. each line start with a 9-char param name, then comes =, then the value
  ! Anything after the value is comment. Also, any lines not exactly in
  ! the format above are treated as comments, as are any parameters the
  ! program does not reognize
  !

  !***********************************************************************

  subroutine readgen(str,n,x,s,f,nl,mode,condflag,n0,x0,s0)
    use my_mpi
    use defs
    use output_logger, only: logger, log_buf
    use para_common, only: debug

    implicit none

    !
    !  Mode determines what is read in:
    !     0       Integer n
    !     1       Real(kind=real64b) x
    !     2       String s (Between '= ' and next space
    !
    !  Routine returns read-in parameter value in either x or n
    !  depending on mode. If condflag is true, read is conditional
    !
    !  This must be called from all processors. However, the actual
    !  reading should be done from only one processor to prevent multiple
    !  file access.
    !

    character(len=*)   :: str,s0
    character(len=8)   :: s
    character(len=9)   :: str1
    character(len=120) :: f(:),line,data
    integer :: n,n0,nl,mode,ios
    logical :: condflag
    real(kind=real64b) :: x,x0

    integer :: i,j,jstart,jlen,l,ic
    character(len=40) :: start
    logical :: datastart

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !print *,'readgen',str
    !

    str1='         '
    do i=1,9
       str1(i:i)=str(i:i)
       ic=ichar(str(i:i))
       if (ic<=32 .or. (ic>=126 .and. ic<=160)) then
          do j=i,9
             str1(i:i)=' '
          enddo
          exit
       endif
    enddo

    if (len(str1)<=0) then
       write (6,*) 'readgen ERROR: zero str1ing size'
       do i=1,9
          print *,i,str1(i:i),ichar(str1(i:i))
       enddo
       call my_mpi_abort('readgen empty string', int(len(str1)))
    endif

    if (nl<=0) then
       write (6,*) 'readgen ERROR: file size'
       call my_mpi_abort('readgen empty file', int(nl))
    endif

    IF(debug)PRINT *,'readgen',str1,len(str1),mode

    if (iprint) then
        jstart = 1  ! Will be updated to something useful in the loop
       do i=1,nl
          !print *,'Handling',i,f(i),len_trim(f(i))
          line=f(i)
          if (len_trim(line) < 11) cycle
          if (line == 'XXXXXXXXXX') cycle
          !print *,line,line(1:9)
          start=line(1:9)
          j=len_trim(start)
          l=len_trim(str1)
          !do k=1,9
          !   print *,k,start(i:i),str1(i:i)
          !enddo
          !print *,start,j,str1,l

          if (j==l .and.                                                  &
               str1 == start .and. line(10:10) .eq. '=') then
             !IF(debug)PRINT *,'found param',str1
             datastart=.false.
             do j=11,80
                if (.not. datastart .and. line(j:j) .ne. ' ') then
                   datastart=.true.
                   jstart=j
                endif
                if (datastart .and. line(j:j) .eq. ' ') exit
             enddo

             ! The tricks below are necessary for the reading in to work in VMS Fortran
             data='                                  '
             jlen=j-jstart
             data(19-jlen:19)=line(jstart:j-1)

             !IF(debug)PRINT *,'Reading from',data
             if (mode==0) then
                read(unit=data,fmt=*,iostat=ios) n
                if (ios /= 0) then
                   print *,'READ error for integer param',str
                   call my_mpi_abort('PARAM READ ERROR', int(i))
                endif
                write(log_buf,'(A,I18)') line(1:10),n
                call logger(log_buf)
             else if (mode==1) then
                read(unit=data,fmt=*,iostat=ios) x
                if (ios /= 0) then
                   print *,'READ error for real param',str
                   call my_mpi_abort('READ ERROR', int(i))
                endif
                write(log_buf,'(A,G18.9)') line(1:10),x
                call logger(log_buf)
             else if (mode==2) then
                do j=jstart,80
                   if (line(j:j)==' ') exit
                   if (j>jstart+7) then
                      print *,'Warning: string length<9',line
                      exit
                   endif
                enddo
                ! print *,'Read string',jstart,j-1,line(jstart:j-1)
                s=line(jstart:j)
                write(log_buf,'(A,A,A)') line(1:10),' ',s
                call logger(log_buf)
             else
                write (6,*) 'Unknown read mode !?!',mode
             endif
             ! Discard already read lines
             f(i)='XXXXXXXXXX'
             goto 99
          endif

       enddo
       !
       !         If the program gets here, param. not found
       !
       if (condflag) then
          write (6,*) "[",'Using default value for param. ',str1,"]"
          if (mode==0) then
             n=n0
             write(log_buf,'(A,A,I10,A)') str1,'=',n,' (default)'
             call logger(log_buf)
          else if (mode==1) then
             x=x0
             write(log_buf,'(A,A,G18.9,A)') str1,'=',x,' (default)'
             call logger(log_buf)
          else if (mode==2) then
             s=s0
             write(log_buf,'(A,A,A,A)') str1,'=',s,' (default)'
             call logger(log_buf)
          endif
       else
          write(6,'(A,A,A)') 'Read error for required parameter ',       &
               str1,' in input file ! Either it is missing'
          write (6,'(A)') 'or the program tries to read it in twice'
          call my_mpi_abort('Readin error in mdinit.f', int(myproc))
       endif

99     continue
    endif

    ! Broadcast the value for all processors.
    if (mode==0) then
       call mpi_bcast(n, 1, my_mpi_integer, 0, mpi_comm_world, ierror)
    else if (mode==1) then
       call mpi_bcast(x, 1, mpi_double_precision, 0, mpi_comm_world, ierror)
    else if (mode==2) then
       call mpi_bcast(s, 8, mpi_character, 0, mpi_comm_world, ierror)
    endif

    return
  end subroutine readgen

  !***********************************************************************
  subroutine readint(str,n,f,nl)
    implicit none
    character(len=*) str
    character(len=120) f(:)
    integer n,nl

    call readgen(str,n,0.0d0,'        ',f,nl,0,.false.,0,0.0d0,' ')
    return
  end subroutine readint
  !***********************************************************************
  subroutine readcint(str,n,f,nl,n0)
    implicit none
    character(len=*) str
    character(len=120) f(:)
    integer n,nl,n0

    call readgen(str,n,0.0d0,'        ',f,nl,0,.true.,n0,0.0d0,' ')
    return
  end subroutine readcint
  !***********************************************************************
  subroutine readreal(str,x,f,nl)
    implicit none
    character(len=*) str
    character(len=120) f(:)
    real(kind=real64b) x
    integer nl

    call readgen(str,0,x,'        ',f,nl,1,.false.,0,0.0d0,' ')
    return
  end subroutine readreal
  !***********************************************************************
  subroutine readcreal(str,x,f,nl,x0)
    implicit none
    character(len=*) str
    character(len=120) f(:)
    real(kind=real64b) x,x0
    integer nl

    call readgen(str,0,x,'        ',f,nl,1,.true.,0,x0,' ')
    return
  end subroutine readcreal

  !***********************************************************************
  subroutine readcstr(str,s,f,nl,s0)
    implicit none
    character(len=*) str,s0
    character(len=8) s
    character(len=120) f(:)
    integer nl

    call readgen(str,0,0.0d0,s,f,nl,2,.true.,0,0.0d0,s0)
    return
  end subroutine readcstr


  !***********************************************************************
  !***********************************************************************
  !****                         1D arrays                            *****
  !***********************************************************************
  !***********************************************************************


  subroutine readcrealarray(str,imin,imax,x,f,nl,x0)
    implicit none

    character(len=*) str
    integer i,imin,imax,l
    character(len=120) f(:)
    real(kind=real64b) x(imin:imax),x0
    integer nl

    character(len=9) buf
    buf='         '

    l=len_trim(str)
    do i=imin,imax
       if (i<10) then
          if (l<7) then
             write(unit=buf,fmt='(A,A1,I1,A1)') str(1:l),'(',i,')'
          else
             write(6,*) 'WARNING: can not read in element',i,' of param ',str
             write (6,*) 'Using default',x0
             x=x0
             return
          endif
       else if (i<100) then
          if (l<6) then
             write(unit=buf,fmt='(A,A1,I2,A1)') str(1:l),'(',i,')'
          else
             write(6,*) 'WARNING: can not read in element',i,' of param ',str
             write (6,*) 'Using default',x0
             x=x0
             return
          endif
       else if (i<1000) then
          if (l<5) then
             write(unit=buf,fmt='(A,A1,I3,A1)') str(1:l),'(',i,')'
          else
             write(6,*) 'WARNING: can not read in element',i,' of param ',str
             write (6,*) 'Using default',x0
             x=x0
             return
          endif
       else
          print *,'Readarray error: can not read in element>=1000'
          print *,str,i
       endif

       buf=trim(buf)
       call readgen(buf,0,x(i),'        ',f,nl,1,.true.,0,x0,' ')

       ! If default value was read in for (1), try without array bounds
       if (x(i)==x0 .and. i==1 .and. imin==1 .and. imax==1) then
          call readgen(str,0,x(1),'        ',f,nl,1,.true.,0,x0,' ')
       endif

    enddo

    return
  end subroutine readcrealarray

  !***********************************************************************
  subroutine readrealarray(str,imin,imax,x,f,nl)
    implicit none

    character(len=*) str
    character(len=9) str1
    integer i,imin,imax,l
    character(len=120) f(:)
    real(kind=real64b) x(imin:imax)
    integer nl

    real(kind=real64b) x0
    character buf*9
    buf='         '

    str1=trim(str)
    l=len_trim(str1)
    do i=imin,imax
       if (i<10) then
          write(unit=buf,fmt='(A,A1,I1,A1)') str1(1:l),'(',i,')'
       else if (i<100) then
          write(unit=buf,fmt='(A,A1,I2,A1)') str1(1:l),'(',i,')'
       else if (i<1000) then
          write(unit=buf,fmt='(A,A1,I3,A1)') str1(1:l),'(',i,')'
       else
          print *,'Readarray ERROR:'
          print *,'can not read in array element>=1000'
          print *,str,i
       endif

       x0=1.37541237897d17
       call readgen(buf,0,x(i),'        ',f,nl,1,.true.,0,x0,' ')

       ! If default value was read in for (1), try without array bounds
       if (x(i)==x0 .and. i==1 .and. imin==1 .and. imax==1) then
          call readgen(str,0,x(i),'        ',f,nl,1,.false.,0,x0,' ')
       endif

    enddo

    return
  end subroutine readrealarray

  !***********************************************************************
  subroutine readintarray(str,imin,imax,n,f,nl)
    implicit none

    character(len=*) str
    integer i,imin,imax,l
    character(len=120) f(:)
    integer n(imin:imax)
    integer nl

    character buf*9
    buf='         '

    l=len_trim(str)
    do i=imin,imax
       if (i<10) then
          write(unit=buf,fmt='(A,A1,I1,A1)') str(1:l),'(',i,')'
       else if (i<100) then
          write(unit=buf,fmt='(A,A1,I2,A1)') str(1:l),'(',i,')'
       else if (i<1000) then
          write(unit=buf,fmt='(A,A1,I3,A1)') str(1:l),'(',i,')'
       else
          print *,'Readarray ERROR:'
          print *,'can not read in array element>=1000'
          print *,str,i
       endif

       buf=trim(buf)
       call readgen(buf,n(i),0.0d0,'        ',f,nl,0,                     &
            .true.,0,0.0d0,' ')
    enddo

    return
  end subroutine readintarray

  !***********************************************************************
  subroutine readcintarray(str,imin,imax,n,f,nl,n0)
    implicit none

    character(len=*) str
    integer i,n0,imin,imax,l
    character(len=120) f(:)
    integer n(imin:imax)
    integer nl

    character(len=9) buf
    buf='         '

    l=len_trim(str)
    do i=imin,imax
       if (i<10) then
          if (l<7) then
             write(unit=buf,fmt='(A,A1,I1,A1)') str(1:l),'(',i,')'
          else
             write(6,*) 'WARNING: can not read in element',i,' of param ',str
             write (6,*) 'Using default',n0
             n=n0
             return
          endif
       else if (i<100) then
          if (l<6) then
             write(unit=buf,fmt='(A,A1,I2,A1)') str(1:l),'(',i,')'
          else
             write(6,*) 'WARNING: can not read in element',i,' of param ',str
             write (6,*) 'Using default',n0
             n=n0
             return
          endif
       else if (i<1000) then
          if (l<5) then
             write(unit=buf,fmt='(A,A1,I3,A1)') str(1:l),'(',i,')'
          else
             write(6,*) 'WARNING: can not read in element',i,' of param ',str
             write (6,*) 'Using default',n0
             n=n0
             return
          endif
       else
          print *,'Readarray ERROR:'
          print *,'can not read in array element>=1000'
          print *,str,i
       endif

       buf=trim(buf)
       call readgen(buf,n(i),0.0d0,'        ',f,nl,0,                     &
            .true.,n0,0.0d0,' ')
    enddo

    return
  end subroutine readcintarray

  !***********************************************************************

  subroutine readstrarray(str,imin,imax,s,f,nl)
    implicit none

    character(len=*) str
    integer i,imin,imax,l
    character(len=8) s(imin:imax)
    character(len=120) f(:)
    integer nl

    character(len=9) buf
    character(len=8) buf2
    buf='         '

    l=len_trim(str)
    do i=imin,imax
       if (i<10) then
          write(unit=buf,fmt='(A,A1,I1,A1)') str(1:l),'(',i,')'
       else if (i<100) then
          write(unit=buf,fmt='(A,A1,I2,A1)') str(1:l),'(',i,')'
       else if (i<1000) then
          write(unit=buf,fmt='(A,A1,I3,A1)') str(1:l),'(',i,')'
       else
          print *,'Readarray ERROR:'
          print *,'can not read in array element>=1000'
          print *,str,i
       endif

       buf=trim(buf)
       call readgen(buf,0,1.0d0,buf2,f,nl,2,.false.,0,1.0d0,' ')
       s(i)=buf2
    enddo


    return
  end subroutine readstrarray

  !***********************************************************************
  !***********************************************************************
  !****                         2D arrays                            *****
  !***********************************************************************
  !***********************************************************************
  subroutine readcint2darray(str,imin,imax,                             &
       jmin,jmax,n,f,nl,n0,symmetric)
    implicit none

    character(len=*) str
    integer i,imin,imax,j,jmin,jmax,jjmin,l
    character(len=120) f(:)
    integer n(imin:imax,jmin:jmax),n0
    integer nl
    logical symmetric

    character buf*9
    buf='         '

    l=len_trim(str)
    do i=imin,imax
       jjmin=jmin
       if (symmetric) jjmin=i
       do j=jjmin,jmax
          if (i<10 .and. j<10) then
             write(unit=buf,fmt='(A,A1,I1,A1,I1,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else
             print *,'Readarray error: can not read in element>=10'
             print *,str,i,j
          endif

          buf=trim(buf)
          call readgen(buf,n(i,j),1.0d0,'        ',f,nl,0,                &
               .true.,n0,1.0d0,' ')
          if (symmetric) n(j,i)=n(i,j)
       enddo
    enddo

    return
  end subroutine readcint2darray

  !***********************************************************************

  subroutine readreal2darray(str,imin,imax,isize,                       &
       jmin,jmax,r,f,nl,symmetric)
    implicit none

    character(len=*) str
    integer i,imin,imax,isize,j,jmin,jmax,jjmin,l
    character(len=120) f(:)
    real(kind=real64b) r(imin:isize,jmin:jmax)
    integer nl
    logical symmetric

    character buf*9
    buf='         '

    l=len_trim(str)
    do i=imin,imax
       jjmin=jmin
       if (symmetric) jjmin=i
       do j=jjmin,jmax
          if (i<10 .and. j<10) then
             write(unit=buf,fmt='(A,A1,I1,A1,I1,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else if (i<10 .and. j<100) then
             write(unit=buf,fmt='(A,A1,I1,A1,I2,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else if (i<100 .and. j<10) then
             write(unit=buf,fmt='(A,A1,I2,A1,I1,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else if (i<100 .and. j<100) then
             write(unit=buf,fmt='(A,A1,I2,A1,I2,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else
             print *,'Read2darray error: can not read in element>=100'
             print *,str(1:l),i,j,l
          endif

          buf=trim(buf)
          call readgen(buf,1,r(i,j),'        ',f,nl,1,                    &
               .false.,1,1.0d0,' ')
          if (symmetric) r(j,i)=r(i,j)
       enddo
    enddo

    return
  end subroutine readreal2darray

  !***********************************************************************

  subroutine readcreal2darray(str,imin,imax,isize,                       &
       jmin,jmax,r,f,nl,x0,symmetric)
    implicit none

    character(len=*) str
    integer i,imin,imax,isize,j,jmin,jmax,jjmin,l
    character(len=120) f(:)
    real(kind=real64b) r(imin:isize,jmin:jmax),x0
    integer nl
    logical symmetric

    character(len=9) buf
    buf='         '

    l=len_trim(str)
    do i=imin,imax
       jjmin=jmin
       if (symmetric) jjmin=i
       do j=jjmin,jmax
          if (i<10 .and. j<10) then
             write(unit=buf,fmt='(A,A1,I1,A1,I1,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else if (i<10 .and. j<100) then
             write(unit=buf,fmt='(A,A1,I1,A1,I2,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else if (i<100 .and. j<10) then
             write(unit=buf,fmt='(A,A1,I2,A1,I1,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else if (i<100 .and. j<100) then
             write(unit=buf,fmt='(A,A1,I2,A1,I2,A1)')                     &
                  str(1:l),'(',i,',',j,')'
          else
             print *,'Read2darray error: can not read in element>=100'
             print *,str(1:l),i,j,l
          endif

          buf=trim(buf)
          call readgen(buf,1,r(i,j),'        ',f,nl,1,                    &
               .true.,1,x0,' ')
          if (symmetric) r(j,i)=r(i,j)
       enddo
    enddo

    return
  end subroutine readcreal2darray

  !***********************************************************************

end module readsubs_mod
