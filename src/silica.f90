!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************


module silica_mod

    use output_logger, only: logger

    use datatypes, only: real64b
    use defs
    use PhysConsts, only: PI
    use typeparam
    use my_mpi
    use splinereppot_mod, only: reppot_only, reppot_fermi
    use timers, only: tmr, TMR_SEMICON_COMMS, TMR_SEMICON_REPPOT
    use para_common, only: myatoms, np0pairtable, buf

    use mdparsubs_mod, only: potential_pass_back_border_atoms


    implicit none
    save

    private
    public :: Init_Silica_Pot
    public :: Silica_Force


    ! Parameters for Stillinger-Weber and its modifications
    ! Added by J. Samela 2005
    integer, parameter :: NSW=5
    real(kind=real64b), dimension(NSW,NSW) :: paraA, paraB, paraC, paraD, parap, paraq
    real(kind=real64b), dimension(NSW,NSW) :: paraR, paraEps, paraSigma, paraBigR, paraSigaP
    real(kind=real64b), dimension(NSW,NSW,NSW) :: ParaSigaT1, ParaSigaT2
    real(kind=real64b), dimension(NSW,NSW,NSW) :: paragamma1, paragamma2
    real(kind=real64b), dimension(NSW,NSW,NSW) :: paracostheta, paraAlpha, paralambda

    integer, allocatable :: element_numbers(:)
    integer, allocatable :: IATAB(:,:)

    ! Ugly repulsive potential cutoff to speed things up a bit
    ! At 2.2, Si fermi function derivative is ~1e-4
    real(kind=real64b) :: reppotcut(3)
    data reppotcut /2.2, 1.0d0, 1.9/ ! Si-O and O-O added


contains

    subroutine Silica_Force(x0,atype,xnp,box,pbc,           &
            nborlist,Epair,Ethree, &
            wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_vir)

        !
        ! This subroutine is an implementation of silica potential
        ! published in Ohta et al., J. Chem. Phys. 115 (2001) 6679-6690.
        ! The potential is a modification of Stillinger-Weber potential.
        ! Therefore, this subroutine is an evolution version of the stilweb.f90
        ! routine.
        !
        ! Code originates from ancient HARWELL code, from which it was modified
        ! for PARCAS by Kai Nordlund in Jan 1997. Modifications for silica
        ! potential by Juha Samela 2005.
        !
        !
        ! In the PARCAS interface:
        ! Input:
        !   x0           contains positions in A scaled by 1/box,
        !   myatoms      Number of atoms in my node, in para_common.f90
        !   np0pairtable myatoms + number of atoms from neighbor nodes
        !   box(3)       Box size (box centered on 0)
        !   pbc(3)       Periodics: if = 1.0d0 periodic, separately for x,y,z
        !
        ! Output:
        !   xnp      contains forces in eV/A scaled by 1/box
        !   Epair    V_2 per atom in eV
        !   Ethree   V_3 per atom in eV
        !   wxxi,... virials per atom in eV
        !

        ! The atom types in atype(:) and typeparam are used in the following way:
        !
        ! On init, all type combinations are looped through. Recognized
        ! iac(it,jt)==1 combinations are set to the correct IATAB value.
        ! For others, a warning is issued.

        ! nborlist(i) has following format (not same as original code !):
        !
        !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
        !  nborlist(2)    Index of first neighbour of atom 1
        !  nborlist(3)    Index of second neighbour of atom 1
        !   ...
        !  nborlist(N1+1)  Index of last neighbour of atom 1
        !  nborlist(N1+2)  Number of neighbours for atom 2
        !  nborlist(N1+3)  Index of first neighbour of atom 2
        !   ...
        !   ...           And so on for all N atoms
        !

        ! Variables passed in and out

        real(kind=real64b), contiguous, intent(out) :: xnp(:)
        real(kind=real64b), contiguous, intent(out) :: Epair(:), Ethree(:)
        real(kind=real64b), contiguous, intent(out) :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)

        real(kind=real64b), contiguous, intent(in) :: x0(:)
        real(kind=real64b), intent(in) :: box(3), pbc(3)
        integer, contiguous, intent(in) :: atype(:)
        integer, intent(in) :: nborlist(:)

        logical, intent(in) :: calc_vir

        ! Local variables

        real(kind=real64b) :: AI
        real(kind=real64b) :: BOX2X,BOX2Y,BOX2Z
        real(kind=real64b) :: PH,DPH,PH1,HALFPH
        real(kind=real64b) :: COSMNL,COSS,DAI,DPHN,DPHM,DPHL
        real(kind=real64b) :: gammasig1, gammasig2

        real(kind=real64b) :: dxnm, dynm, dznm
        real(kind=real64b) :: dxnl, dynl, dznl

        real(kind=real64b) :: HCOS,HMNL,HKMNL
        real(kind=real64b) :: RNL,RNLA,RNLEF,RNM,RNMA,RNMEF,RNMSQ

        integer :: K,K1,N,M,L,IATN,IATM,IATL,IATYP1,IATYP2,IATYP3
        integer :: IAIND,IAIND2

        integer :: ingbr,jngbr,nngbr
        real(kind=real64b) :: help1,help2

        integer :: i
        real(kind=real64b) :: t1
        real(kind=real64b) :: fermi, dfermi
        integer :: nbor_start
        logical :: do_twobody
        logical :: is_pbc(3)

        real(real64b) :: dx_arr(3, NNMAX)
        real(real64b) :: r_arr(NNMAX)
        real(real64b) :: dzsmooth_arr(NNMAX)

        !-------------------------------------------------------------------------------!

        ! Fermi joining parameters.
        ! Si-Si 15,1.4 checked: gave threshold displ. energy 18 eV,
        ! looked good, energy conserved with both ZBL and DMol
        ! TO BE MODIFIED FOR O-SI AND O-O
        real(kind=real64b), parameter :: &
            bf(3) = [15.0, 15.0, 15.0], &
            rf(3) = [1.4, 1.4, 1.4]

        integer :: a1, a2, a3
        real(kind=real64b) :: paraEpsA
        real(kind=real64b) :: gsmooth, dgsmooth ! pair interaction smoothing function
        real(kind=real64b) :: dphz_tot, dphz
        real(kind=real64b) :: tmp1, tmp2, tmp3
        real(kind=real64b), parameter :: p1 = 0.097d0
        real(kind=real64b), parameter :: p2 = 1.6d0
        real(kind=real64b), parameter :: p3 = 0.3654d0
        real(kind=real64b), parameter :: p4 = 0.1344d0
        real(kind=real64b), parameter :: p5 = 6.4176d0
        real(kind=real64b) :: zsmooth, smoothmin, smoothmax
        real(real64b) :: R, D

        !-------------------------------------------------------------------------------

        ! Convert from internal units to Angstrom.
        ! Use buf(i) instead of x0(i) in the computations.
        do i = 1, 3*np0pairtable, 3
            buf(i+0) = x0(i+0) * box(1)
            buf(i+1) = x0(i+1) * box(2)
            buf(i+2) = x0(i+2) * box(3)
        end do


        !-----------------------------------------------------------------------------
        ! Compute the accelerations into xnp in actual units, then convert
        ! to internal units at the end. Compute energies into Epair and Ethree.
        !
        ! Take care to calculate pressure, total pot. only for atoms in my node!
        !
        ! rnmef and rnlef are the differences between distance and the potential cutoff.
        !
        ! N.B. No particles are moved at this stage.
        !

        ! Initialize force calculation stuff

        is_pbc(:) = (pbc(:) == 1d0)

        box2x = box(1) / 2.0
        box2y = box(2) / 2.0
        box2z = box(3) / 2.0

        xnp(:3*np0pairtable) = 0.0
        Epair(:np0pairtable) = 0.0
        Ethree(1 : myatoms) = 0.0
        wxxi(1 : myatoms) = 0.0
        wyyi(1 : myatoms) = 0.0
        wzzi(1 : myatoms) = 0.0
        if (calc_vir) then
            wxyi(1 : myatoms) = 0.0
            wxzi(1 : myatoms) = 0.0
            wyzi(1 : myatoms) = 0.0
        end if


        K = 0  ! index into neighbour list

        do IATN=1,myatoms   !  <------- main loop over atoms N start
            N=IATN*3-2
            IATYP1=abs(atype(IATN))
            a1 = element_numbers(iatyp1)

            K = K + 1
            nngbr = nborlist(K)
            if (nngbr < 0 .or. nngbr > NNMAX) then
                print *,'Silica force: nborlist HORROR ERROR !',nngbr
                call my_mpi_abort("Silica force: nborlist error", nngbr)
            endif

            nbor_start = K

            ! First loop over neighbors M: distances and coordination
            zsmooth = 0  ! coordination number
            do ingbr = 1, nngbr
                K = K + 1
                iatm = nborlist(K)
                m = 3*iatm - 2
                a2 = element_numbers(abs(atype(iatm)))

                dxnm = buf(n) - buf(m)
                if (is_pbc(1)) then
                    if (dxnm >= box2x) then
                        dxnm = dxnm - 2 * box2x
                    else if (dxnm < -box2x) then
                        dxnm = dxnm + 2 * box2x
                    end if
                end if
                dx_arr(1, ingbr) = dxnm

                dynm = buf(n+1) - buf(m+1)
                if (is_pbc(2)) then
                    if (dynm >= box2y) then
                        dynm = dynm - 2 * box2y
                    else if (dynm < -box2y) then
                        dynm = dynm + 2 * box2y
                    end if
                end if
                dx_arr(2, ingbr) = dynm

                dznm = buf(n+2) - buf(m+2)
                if (is_pbc(3)) then
                    if (dznm >= box2z) then
                        dznm = dznm - 2 * box2z
                    else if (dznm < -box2z) then
                        dznm = dznm + 2 * box2z
                    end if
                end if
                dx_arr(3, ingbr) = dznm

                rnm = sqrt(dxnm**2 + dynm**2 + dznm**2)
                r_arr(ingbr) = rnm

                ! Only oxygen needs the coordination, and only non-oxygen
                ! atoms are counted. Cannot count a2==-1 atoms due to missing
                ! parameters R, D.
                dzsmooth_arr(ingbr) = 0
                if (a1 == 2 .and. a2 /= 2 .and. a2 /= -1) then
                    R = paraBigR(a1,a2)
                    D = paraD(a1,a2)
                    smoothmin = R - D
                    smoothmax = R + D

                    if (rnm < smoothmin) then
                        zsmooth = zsmooth + 1
                    else if (rnm < smoothmax) then
                        tmp1 = (rnm - smoothmin) / D
                        tmp2 = sin(PI * tmp1)
                        zsmooth = zsmooth + 1 + 0.5d0 * (-tmp1 + tmp2 / PI)
                        dzsmooth_arr(ingbr) = (-1 + sqrt(1 - tmp2**2)) / (2 * D)
                    endif
                end if
            end do

            ! Collect contributions to derivatives of pair smoothing function
            ! here and apply them once at the end.
            dphz_tot = 0


            ! Second loop over neighbors M: forces and energies
            K = nbor_start

            do ingbr=1,nngbr

                K = K + 1
                IATM=nborlist(K)
                M = IATM*3-2

                IATYP2=abs(atype(IATM))

                if (iac(IATYP1,IATYP2) /= 1) then
                    ! Handle other interaction types
                    if (iac(IATYP1,IATYP2) == 0) cycle
                    if (iac(IATYP1,IATYP2) == -1) then
                        call logger("ERROR: IMPOSSIBLE INTERACTION")
                        call logger("myproc:", myproc, 4)
                        call logger("iatn:  ", iatn, 4)
                        call logger("iatm:  ", iatm, 4)
                        call logger("IATYP1:", IATYP1, 4)
                        call logger("IATYP2:", IATYP2, 4)
                        call my_mpi_abort('INTERACTION -1', myproc)
                    endif
                endif

                rnm = r_arr(ingbr)

                ! rcut contains the largest cutoff for this atom pair.
                if (rnm > rcut(iatyp1, iatyp2)) cycle

                rnmsq = rnm**2
                dxnm = dx_arr(1, ingbr)
                dynm = dx_arr(2, ingbr)
                dznm = dx_arr(3, ingbr)

                IAIND=IATAB(IATYP1,IATYP2)

                a2 = element_numbers(iatyp2)

                rnmef = rnm - paraSigaP(a1,a2) ! r_ij - sigma_ij*A_ij

                PH=0.0
                DPH=0.0

                ! Check for cutoff
                ! According to stilweb publication, also 3-body
                ! part should be skipped if this is less than cutoff.
                ! However, this parametrization requires 3-body interaction,
                ! otherwise bad discontinuities appear.

                ! 0.003 is chosen to ensure numerical stability
                if (rnmef < -0.003 .or. iac(IATYP1,IATYP2)==2) then

                    !  Two particle potential

                    ! Use Newton's III law.
                    ! Make sure that for pairs where smoothing has to be computed,
                    ! the N atom is oxygen. Otherwise we would need the neighbors
                    ! of neighbors, which would require double_borders (in mdx).
                    if ((a1 == 2) .neqv. (a2 == 2)) then
                        do_twobody = (a1 == 2)
                    else
                        do_twobody = (dxnm > 0d0 .or. &
                            (dxnm == 0d0 .and. dynm > 0d0) .or. &
                            (dxnm == 0d0 .and. dynm == 0d0 .and. dznm > 0d0))
                    end if
                    if (do_twobody) then

                        if (iac(IATYP1,IATYP2)==2) then
                            call reppot_only(rnm,ph,dph,iatyp1,iatyp2)

                        else

                            ! Pair interaction smoothing function
                            ! See J. Chem. Phys. 115 (2001) 6679-6690

                            ! The base atom is oxygen, the second atom is not
                            if (a1 == 2 .and. a2 /= 2) then
                                ! Smoothing function and derivative
                                if (zsmooth == 0.0) then ! TODO why this?
                                    gsmooth = 1.0d0
                                    dgsmooth = 0
                                else
                                    tmp1 = zsmooth - p5
                                    tmp2 = exp((p2 - zsmooth) / p3)
                                    tmp3 = 1 + tmp2
                                    gsmooth =  p1 * exp(p4 * tmp1**2) / tmp3
                                    dgsmooth = gsmooth / tmp3 * (2 * p4 * tmp1 * tmp3 + (1/p3) * tmp2)
                                end if

                            else
                                gsmooth = 1.0d0
                                dgsmooth = 0
                            end if

                            ! Pair potential
                            paraEpsA = paraA(a1,a2)*paraEps(a1,a2)*gsmooth
                            ph1      = paraEpsA*exp( paraC(a1,a2)*paraSigma(a1,a2)/rnmef )
                            help1    = paraB(a1,a2)*(paraSigma(a1,a2)/rnm)**parap(a1,a2)
                            help2    = (paraSigma(a1,a2)/rnm)**paraq(a1,a2)
                            ph  = (help1-help2)*ph1

                            ! Pair force
                            dph = (parap(a1,a2)*help1 - paraq(a1,a2)*help2)/rnm + &
                                (help1-help2)*paraC(a1,a2)*paraSigma(a1,a2)/(rnmef*rnmef)
                            dph = -ph1*dph

                            ! Force contribution to all neighbors from derivative of the
                            ! coordination number in gsmooth. Still needs factor of Fermi
                            ! function from reppot below.
                            dphz = dgsmooth * ph / gsmooth

                            ! Add repulsive pair potential times fermi function
                            if (rnm < reppotcut(iaind)) then
                                t1=mpi_wtime()
                                call reppot_fermi(rnm, ph, dph, bf(iaind), rf(iaind), &
                                    iatyp1, iatyp2, fermi, dfermi)

                                dphz = dphz * fermi
                                tmr(TMR_SEMICON_REPPOT)=tmr(TMR_SEMICON_REPPOT)+(mpi_wtime()-t1)
                            endif

                            dphz_tot = dphz_tot + dphz

                        endif

                        ! Calculate physical properties
                        ! dai = (x/r) (d pot/dr) = vector component of force/accelerations

                        HALFPH=PH/2.0
                        Epair(IATN)=Epair(IATN)+HALFPH
                        Epair(IATM)=Epair(IATM)+HALFPH

                        AI=-RNM*DPH

                        AI = AI/RNMSQ
                        DAI = DXNM*AI
                        xnp(N) = xnp(N) + DAI
                        xnp(M) = xnp(M) - DAI
                        wxxi(IATN)=wxxi(IATN)+DAI*DXNM

                        if (calc_vir) then
                            wxyi(IATN)=wxyi(IATN)+DAI*DYNM
                            wxzi(IATN)=wxzi(IATN)+DAI*DZNM
                        endif

                        DAI = DYNM*AI
                        xnp(N+1) = xnp(N+1) + DAI
                        xnp(M+1) = xnp(M+1) - DAI
                        wyyi(IATN)=wyyi(IATN)+DAI*DYNM

                        if (calc_vir) then
                            wyzi(IATN)=wyzi(IATN)+DAI*DZNM
                        endif

                        DAI = DZNM*AI
                        xnp(N+2) = xnp(N+2) + DAI
                        xnp(M+2) = xnp(M+2) - DAI
                        wzzi(IATN)=wzzi(IATN)+DAI*DZNM

                    endif

                endif

                if (iac(IATYP1,IATYP2) /= 1) cycle

                K1=K
                do jngbr=ingbr+1,nngbr      ! <---- three body loop over atoms L

                    K1=K1+1
                    IATL=nborlist(K1)
                    L=IATL*3-2

                    IATYP3=abs(atype(IATL))

                    ! Handle other interaction types
                    if (iac(IATYP1,IATYP3) /= 1) cycle

                    ! a1 is the type of the middle atom
                    a3 = element_numbers(iatyp3)
                    if (a1==2 .and. (a2==2 .or. a3==2)) cycle ! O-O-Si, Si-O-O, O-O-O

                    IAIND2=IATAB(IATYP1,IATYP3)

                    dxnl = dx_arr(1, jngbr)
                    dynl = dx_arr(2, jngbr)
                    dznl = dx_arr(3, jngbr)
                    rnl = r_arr(jngbr)

                    rnmef = rnm - paraSigaT1(a2,a1,a3)
                    rnlef = rnl - paraSigaT2(a2,a1,a3)

                    ! Three particle potential
                    !
                    ! Check for cutoff (skip over if exceeded)

                    if (rnlef > -0.03) cycle  ! Numerical errors if -0.03<rnlef<0.0
                    if (rnmef > -0.03) cycle

                    COSMNL=(DXNM*DXNL+DYNM*DYNL+DZNM*DZNL)/(RNM*RNL)
                    coss = cosmnl - paracostheta(a2,a1,a3)

                    gammasig1 = paragamma1(a2,a1,a3)*paraSigma(a1,a2)
                    gammasig2 = paragamma2(a2,a1,a3)*paraSigma(a1,a3)

                    ! potential
                    hmnl = sqrt(paraEps(a1,a2)*paraEps(a1,a3)) * &
                        paraLambda(a2,a1,a3) * &
                        exp(gammasig1/rnmef + gammasig2/rnlef) * &
                        abs(coss)**paraAlpha(a2,a1,a3)

                    Ethree(iatn) = ethree(iatn) + hmnl

                    ! forces and virials

                    hcos   = paraAlpha(a2,a1,a3) * hmnl / coss
                    hkmnl  = hcos / (rnm * rnl)
                    rnma   = (gammasig1 * hmnl) / (rnmef**2 * rnm)
                    rnla   = (gammasig2 * hmnl) / (rnlef**2 * rnl)
                    rnma   = rnma + hcos * cosmnl / rnmsq
                    rnla   = rnla + hcos * cosmnl / rnl**2

                    dphm = -rnma*dxnm + hkmnl*dxnl
                    dphl = -rnla*dxnl + hkmnl*dxnm
                    dphn = -dphm - dphl
                    xnp(N) = xnp(N) + dphn
                    xnp(M) = xnp(M) + dphm
                    xnp(L) = xnp(L) + dphl
                    wxxi(iatn) = wxxi(iatn) - dphm*dxnm - dphl*dxnl

                    if (calc_vir) then
                        wxyi(iatn) = wxyi(iatn) - dphm*dynm - dphl*dynl
                        wxzi(iatn) = wxzi(iatn) - dphm*dznm - dphl*dznl
                    endif

                    dphm = -rnma*dynm + hkmnl*dynl
                    dphl = -rnla*dynl + hkmnl*dynm
                    dphn = -dphm - dphl
                    xnp(N+1) = xnp(N+1) + dphn
                    xnp(M+1) = xnp(M+1) + dphm
                    xnp(L+1) = xnp(L+1) + dphl
                    wyyi(iatn) = wyyi(iatn) - dphm*dynm - dphl*dynl

                    if (calc_vir) then
                        wyzi(iatn) = wyzi(iatn) - dphm*dznm - dphl*dznl
                    endif

                    dphm = -rnma*dznm + hkmnl*dznl
                    dphl = -rnla*dznl + hkmnl*dznm
                    dphn = -dphm - dphl
                    xnp(N+2) = xnp(N+2) + dphn
                    xnp(M+2) = xnp(M+2) + dphm
                    xnp(L+2) = xnp(L+2) + dphl
                    wzzi(iatn) = wzzi(iatn) - dphm*dznm - dphl*dznl

                enddo   !  <---- end of three body loop over atoms L

            enddo  !  <---- end of 2-body loop over atoms M


            ! Apply forces from derivatives of smoothing function.
            if (dphz_tot /= 0) then
                do ingbr = 1, nngbr
                    if (dzsmooth_arr(ingbr) == 0) cycle

                    iatm = nborlist(nbor_start + ingbr)
                    M = 3*iatm - 2

                    ai = -dphz_tot * dzsmooth_arr(ingbr) / r_arr(ingbr)

                    dai = ai * dx_arr(1, ingbr)
                    xnp(N) = xnp(N) + dai
                    xnp(M) = xnp(M) - dai
                    wxxi(iatn) = wxxi(iatn) + dai * dx_arr(1, ingbr)

                    if (calc_vir) then
                        wxyi(iatn) = wxyi(iatn) + dai * dx_arr(2, ingbr)
                        wxzi(iatn) = wxzi(iatn) + dai * dx_arr(3, ingbr)
                    end if

                    dai = ai * dx_arr(2, ingbr)
                    xnp(N+1) = xnp(N+1) + dai
                    xnp(M+1) = xnp(M+1) - dai
                    wyyi(iatn) = wyyi(iatn) + dai * dx_arr(2, ingbr)

                    if (calc_vir) then
                        wyzi(iatn) = wyzi(iatn) + dai * dx_arr(3, ingbr)
                    end if

                    dai = ai * dx_arr(3, ingbr)
                    xnp(N+2) = xnp(N+2) + dai
                    xnp(M+2) = xnp(M+2) - dai
                    wzzi(iatn) = wzzi(iatn) + dai * dx_arr(3, ingbr)
                end do
            end if

        end do !  <-------- end of main loop over atoms N

        ! Send back forces and energies to the neighboring nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            call potential_pass_back_border_atoms(xnp, Epair)
            tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
        end if


        ! Convert from eV/A to internal units = eV/A/box.
        do i = 1, 3*myatoms, 3
            xnp(i+0) = xnp(i+0) / box(1)
            xnp(i+1) = xnp(i+1) / box(2)
            xnp(i+2) = xnp(i+2) / box(3)
        end do

    end subroutine Silica_Force


    !-----------------------------------------------------------------------------

    subroutine Init_Silica_Pot(reppotcutin)

        real(kind=real64b), intent(in) :: reppotcutin

        character(len=12), parameter :: silicaparameters = 'in/silica.in'
        integer :: i,j,k, a1, a2
        real(kind=real64b) :: sigma
        integer :: npara, nline
        character(len=80) :: line
        real(kind=real64b), dimension(64):: values
        integer :: status


        if (reppotcutin /= 10.0d0) then
            reppotcut(:) = reppotcutin
        end if

        allocate(IATAB(itypelow:itypehigh, itypelow:itypehigh))

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                IATAB(i,j)=1
                if (iac(i,j) == 1) then
                    if (element(i)=='Si'.and.element(j)=='Si') then
                        IATAB(i,j)=1
                    else if (element(i)=='Si'.and.element(j)=='O') then
                        IATAB(i,j)=2
                    else if (element(i)=='O'.and.element(j)=='Si') then
                        IATAB(i,j)=2
                    else if (element(i)=='O'.and.element(j)=='O') then
                        IATAB(i,j)=3
                    else
                        call logger("Silica_Force error: can't handle pair")
                        call logger("element(i):", element(i), 4)
                        call logger("element(j):", element(j), 4)
                        call my_mpi_abort('Silica_Force', myproc)
                    end if
                end if
            end do
        end do

        ! Find out which element numbers the user has given to the particular
        ! elements that this potential can handle. Notice that the order of
        ! elements in parameter tables is pre-defined and thus also the
        ! ifs should be in the same order.
        allocate(element_numbers(itypelow:itypehigh))

        do i=itypelow,itypehigh
            if (element(i)=='Si') then
                element_numbers(i) = 1
            else if (element(i)=='O') then
                element_numbers(i) = 2
            else if (element(i)=='Cl') then
                element_numbers(i) = 3
            else if (element(i)=='F') then
                element_numbers(i) = 4
            else if (element(i)=='Ge') then
                element_numbers(i) = 5
            else
                element_numbers(i) = -1  ! Element not included to this potential
            endif
        enddo


        paraA=0.0;paraB=0.0;paraC=0.0;parap=0.0;paraq=0.0
        paraR=0.0;paraD=0.0;paraEps=0.0;paraSigma=0.0
        paragamma1=0.0;paragamma2=0.0;paralambda=0.0;paracostheta=0.0
        paraBigR=0.0;paraSigaP=0.0;paraSigaT1=0.0;paraSigaT2=0.0;paraAlpha=0.0


        ! Read parameter file silica.in
        open(unit=25,file=silicaparameters,action='read',iostat=status)
        if ( status /= 0 ) then
            call logger("Silica parameter file could not be opened for reading!")
            call my_mpi_abort('silicapot', int(status))
        end if


        npara = 0; nline = 0
        do
            nline = nline + 1
            read(25,*,iostat=status) line

            if (status < 0) then
                exit
            else if ( status > 0 ) then
                call logger("Error while reading the silica parameter file in line:", nline, 0)
                call my_mpi_abort('silicapot', int(nline))
            end if

            if (line == '') cycle
            if (line(1:1) == '#') cycle ! comment line

            npara = npara + 1
            read(unit=line,fmt=*,iostat=status) values(npara)
            if (status /= 0) then
                call logger("Invalid value in the silica parameter file in line:", nline, 0)
                call my_mpi_abort('silicapot', int(nline))
            endif
        enddo
        close(25)

        do i=1,npara
            write(*,*) i,values(i)
        end do

        sigma = values(1) ! Angstrom

        ! Fill parameter table

        ! Si-Si
        paraA(1,1)     = values(2)
        paraB(1,1)     = values(3)
        paraC(1,1)     = values(4)
        parap(1,1)     = values(5)
        paraq(1,1)     = values(6)
        paraEps(1,1)   = values(7)
        paraSigma(1,1) = values(8)*sigma
        paraBigR(1,1)  = 0.0*sigma
        paraD(1,1)     = 0.0*sigma
        paraSigaP(1,1) = values(9)*sigma

        ! Si-O
        paraA(1,2)     = values(10)
        paraB(1,2)     = values(11)
        paraC(1,2)     = values(12)
        parap(1,2)     = values(13)
        paraq(1,2)     = values(14)
        paraEps(1,2)   = values(15)
        paraSigma(1,2) = values(16)*sigma
        paraBigR(1,2)  = values(17)*sigma
        paraD(1,2)     = values(18)*sigma
        paraSigaP(1,2) = values(19)*sigma

        ! 0-Si (symmetrical)
        paraA(2,1)     = paraA(1,2)
        paraB(2,1)     = paraB(1,2)
        paraC(2,1)     = paraC(1,2)
        parap(2,1)     = parap(1,2)
        paraq(2,1)     = paraq(1,2)
        paraEps(2,1)   = paraEps(1,2)
        paraSigma(2,1) = paraSigma(1,2)
        paraBigR(2,1)  = paraBigR(1,2)
        paraD(2,1)     = paraD(1,2)
        paraSigaP(2,1) = paraSigaP(1,2)

        ! O-O
        paraA(2,2)     = values(20)
        paraB(2,2)     = values(21)
        paraC(2,2)     = values(22)
        parap(2,2)     = values(23)
        paraq(2,2)     = values(24)
        paraEps(2,2)   = values(25)
        paraSigma(2,2) = values(26)*sigma
        paraBigR(2,2)  = values(27)*sigma
        paraD(2,2)     = values(28)*sigma
        paraSigaP(2,2) = values(29)*sigma

        ! Si-Si-Si
        paralambda(1,1,1)   = values(30)
        paragamma1(1,1,1)   = values(31)
        paragamma2(1,1,1)   = values(32)
        paracostheta(1,1,1) = values(33)
        paraAlpha(1,1,1)    = 2.0d0*values(34)
        paraSigaT1(1,1,1)   = values(35)*sigma
        paraSigaT2(1,1,1)   = values(36)*sigma

        ! Si-Si-O
        paralambda(1,1,2)   = values(37)
        paragamma1(1,1,2)   = values(38)
        paragamma2(1,1,2)   = values(39)
        paracostheta(1,1,2) = values(40)
        paraAlpha(1,1,2)    = 2.0d0*values(41)
        paraSigaT1(1,1,2)   = values(42)*sigma
        paraSigaT2(1,1,2)   = values(43)*sigma

        ! O-Si-Si
        paralambda(2,1,1)   = values(44)
        paragamma1(2,1,1)   = values(45)
        paragamma2(2,1,1)   = values(46)
        paracostheta(2,1,1) = values(47)
        paraAlpha(2,1,1)    = 2.0d0*values(48)
        paraSigaT1(2,1,1)   = values(49)*sigma
        paraSigaT2(2,1,1)   = values(50)*sigma

        ! Si-O-Si
        paralambda(1,2,1)   = values(51)
        paragamma1(1,2,1)   = values(52)
        paragamma2(1,2,1)   = values(53)
        paracostheta(1,2,1) = values(54)
        paraAlpha(1,2,1)    = 2.0d0*values(55)
        paraSigaT1(1,2,1)   = values(56)*sigma
        paraSigaT2(1,2,1)   = values(57)*sigma

        ! O-Si-O
        paralambda(2,1,2)   = values(58)
        paragamma1(2,1,2)   = values(59)
        paragamma2(2,1,2)   = values(60)
        paracostheta(2,1,2) = values(61)
        paraAlpha(2,1,2)    = 2.0d0*values(62)
        paraSigaT1(2,1,2)   = values(63)*sigma
        paraSigaT2(2,1,2)   = values(64)*sigma

        ! Si-O-O and O-O-O no interaction, all parameters zero and
        ! cycle statements in code

        ! As per the Ohta article, all 3-body parameters jik=kij
        do i = 1, NSW
            do j = 1, NSW
                do k = 1, NSW
                    if (paraLambda(j,i,k) /= paraLambda(k,i,j)) then
                        if (iprint) print *, "WARNING: paraLambda not symmetric:", i, j, k
                    end if
                    if (paragamma1(j,i,k) /= paragamma2(k,i,j)) then
                        if (iprint) print *, "WARNING: paragamma not symmetric:", i, j, k
                    end if
                    if (paragamma2(j,i,k) /= paragamma1(k,i,j)) then
                        if (iprint) print *, "WARNING: paragamma not symmetric:", i, j, k
                    end if
                    if (paracostheta(j,i,k) /= paracostheta(k,i,j)) then
                        if (iprint) print *, "WARNING: paracostheta not symmetric:", i, j, k
                    end if
                    if (paraAlpha(j,i,k) /= paraAlpha(k,i,j)) then
                        if (iprint) print *, "WARNING: paraAlpha not symmetric:", i, j, k
                    end if
                    if (paraSigaT1(j,i,k) /= paraSigaT2(k,i,j)) then
                        if (iprint) print *, "WARNING: paraSigaT not symmetric:", i, j, k
                    end if
                    if (paraSigaT2(j,i,k) /= paraSigaT1(k,i,j)) then
                        if (iprint) print *, "WARNING: paraSigaT not symmetric:", i, j, k
                    end if
                end do
            end do
        end do


        ! Cutoff distances
        do i=itypelow,itypehigh
            do j=itypelow,itypehigh
                rcut(i,j) = 0

                if (iac(i,j) == 1) then
                    a1 = element_numbers(i)
                    a2 = element_numbers(j)

                    rcut(i,j) = max( &
                        paraSigaP(a1,a2), &
                        maxval(paraSigaT1(a2,a1,:)), &
                        maxval(paraSigaT2(a2,a1,:)), &
                        maxval(paraSigaT1(:,a1,a2)), &
                        maxval(paraSigaT2(:,a1,a2)), &
                        paraBigR(a1,a2) + paraD(a1,a2))

                else if (iac(i,j) == 2) then
                    rcut(i,j) = paraSigaP(1,1)
                endif
            enddo
        enddo

    end subroutine Init_Silica_Pot

end module silica_mod
