!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module splinereppot_mod

    use output_logger, only: log_buf, logger

    use datatypes, only: real64b
    use typeparam
    use my_mpi
    use defs
    use para_common, only: debug
    use file_units, only: TEMP_FILE


    implicit none
    save

    private
    public :: reppot_init
    public :: reppot_only
    public :: reppot_fermi


    ! TODO: adding "d0" to the end of the the number changes
    ! some results in the tests. Which is correct, with or without?
    real(real64b), parameter :: screenfact = 14.399758

    real(real64b), allocatable :: x(:,:,:)   ! x array
    real(real64b), allocatable :: VR(:,:,:)  ! Repulsive potential
    ! Estimation of second derivative of VR for spline int.
    ! Corresponds to Y2 in Numerical Recipes.
    real(real64b), allocatable :: VR2(:,:,:)

    ! Real size is current_array_size + 1, because arrays start at 0
    integer :: current_array_size

    ! Number of reppot points.
    integer, allocatable :: Nmax(:,:)

    logical, allocatable :: firstpair(:,:)


contains

    ! Spline repulsive potential subroutines.
    !
    ! - First time each pair is requested, read in the potential values
    !   from reppot.namei.namej.in. Derivatives, if present in the file,
    !   are ignored.
    !
    ! - Assumes symmetric interactions, always uses ti < tj.
    !
    ! - Both routines evaluate the repulsive potentials.
    !   reppot_only returns it as-is, taking no input attractive potential.
    !   reppot_fermi fits the repulsive potential to the attractive input
    !   potential using the Fermi function.
    !
    ! - Interpolation of V and dV done with cubic splines. See Numerical
    !   Recipes, ch. 3.3.
    !
    ! - dV obtained from spline derivative, which mdrange and hcparcas tests
    !   showed is clearly better.
    !
    ! - Input scaled by screenfact/r for spline interpolation, result scaled
    !   back again. Thus the interpolated factor is really the screening
    !   function, which is less strongly repulsive and thus better suited for
    !   spline interpolation.
    !
    !   screenfact in here is simply 14.399758 (i.e. Z1 and Z2 are missing).
    !
    !   The screening factor calc. was tested for Kr->Pt up to an energy
    !   of 10 MeV an seemed to work perfectly - no nasty spline oscillations.


    subroutine reppot_init()
        ! Array indexing starts from 0, not 1!

        ! Hard-coded default size, will increase automatically if needed
        current_array_size = 10000

        allocate(x(0:current_array_size, itypelow:itypehigh, itypelow:itypehigh))
        allocate(VR(0:current_array_size, itypelow:itypehigh, itypelow:itypehigh))

        allocate(VR2(0:current_array_size, itypelow:itypehigh, itypelow:itypehigh))

        allocate(Nmax(itypelow:itypehigh, itypelow:itypehigh))
        nmax(:,:) = 0  ! In case reppot_grow_arrays is called

        allocate(firstpair(itypelow:itypehigh, itypelow:itypehigh))
        firstpair = .true.
    end subroutine reppot_init


    subroutine reppot_grow_arrays()
        real(real64b), allocatable :: tmp_x(:,:,:), tmp_VR(:,:,:), tmp_VR2(:,:,:)

        integer :: i, j

        current_array_size = 2 * current_array_size

        allocate(tmp_x(0:current_array_size, itypelow:itypehigh, itypelow:itypehigh))
        allocate(tmp_VR(0:current_array_size, itypelow:itypehigh, itypelow:itypehigh))
        allocate(tmp_VR2(0:current_array_size, itypelow:itypehigh, itypelow:itypehigh))

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                tmp_x(:Nmax(i,j), i, j) = x(:Nmax(i,j), i, j)
                tmp_VR(:Nmax(i,j), i, j) = VR(:Nmax(i,j), i, j)
                tmp_VR2(:Nmax(i,j), i, j) = VR2(:Nmax(i,j), i, j)
            end do
        end do

        ! Might be redundant? Haven't found a good source telling otherwise.
        deallocate(x)
        deallocate(VR)
        deallocate(VR2)

        call move_alloc(from=tmp_x, to=x)
        call move_alloc(from=tmp_VR, to=VR)
        call move_alloc(from=tmp_VR2, to=VR2)
    end subroutine reppot_grow_arrays


    !
    ! Compute the repulsive potential for atom types ti_in and tj_in at
    ! distance r into the parameters V and dV.
    !
    subroutine reppot_only(r, V, dV, ti_in, tj_in)

        real(real64b), intent(in) :: r
        real(real64b), intent(out) :: V, dV
        integer, intent(in) :: ti_in, tj_in

        integer :: ti, tj

        integer, save :: klo = 0, khi = 0
        integer :: k

        real(real64b) :: screenhelp
        real(real64b) :: h, a, b, rlo, rhi
        real(real64b) :: temp, atemp, btemp, htemp
        real(real64b) :: dVspline


        ! Use symmetric interactions to reduce need of files.
        ti = min(ti_in, tj_in)
        tj = max(ti_in, tj_in)


        ! On first time for each pair read in potential data.
        ! TODO: consider moving this into reppot_init function.
        ! Problem with it: if files are missing, the simulation might currently
        ! succeed, as long as the atom types never meet.
        if (firstpair(ti,tj)) then
            firstpair(ti,tj) = .false.
            call read_reppot_file(ti, tj)
            call init_reppot_values(ti, tj)
        end if


        if (r >= x(Nmax(ti,tj),ti,tj)) then
            ! No repulsive potential
            V = 0.0d0
            dV = 0.0d0
            return
        end if


        ! Obtain array limits klo and khi.

        if (.not. ((khi - klo == 1) .and. x(khi,ti,tj) > r .and. x(klo,ti,tj) < r)) then
            ! Bisect your way to the correct point.
            klo = 0
            khi = Nmax(ti,tj)
            do
                if (khi-klo <= 1) exit
                k = (khi + klo) / 2
                if (x(k,ti,tj) > r) then
                    khi = k
                else
                    klo = k
                end if
            end do
        end if

        if (khi > Nmax(ti,tj)) then
            write(log_buf, *) 'Warning: reppot outside array', khi, Nmax(ti,tj), r
            call logger(log_buf)
        endif

        rlo = x(klo,ti,tj)
        rhi = x(khi,ti,tj)


        ! Spline interpolate V

        h = rhi - rlo
        temp = 1 / h
        a = (rhi - r) * temp
        b = (r - rlo) * temp

        atemp = a**2
        btemp = b**2
        htemp = h**2 / 6.0d0

        V = a * VR(klo,ti,tj) + b * VR(khi,ti,tj) + htemp * ( &
            (atemp * a - a) * VR2(klo,ti,tj) + &
            (btemp * b - b) * VR2(khi,ti,tj))


        ! Compute spline derivative, eq. 3.3.5 in Numerical recipes

        dVspline = temp * ((VR(khi,ti,tj) - VR(klo,ti,tj)) - htemp * ( &
            (3 * atemp - 1) * VR2(klo,ti,tj) - &
            (3 * btemp - 1) * VR2(khi,ti,tj)))


        ! Correct for screening

        screenhelp = screenfact / r
        dV = screenhelp * (r * dVspline - V) / r
        V = V * screenhelp

        !dV = dVspline   ! For no screening

    end subroutine reppot_only


    !
    ! Compute the repulsive potential for atom types ti_in and tj_in at
    ! distance r, then combine it with the potential given in V,dV using
    ! the Fermi function with parameters bf and rf. Write the Fermi function
    ! and its derivative into the parameters fermi and dfermi.
    !
    subroutine reppot_fermi(r, V, dV, bf, rf, ti_in, tj_in, fermi, dfermi)

        real(real64b), intent(in) :: r
        real(real64b), intent(inout) :: V, dV
        real(real64b), intent(in) :: bf, rf
        integer, intent(in) :: ti_in, tj_in
        real(real64b), intent(out) :: fermi, dfermi

        real(real64b) :: expterm
        real(real64b) :: Vrep, dVrep ! Repulsive potential
        real(real64b) :: Vin, dVin   ! Attractive input potential to be combined with reppot.


        Vin = V
        dVin = dV


        if (bf <= 0.0d0 .or. rf <= 0.0d0) then
            write(log_buf, *) 'splinereppot: attempt to use crazy Fermi params', bf, rf, &
                new_line('x'), 'for atom types', ti_in, tj_in
            call logger(log_buf)
            call my_mpi_abort('Invalid Fermi params', ti_in)
        end if


        ! Compute the repulsive potential into Vrep and dVrep.
        call reppot_only(r, Vrep, dVrep, ti_in, tj_in)


        ! Fit the repulsive potential to the attractive input potential
        ! using the Fermi function.
        expterm = exp(-bf * (r - rf))
        fermi = 1.0d0 / (1.0d0 + expterm)
        dfermi = fermi**2 * expterm * bf

        V = (1.0d0 - fermi) * Vrep + fermi * Vin
        dV = (1.0d0 - fermi) * dVrep - dfermi * Vrep + fermi * dVin + dfermi * Vin

    end subroutine reppot_fermi


    !
    ! Open the correct reppot file for the given types and read in the
    ! position and potential values into the array x and VR.
    ! Fills in the number of reppot values read into Nmax.
    !
    subroutine read_reppot_file(ti, tj)
        integer, intent(in) :: ti, tj

        character(len=120) :: buf
        real(real64b) :: rin, Vin
        integer :: i
        integer :: ierr
        logical :: file_found

        file_found = .false.

        buf = "in/reppot." // trim(element(ti)) // "." // &
            trim(element(tj)) // ".in"

        open(TEMP_FILE, file=buf, status='old', iostat=ierr)
        file_found = (ierr == 0)

        if (.not. file_found) then
            buf = "in/reppot." // trim(element(tj)) // "." // &
                trim(element(ti)) // ".in"

            open(TEMP_FILE, file=buf, status='old', iostat=ierr)
            file_found = (ierr == 0)
        end if

        if ((.not. file_found) .and. ti == 1 .and. tj == 1) then
            open(TEMP_FILE, file="in/reppot.in", status='old', iostat=ierr)
            file_found = (ierr == 0)
        end if

        if (.not. file_found) then
            write(log_buf,*) "splinereppot ERROR: No reppot for atom pair: ", ti, tj
            call logger(log_buf)
            call my_mpi_abort('no reppot', int(ti))
        end if

        if (debug) then
            write(log_buf,"(A,2I4,A,A,A)") &
                "Reading in pair pot for atom pair i j: ", ti, tj, &
                new_line('x'), "from file: ", trim(buf)
            call logger(log_buf)
        end if

        i = 0
        do
            read(TEMP_FILE, fmt='(A)', iostat=ierr) buf

            if (ierr < 0) exit  ! End of file
            if (ierr > 0) then
                call logger("Error reading reppot file")
                call my_mpi_abort("Error reading reppot file", ierr)
            end if

            ! Ignore comment lines and empty lines.
            select case (buf(1:1))
            case ('%', '#', '!')
                cycle
            end select
            if (len_trim(buf) == 0) cycle

            read(buf, *) rin, Vin

            i = i + 1
            Nmax(ti,tj) = i

            if (i >= current_array_size) then
                call reppot_grow_arrays()
            end if

            if (i == 1 .and. rin < 1d-20) then
                write(log_buf, *) 'splinereppot error: first x point in file should not be zero', rin
                call logger(log_buf)
                call my_mpi_abort('splinereppot 0', 0)
            endif

            x(i,ti,tj) = rin
            VR(i,ti,tj) = Vin / screenfact * rin
        end do

        close(TEMP_FILE)

        if (debug) print *, 'reppot() Read in repulsive potential points', i
    end subroutine read_reppot_file


    !
    ! Assumes that the reppot file for the types ti and tj has been read.
    ! Fills the arrays VR2, and u for types ti and tj.
    ! Fills in the rest of x and VR, from Nmax to current_array_size.
    ! Fills in the 0-point estimate VR(r=0).
    !
    subroutine init_reppot_values(ti, tj)
        integer, intent(in) :: ti, tj

        real(real64b) :: u(0 : current_array_size) ! Spline help array
        real(real64b) :: dVlinear
        real(real64b) :: p, sig
        integer :: i, j, k, N

        N = Nmax(ti, tj)

        do j = N + 1, current_array_size
            x(j,ti,tj) = x(N,ti,tj) + real(j - N, real64b) * (x(N,ti,tj) - x(N-1,ti,tj))
            VR(j,ti,tj) = 0.0d0

            VR2(j,ti,tj) = 0.0d0
            u(j) = 0.0d0
        end do

        ! Obtain 0 point estimate by linear extrapolation.
        ! Since screening factors are used, this should be pretty good.

        ! First estimate derivative from first 2 points.
        dVlinear = (VR(2,ti,tj) - VR(1,ti,tj)) / (x(2,ti,tj) - x(1,ti,tj))

        x(0,ti,tj) = 0.0d0
        VR(0,ti,tj) = VR(1,ti,tj) - dVlinear * (x(2,ti,tj) - x(1,ti,tj))


        ! Initialize spline interpolation of VR
        ! Natural spline boundary conditions are used (mdrange tests
        ! showed this is clearly the best)!
        ! Note that array bounds are 0 and N, not 1 and N!

        VR2(0,ti,tj) = 0.0d0
        u(0) = 0.0d0

        do i = 1, N
            sig = (x(i,ti,tj) - x(i-1,ti,tj)) / (x(i+1,ti,tj) - x(i-1,ti,tj))
            p = sig * VR2(i-1,ti,tj) + 2.0d0
            VR2(i,ti,tj) = (sig - 1.0d0) / p
            u(i) = (6.0d0 * ( &
                (VR(i+1,ti,tj) - VR(i,ti,tj)) / (x(i+1,ti,tj) - x(i,ti,tj)) - &
                (VR(i,ti,tj) - VR(i-1,ti,tj)) / (x(i,ti,tj) - x(i-1,ti,tj)) &
                ) / (x(i+1,ti,tj) - x(i-1,ti,tj)) - sig * u(i-1)) / p
        end do

        VR2(N,ti,tj) = 0.0d0
        do k = N-1, 0, -1
            VR2(k,ti,tj) = VR2(k,ti,tj) * VR2(k+1,ti,tj) + u(k)
        end do

        if (debug) then
            print *,'Spline initialization done for atom pair', ti, tj
            print *,'Estimate of Vrepulsive screen at r=0', VR(0,ti,tj)
        end if
    end subroutine init_reppot_values

end module splinereppot_mod
