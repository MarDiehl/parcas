!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module tersoff_mod

    use output_logger, only: logger

    use typeparam
    use datatypes, only: real64b
    use PhysConsts, only: pi
    use my_mpi

    use timers, only: tmr, TMR_SEMICON_COMMS

    use defs
    use splinereppot_mod, only: reppot_only, reppot_fermi

    use mdparsubs_mod, only: potential_pass_back_border_atoms

    use para_common, only: &
        myatoms, np0pairtable, &
        debug, iprint


    implicit none
    save

    private
    public :: Init_Ter_Pot
    public :: Tersoff_Force


    ! Potential parameters, initialized in Init_Ter_Pot.
    real(real64b) :: tersA, tersB, beta, rlambda, rmu, gc, gd, gh, ters_n, rlambda3_cube
    real(real64b) :: gd2, gd2i, gc2, g2c2
    real(real64b) :: trcut, dcut, rmd, rpd, a, halfa
    real(real64b) :: reppotcut, bf, rf


contains


    !***********************************************************************
    ! Routines to calculate Tersoff potential
    ! see PRB 39, 5566 (1989).
    ! Jamie Morris 6 June 1995, parallel version by Dave Turner - Nov of 1995
    ! Taken into PARCAS from alcmd V3.0 starting Jan 28 1998 - Kai Nordlund
    !
    !
    !***********************************************************************
    !
    ! potmode determines mode, name(1) element according to following:
    !
    ! name(1)   potmode      Potential
    ! -------   --------     ----------
    ! Si        5            Tersoff III, with lambda3
    ! Si        6            Tersoff III, no lambda3
    ! Si        7            Tersoff II, with lambda3
    ! Si        8            Tersoff II, no lambda3
    !
    !
    ! This version is an elemental version, i.e. Tersoff alloys can not
    ! be handled by it ! Another version should be made to handle alloys;
    ! it is better to keep the two versions separated for speed purposes
    !

    ! nborlist(i) has following format:
    !
    !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
    !  nborlist(2)    Index of first neighbour of atom 1
    !  nborlist(3)    Index of second neighbour of atom 1
    !   ...
    !  nborlist(N1+1)  Index of last neighbour of atom 1
    !  nborlist(N1+2)  Number of neighbours for atom 2
    !  nborlist(N1+3)  Index of first neighbour of atom 2
    !   ...
    !   ...           And so on for all N atoms
    !

    subroutine Tersoff_Force(x0,atype,xnp,box,pbc,          &
            nborlist,Epair,Ethree, &
            wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_vir)

        ! ------------------------------------------------------------------
        ! Variables passed in and out

        real(real64b), contiguous, intent(in) :: x0(:)
        real(real64b), intent(in) :: box(3), pbc(3)
        real(real64b), contiguous, intent(out) :: xnp(:)
        integer, contiguous, intent(in) :: atype(:)

        real(real64b), contiguous, intent(out) :: Epair(:), Ethree(:)
        real(real64b), contiguous, intent(out) :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)
        logical, intent(in) :: calc_vir

        integer, intent(in) :: nborlist(:)


        ! ------------------------------------------------------------------
        ! Local variables and constants

        real(kind=real64b) :: xa(3,NNMAX),xab(3,NNMAX),xai(3,NNMAX)
        real(kind=real64b) :: ra(NNMAX),rai(NNMAX),ra2i(NNMAX)
        real(kind=real64b) :: fc(NNMAX),dfc(NNMAX),z(NNMAX),db(NNMAX)
        real(kind=real64b) :: gt(NNMAX*NNMAX),dG(NNMAX*NNMAX),cth(NNMAX*NNMAX)
        real(kind=real64b) :: dfcxr(3,NNMAX),xr2(3,NNMAX)


        ! Previously implicit variables
        integer :: I,J,K,I3,J3,K3,IJ,IK,MJK
        integer :: NNBORS


        real(kind=real64b) :: TERS2I,dr,RA2,DFCR,FATT,FREP
        real(kind=real64b) :: DFATT,DFREP,BZP,BZP1,BIJ,cpair,cmany,DF,C1
        real(kind=real64b) :: CJ,CK

        real(kind=real64b) :: dbfcdgcth,dbfcdgrr,dbjgt,dbkgt,dbfcdg,dbfcjk,dbfckj

        ! ------------------------------------------------------------------
        ! New variables by Kai Nordlund
        integer :: in,in0

        real(kind=real64b) :: help1,help2,help3,V

        real(kind=real64b) :: expij(NNMAX*NNMAX),expik(NNMAX*NNMAX)
        real(kind=real64b) :: dexpij(NNMAX*NNMAX),dexpik(NNMAX*NNMAX)
        real(kind=real64b) :: dbfcgde

        real(kind=real64b) :: ghmctheta,denomi
        real(kind=real64b) :: theta,s,fermi,dfermi

        real(real64b) :: t1

        ! type variables
        integer :: typei,typeij(NNMAX)
        integer :: iact(NNMAX)


        xnp(:3*np0pairtable) = 0.0
        Epair(:np0pairtable) = 0.0
        Ethree(1 : myatoms) = 0.0
        wxxi(1 : myatoms) = 0.0
        wyyi(1 : myatoms) = 0.0
        wzzi(1 : myatoms) = 0.0

        if (calc_vir) then
            wxyi(1 : myatoms) = 0.0
            wxzi(1 : myatoms) = 0.0
            wyzi(1 : myatoms) = 0.0
        end if

        ters2i = -0.5d0/ters_n


        ! Neighbour list counter
        in=0
        do i = 1,myatoms   ! <-------------------- Loop over atoms i
            i3 = i*3-3

            typei=abs(atype(i))

            in=in+1; in0=in
            nnbors = nborlist(in)
            in=in+nnbors

            if (nnbors < 0 .or. nnbors > NNMAX) then
                print *,'Tersoff nnbors HORROR ERROR',nnbors,NNMAX
                print *,i,i3
                call my_mpi_abort('Tersoff nnbors', int(nnbors))
            endif

            !
            ! Calculate f_C and df_C
            !
            do ij = 1,nnbors          ! <-------- First loop over neighbours j
                j = nborlist(in0+ij)
                j3 = j*3-3

                typeij(ij)=abs(atype(j))
                iact(ij)=iac(typei,typeij(ij))

                if (iact(ij) /= 1) then
                    ! Handle other interaction types
                    ! Set fc to zero - all other steps will be ignored
                    fc(ij)=0d0

                    if (iact(ij) == 0) cycle
                    if (iact(ij) == -1) then
                        call logger("TERSOFF ERROR: IMPOSSIBLE INTERACTION")
                        call logger("myproc:    ", myproc, 4)
                        call logger("i:         ", i, 4)
                        call logger("j:         ", j, 4)
                        call logger("ij:        ", ij, 4)
                        call logger("typei:     ", typei, 4)
                        call logger("typeij(ij):", typeij(ij), 4)
                        call my_mpi_abort('INTERACTION -1', int(myproc))
                    endif
                endif

                xa(1,ij) = x0(j3+1)-x0(i3+1)
                if (xa(1,ij) >=  0.5d0) xa(1,ij) = xa(1,ij)-pbc(1)
                if (xa(1,ij) < -0.5d0) xa(1,ij) = xa(1,ij)+pbc(1)
                xab(1,ij) = box(1)*xa(1,ij)

                xa(2,ij) = x0(j3+2)-x0(i3+2)
                if (xa(2,ij) >=  0.5d0) xa(2,ij) = xa(2,ij)-pbc(2)
                if (xa(2,ij) < -0.5d0) xa(2,ij) = xa(2,ij)+pbc(2)
                xab(2,ij) = box(2)*xa(2,ij)

                xa(3,ij) = x0(j3+3)-x0(i3+3)
                if (xa(3,ij) >=  0.5d0) xa(3,ij) = xa(3,ij)-pbc(3)
                if (xa(3,ij) < -0.5d0) xa(3,ij) = xa(3,ij)+pbc(3)
                xab(3,ij) = box(3)*xa(3,ij)

                ra2 = xab(1,ij)*xab(1,ij)+xab(2,ij)*xab(2,ij)+xab(3,ij)*xab(3,ij)

                ra(ij) = SQRT(ra2)

                if (iact(ij)==2) then

                    if (ra(ij) > rpd) cycle

                    ! Here must multiply by box for virial, since xab only contains
                    ! one box at this point.

                    call reppot_only(ra(ij),V,df,typei,typeij(ij))
                    !print *,'reppot',i,j,ra(ij),df
                    V=V*0.5d0
                    df=df*0.5d0/ra(ij)
                    Epair(i)=Epair(i)+V; Epair(j)=Epair(j)+V
                    c1 = df*xa(1,ij)
                    xnp(i3+1) = xnp(i3+1)+c1; xnp(j3+1) = xnp(j3+1)-c1
                    wxxi(i) = wxxi(i) - c1*xab(1,ij)*box(1)

                    if (calc_vir) then
                        wxyi(i) = wxyi(i) - c1*xab(2,ij)*box(2)
                        wxzi(i) = wxzi(i) - c1*xab(3,ij)*box(3)
                    endif

                    c1 = df*xa(2,ij)
                    xnp(i3+2) = xnp(i3+2)+c1; xnp(j3+2) = xnp(j3+2)-c1
                    wyyi(i) = wyyi(i) - c1*xab(2,ij)*box(2)

                    if (calc_vir) then
                        wyzi(i) = wyzi(i) - c1*xab(3,ij)*box(3)
                    endif

                    c1 = df*xa(3,ij)
                    xnp(i3+3) = xnp(i3+3)+c1; xnp(j3+3) = xnp(j3+3)-c1
                    wzzi(i) = wzzi(i) - c1*xab(3,ij)*box(3)

                    cycle
                endif
                if (iact(ij)/=1) cycle

                fc(ij) = 0d0
                dfc(ij) = 0d0
                if (ra(ij) <= rpd) then
                    if (ra(ij) < rmd) then
                        fc(ij)=1d0
                    else
                        theta=a*(ra(ij)-trcut)
                        s = SIN(theta)
                        fc(ij)= 0.5d0*(1d0-s)
                        ! replaced dFcut= -0.5d0*a*COS(theta)
                        ! using cos theta =sqrt(1-sin^2 theta)
                        dfc(ij)= halfa*SQRT(1d0-s*s)
                    endif
                endif

                if (fc(ij) == 0d0) cycle

                rai(ij) = 1d0/ra(ij)
                dfcr = dfc(ij)*rai(ij)
                ra2i(ij) = 1d0/ra2

                xai(1,ij) = rai(ij)*xa(1,ij)
                dfcxr(1,ij) = dfcr*xa(1,ij)
                xr2(1,ij) = xa(1,ij)*ra2i(ij)

                xai(2,ij) = rai(ij)*xa(2,ij)
                dfcxr(2,ij) = dfcr*xa(2,ij)
                xr2(2,ij) = xa(2,ij)*ra2i(ij)

                xai(3,ij) = rai(ij)*xa(3,ij)
                dfcxr(3,ij) = dfcr*xa(3,ij)
                xr2(3,ij) = xa(3,ij)*ra2i(ij)

                z(ij) = 0d0

            enddo                     ! <-------- End of first loop over neighbours j

            !
            ! Calculate g(theta) and dg(theta) and hence zeta_ij and zeta_ik
            !
            mjk = 0
            do ij = 1,nnbors-1        ! <-------- Second loop over neighbours j

                if (fc(ij) == 0d0) cycle

                j = nborlist(in0+ij)
                do ik = ij+1,nnbors            ! <------- Loop over neighbours k

                    if (fc(ik) == 0d0) cycle

                    mjk = mjk + 1
                    cth(mjk) = (xab(1,ij)*xab(1,ik)+xab(2,ij)*xab(2,ik)+xab(3,ij)*xab(3,ik)) &
                        * rai(ij)*rai(ik)

                    ! Inlined function G_theta
                    ghmctheta = gh - cth(mjk)
                    denomi = 1d0/(gd2+ghmctheta*ghmctheta)
                    gt(mjk) = 1d0+(gd2i - denomi)*gc2
                    dG(mjk) = g2c2*denomi*denomi*ghmctheta

                    ! exp(lambda3...) term
                    if (rlambda3_cube /= 0d0) then
                        dr = ra(ij) - ra(ik)
                        help1 = rlambda3_cube * dr * dr
                        help2 = help1 * dr
                        expij(mjk) = exp(help2)
                        expik(mjk) = 1d0/expij(mjk)

                        help3=3d0*help1
                        dexpij(mjk)= expij(mjk)*help3
                        dexpik(mjk)= expik(mjk)*help3
                        !print *,'lambda3',i,j,k,dr,expij(mjk),expik(mjk),dexpij(mjk),dexpik(mjk)
                    else
                        expij(mjk) = 1d0
                        expik(mjk) = 1d0
                        dexpij(mjk) = 0d0
                        dexpik(mjk) = 0d0
                    end if

                    z(ij) = z(ij) + fc(ik)*gt(mjk)*expij(mjk)
                    z(ik) = z(ik) + fc(ij)*gt(mjk)*expik(mjk)

                enddo                          ! <------ End of loop over k
            enddo                     ! <-------- End of second loop over neighbours j

            !
            !  Calculate f_A, f_R and b_ij
            !
            do ij = 1,nnbors          ! <-------- Third loop over neighbours j

                if (fc(ij) == 0d0) cycle

                ! Now xab is only used for virials, so add another box
                ! for scaling directly to the correct units.
                xab(:,ij) = box(:) * xab(:,ij)

                j = nborlist(in0+ij)
                j3=j*3-3

                Fatt = -tersB*exp(-rmu*ra(ij))
                Frep = tersA*exp(-rlambda*ra(ij))
                dFatt = -Fatt*rmu
                dFrep = -Frep*rlambda

                bzp = (beta*z(ij))**ters_n
                bzp1 = 1d0+bzp
                bij = bzp1**ters2i

                ! Energies and 'two-body' forces done

                cpair=fc(ij)*Frep
                cmany=fc(ij)*bij*Fatt
                ! Send in V=0 here because we split up cpair and cmany
                !V=cpair+cmany
                V=0;
                df=(fc(ij)*(dFrep+bij*dFatt)+dfc(ij)*(Frep+bij*Fatt))

                fermi=1d0
                dfermi=1d0
                if (ra(ij) < reppotcut) then
                    !print '(A,4F10.3)','rep0',ra(ij),V,df
                    call reppot_fermi(ra(ij),V,df,bf,rf,typei,typeij(ij),fermi,dfermi)
                    !if (ra(ij) < 2.2) print '(A,4F10.3)','rep2',ra(ij),V,df,fermi

                    df=df+dfermi*(cpair+cmany)

                endif
                ! reppot routine has output (1.0d0-fermi)*Vrepulsive
                ! so it can be added to Epair without additional
                ! multiplication with the fermi term.
                cpair=cpair*0.5d0*fermi+0.5d0*V
                cmany=cmany*fermi
                df=df*0.5d0*rai(ij)

                Epair(i)=Epair(i)+cpair
                Epair(j)=Epair(j)+cpair
                Ethree(i)=Ethree(i)+cmany

                if (z(ij) /= 0d0) then
                    db(ij) = -0.25d0*cmany*bzp/(bzp1*z(ij))
                else
                    db(ij) = 0d0
                endif

                ! Get ij derivative, i.e. everything except db terms

                c1 = df*xa(1,ij)
                xnp(i3+1) = xnp(i3+1)+c1
                xnp(j3+1) = xnp(j3+1)-c1
                wxxi(i) = wxxi(i) - c1*xab(1,ij)

                if (calc_vir) then
                    wxyi(i) = wxyi(i) - c1 * xab(2,ij)
                    wxzi(i) = wxzi(i) - c1 * xab(3,ij)
                endif

                c1 = df*xa(2,ij)
                xnp(i3+2) = xnp(i3+2)+c1
                xnp(j3+2) = xnp(j3+2)-c1
                wyyi(i) = wyyi(i) - c1 * xab(2,ij)

                if (calc_vir) then
                    wyzi(i) = wyzi(i) - c1 * xab(3,ij)
                endif

                c1 = df*xa(3,ij)
                xnp(i3+3) = xnp(i3+3)+c1
                xnp(j3+3) = xnp(j3+3)-c1
                wzzi(i) = wzzi(i) - c1 * xab(3,ij)

            enddo                     ! <-------- End of third loop over neighbours j

            !
            !  Get three-body db derivatives
            !
            mjk = 0
            do ij = 1,nnbors-1        ! <-------- Fourth loop over neighbours j

                if (fc(ij) == 0d0) cycle

                j = nborlist(in0+ij)
                j3=j*3-3
                !!$
                do ik = ij+1,nnbors        ! <---- Loop over neighbours k

                    if (fc(ik) == 0d0) cycle

                    k = nborlist(in0+ik)
                    k3=k*3-3
                    mjk = mjk + 1

                    dbfcjk=db(ij)*fc(ik)
                    dbfckj=db(ik)*fc(ij)
                    dbjgt = db(ij)*gt(mjk)*expij(mjk)
                    dbkgt = db(ik)*gt(mjk)*expik(mjk)
                    dbfcdG = (dbfcjk*expij(mjk)+dbfckj*expik(mjk))*dG(mjk)
                    dbfcdGcth = dbfcdG*cth(mjk)
                    dbfcdGrr = dbfcdG*rai(ik)*rai(ij)

                    dbfcgde=(dbfcjk*dexpij(mjk)-dbfckj*dexpik(mjk))*gt(mjk)

                    cj=dbkgt*dfcxr(1,ij)+dbfcdGrr*xa(1,ik)-dbfcdGcth*xr2(1,ij)
                    cj=cj+dbfcgde*xai(1,ij)
                    ck=dbjgt*dfcxr(1,ik)+dbfcdGrr*xa(1,ij)-dbfcdGcth*xr2(1,ik)
                    ck=ck-dbfcgde*xai(1,ik)

                    xnp(i3+1) = xnp(i3+1)+cj+ck
                    xnp(j3+1) = xnp(j3+1)-cj
                    xnp(k3+1) = xnp(k3+1)-ck
                    wxxi(i) = wxxi(i) - cj*xab(1,ij) - ck*xab(1,ik)

                    if (calc_vir) then
                        wxyi(i) = wxyi(i) - cj*xab(2,ij) - ck*xab(2,ik)
                        wxzi(i) = wxzi(i) - cj*xab(3,ij) - ck*xab(3,ik)
                    endif

                    cj=dbkgt*dfcxr(2,ij)+dbfcdGrr*xa(2,ik)-dbfcdGcth*xr2(2,ij)
                    cj=cj+dbfcgde*xai(2,ij)
                    ck=dbjgt*dfcxr(2,ik)+dbfcdGrr*xa(2,ij)-dbfcdGcth*xr2(2,ik)
                    ck=ck-dbfcgde*xai(2,ik)

                    xnp(i3+2) = xnp(i3+2)+cj+ck
                    xnp(j3+2) = xnp(j3+2)-cj
                    xnp(k3+2) = xnp(k3+2)-ck
                    wyyi(i) = wyyi(i)  -cj*xab(2,ij) - ck*xab(2,ik)

                    if (calc_vir) then
                        wyzi(i) = wyzi(i) - cj*xab(3,ij) - ck*xab(3,ik)
                    endif

                    cj=dbkgt*dfcxr(3,ij)+dbfcdGrr*xa(3,ik)-dbfcdGcth*xr2(3,ij)
                    cj=cj+dbfcgde*xai(3,ij)
                    ck=dbjgt*dfcxr(3,ik)+dbfcdGrr*xa(3,ij)-dbfcdGcth*xr2(3,ik)
                    ck=ck-dbfcgde*xai(3,ik)

                    xnp(i3+3) = xnp(i3+3)+cj+ck
                    xnp(j3+3) = xnp(j3+3)-cj
                    xnp(k3+3) = xnp(k3+3)-ck
                    wzzi(i) = wzzi(i) - cj*xab(3,ij) - ck*xab(3,ik)

                enddo! <---- End of loop over neighbours k
            enddo                     ! <-------- Fourth loop over neighbours j
        enddo             ! <-------------------- End of loop over atoms i


        if (nprocs > 1) then
            t1 = mpi_wtime()
            call potential_pass_back_border_atoms(xnp, Epair)
            tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
        end if

        Epair(:myatoms) = 0.5d0 * Epair(:myatoms)
        Ethree(:myatoms) = 0.5d0 * Ethree(:myatoms)

    end subroutine Tersoff_Force



    ! ------------------------------------------------------------------------

    subroutine Init_Ter_Pot(rc, potmode, reppotcutin)

        ! ------------------------------------------------------------------
        ! Variables passed in and out
        real(kind=real64b), intent(out) :: rc
        real(kind=real64b), intent(in) :: reppotcutin
        integer, intent(in) :: potmode

        real(kind=real64b) :: rlambda3

        real(kind=real64b) :: atersA(6),atersB(6),abeta(6),arlambda(6),armu(6),agc(6),agd(6),agh(6)
        real(kind=real64b) :: aters_n(6),arlambda3(6),atrcut(6),adcut(6)
        real(kind=real64b) :: areppotcut(6),abf(6),arf(6)
        integer :: i

        !                Si III      Si II      Ge        Ge        C plain   C mod.
        data atersA     /1.8308d3  , 3.2647d3 , 1.769d3  , 1.769d3  , 0.0d0   , 0.0d0   /
        data atersB     /4.7118d2  , 9.5373d1 , 4.1923d2 , 4.1923d2 , 0.0d0   , 0.0d0   /
        data abeta      /1.1000d-6 , 3.3675d-1, 9.0166d-7, 9.0166d-7, 0.0d0   , 0.0d0   /
        data arlambda   /2.4799d0  , 3.2394d0 , 2.4451d0 , 2.4451d0 , 0.0d0   , 0.0d0   /
        data armu       /1.7322d0  , 1.3258d0 , 1.7047d0 , 1.7047d0 , 0.0d0   , 0.0d0   /
        data agc        /1.0039d5  , 4.8381d0 , 1.0643d5 , 1.0643d5 , 0.0d0   , 0.0d0   /
        data agd        /1.6217d1  , 2.0417d0 , 1.5625d1 , 1.5625d1 , 0.0d0   , 0.0d0   /
        data agh        /-5.9825d-1, 0.0d0    ,-4.3884d-1,-4.3884d-1, 0.0d0   , 0.0d0   /
        data aters_n    /7.8734d-1 , 2.2956d1 , 7.5627d-1, 7.5627d-1, 0.0d0   , 0.0d0   /
        data arlambda3  /1.7322d0  , 1.3258d0 , 1.7047d0 , 1.7047d0 , 0.0d0   , 0.0d0   /
        data atrcut     /2.85d0    , 3.0d0    , 2.95d0   , 2.95d0   , 1.95d0  , 2.13d0  /
        data adcut      /0.15d0    , 0.2d0    , 0.15d0   , 0.15d0   , 0.15d0  , 0.33d0  /

        data areppotcut /3.0d0     , 3.2d0    , 3.1d0   , 3.1d0   , 2.0d0   , 2.0d0   /
        data abf        /12.0d0    , 12.0d0   , 12.0d0  , 12.0d0  , 14.0d0  , 14.0d0  /
        data arf        /1.60d0    , 1.60d0   , 1.66d0  , 1.66d0  , 0.95d0  , 0.95d0  /

        ! If everything is done properly, this value should get updated in the
        ! type checks below.
        i = 0

        if (element(1)=="Si") then
            i=1
        else if (element(1)=="Ge") then
            i=3
        else
            print *,'Element',element(1),' not supported in Tersoff yet !!'
            call my_mpi_abort('Tersoff element', int(potmode))
        endif
        if (potmode==7 .or. potmode==8) then
            i=i+1
            if (element(1)=='Si') print *,'Si Tersoff potential II (B)'
            if (element(1)=='Ge') print *,'Ge Tersoff potential'
            if (element(1)=='C') print *,'C Tersoff potential with PRL 77, 699 modified max'
        else
            if (element(1)=='Si') print *,'Si Tersoff potential III (C)'
            if (element(1)=='Ge') print *,'Ge Tersoff potential'
            if (element(1)=='C') print *,'C Tersoff potential, original'
        endif

        tersA=atersA(i)
        tersB=atersB(i)
        beta=abeta(i)
        rlambda=arlambda(i)
        rmu=armu(i)
        gc=agc(i)
        gd=agd(i)
        gh=agh(i)
        ters_n=aters_n(i)
        trcut=atrcut(i)
        dcut=adcut(i)

        reppotcut=areppotcut(i)
        if (reppotcutin < reppotcut) reppotcut=reppotcutin
        bf=abf(i)
        rf=arf(i)

        if (potmode==5 .or. potmode==7) then
            rlambda3=arlambda3(i)
            print *,'Tersoff using lambda3',rlambda3
        else if (potmode==6 .or. potmode==8) then
            rlambda3=0d0
            print *,'Tersoff NOT using lambda3'
        endif
        rlambda3_cube=rlambda3*rlambda3*rlambda3

        rc = trcut+dcut

        rpd = trcut+dcut
        rmd = trcut-dcut
        a = 0.5d0*pi/dcut
        halfa = -0.5d0*a

        gd2 = gd*gd
        gd2i = 1d0/gd2
        gc2 = gc*gc
        g2c2 = -2d0*gc2

    end subroutine Init_Ter_Pot


end module tersoff_mod
