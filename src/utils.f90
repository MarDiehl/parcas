!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2018 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module utils

    use datatypes, only: int32b, real64b

    implicit none

    private


    public to_str


    interface to_str
        module procedure :: int32b_to_str
        module procedure :: real64b_to_str
        module procedure :: logical_to_str
    end interface


  contains


    !===========================================================================
    ! Converter functions
    !===========================================================================

    function int32b_to_str(v) result(ret)
        integer(int32b), intent(in) :: v
        character(:), allocatable :: ret
        character(30) :: tmp
        write(tmp, "(I13)") v
        ret = trim(adjustl(tmp))
    end function


    function real64b_to_str(v) result(ret)
        real(real64b), intent(in) :: v
        character(:), allocatable :: ret
        character(30) :: tmp
        write(tmp, "(G17.6)") v
        ret = trim(adjustl(tmp))
    end function


    function logical_to_str(v) result(ret)
        logical, intent(in) :: v
        character(:), allocatable :: ret
        character(30) :: tmp
        write(tmp, "(L1)") v
        ret = trim(adjustl(tmp))
    end function

end module
