Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = STEPS
tmax     = 30000.0       Maximum time in fs 
restartt = 0.0
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

Atom type-dependent parameters. 
Time step is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 2            Number of atom types
mass(0)  = 69.7230       
name(0)  = Ga            
mass(1)  = 28.0855       Atom mass in u
name(1)  = Si            Name of atom type
substrate= Si            Substrate name, for use in elstop file name

iac(0,0) = -1             Interaction types: -1 kill 0 none 1 potmode 2 pair 
iac(0,1) = 2             - automatic symmetry used: iac(1,0) = iac(0,1)
iac(1,1) = 1             

potmode  = 30          0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW


Simulation cell
---------------

latflag  = 2             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2


natoms   = 4096          Number of atoms in simulation
box(1)   = 43.4432       Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 43.4432       Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 43.4432       Box size in the Z-dir (Angstroms, sigma for LJ)
ncell(1) = 8            Number of unit cells along X-direction
ncell(2) = 8            Number of unit cells along Y-direction
ncell(3) = 8            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 0.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 7             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 500.0         Initial T for mtemp=6
timeini  = 5000.0        Time at temp0 before quench
ntimeini = 999900        Time steps at temp0 before quench
initemp  = 10.0           Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 0.0           Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 70.0          Berendsen temp. control tau (fs), if 0 not used
trate    = 0.12          Quench rate for mtemp=6/8 (K/fs)

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 625.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 0.0           Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcP0z   = 0.0           Berendsen pres. control. desired P_z (kbar)

tscaleth = 2.72         Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor

ndump    = 5             Print data every ndump steps
nmovie   = 99500         Number of steps btwn writing to md.movie (0=no movie)
dtmov(1) = 50.0 500.0         Time interval for movie output before tmov(1) (fs)
 tmov(1)  = 1000.0        Time at which to change output interval (fs)
 dtmov(2) = 2000.0        Time interval for movie output before tmov(1) (fs)
 tmov(2)  = 5000.0        Time at which to change output interval (fs)
 dtmov(3) = 5000.0        Time interval for movie output for rest of run (fs)
ndefmovie= 200           Number of steps btwn writing to defects.out 
nrestart = 1000

nintanal = 1000          Number of steps between interstitial analysis
nliqanal = 100           Nsteps between liquid analysis; works in parallell !

Ekdef    = 0.22          Ekin threshold for labeling an atom defect/liquid 


Recoil calculation definitions
------------------------------

irec     = -2             Index of recoiling atom (-1 closest, -2 create)
recatype = 0              Recoil atom type
xrec     = 1.0          Desired initial position
yrec     = 0.7    
zrec     = -24.0

recen    = 200.0       Initial recoil energy in eV
rectheta = 8.0           Initial recoil direction in degrees
recphi   = 17.0          Initial recoil direction in degrees

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek>10

Lattice structure for latflag=5
--------------------------------

# This one here is for 111 diamond
# if a0 is the plain diamond lattice constant, 111 diamond has
# the unit cell a=a0/sqrt(2), b=a0*sqrt(3/2), c=a0*sqrt(3)
# If a0=5.43095 a=3.84026157  b=6.65152816    c=9.4066813

nbasis   = 12

offset(1)= 0.25
offset(2)= 0.0833333
offset(3)= 0.0833333

# sublattice a - 111 FCC

lx(1,1)  = 0.0  
lx(1,2)  = 0.0  
lx(1,3)  = 0.0  

lx(2,1)  = 0.5 
lx(2,2)  = 0.5 
lx(2,3)  = 0.0 

lx(3,1)  = 0.0 
lx(3,2)  = 0.3333333 
lx(3,3)  = 0.3333333 

lx(4,1)  = 0.5 
lx(4,2)  = 0.8333333 
lx(4,3)  = 0.3333333 

lx(5,1)  = 0.5 
lx(5,2)  = 0.1666667 
lx(5,3)  = 0.6666667 

lx(6,1)  = 0.0 
lx(6,2)  = 0.6666667 
lx(6,3)  = 0.6666667 

# sublattice b - 111 FCC + c/4

lx(7,1)  = 0.0  
lx(7,2)  = 0.0  
lx(7,3)  = 0.25 

lx(8,1)  = 0.5 
lx(8,2)  = 0.5 
lx(8,3)  = 0.25

lx(9,1)  = 0.0 
lx(9,2)  = 0.3333333 
lx(9,3)  = 0.5833333 

lx(10,1) = 0.5 
lx(10,2) = 0.8333333 
lx(10,3) = 0.5833333 

lx(11,1) = 0.5 
lx(11,2) = 0.1666667 
lx(11,3) = 0.9166667 

lx(12,1) = 0.0 
lx(12,2) = 0.6666667 
lx(12,3) = 0.9166667 


