Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 10
tmax     = 5000.0        Maximum time in fs 
restartt = 0.0           Restart time in fs for latflag=3-4
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 128932        Seed for random number generator

Atom type-dependent parameters. 
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.03          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 3             Number of atom types
mass(0)  = 83.80         Atom mass in u
name(0)  = Kr            Name of atom type
mass(1)  = 55.84704      Atom mass in u
name(1)  = Fe            Name of atom type
mass(2)  = 51.996104     Atom mass in u
name(2)  = Cr            Name of atom type
substrate= FeCr         Substrate name, for use in elstop file name

iac(0,0) = -1            Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = 2               3 create EAM cross potential
iac(0,2) = 2               3 create EAM cross potential
iac(1,1) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
iac(1,2) = 6            
iac(2,2) = 1            
potmode  = 2             0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW
eamnbands= 2

reppotcut= 10.0          Reppotcut for SW and Tersoff: use 0 for no reppot

Simulation cell
---------------

latflag  = 5            Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = 6750         Number of atoms in simulation
box(1)   = 42.8295 28.553         Box size in the X-dir (Angstroms)
box(2)   = 42.8295 28.553        Box size in the Y-dir (Angstroms)
box(3)   = 42.8295 28.553        Box size in the Z-dir (Angstroms)
ncell(1) = 15 10            Number of unit cells along X-direction
ncell(2) = 15 10            Number of unit cells along Y-direction
ncell(3) = 15 10            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 7             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 3000.0        Initial T for mtemp=6
timeini  = 1000.0        Time at temp0 before quench
ntimeini = 99991000      Time steps at temp0 before quench
initemp  = 0.0        Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 0.0        Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 100.0         Berendsen temp. control tau (fs), if 0 not used
trate    = 5.0           Quench rate for mtemp=6/8 (K/fs)

tdebye   = 0.0           Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge, 5.1d-4 Co

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 2000.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcmode  = 1             Pressure control mode: 0 none, 1 xyz, 2 isotropic

tscaleth = 3.615         Min. thickness of border region at which T is scaled 
 Emaxbrdr = 10.0          Maximum allowed kinetic energy for border atoms

damp     = 0.00000       Damping factor

ndump    = 10             Print data every ndump steps
nmovie   = 99500         Number of steps btwn writing to md.movie (0=no movie)
moviemode= 1             Movie printing mode: 0 compressed, 1 normal, 2 vels
dtmov(1) = 250.0         Time interval for movie output before tmov(1) (fs)
tmov(1)  = 1000.0        Time at which to change output interval (fs)
dtmov(2) = 1000.0        Time interval for movie output before tmov(1) (fs)
tmov(2)  = 5000.0        Time at which to change output interval (fs)
dtmov(3) = 5000.0        Time interval for movie output for rest of run (fs)
tmov(3)  = 100000.0        Time at which to change output interval (fs)
dtmov(4) = 20000.0        Time interval for movie output for rest of run (fs)
tmov(4)  = 1000000.0        Time at which to change output interval (fs)
dtmov(5) = 200000.0        Time interval for movie output for rest of run (fs)

nliqanal = 100           Nsteps between liquid analysis; works in parallell !
ndefmovie= 200           Number of steps between writing pressures.out
nrestart = 1000          Number of steps between restart output

Ekdef    = 0.234         Ekin threshold for labeling an atom defect/liquid 
                         1.5 kB Tmelt in units of eV


Recoil calculation definitions
------------------------------

irec     = -1            Index of recoiling atom (0 none, -1 closest, -2 create)
recatype = -1            Recoil atom type (if < 0 no change)
xrec     = 0.0            Desired initial position
yrec     = 0.0    
zrec     = 0.0    

sputlim  = 0.7           Sputtering limit for timestep, in internal units.

recen    = 200.0         Initial recoil energy in eV
rectheta = 7.0           Initial recoil direction in degrees
recphi   = 24.0          Initial recoil direction in degrees

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek
                         4 as 3 + sputtered, 5 as 3 - sputtered
elstopmin= 5.0           Lower limit for applying elstop  
essputlim= 0.5           Sputtering limit for elstop, in internal length units.


Lattice structure for latflag=5
--------------------------------

# This one here is for BCC

nbasis   = 2

offset(1)= 0.25
offset(2)= 0.25
offset(3)= 0.25

lx(1,1)  = 0.0  
lx(1,2)  = 0.0  
lx(1,3)  = 0.0  
ltype(1) = 1
pchang(1)= 0.2
changt(1)= 2


lx(2,1)  = 0.5 
lx(2,2)  = 0.5 
lx(2,3)  = 0.5 
ltype(2) = 1
pchang(2)= 0.2
changt(2)= 2

