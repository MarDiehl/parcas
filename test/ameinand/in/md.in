Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 10

endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

Atom type-dependent parameters. 
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.015         Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 3             Number of atom types
mass(0)  = 55.84704       Atom mass in u
name(0)  = Fe            Name of atom type
mass(1)  = 55.84704       Atom mass in u
name(1)  = Fe            Name of atom type
mass(2)  = 51.996104       Atom mass in u
name(2)  = Cr            Name of atom type
substrate= FeCr          Substrate name, for use in elstop file name


iac(0,0) = -1             - automatic symmetry used: iac(1,0) = iac(0,1) 
iac(0,1) = 2             - automatic symmetry used: iac(1,0) = iac(0,1) 
iac(0,2) = 2             - automatic symmetry used: iac(1,0) = iac(0,1) 
 iac(0,3) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
iac(1,1) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
iac(1,2) = 6             - automatic symmetry used: iac(1,0) = iac(0,1) 
 iac(1,3) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
iac(2,2) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
 iac(2,3) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
 iac(3,3) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 

potmode  = 2            0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW
eamnbands= 2


reppotcut= 10.0          Reppotcut for SW and Tersoff: use 0 for no reppot

Simulation cell
---------------

mdlatxyz = 1
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2


ncell(1) = 21 42             Number of unit cells along X-direction
ncell(2) = 21 42             Number of unit cells along Y-direction
ncell(3) = 21 42             Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 0.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

 pscale   = 1

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge, 5.1d-4 Co


Emaxbrdr = 10.0          Maximum allowed kinetic energy for border atoms

damp     = 0.00000       Damping factor

ndump    = 10            Print data every ndump steps
nmovie   = 99500         Number of steps btwn writing to md.movie (0=no movie)

dtmov(1) = 5000.0        Time interval for movie output before tmov(1) (fs)
tmov(1)  = 5000.0        Time at which to change output interval (fs)
dtmov(2) = 5000.0        Time interval for movie output for rest of run (fs)
tmov(2)  = 10000.0        Time interval for movie output for rest of run (fs)
dtmov(3) = 10000.0        Time interval for movie output for rest of run (fs)


dslice(1)= -1.0          Slice selection: -1 all x, >= 0: ECMx +- dslice(1)
dslice(2)= -1.0          Slice selection: -1 all y, >= 0: ECMy +- dslice(2)
dslice(3)=  6.0          Slice selection: -1 all z, >= 0: ECMz +- dslice(3)



dtsli(1) =   20.0       Time interval for slice output before tsli(1) (fs)
tsli(1)  =  500.0       Time at which to change output interval (fs)
dtsli(2) =  500.0        Time interval for slice output for rest of run (fs)
tsli(2)  = 1000.0       Time at which to change output interval (fs)
dtsli(3) =  2000.0       Time interval for slice output before tsli(1) (fs)
tsli(3)  = 3000.0       Time at which to change output interval (fs)
dtsli(4) =  5000.0       Time interval for slice output for rest of run (fs)

 ECM(1)   = 0.0
 ECM(2)   = 0.0
 ECM(3)   = 0.0
 ECMfix   = 1

nliqanal = 100           Nsteps between liquid analysis; works in parallell !
ndefmovie= 200           Number of steps between writing pressures.out
 nrestart = 100           Number of steps between restart output

Ekdef    = 0.16          Ekin threshold for labeling an atom defect/liquid 
                         1.5 kB Tmelt in units of eV

Recoil calculation definitions
------------------------------

sputlim  = 0.7           Sputtering limit for timestep, in internal units.

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek
                         4 as 3 + sputtered, 5 as 3 - sputtered
  elstopmin= 5.0           Lower limit for applying elstop  
  essputlim= 0.5           Sputtering limit for elstop, in internal length units.


Lattice structure for latflag=5
--------------------------------

# This one here is for 111 FCC
# if a0 is the plain FCC lattice constant, 111 FCC has
# the unit cell a=a0/sqrt(2), b=a0*sqrt(3/2), c=a0*sqrt(3)

nbasis   = 6

offset(1)= 0.25
offset(2)= 0.0833333
offset(3)= 0.1666667

lx(1,1)  = 0.0  
lx(1,2)  = 0.0  
lx(1,3)  = 0.0  
ltype(1) = 1

lx(2,1)  = 0.5 
lx(2,2)  = 0.5 
lx(2,3)  = 0.0 
ltype(2) = 1

lx(3,1)  = 0.0 
lx(3,2)  = 0.3333333 
lx(3,3)  = 0.3333333 
ltype(3) = 1

lx(4,1)  = 0.5 
lx(4,2)  = 0.8333333 
lx(4,3)  = 0.3333333 
ltype(4) = 1

lx(5,1)  = 0.5 
lx(5,2)  = 0.1666667 
lx(5,3)  = 0.6666667 
ltype(5) = 1

lx(6,1)  = 0.0 
lx(6,2)  = 0.6666667 
lx(6,3)  = 0.6666667 
ltype(6) = 1




tmax     = 20000.0         Maximum time in fs
restartt = 0.0           Restart time in fs for latflag=3-4

latflag  = 1      Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas

natoms   = 18522 148176          Number of atoms in simulation
box(1)   = 60.27000  120.54    Box size in the X-dir (Angstroms)
box(2)   = 60.27000  120.54    Box size in the Y-dir (Angstroms)
box(3)   = 60.27000  120.54    Box size in the Z-dir (Angstroms)

tscaleth = 3.0         Min. thickness of border region at which T is scaled


mtemp    = 5             Temp. control (0 none,1=linear,4=set,5/7=borders)
initemp  = 150.0         Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 300.0           Desired temperature (Kelvin)
btctau   = 100.0         Berendsen temp. control tau (fs), if 0 not used
toll     = 0.0           Tolerance for the temperature control

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 100.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcmode  = 0             Pressure control mode: 0 none, 1 xyz, 2 isotropic

irec     = -1            Index of recoiling atom (-1 closest, -2 create)
recatype = 1             Recoil atom type (if < 0 no change)
xrec     = 0.7         Desired initial position
yrec     = 0.0
zrec     = 0.0

recen    = 5000.0          Initial recoil energy in eV
rectheta = 54.329        Initial recoil direction in degrees
recphi   = 75.035          Initial recoil direction in degrees


