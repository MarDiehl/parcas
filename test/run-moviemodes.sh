#!/bin/bash

PATH=$PWD/..:$PATH
start=$PWD

# To add a test, just add it between the parentheses.
# You need to also first produce the "correct" answers.
# The way to produce them is to simply run parcas once on your
# test input (with 4 processes) and remove the file out/time.
# Also remove the final timing line from out/md.out.
moviemodes=( 0 1 2 3 4 5 6 7 8 15 16 17 18 )

num_limit=1e-3 # numeric tolerance

n_failed=0

for mode in "${moviemodes[@]}"
do
    printf 'Moviemode: %3d: ' $mode
    cd "${start}/moviemode-tests"
    m4 -DSTEPS=10 -DMOVIEMODE=$mode in/md.in.in > in/md.in
    mpirun -np 4 parcas >/dev/null
    rm out/time
    sed -i -e '/s\/step\/nat/d' out/md.out

    # use 'numdiff' if avaiable to test differences
    if [[ -z "$(which numdiff 2>/dev/null)" ]]
    then
        echo "WARNING: Please install 'numdiff' (GNUtool)"
        diff_out=$(diff -q out out_${mode}_correct -x .svn | grep -v "Only in out:")
    else
        for f in $(ls out_${mode}_correct/)
        do
            diff_out+=$(numdiff out{,_${mode}_correct}/$f -a ${num_limit} \
                | sed -e '/^$/d' -e '/.*equal$/d')
        done
    fi

    if [[ -z "${diff_out}" ]]
    then
        echo 'Ok'
    else
        echo 'FAIL'
        n_failed=$((n_failed+1))
        diff -q out out_${mode}_correct -x .svn | grep -v "Only in out:"
        for f in $(diff -q out out_${mode}_correct -x .svn | \
            grep -v "Only in out:" | \
            awk '{gsub("out/","",$2); print $2}')
        do
            file -bi "out/${f}"
            file -bi "out_${mode}_correct/${f}"
            echo -n "Compare '$f' in detail [yn]: "
            read yn
            case $yn in
                y) diff -y out{,_${mode}_correct}/"${f}"
                ;;
            esac
            echo -n "Move '$f' in to 'out_${mode}_correct' [yn]: "
            read yn
            case $yn in
                y) cp out{,_${mode}_correct}"/${f}"
                ;;
            esac
        done
    fi

    rm -rf out
done

if [[ $n_failed -gt 0 ]]
then
    echo """
********************************
* a total of $(printf "%3d" $n_failed) tests failed *
*******************************
"""
fi
