Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 10
tmax     = 20000.0       Maximum time in fs
restartt = 0.0           Restart time in fs for latflag=3-4
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

nnodes(1)= 2
nnodes(2)= 2
nnodes(3)= 1

Atom type-dependent parameters. 
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.003          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 3             Number of atom types
mass(0)  = 20.1797
name(0)  = Ne
mass(1)  = 28.0855       Atom mass in u
name(1)  = Si            Name of atom type
mass(2)  = 15.9994       Atom mass in u
name(2)  = O             Name of atom type

substrate= Si            Substrate name, for use in elstop file name

iac(0,0) = 2             Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = 2             - automatic symmetry used: iac(1,0) = iac(0,1)
rcin(0,1)= 5.0           ORIG. 5.0
iac(0,2) = 2
rcin(0,2)= 5.0           ORIG. 5.0
iac(1,1) = 1
iac(1,2) = 1
rcin(1,2)= 5.0           ORIG. 5.0

rcin(2,2)= 5.0           ORIG. 5.0
iac(2,2) = 1

potmode  = 212           0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW

reppotcut= 0.0

Simulation cell
---------------

latflag  = 4             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
mdlatxyz = 1
nprtbl   = 30000        Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = 5371
box(1)   = 49.014450
box(2)   = 48.998177
box(3)   = 49.038456
 ncell(1) = 7             Number of unit cells along X-direction
 ncell(2) = 7             Number of unit cells along Y-direction
 ncell(3) = 7             Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 0            Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 1000          Initial T for mtemp=6
timeini  = 1000.0        Time at temp0 before quench
ntimeini = 999900        Time steps at temp0 before quench
temp     = 300.0         Desired temperature (Kelvin)
toll     = 13.0          Tolerance for the temperature control
btctau   = 10.0          Berendsen temp. control tau (fs), if 0 not used

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 0.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge, 5.1d-4 Co, 2.26d-4 diamond

bpcbeta  = 1.0d-3       Berendsen pressure control beta, 1/B (1/kbar)  ORIG. 1.2d-3 !!
bpctau   = 200.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 10.0           Berendsen pressure control desired P (kbar)
bpcmode  = 0             Pressure control mode: 0 none, 1 xyz, 2 isotropic

damp     = 0.00000       Damping factor

ndump    = 1              Print data every ndump steps
nmovie   = 5           Number of steps btwn writing to md.movie (0=no movie)
moviemode= 2

dslice(1)= -1.0          Slice selection: -1 all x, >= 0: ECMx +- dslice(1)
dslice(2)= 5.00          Slice selection: -1 all y, >= 0: ECMy +- dslice(2)
dslice(3)= -1.0          Slice selection: -1 all z, >= 0: ECMz +- dslice(3)
dtsli(1) = 50.0         Time interval for slice output before tsli(1) (fs)
tsli(1)  = 2000.0       Time at which to change output interval (fs)
dtsli(2) = 500.0        Time interval for slice output before tsli(2) (fs)
tsli(2)  = 10000.0      Time at which to change output interval (fs)
dtsli(3) = 5000.0        Time interval for slice output for rest of run (fs)
ECM(1)   = 1.0
ECM(2)   = 2.0
ECM(3)   = 3.0
ECMfix   = 1

nliqanal = 100           Nsteps between liquid analysis; works in parallell !
ndefmovie= 200           Number of steps between writing pressures.out
nrestart = 1000          Number of steps between restart output

Ekdef    = 0.22          Ekin threshold for labeling an atom defect/liquid 
                         1.5 kB Tmelt in units of eV

Recoil calculation definitions
------------------------------

irec     = -2            Index of recoiling atom (-1 closest, -2 create)
recatype = 0              Recoil atom type (if < 0 no change)
xrec     = 0.0            Desired initial position
yrec     = 0.0    
zrec     = -24.0    

 sputlim  = 0.7           Sputtering limit for timestep, in internal units.

recen    = 1000.0         Initial recoil energy in eV ORIG. 1000.0
rectheta = 7.0           Initial recoil direction in degrees
recphi   = 14.0          Initial recoil direction in degrees

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek
                         4 as 3 + sputtered, 5 as 3 - sputtered
elstopmin= 5.0           Lower limit for applying elstop  
essputlim= 0.5           Sputtering limit for elstop, in internal length units.
