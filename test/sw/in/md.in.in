Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = STEPS
tmax     = 5000.0        Maximum time in fs 
restartt = 0.0           Restart time in fs for latflag=3-4
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

nnodes(1)= 2
nnodes(2)= 2
nnodes(3)= 1

Atom type-dependent parameters. 
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 2             Number of atom types
mass(0)  = 83.80         Atom mass in u
name(0)  = Kr            Name of atom type
mass(1)  = 28.086        Atom mass in u
name(1)  = Si            Name of atom type
substrate= Si            Substrate name, for use in elstop file name

iac(0,0) = -1            Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = 2               3 create EAM cross potential
iac(1,1) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
potmode  = 3             0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW

reppotcut= 10.0          Reppotcut for SW and Tersoff: use 0 for no reppot

Simulation cell
---------------

latflag  = 2             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = -1            Number of atoms in simulation (-1 = auto)
box(1)   = 54.3095       Box size in the X-dir (Angstroms)
box(2)   = 54.3095       Box size in the Y-dir (Angstroms)
box(3)   = 54.3095       Box size in the Z-dir (Angstroms)
ncell(1) = 10            Number of unit cells along X-direction
ncell(2) = 10            Number of unit cells along Y-direction
ncell(3) = 10            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 0.0           Periodicity in Z-direction (1=per, 0=non)

 fixzmin  = -20.4535      Atom fixing at bottom of cell. For EAM pots. need to
 fixzmax  = -14.31745     fiox at least three layers

Simulation
----------

mtemp    = 7             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 3000.0        Initial T for mtemp=6
timeini  = 1000.0        Time at temp0 before quench
ntimeini = 99991000      Time steps at temp0 before quench
initemp  = 50.0          Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 77.0          Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 70.0          Berendsen temp. control tau (fs), if 0 not used
trate    = 5.0           Quench rate for mtemp=6/8 (K/fs)

  taddvel  = 2000.0        Time at which to add atom energies, in fs
  zaddvel  = 0.0           Add energy for atoms above this in z in A
  eaddvel  = 0.005         Energy per atom to add, in eV.

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 315.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge, 5.1d-4 Co

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 0.0           Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcmode  = 1             Pressure control mode: 0 none, 1 xyz, 2 isotropic

  tscalzmin= -14.31745     Bottom T scaling region for mtemp=7/8
  tscalzmax= -10.22675     
tscaleth = 2.82          Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor

ndump    = 1             Print data every ndump steps
nmovie   = 99500         Number of steps btwn writing to md.movie (0=no movie)
dtmov(1) = 250.0         Time interval for movie output before tmov(1) (fs)
tmov(1)  = 1000.0        Time at which to change output interval (fs)
dtmov(2) = 1000.0        Time interval for movie output before tmov(1) (fs)
tmov(2)  = 5000.0        Time at which to change output interval (fs)
dtmov(3) = 5000.0        Time interval for movie output for rest of run (fs)

  dslice(1)= -1.0          Slice selection: -1 all x, >= 0: ECMx +- dslice(1)
  dslice(2)= -1.0          Slice selection: -1 all y, >= 0: ECMy +- dslice(2)
  dslice(3)= 4.0907        Slice selection: -1 all z, >= 0: ECMz +- dslice(3)
  dtsli(1) = 1000.0        Time interval for slice output before tsli(1) (fs)
  tsli(1)  = 20000.0       Time at which to change output interval (fs)
  dtsli(2) = 2500.0        Time interval for slice output before tsli(2) (fs)
  tsli(2)  = 300000.0      Time at which to change output interval (fs)
  dtsli(3) = 5000.0        Time interval for slice output for rest of run (fs)

nliqanal = 100           Nsteps between liquid analysis; works in parallell !
ndefmovie= 200           Number of steps between writing pressures.out
nrestart = 1000          Number of steps between restart output

Ekdef    = 0.22          Ekin threshold for labeling an atom defect/liquid 


Recoil calculation definitions
------------------------------

irec     = -2             Index of recoiling atom (-1 closest, -2 create)
recatype = 0              Recoil atom type (if < 0 no change)
xrec     = -2.0          Desired initial position
yrec     = -2.0    
zrec     = -32.0    

recen    = 200.0         Initial recoil energy in eV
rectheta = 7.0           Initial recoil direction in degrees
recphi   = 24.0          Initial recoil direction in degrees

melstop  = 5             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek
                         4 as 3 + sputtered, 5 as 3 - sputtered
elstopmin= 5.0           Lower limit for applying elstop  
sputlim  = 0.5           Sputtering limit, in internal l units. 0.5=box edge


Lattice structure for latflag=5
--------------------------------

# This one here is for 111 FCC
# if a0 is the plain FCC lattice constant, 111 FCC has
# the unit cell a=a0/sqrt(2), b=a0*sqrt(3/2), c=a0*sqrt(3)

nbasis   = 6

offset(1)= 0.25
offset(2)= 0.0833333
offset(3)= 0.1666667

lx(1,1)  = 0.0  
lx(1,2)  = 0.0  
lx(1,3)  = 0.0  
ltype(1) = 1

lx(2,1)  = 0.5 
lx(2,2)  = 0.5 
lx(2,3)  = 0.0 
ltype(2) = 1

lx(3,1)  = 0.0 
lx(3,2)  = 0.3333333 
lx(3,3)  = 0.3333333 
ltype(3) = 1

lx(4,1)  = 0.5 
lx(4,2)  = 0.8333333 
lx(4,3)  = 0.3333333 
ltype(4) = 1

lx(5,1)  = 0.5 
lx(5,2)  = 0.1666667 
lx(5,3)  = 0.6666667 
ltype(5) = 1

lx(6,1)  = 0.0 
lx(6,2)  = 0.6666667 
lx(6,3)  = 0.6666667 
ltype(6) = 1



