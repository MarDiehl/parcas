#ifndef HELPERS_H
#define HELPERS_H

#include <stdint.h>
#include <stdio.h>
#include "types.h"

void free_variable_header(const struct fixed_header* fhdr,
        struct variable_header* vhdr);
void allocate_variable_header(const struct fixed_header* fhdr,
        struct variable_header* vhdr);

void ** malloc_2d(int rows, int columns, int datasize);
void free_2d(void ** array);

void get_and_dump_description(const struct fixed_header * const fhdr_in,
         FILE * fp_in, FILE * out_fp);

void usage(const char *progname);

void parse_command_line(int argc, char** argv, struct configuration* conf);

void check_offsets(const struct fixed_header* fhdr, FILE* fp, char** argv,
        char* fname);

void* try_malloc(int size, char* progname,
        const char* errmsg);

struct atom * try_malloc_atoms(int count, const struct configuration* conf,
        const struct fixed_header* fhdr);
void free_atoms(struct atom* atoms);

#endif
