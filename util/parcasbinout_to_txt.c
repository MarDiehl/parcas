#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "input.h"
#include "types.h"
#include "helpers.h"
#include "output.h"
#include "filter.h"

/* This is a hack to fix inplace the first line with the
 * number of atoms when writing an xyz-file from filtered
 * input. */
void fix_xyz_first_line(FILE * out_fp, int atoms_written) {
  rewind(out_fp);
  char line[50];
  fgets(line, 50, out_fp);
  int line_length = strlen(line);
  sprintf(line, "%d", atoms_written);
  int i;
  for (i = strlen(line); i <= line_length - 2; i++)
    line[i] = ' ';
  line[line_length - 1] = '\n';
  line[line_length] = '\0';
  rewind(out_fp);
  fwrite(line, line_length, 1, out_fp);
}


/* This program should be fairly easy to parallelize, since now
 * The program fseeks to the correct place when reading the atoms
 * of one processor.
 *
 * Also an option for outputting binary and another for inputting xyz
 * should probably be implemented. Neither of thos should be very hard
 * to do. Most (probably all) of the changes need to be done in other
 * files than this main file. 
 *
 * -- Daniel Landau */
int main(int argc, char **argv){
  struct fixed_header fhdr;
  struct variable_header vhdr;
  struct configuration conf;

  conf.format = FORMAT_ASCII;
  conf.printproc=-1; // which processor info to print, -1 means all
  conf.filter = FILTER_NONE;
  conf.write_to_file = 0;

  parse_command_line(argc, argv, &conf);

  int k;
  for (k = optind; k < argc; k++){ // optind is from getopt, start after options
    conf.fname = argv[k];
    FILE * fp = try_fopen(conf.fname, "rb");
    FILE * out_fp;

    if (!conf.write_to_file)
      out_fp = stdout;
    else {
      out_fp = open_output_file_with_correct_name(&conf);
    }

    /*if xyz format write out the following to stderr*/
    fprintf((conf.format != FORMAT_ASCII) ? stderr : out_fp,
            "=========FILE %s=========\n", conf.fname);

    /* Read in the static part of the header. It is 108 bytes in length. */
    read_fixed_header(fp, &fhdr, conf.progname, conf.fname);

    /* allocate arrays for variable length header and calculated values */
    allocate_variable_header(&fhdr, &vhdr);

    /* Read in the variable length header */
    read_fields_info(&fhdr, &vhdr, fp);
    read_types_info(&fhdr, &vhdr, fp);
    read_procs_info(&fhdr, &vhdr, fp);

    /* Dump the headers. */
    print_headers(&fhdr, &vhdr, &conf, out_fp);

    /*
     * The actual header is now parsed. Check the offsets and continue
     * dumping the description and atom records.
     */
    check_offsets(&fhdr, fp, argv, conf.fname);
    if (! conf.format == FORMAT_XYZ) {
      get_and_dump_description(&fhdr, fp, out_fp); /* This is not supported properly */
    }

    /* Calculate all offsets */
    calculate_offsets(&vhdr, &fhdr);

    int i, start, end;
    start = conf.printproc < 0 ? 0         : conf.printproc;
    end   = conf.printproc < 0 ? fhdr.cpus : conf.printproc + 1;
    uint64_t atoms_written = 0;
    for (i = start; i < end; i++) {
      int file_atomsize = (3 + fhdr.realsize / 4 * (3 + fhdr.n_fields)) * sizeof(uint32_t);
      int count = vhdr.atomsperproc[i];

      uint8_t *buffer = (uint8_t*)try_malloc(count * file_atomsize, conf.progname, "Couldn't allocate buffer for reading one processes atoms.");
      struct atom * atoms = try_malloc_atoms(count, &conf, &fhdr);

      try_fseek(vhdr.file_offsets[i], fp, &conf);
      try_fread(buffer, file_atomsize, count, fp, &conf);

      uint8_t * ptr = buffer;

      int j;
      for (j = 0; j < count; j++) {
        parse_atom(&conf, &fhdr, &atoms[j], &ptr, j);
      }

      int * array_indices_to_keep;
      int max_atoms_to_keep;
      run_filters(atoms, &vhdr, &fhdr, &conf, i,
              &array_indices_to_keep, &max_atoms_to_keep);


      for (j = 0; j < max_atoms_to_keep; j++) {
        if (array_indices_to_keep[j] != -1) {
          atoms_written++;
          print_atom(&conf, &vhdr, &fhdr,
                  &atoms[array_indices_to_keep[j]], out_fp);
        }
      }

      free(array_indices_to_keep);
      free_atoms(atoms);
      free(buffer);
    }

    free_variable_header(&fhdr, &vhdr);
    if (conf.filter != FILTER_NONE && conf.format == FORMAT_XYZ && conf.write_to_file)
      fix_xyz_first_line(out_fp, atoms_written);
    fclose(fp);
    if (conf.write_to_file)
      fclose(out_fp);
  }

  return 0;
}
