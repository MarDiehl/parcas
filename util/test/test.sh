#!/bin/bash

for d in tmp correct_tmp
do
  if [ -d $d ]
  then
    rm -rf $d;
  fi
done

mkdir tmp
../parcasbinout_to_txt slice.0000000 >tmp/slice.out
../parcasbinout_to_txt slice.0000000 -x >tmp/slice.xyz 2>/dev/null
../parcasbinout_to_txt slice.0000000 -p 2 >tmp/slice.out.2
../parcasbinout_to_txt slice.0000000 -p 0 >tmp/slice.out.0
../parcasbinout_to_txt slice.0000000 -x -p 2 >tmp/slice.xyz.2 2>/dev/null
../parcasbinout_to_txt slice.0000000 -x -p 0 >tmp/slice.xyz.0 2>/dev/null
../parcasbinout_to_txt movie.0000000 >tmp/movie.out
../parcasbinout_to_txt movie.0000000 -x >tmp/movie.xyz 2>/dev/null
../parcasbinout_to_txt movie.0000000 -p 2 >tmp/movie.out.2
../parcasbinout_to_txt movie.0000000 -p 0 >tmp/movie.out.0
../parcasbinout_to_txt movie.0000000 -x -p 2 >tmp/movie.xyz.2 2>/dev/null
../parcasbinout_to_txt movie.0000000 -x -p 0 >tmp/movie.xyz.0 2>/dev/null
../parcasbinout_to_txt restart >tmp/restart.out
../parcasbinout_to_txt restart -x >tmp/restart.xyz 2>/dev/null
../parcasbinout_to_txt restart -p 2 >tmp/restart.out.2
../parcasbinout_to_txt restart -p 0 >tmp/restart.out.0
../parcasbinout_to_txt restart -x -p 2 >tmp/restart.xyz.2 2>/dev/null
../parcasbinout_to_txt restart -x -p 0 >tmp/restart.xyz.0 2>/dev/null
../parcasbinout_to_txt movie.0000000 -x -F top -f 2>/dev/null
mv movie.0000000.xyz tmp/

svn export correct correct_tmp &>/dev/null
if diff -q correct_tmp tmp
then
  echo "Tests passed"
  rm -rf tmp correct_tmp
else
  echo "*** Tests FAILED ***"
  echo "Left the directories in place for debugging."
fi

